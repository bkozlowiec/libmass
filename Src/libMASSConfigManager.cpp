//
//  libMASSConfigManager.cpp
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Dmitry Skvortsov on 10/12/2016.
//
//
#include "libMASSConfigManager.h"

#include "spc_implementation.h"

#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>


SPCManager::~SPCManager()
{
}

SPCManager* SPCFactory::createManager(const std::string & iniFilePath, const std::string & analyzerType)
{
    return new LibMASSConfigManager(iniFilePath, analyzerType);
}

// Temporary constructor until proper implemented
LibMASSConfigManager::LibMASSConfigManager(const std::string & iniFilePath, const std::string & analyzerType) :
    mConfDirectory(iniFilePath),
    mSPCInterface(NULL),
    iniVersion_("0.0")
{
	Errors_.clear();
	analyzerType_ = analyzerType;
    
	// ---- Read everything from config file
    // Sets strPeaksINI_, strLinelockINI_, EEPROM_, stack_, flanges_, flangesTP_, listOfGases_
	readCrossStackINI(iniFilePath + "/" + "crossstack.ini");
    
    
	// ---- Read Physical Constants from phys_consts.ini
    // Sets physConstsIni_, the vector<StructIsotopeConsts>.  (Rename!?  It's a vector.)
	readPhysConstsINI(iniFilePath + "/" + "phys_consts.ini");
    
    
	// ---- Get number of peaks
	numPeaks_ = strPeaksINI_.peaks.size();
    
    
	// ---- Create Calibration INI
    // Sets strCalibrationINI_
	createCalibrationINI();
}

LibMASSConfigManager::~LibMASSConfigManager()
{
}


/*
    This is really just horrible design for the config manager to be told the
    very nice stack and flange configuration and then boil out bits of it and
    stuff it into the stupid peaks INI structure for the SPC implementation.
    SPC Impl then does not have a model of what's going on physically in
    the instrument because it doesn't actually have the flanges and the stack
    stored anywhere nice, just kind of hanging around in the peak params.
    
    In fact, ConfigINI has a cavityLength() accessor but there's nothing like
    that for the flanges.  It's physically very incomplete.
    
    Now problems start to show up because if I want to initialize the chamber
    concentrations I need to come up with a patch now that gets in just the
    missing information—the R coefficients...  we try to tuck all this stuff
    away in the PEAK lines in the INI... just gross.
    
    STACK_GAS and FLANGE_GAS are very correct, civilized notions and should
    be encouraged to develop further.
*/


void LibMASSConfigManager::initStack(const StructStackConfig & stackConfig)
{
    clearInterfaceIfNeeded();
    
	// Set initial Concentrations
	std::map<std::string, double> gasConc;
	for (std::size_t iGas = 0; iGas < stackConfig.absorbers.size(); iGas++)
	{
        const StructAbsorber & absorber = stackConfig.absorbers[iGas];
		gasConc[absorber.moleculeName] = absorber.concentration_ppm;
        strConfigINI_.stackGasProperties.initialGasConcentration_ppm[absorber.moleculeName] = absorber.concentration_ppm;
	}
	// Remove all the "FLANGE" peaks if they were previously created
	strPeaksINI_.peaks.resize(numPeaks_);
	for (int ii = 0; ii < numPeaks_; ii++)
	{
		// Find Molecule name by the Isotope name and set concentration
		std::string molName = getMoleculeName(strPeaksINI_.peaks[ii].isotopeName);
		strPeaksINI_.peaks[ii].molName = molName;
		strPeaksINI_.peaks[ii].self_ppm = gasConc[molName];
	}
	// Set Stack Length
	strConfigINI_.cavityLength_m = stackConfig.length_m;
    
    strConfigINI_.stackGasProperties.broadeningCoefficients = makeBroadeningMatrix(stackConfig.absorbers);
//    strConfigINI_.cavityBroadeningCoefficients = makeBroadeningMatrix(stackConfig.absorbers);
}


void LibMASSConfigManager::initFlanges(const std::vector<StructStackConfig> & flanges)
{
    clearInterfaceIfNeeded();

	int flangePeakIndex = numPeaks_; // flanges start after normal peaks
    
    strConfigINI_.flangeLength_m.clear();
    strConfigINI_.flangeGasProperties.clear();
    
	for (std::size_t iFlange = 0; iFlange < flanges.size(); iFlange++)
	{
        const StructStackConfig & flange = flanges.at(iFlange);
        strConfigINI_.flangeLength_m.push_back(flange.length_m);
        StructCavityGasIni flangeGasInfo;
        
		// create gas-concentration map for each flange
		std::map<std::string, double> gasConc;
		for (std::size_t iGas = 0; iGas < flange.absorbers.size(); iGas++)
		{
            const StructAbsorber & absorber = flange.absorbers.at(iGas);
            const std::string & moleculeName = absorber.moleculeName;
			gasConc[moleculeName] = absorber.concentration_ppm;
            flangeGasInfo.initialGasConcentration_ppm[absorber.moleculeName] = absorber.concentration_ppm;
		}
        
		// loop over each gas and copy peak constants to the end of all peaks
		for (std::size_t iGas = 0; iGas < flange.absorbers.size(); iGas++)
		{
			for (int iPeak = 0; iPeak < numPeaks_; iPeak++)
			{
                // Copy whole peak
                StructPeakConstants peak;
                peak = strPeaksINI_.peaks[iPeak];
                
				// Find Molecule name by the Isotope name and set concentration
				std::string molName = getMoleculeName(strPeaksINI_.peaks[iPeak].isotopeName);
				if (molName == flange.absorbers[iGas].moleculeName)
				{
					// Update needed parameters
					peak.key = "FLANGE";
					peak.idx = flangePeakIndex++;
                    peak.flangeIndex = iFlange;
					peak.fitFlagString = "N";
					peak.wantConc = false;
					peak.self_ppm = gasConc[molName];
					// Add to PeaksINI
					strPeaksINI_.peaks.push_back(peak);
					//Update X_SLAVEs
                    StructSlaveParameters x_slave;
					x_slave.key = "X_SLAVE";
					x_slave.master = strPeaksINI_.peaks[iPeak].idx;
					x_slave.slave = peak.idx;
					strPeaksINI_.slaveMasters.push_back(x_slave);
				}
			}
		}
        flangeGasInfo.broadeningCoefficients = makeBroadeningMatrix(flange.absorbers);
        
        strConfigINI_.flangeGasProperties.push_back(flangeGasInfo);
//        std::cerr << strConfigINI_.flangeGasProperties.at(iFlange) << ".\n";
	}
}
//void LibMASSConfigManager::initFlanges(const std::vector<StructStackConfig> & flanges)
//{
//    clearInterfaceIfNeeded();
//
//	int flangePeakIndex = numPeaks_; // flanges start after normal peaks
//    
//    strConfigINI_.flangeLength_m.clear();
//    strConfigINI_.flangeGasProperties.clear();
//    
//	for (std::size_t iFlange = 0; iFlange < flanges.size(); iFlange++)
//	{
//        const StructStackConfig & flange = flanges.at(iFlange);
//        strConfigINI_.flangeLength_m.push_back(flange.length_m);
//        strConfigINI_.flangeGasProperties.push_back(StructCavityGasIni());
//        
//		// create gas-concentration map for each flange
//		std::map<std::string, double> gasConc;
//		for (std::size_t iGas = 0; iGas < flange.absorbers.size(); iGas++)
//		{
//            const StructAbsorber & absorber = flange.absorbers.at(iGas);
//            const std::string & moleculeName = absorber.moleculeName;
//			gasConc[moleculeName] = absorber.concentration_ppm;
//            strConfigINI_.flangeGasProperties.at(iFlange).initialGasConcentration_ppm[absorber.moleculeName] = absorber.concentration_ppm;
//		}
//        
//		// loop over each gas and copy peak constants to the end of all peaks
//		for (std::size_t iGas = 0; iGas < flange.absorbers.size(); iGas++)
//		{
//			for (int iPeak = 0; iPeak < numPeaks_; iPeak++)
//			{
//                // Copy whole peak
//                StructPeakConstants peak;
//                peak = strPeaksINI_.peaks[iPeak];
//                
//				// Find Molecule name by the Isotope name and set concentration
//				std::string molName = getMoleculeName(strPeaksINI_.peaks[iPeak].isotopeName);
//				if (molName == flange.absorbers[iGas].moleculeName)
//				{
//					// Update needed parameters
//					peak.key = "FLANGE";
//					peak.idx = flangePeakIndex++;
//                    peak.flangeIndex = iFlange;
//					peak.fitFlagString = "N";
//					peak.wantConc = false;
//					peak.self_ppm = gasConc[molName];
//					// Add to PeaksINI
//					strPeaksINI_.peaks.push_back(peak);
//					//Update X_SLAVEs
//                    StructSlaveParameters x_slave;
//					x_slave.key = "X_SLAVE";
//					x_slave.master = strPeaksINI_.peaks[iPeak].idx;
//					x_slave.slave = peak.idx;
//					strPeaksINI_.slaveMasters.push_back(x_slave);
//				}
//			}
//		}
//        strConfigINI_.flangeGasProperties.at(iFlange).broadeningCoefficients = makeBroadeningMatrix(flange.absorbers);
//        
//        
//        std::cerr << strConfigINI_.flangeGasProperties.at(iFlange) << ".\n";
//	}
//}

// PCH: peaks INI has etalon coefficients.  Also has shift.  What?
void LibMASSConfigManager::initLaserRampArguments(const StructLaserRampArguments & strEEPROM)
{
    clearInterfaceIfNeeded();
    
	strPeaksINI_.etalonCoeffients.assign(5, 0.0);
	for (int ii = 0; ii < 5; ii++)
	{
		strPeaksINI_.etalonCoeffients[ii] = strEEPROM.etalonCoeffients[ii];
	}

	// Shift
	strPeaksINI_.shift_GHz = strEEPROM.shift_GHz;

	// Baseline Sample
	strPeaksINI_.blSample.assign(4, 0);
	strPeaksINI_.blSample[0] = strEEPROM.baselineSample[0];
	strPeaksINI_.blSample[1] = strEEPROM.baselineSample[1];
	strPeaksINI_.blSample[2] = strEEPROM.baselineSample[2];
	strPeaksINI_.blSample[3] = strEEPROM.baselineSample[3];

	// Fit Window
	strPeaksINI_.fitWindow.assign(2, 0);
    strPeaksINI_.fitWindow[0] = strEEPROM.fitWindow[0];
	strPeaksINI_.fitWindow[1] = strEEPROM.fitWindow[1];

	// spc_config parameters
	strConfigINI_.laserRampStart = strEEPROM.laserRampStart;
	strConfigINI_.laserRampLength = strEEPROM.laserRampLength;
	strConfigINI_.laserOffStart = strEEPROM.laserOffStart;
	strConfigINI_.laserOffLength = strEEPROM.laserOffLength;
}

void LibMASSConfigManager::setMode(int mode)
{
    clearInterfaceIfNeeded();
    
	// 0 - Default Mode
	if (mode == SPCInterface::kDefaultMode)
	{
		return;
	}
	// 1 - Pressure Width Fitting Mode
	else if (mode == SPCInterface::kPressureWidthMode)
	{
		// Add P to Fit string: AX=>AXP
		for (int ii = 0; ii < numPeaks_; ii++)
		{
			std::size_t found = (strPeaksINI_.peaks[ii].fitFlagString).find("A");
			if (found != std::string::npos)
			{
				strPeaksINI_.peaks[ii].fitFlagString += "P";
			}
		}
		// Add Pressure Width Slaving identical to Area slaving
		for (std::size_t i = 0; i < strPeaksINI_.slaveMasters.size(); i++)
		{
			if (strPeaksINI_.slaveMasters[i].key == "AREA_SLAVE")
			{
                StructSlaveParameters strSlaveParams;
				strSlaveParams.key = "PW_SLAVE";
				strSlaveParams.slave = strPeaksINI_.slaveMasters[i].slave;
				strSlaveParams.master = strPeaksINI_.slaveMasters[i].master;
				strPeaksINI_.slaveMasters.push_back(strSlaveParams);
			}
		}
	}
}


SPCInterface* LibMASSConfigManager::getInterface()
{
    if (!hasSPCInterface())
    {
        createInterface();
    }
    return mSPCInterface;
}


bool LibMASSConfigManager::hasSPCInterface() const
{
    return (mSPCInterface != NULL);
}

void LibMASSConfigManager::createInterface()
{
    if (hasSPCInterface())
    {
        throw std::runtime_error("SPCInterface already exists");
    }
    mSPCInterface = new SPCImplementation();
    mSPCInterface->initSPC(getConfigINI(), getPhysConstsINI(), getPeaksINI(), getCalibrationINI(), getLinelockINI());
}

void LibMASSConfigManager::clearInterface()
{
    if (!hasSPCInterface())
    {
        throw std::runtime_error("No SPCInterface exists");
    }
    delete mSPCInterface;
    mSPCInterface = NULL;
}

void LibMASSConfigManager::clearInterfaceIfNeeded()
{
    if (hasSPCInterface())
    {
        delete mSPCInterface;
        mSPCInterface = NULL;
    }
}

const std::vector<StructError> & LibMASSConfigManager::getErrors()
{
	if (hasSPCInterface())
	{
		checkForErrors();
	}
	return Errors_;
}


// Helper Functions
void LibMASSConfigManager::checkForErrors()
{
	Errors_.clear();
	//ToDo: Placeholder for Error Handling
	//Errors_.push_back({ ErrorCodes::badDetectorNoise, false, "badDetectorNoise" });

	// Excessive Chi2
	// badGoodnessOfFit = 0,
	double goodNessOfFit = getInterface()->getGoodnessOfFit(0);
	if (goodNessOfFit > 100.0)
	{
		Errors_.push_back({ ErrorCodes::badGoodnessOfFit, false, "badGoodnessOfFit" });
	}

	// Negative fit pressure width
	// negativePressureWidth = 1,
	// ToDo: Implement negative PW
	bool isPWNegative = getInterface()->getIsPWNegative(0);
	if (isPWNegative == true)
	{
		Errors_.push_back({ ErrorCodes::negativePressureWidth, false, "negativePressureWidth" });
	}

	// Negative fit area
	// negativeArea = 2,
	// ToDo: Implement negative Area
	bool isAreaNegative = getInterface()->getIsAreaNegative(0);
	if (isAreaNegative == true)
	{
		Errors_.push_back({ ErrorCodes::negativeArea, false, "negativeArea" });
	}

	// Excessive etalon stretch
	// badEtalonStretch = 3,
	double etalonStretch = getInterface()->getEtalonStretchFactor(0);
	if (fabs(etalonStretch-1.0) >= 0.1)
	{
		Errors_.push_back({ ErrorCodes::badEtalonStretch, false, "badEtalonStretch" });
	}

	// Peak center OOB
	// linelockCenterOOB = 4,
	bool isLinelcokCenterOBB = getInterface()->getIsLinelockOOB(0);
	if (isLinelcokCenterOBB == true)
	{
		Errors_.push_back({ ErrorCodes::linelockCenterOOB, false, "linelockCenterOOB" });
	}

	// Detector offset StDev too large
	// badDetectorNoise = 5
	double detectorNoise = getInterface()->getDetectorNoise(0);
	// ToDo: Define good metrics
	if (detectorNoise > 1000.0)
	{
		Errors_.push_back({ ErrorCodes::badDetectorNoise, false, "badDetectorNoise" });
	}
}

void LibMASSConfigManager::createCalibrationINI()
{
	strCalibrationINI_.lineStrengthFudge.clear();
	strCalibrationINI_.lineStrengthFudge.assign(numPeaks_, 1.0);
}

void LibMASSConfigManager::readCrossStackINI(const std::string & filepath)
{
	bool foundAnalyzerTypeTag = false;
	std::string typeSTARTToken = analyzerType_ + "_START";
	std::string typeSTOPToken = analyzerType_ + "_STOP";

	std::string lineFromFile, key, token; // A line has a key often followed by some tokens

	strPeaksINI_.peaks.clear();
	strPeaksINI_.slaveMasters.clear();
	strLinelockINI_.linelockPeakIndexes.clear();
	strLinelockINI_.peakAreaThresholds.clear();
	//listOfGases_.clear();


	std::stringstream sstr("");
	std::ifstream crossstackFile;
	crossstackFile.open(filepath.c_str(), std::ios::in);

	while (getline(crossstackFile, lineFromFile))
	{
		if (lineFromFile.length() < 1 || lineFromFile.at(0) == '#')
		{
			// Skip blank lines or comments (lines starting with #)
		}
		else
		{
			std::istringstream ss(lineFromFile);
			// Read a key and convert to uppercase
            key = "";
			ss >> key;
			std::transform(key.begin(), key.end(), key.begin(), ::toupper);

			if (key == typeSTARTToken)
			{
				foundAnalyzerTypeTag = true;
			}
			else if (key == typeSTOPToken)
			{
				return;
			}
			else if (foundAnalyzerTypeTag == true)
			{
				//if (key == "AVAILABLE_GASES")
				//{
				//	ss >> token;
				//	listOfGases_.push_back(token);
				//}
				// Version of the INI
				if (key == "VERSION")
				{
					ss >> iniVersion_;
				}
                
                if (key == "DIRECT_ABSORBER")
                {
                    std::string moleculeName, yesNo;
                    ss >> moleculeName >> yesNo;
                    strConfigINI_.trustFitConcentration[moleculeName] = (yesNo == "Y" || yesNo == "y");
                }
                
				// Etalon Stretch
				if (key == "ETALON_STRETCH_ENABLED")
				{
					ss >> token;
					strPeaksINI_.etalonStretchEnabled = (token == "Y") || (token == "y");
				}
				if (key == "ETALON_STRETCH_PEAK_INDEXES")
				{
					strPeaksINI_.etalonStretchIndexes.assign(2, 0);
					ss >> strPeaksINI_.etalonStretchIndexes[0];
					ss >> strPeaksINI_.etalonStretchIndexes[1];
				}
				// Zero peak
				if (key == "ZERO_PEAK")
				{
					ss >> strPeaksINI_.zeroIdx;
				}

				// BASELINE
				if (key == "BASELINE")
				{
					// Baseline model, an integer
					ss >> strPeaksINI_.blModel_;

					// Baseline parameters, doubles
					strPeaksINI_.baselineParameters.assign(4, 0.0);
					ss >> strPeaksINI_.baselineParameters[0];
					ss >> strPeaksINI_.baselineParameters[1];
					ss >> strPeaksINI_.baselineParameters[2];
					ss >> strPeaksINI_.baselineParameters[3];

					// Baseline fit flags, bools
					strPeaksINI_.baselineFlags.assign(4, false);
					ss >> token;
					strPeaksINI_.baselineFlags[0] = (token == "T") || (token == "t");
					ss >> token;
					strPeaksINI_.baselineFlags[1] = (token == "T") || (token == "t");
					ss >> token;
					strPeaksINI_.baselineFlags[2] = (token == "T") || (token == "t");
					ss >> token;
					strPeaksINI_.baselineFlags[3] = (token == "T") || (token == "t");
				}

				// Standard Parameters
				if (key == "ENABLE_FIT")
				{
					ss >> token;
					strPeaksINI_.enableFit = ((token == "Y") || (token == "y"));
				}
				if (key == "RESET_EVERY_FIT")
				{
					ss >> token;
					strPeaksINI_.resetEveryFit = ((token == "Y") || (token == "y"));
				}

				if (key == "FIT_ALGORITHM")
				{
					ss >> strPeaksINI_.fitAlgorithm;
				}
				// Flange Fit Period
				if (key == "FLANGE_FIT_PERIOD")
				{
					ss >> strPeaksINI_.flangeFitPeriod;
				}
				// default Temperature
				if (key == "CELSIUS")
				{
					//ss >> strPeaksINI_.celsius;
                    ss >> strConfigINI_.celsius;
				}
				// default Pressure
				if (key == "TORR")
				{
					//ss >> strPeaksINI_.torr;
                    ss >> strConfigINI_.torr;
				}
				// Peaks
				if (key == "PEAK")
				{
                    StructPeakConstants strPeakConsts;
					strPeakConsts.key = "PEAK";
					ss >> strPeakConsts.idx;
					ss >> strPeakConsts.isotopeName;
					ss >> strPeakConsts.evName;
					ss >> strPeakConsts.microns;
					ss >> strPeakConsts.shift_GHz;
					ss >> strPeakConsts.lineStrength;
					ss >> strPeakConsts.lineStrengthFudgeFactor;
					ss >> strPeakConsts.gammaAir;
					ss >> strPeakConsts.gammaSelf;
					ss >> strPeakConsts.gammaWater;
					ss >> strPeakConsts.gammaAirAdj;
					ss >> strPeakConsts.gammaSelfAdj;
					ss >> strPeakConsts.gammaH2OAdj;
					ss >> strPeakConsts.tExponent;
					ss >> strPeakConsts.press_shift_cm1;
					ss >> strPeakConsts.water_ppm;
					ss >> strPeakConsts.epp;
					ss >> strPeakConsts.range_GHz;
					ss >> token;
					strPeakConsts.wantConc = ((token == "Y") || (token == "y"));
					ss >> strPeakConsts.fitFlagString;

					strPeaksINI_.peaks.push_back(strPeakConsts);
				}
                
                if (key == "X_SLAVE" || key == "AREA_SLAVE" || key == "DW_SLAVE" || key == "PW_SLAVE")
                {
                    StructSlaveParameters strSlaveParams;
					strSlaveParams.key = key;
					ss >> strSlaveParams.master;
					ss >> strSlaveParams.slave;
					strPeaksINI_.slaveMasters.push_back(strSlaveParams);
                }

				// spc_config parameters
				if (key == "FIT_ITERATIONS")
				{
					ss >> strConfigINI_.fitIterations;
				}
				if (key == "LASER_PATH")
				{
					ss >> strConfigINI_.laserPathAlgo;
				}
				//Linelock
				if (key == "GHZ_SAFETY_THRESHOLD")
				{
					ss >> strLinelockINI_.safetyGHzThreshold;
				}

				if (key == "GHZ_START_THRESHOLD")
				{
					ss >> strLinelockINI_.startGHzThreshold;
				}

				if (key == "GHZ_STOP_THRESHOLD")
				{
					ss >> strLinelockINI_.stopGHzThreshold;
				}

				if (key == "KELVIN_PER_GHZ")
				{
					ss >> strLinelockINI_.kelvinPerGHz;
				}
				if (key == "ITERATIONS2STOP")
				{
					ss >> strLinelockINI_.iterations2StopLineLock;
				}
				if (key == "UPDATE_INTERVAL_SEC")
				{
					ss >> strLinelockINI_.updateIntervalSec;
				}
				if (key == "TEC_SCALE_ADJUSTMENT")
				{
					ss >> strLinelockINI_.scaleAdjustmentFactorTEC;
				}
				if (key == "DELTA_TEC_MAX")
				{
					ss >> strLinelockINI_.deltaTECmax;
				}
				if (key == "LINELOCK_PEAK")
				{
					int indx;
					double min_area;
					ss >> indx >> min_area;
					strLinelockINI_.linelockPeakIndexes.push_back(indx);
					strLinelockINI_.peakAreaThresholds.push_back(min_area);
				}
				if (key == "GOODNESS_OF_FIT_MAX")
				{
					ss >> strLinelockINI_.goodnessOfFitMax;
				}
			}
		}
	}
	crossstackFile.close();
}

void LibMASSConfigManager::readPhysConstsINI(const std::string & filepath)
{
	StructIsotopeConsts isotopeConstants;
	std::string isoName, molName;
	double mass, abundance, qc0, qc1, qc2, qc3, qc4, qc5;
	physConstsINI_.clear();

	//	structIsotopeConsts iso1 = { "O2_16", "O2", 31.98982923914, 0.99757,
	//		4.6732, 0.70785, -5.4111e-005, 2.0673e-007, -5.6077e-011, 7.027e-015 };

	//	allIsotopes.push_back(iso1);

	std::string str;
	std::stringstream sstr("");
	std::ifstream phyConstsINIfile;
	phyConstsINIfile.open(filepath.c_str(), std::ios::in);

	while (getline(phyConstsINIfile, str)) {

		if (str.length() < 1 || str.at(0) == '#') {

			// Skip blank lines or comments (lines starting with #)
		}
		else {
			std::istringstream ss(str);
			// Read a key and convert to uppercase
			ss >> str;
			std::transform(str.begin(), str.end(), str.begin(), ::toupper);
			if (str == "ISOTOPE")
			{
				ss >> isoName >> molName >> mass >> abundance
					>> qc0 >> qc1 >> qc2 >> qc3 >> qc4 >> qc5;
				isotopeConstants.isotopeName = isoName;
				isotopeConstants.moleculeName = molName;
				isotopeConstants.molarMass_g = mass;
				isotopeConstants.abundance = abundance;
				isotopeConstants.partitionConst0 = qc0;
				isotopeConstants.partitionConst1 = qc1;
				isotopeConstants.partitionConst2 = qc2;
				isotopeConstants.partitionConst3 = qc3;
				isotopeConstants.partitionConst4 = qc4;
				isotopeConstants.partitionConst5 = qc5;
				physConstsINI_.push_back(isotopeConstants);
			}
		}
	}
	phyConstsINIfile.close();
}

const std::string & LibMASSConfigManager::getMoleculeName(const std::string &IsoName) const
{
	for (std::size_t i = 0; i < physConstsINI_.size(); i++)
	{
		if (physConstsINI_[i].isotopeName == IsoName)
		{
			return physConstsINI_[i].moleculeName;
		}
	}
    throw std::runtime_error("Isotope molecule name is unknown");
}
