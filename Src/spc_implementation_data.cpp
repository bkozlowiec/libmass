//
//  spc_implementation_data.cpp
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 3/23/17.
//
//

#include "spc_implementation_data.h"



LabeledMatrix makeBroadeningMatrix(const std::vector<StructAbsorber> & absorbers)
{
    LabeledMatrixMaker maker;
    
    for (std::size_t col = 0; col < absorbers.size(); col++)
    {
        const std::string & colName = absorbers[col].moleculeName;
        
        for (std::size_t row = 0; row < absorbers[col].broadeningCoefficents.size(); row++)
        {
            const std::string & rowName = absorbers[col].broadeningCoefficents[row].moleculeName;
            double coeff = absorbers[col].broadeningCoefficents[row].coefficient;
            
            maker.set(rowName, colName, coeff);
        }
    }
    
    return maker.make();
}
