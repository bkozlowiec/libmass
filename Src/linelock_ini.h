//
//  linelock_ini.h
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Dmitry Skvortsov on 5/18/16.
//
//

#ifndef linelock_ini_h
#define linelock_ini_h

#include <vector>
#include <string>
#include "spc_implementation_data.h"

class LinelockINI
{
public:
	LinelockINI();
	LinelockINI(const std::string & fileName);
	LinelockINI(const StructLinelockIni & structLinelockIni);
	~LinelockINI();

	void updateLinelockIni(const StructLinelockIni & structLinelockIni);
	void updateLinelockIni();
	void clear();

	int numPeaks() const
	{
		return linelockPeakIndexes_.size();
	}

	double updateIntervalSec() const
	{
		return updateIntervalSec_;
	}

	double safetyGHzThreshold() const
	{
		return safetyGHzThreshold_;
	}

	double startGHzThreshold() const
	{
		return startGHzThreshold_;
	}

	double stopGHzThreshold() const
	{
		return stopGHzThreshold_;
	}

	int iterations2StopLineLock() const
	{
		return iterations2StopLineLock_;
	}

	double kelvinPerGHz() const
	{
		return kelvinPerGHz_;
	}

	double scaleAdjustmentFactorTEC() const
	{
		return scaleAdjustmentFactorTEC_;
	}

	double deltaTECmax() const
	{
		return deltaTECmax_;
	}

	std::vector<int> linelockPeakIndexes() const
	{
		return linelockPeakIndexes_;
	}

	std::vector<double> peakAreaThresholds() const
	{
		return peakAreaThresholds_;
	}

	double goodnessOfFitMax() const
	{
		return goodnessOfFitMax_;
	}
protected:
	std::string fileName_;
	/**
	* Variable to hold the number of seconds before next update. It is useful to limit TEC update rate to slower than > 1 Hz
	*/
	double updateIntervalSec_;

	/**
	* Maximum deviation of line postion [GHz] where the Linelock will start.
	* If deviation is larger than this Linelock will not start
	*/
	double safetyGHzThreshold_;

	/**
	* Maximum deviation of line postion [GHz] before Linelock will start
	*/
	double startGHzThreshold_;

	/**
	* Maximum deviation of line positions [GHz] before Linelock will stop
	*/
	double stopGHzThreshold_;

	/**
	* Number of succesful (|deltaGHz| < stopGHzThreshold) iterations before stopping linelock
	*/
	int iterations2StopLineLock_;

	/**
	* HW related coefficent. Depends on the laser (can be positive or negative)
	*/
	double kelvinPerGHz_;

	/**
	*  Magic number from Roy's code. We just do not want to approach final value too quick
	*/
	double scaleAdjustmentFactorTEC_;

	/**
	*  Maximum change of TEC temperature per iteration [Kelvin]
	*/
	double deltaTECmax_;

	/**
	*  Peak indexes for linelocking
	*/
	std::vector<int> linelockPeakIndexes_;

	/**
	*  Minimal Peak Areas.
	* If peak area is smaller than this value, peak position is ignored
	*/
	std::vector<double> peakAreaThresholds_;

	/**
	*  Maximum goodness of fit (if Current GoF > goodnessOfFitMax_ linelock will NOT perform deltaTEC calculations)
	*/
	double goodnessOfFitMax_;
};

#endif /* linelock_ini_h */
