//---------------------------------------------------------------------------

#ifndef physicalConstsH
#define physicalConstsH
//---------------------------------------------------------------------------
#include <vector>
#include <string>
#include <map>

//---------------------------------------------------------------------------
// Definitions, conversion methods, and utility methods related to physical
// constants.
//
// All physical constants *should* be here to keep the code base tidy and
// more portable.  The goal is also to minimize the use of #defines which
// produces symbols which are not accessible by the Kylix debugger.
//


namespace Constants {
	static const double pi = 3.14159265358979323846;
	static const double ln2 = 0.6931471;
	static const double sqrt_ln2 = 0.8325545;
	static const double sqrt_pi_ln2 = 1.4756645;
	static const double sqrt_ln2_pi = 0.4697186;
	static const double sqrt_2ln2 = 1.1774100;

	// Boltzmann constant in J/K or kg*m^2/s^2
	static const double boltzmann = 1.3806488e-23;

	// Speed of light in m/s
	static const double speedOfLight = 2.99792458e+08;

	// Conversion factor for atomic mass to kg
	static const double amToKg = 1.660642e-27;

	// wavenumber per GHz
	static const double cmGHz = 1e7 / speedOfLight;

	// 296 Kelvin
	static const double Tref = 296.0;

	// hc/k [cm-K]
	static const double hck = 1.4387770;

	// Torr per Pascal. (Pa = J*cm^-3)
	static const double torrToPa = 133.3224;
	static const double torrToJouleWavenumberCubed = 133.3224;

	static const double Avogadro = 6.02214129e23;

	// R = 62.3637 [L*Torr]/[mole*K]
	static const double R = 62.363577;
};

namespace PhysicalConsts
{
	/**
	* Return molecule specific partition function coefficients from HITRAN2004.
	* The partition function is
	*  Q(T) = a + bT + cT^2 + dT^3
	* The valid temperature range is 70K < T < 500K
	*
	* 16FEB2010 Modification:
	* Feng uses an additional term (eT^4) with constants derived from a
	* refit of the raw data.
	*/
	double partitionFunction(const std::vector<double> & coefficients, const double & K);
    
    /**
     * Calculate Z(T)
     * T = temp in Kelvin
     * E = E" in cm-1 from Hitran
     */
    double Z_Et(const double &K, const double &energy);

    /**
     * Convert number density to ppm
     *
     * Multiply number density by kT/p to get mole fraction.
     * Multiply mole fraction by 1e6 to get ppm.
     *
     * R = 62.3637 [L*Torr]/[mole*K]
     * k (boltzmann const) = R/Avogadro's Number
     */
    double numDen2ppm(const double & numDen, const double & pressure, const double & K);
    double ppm2numDen(const double & ppm, const double & pressure, const double & K);
    double micronsToGHz(const double & microns);
    double wavenumberToGHz(const double & wavenumber);
    double dopplerWidthPrefactor(const double & mass, const double & frequency);
    double cmPerAtmToGHzPerTorr(const double & gamma);
    double gHzPerTorrToCmPerAtm(const double & gamma);
    
    /**
     * Predict peak area based on T, P, isotope concentration, time constant and
     * physical parameters.
     */
    double peakArea(const std::vector<double> & Qcoeffs, const double & T_kelvin, const double & P_torr,
        const double & epp, const double & lineStrength, const double & concentration_ppm,
		const double & tau_us);
    
    /**
     * Predict peak doppler width based on T and physical parameters.
     */
	double peakDopplerWidth(const double & mass, const double & centerGHz,
		const double & T_kelvin);
    
    /**
     * Predict peak pressure width based on T, P, molecule concentrations, and
     * physical parameters.
     */
    double peakPressureWidth_new(double selfConc_ppm, double effectiveAirConc_ppm,
        double gammaSelf, double gammaAir,
        double tExp, double T_kelvin, double P_torr);
//    
//    /**
//     * Predict peak pressure width based on T, P, molecule concentrations, and
//     * physical parameters.
//     */
//    double peakPressureWidth(const double & conc_ppm, const double & water_ppm,
//        const bool & isH2Omolecule,
//		const double & gammaAir,
//		const double & gammaSelf,
//		const double & gammaWater,
//		const double & tExp,
//		const double & T_kelvin,
//		const double & P_torr);
    
//    /**
//     * Determine whether a given molecule is water or an isotopologue of water.
//     * This is used to determine which PW temperature coefficient calculation is
//     * most appropriate.
//     */
//	bool isWater(const bool & isH2Omolecule, const double & gammaWater);
    
    /**
     * Get the temperature-dependent pressure-broadening factor.
     * The total pressure broadening is gpt * (temperature dependence).
     * gpt is [Paul doesn't really know what it is]
     */
    //double getPWTemperatureCoefficientconst(const double & T_kelvin, const double & P_torr, const double & tExp);
    
    /**
     * Get the temperature-independent pressure broadening factor for the
     * formulation where all other species concentrations are weighted and
     * combined into effectiveAir_ppm.
     */
    double pressureWidthCoefficient_new(const double & self_ppm, const double & effectiveAir_ppm,
        const double & gammaSelf, const double & gammaAir);
    
//    /**
//     * Get the temperature-independent pressure broadening factor for water and
//     * its isotopologues.
//     */
//    double pressureWidthCoefficient_water(const double & self_ppm, const double & water_ppm,
//		const double & gammaAir, const double & gammaSelf);
//    
//    /**
//     * Get the temperature-independent pressure broadening factor for molecules
//     * other than water and its isotopologues.
//     */
//    double pressureWidthCoefficient_nonWater(const double & self_ppm, const double & water_ppm,
//		const double & gammaAir, const double & gammaSelf, const double & gammaH2O);
    
    // FIXME: UNUSED
    /**
     * Inverse of pressureWidthCoefficient_water
     */
    double gammaAir_water(const double P_torr, const double & T_celsius, const double & pressureWidth,
		const double & tExp, const double & selfConc_ppm, const double & waterConc_ppm,
		const double & gammaSelf);
    
    // FIXME: UNUSED
    /**
     * Inverse of pressureWidthCoefficient_nonWater
     */
    double gammaAir_nonWater(const double & P_torr, const double & T_celsius, const double & pressureWidth,
		const double & tExp, const double & selfConc_ppm, const double & waterConc_ppm,
		const double & gammaSelf, const double & gammaH2O);
};


#endif
