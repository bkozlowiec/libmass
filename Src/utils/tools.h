// This is a collection of utility methods. Don't instantiate this.
// FIXME: shouldn't be a class at all PCH150928
#ifndef TOOLS_H
#define TOOLS_H

#include <sstream>
#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <deque>
#include <map>
#include "../rate/rate.h"
#include "../nr/nrutil.h"


template<class Key, class Value>
std::ostream & operator<<(std::ostream & str, const std::map<Key,Value> & rhs)
{
    typename std::map<Key,Value>::const_iterator itr = rhs.begin();
    
    while (itr != rhs.end())
    {
        str << "[" << itr->first << ":" << itr->second << "]";
        itr++;
        if (itr != rhs.end())
            str << ", ";
    }
    return str;
}

template<class T>
std::ostream & operator<<(std::ostream & str, const NRMat<T> & rhs)
{
    str << "[ ";
    for (int rr = 0; rr < rhs.nrows(); rr++)
    {
        for (int cc = 0; cc < rhs.ncols(); cc++)
        {
            str << rhs[rr][cc] << " ";
        }
        if (rr < rhs.nrows()-1)
        {
            str << "\n  ";
        }
    }
    str << "]";
    return str;
}

namespace Tools
{
    void add(const std::vector<double> & lhs, double rhs,
        std::vector<double> & outSum);
    
    double mean(const std::vector<double> &data);
    double mean(const std::vector<double> &data, int i0, int i1);
    double mean(std::deque <double> que, int numToAvg);
    void standardDeviation(const std::vector<double> &data, double &avg, double &stdDev);

    //Convert rate value read from configuration to Rate data type
    // TODO: Move to rate.h; maybe re-implement with map<> PCH150928
    Rate::RateID str2RateID (std::string str);

    //Convert Rate data type to its equivalent string value for saving to configuration
    // TODO: Move to rate.h; maybe re-implement with map<> PCH150928
    std::string rateID2str (Rate::RateID rate);
    
    
    /**
     * Find the closest pair of indices i0, i1 such that either
     *  array[i0] <= x0,x1 <= array[i1]
     * or
     *  array[i0] >= x0,x1 >= array[i1].
     * x0, x1 may be in any order.
     *
     *
     * @param array:   an array monotonic from startIndex on
     * @param x0:      one endpoint of interval (lower or upper)
     * @param x1:      other endpoint of interval (upper or lower)
     * @param i0:      first index to return
     * @param i1:      second index to return
     */
    void findElementsSpanning(const std::vector<double> & array,
        double x0, double x1,
        int & i0, int & i1,
        int startIndex = 0);
    
    class RunningAverage
    {
    public:
        RunningAverage();
        RunningAverage(int numSamples, double initialValue);
        void push(double val);
        
        /**
         * Return the mean over all stored samples.  If no samples are stored,
         * return the empty value.
         */
        double mean() const;
        
        /**
         * Return the nominal mean value used when there are zero samples.
         */
        double emptyValue() const;
        
        int size() const { return mCurrentSize; }
        int maxSize() const { return mValues.size(); }
        
    private:
        std::vector<double> mValues;
        double mEmptyValue;
        int mCurrentSize;
        int mCurrentIndex;
    };
};

#endif // TOOLS_H
