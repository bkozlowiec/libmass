//---------------------------------------------------------------------------
#include <cmath>
#include <math.h>
#include "physicalConsts.h"


namespace PhysicalConsts
{

using namespace Constants;

//---------------------------------------------------------------------------
// Return molecule specific partition function coefficients from HITRAN2004.
// The partition function is
//  Q(T) = a + bT + cT^2 + dT^3
// The valid temperature range is 70K < T < 500K
//
// 16FEB2010 Modification:
// Feng uses an additional term (eT^4) with constants derived from a
// refit of the raw data.
//
double partitionFunction(const std::vector<double> & ht, const double & K) {
	return ht[0] + ht[1]*K + ht[2] * K*K + ht[3] * K*K*K + ht[4] * K*K*K*K + ht[5] * K*K*K*K*K;
}


//---------------------------------------------------------------------
// Calculate Z(T)
// T = temp in Kelvin
// E = E" in cm-1 from Hitran
//
double Z_Et(const double & temperature, const double & energy) {
    return exp(-hck * energy/temperature);
}

// Convert number density to ppm
//
// Multiply number density by kT/p to get mole fraction.
// Multiply mole fraction by 1e6 to get ppm.
//
// R = 62.3637 [L*Torr]/[mole*K]
// k (boltzmann const) = R/Avogadro's Number
//
double numDen2ppm(const double & numDen, const double & P_torr, const double & T_kelvin) {
    double ccPerLiter = 1e3;
    double moleFractionToPPM = 1e6;
    double k = R*ccPerLiter*moleFractionToPPM/Avogadro;
    double numDen2ppm = 0.0;
    if(P_torr > 1e-10)
        numDen2ppm = k*T_kelvin/P_torr;
    return numDen*numDen2ppm;
}

double ppm2numDen(const double & ppm, const double & pressure, const double & K) {
    double L2cc = 1e3; // litres to cm-3
    double mf2ppm = 1e6;
    double k = R*L2cc*mf2ppm/Avogadro;
    double numDen2ppm = 0.0;
    if(pressure > 1e-10)
        numDen2ppm = k*K/pressure;
    return ppm/numDen2ppm;
}

double micronsToGHz(const double & microns) {
    return (speedOfLight * 1.0e6 / microns) / 1.0e9;
}
double wavenumberToGHz(const double & wavenumber) {
    return micronsToGHz( 1.0e4 / wavenumber );
}

// Doppler width in the voigt function methods is defined as the half-width
// half-max (hwhm) of the Gaussian component of the voigt profile. Physically,
// the hwhm of the observed doppler distribution is
//
//    delta_f = sqrt(2*ln2) * f * ( sqrt( (k*T)/(m*c^2) ) )
//
// where
//    f         Transition frequency
//    delta_f   HWHM of the doppler profile, units are defined by f
//    k         Boltzmann constant
//    T         Temperature
//    m         Mass of the molecule
//    c         Speed of light
//
// This method returns the temperature independent doppler width prefactor
//
//    sqrt(2*ln2) * f * ( sqrt( (k)/(m*c^2) ) )
//
// The fit code uses this factor and multiplies it by the current sqrt(T).
//
double dopplerWidthPrefactor(const double & mass, const double & frequency) {
    return sqrt_2ln2 * frequency * sqrt( boltzmann / (mass*amToKg*speedOfLight*speedOfLight) );
}

double cmPerAtmToGHzPerTorr(const double & gamma) {
    return gamma/(cmGHz*760.0);
}
double gHzPerTorrToCmPerAtm(const double & gamma) {
    //delH_A1_ = 3; // huh?
    return gamma*cmGHz*760.0;
}


double peakArea(const std::vector<double> & Qcoeffs, const double & T_kelvin, const double & P_torr,
	const double & epp, const double & lineStrength, const double & concentration_ppm,
	const double & tau_us)
{
    const double c_cm_us = Constants::speedOfLight/1e4;
    double QT = PhysicalConsts::partitionFunction(Qcoeffs, T_kelvin);
    double QTo = PhysicalConsts::partitionFunction(Qcoeffs, Constants::Tref);
    double ZT = PhysicalConsts::Z_Et(T_kelvin, epp);
    double ZTo = PhysicalConsts::Z_Et(Constants::Tref, epp);
    double lsT = (epp > 0)? (lineStrength*(QTo/QT)*(ZT/ZTo)) : lineStrength;
    double numDen = PhysicalConsts::ppm2numDen(concentration_ppm,P_torr,T_kelvin);
    double area = (numDen * c_cm_us * tau_us * lsT)/Constants::cmGHz;

    return area;
}


double peakDopplerWidth(const double & mass, const double & centerGHz,
    const double & T_kelvin)
{
    double dwCoefficient = PhysicalConsts::dopplerWidthPrefactor(mass, centerGHz);
    double dopplerWidth = dwCoefficient * sqrt(T_kelvin);
    
    return dopplerWidth;
}

double getPWTemperatureCoefficient(const double & T_kelvin,
	const double & P_torr, const double & tExp)
{
	double coeff = pow(296.0 / T_kelvin, tExp)*P_torr;
	return coeff;
}

double peakPressureWidth_new(double selfConc_ppm, double effectiveAirConc_ppm,
    double gammaSelf, double gammaAir,
    double tExp, double T_kelvin, double P_torr)
{
    double pressureWidthCoefficient = PhysicalConsts::pressureWidthCoefficient_new(
        selfConc_ppm, effectiveAirConc_ppm, gammaSelf, gammaAir);
    
    double pressureWidth = pressureWidthCoefficient *
        PhysicalConsts::getPWTemperatureCoefficient(T_kelvin, P_torr, tExp);
    
    return pressureWidth;
}

//double peakPressureWidth(const double & conc_ppm, const double & water_ppm,
//	const bool & isH2Omolecule,
//	const double & gammaAir,
//	const double & gammaSelf,
//	const double & gammaWater,
//	const double & tExp,
//	const double & T_kelvin,
//	const double & P_torr)
//{
//    // TODO: send in the "effective air concentration" to multiply gamma_air by.
//    double pressureWidthCoefficient;
//    if (PhysicalConsts::isWater(isH2Omolecule, gammaWater))
//    {
//        // The reason for checking if it's water is that we don't trust that
//        // the peak fitter is providing an accurate self_ppm to handle the
//        // self broadening.  Water is weird that way.  Instead we take the
//        // water_ppm value, which comes from INI/outside and not from the
//        // fitter, and use that for self broadening.
//        //
//        // Water in turn does not have a water-broadening contribution to the
//        // pressure broadening.
//        pressureWidthCoefficient = PhysicalConsts::pressureWidthCoefficient_water(
//            conc_ppm, water_ppm, gammaAir, gammaSelf);
//    }
//    else
//    {
//        // This is the usual case since most peaks are not water peaks!
//        // There is self broadening, water broadening and air broadening at
//        // present.  However we want to stop the special treatment of water
//        // and move towards using the wantConc flag ("Y" in the INI) to indicate
//        // which molecules we want to calculate concentrations for, and whenever
//        // we calculate a concentration we will use that.  Water in the future
//        // will just not have a Y.
//        //
//        // For each pressure-broadened peak (each call here), the other species'
//        // concentrations will be roled in with their broadening coefficients
//        // to create an effective air concentration.  We can multiply that with
//        // gammaAir
//        pressureWidthCoefficient = PhysicalConsts::pressureWidthCoefficient_nonWater(
//            conc_ppm, water_ppm, gammaAir, gammaSelf, gammaWater);
//    }
//    
//    double pressureWidth = pressureWidthCoefficient *
//        PhysicalConsts::getPWTemperatureCoefficient(T_kelvin, P_torr, tExp);
//    
//    return pressureWidth;
//}
//
//
//// The negative gammaH2O_ test prevents the dilution effect when the water broadening
//// coefficient is not desired but [H2O] is measured.
//bool isWater(const bool & isH2Omolecule, const double & gammaWater)
//{
//    return isH2Omolecule || gammaWater < 0.0;
//}

// Pressure width coefficient
// NB: self_ppm + effectiveAir_ppm won't generally sum to 1e6 because we've
// rolled all broadening due to other species into the air broadening here.
double pressureWidthCoefficient_new(const double & self_ppm, const double & effectiveAir_ppm,
	const double & gammaSelf, const double & gammaAir)
{
    double ret = gammaAir*(effectiveAir_ppm*1.0e-6) + gammaSelf*(self_ppm*1.0e-6);
    return ret;
}

//// Pressure width coefficient for water and its isotopologues
//double pressureWidthCoefficient_water(const double & self_ppm, const double & water_ppm,
//	const double & gammaAir, const double & gammaSelf)
//{
//    double ret = gammaAir*((1.0e6-water_ppm)/1.0e6) + gammaSelf*(water_ppm/1.0e6);
//    return ret;
//}
//
//// TODO: this needs to take a vector of gammas and a vector of
//// concentrations.  I might want to put gamma_self in the vector... ok either
//// way.
//double pressureWidthCoefficient_nonWater(const double & self_ppm, const double & water_ppm,
//	const double & gammaAir, const double & gammaSelf, const double & gammaH2O)
//{
//    double ret = gammaAir*((1.0e6-self_ppm-water_ppm)/1.0e6) +
//        gammaSelf*(self_ppm/1.0e6) +
//        gammaH2O*(water_ppm/1.0e6);
//    return ret;
//}

double gammaAir_water(const double P_torr, const double & T_celsius, const double & pressureWidth,
	const double & tExp, const double & selfConc_ppm, const double & waterConc_ppm,
	const double & gammaSelf)
{
    // Get the part of pressure broadening that is not temperature-dependent
    // WHY is this not using the STORED pressure width coefficient?
    double pressureWidthCoefficient = pressureWidth /
        PhysicalConsts::getPWTemperatureCoefficient(T_celsius+273.15, P_torr, tExp);
    
    return (1.0e6/(1.0e6 - waterConc_ppm)) *
        (pressureWidthCoefficient -
         gammaSelf*(waterConc_ppm/1.0e6));
}

double gammaAir_nonWater(const double & P_torr, const double & T_celsius, const double & pressureWidth,
	const double & tExp, const double & selfConc_ppm, const double & waterConc_ppm,
	const double & gammaSelf, const double & gammaH2O)
{
    // Get the part of pressure broadening that is not temperature-dependent
    // WHY is this not using the STORED pressure width coefficient?
    double pressureWidthCoefficient = pressureWidth /
        PhysicalConsts::getPWTemperatureCoefficient(T_celsius+273.15, P_torr, tExp);
    
    return (1.0e6/(1.0e6 - selfConc_ppm - waterConc_ppm)) *
        (pressureWidthCoefficient -
         gammaSelf*(selfConc_ppm/1.0e6) -
         gammaH2O*(waterConc_ppm/1.0e6));
}

} // namespace PhysicalConsts
