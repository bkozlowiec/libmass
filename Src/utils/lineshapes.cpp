//
//  lineshapes.cpp
//  icosSPClib
//
//  Created by Paul Hansen on 11/13/15.
//
//

#include "lineshapes.h"
#include <cmath>

#define SQR(a)((a)*(a))

namespace Lineshapes
{

//---------------------------------------------------------------------------
//
//      double voigt(double x, double y)
//
//      Voigt approximation from paper by J. Humlicek,
//      Journal of Quantitative Spectroscopy & Radiative Transfer,
//      vol 21 (1979).
//
//      Accurate to six decimal places or better for all values of x,y
//
//      Normalized lineshape defined such that
//    Integrate[Kv(x,y),{x,-Infinity,+Infinity}] = Sqrt[Pi],
//
//      x = dimensionless frequency
//    y = dimensionless pressure broadening
//
//    x = (F-Fo)/DW, where DW = Fo*sqrt(2kT/mc^2) is the Doppler
//              Width at frequency Fo.
//
//void peak::voigt_c_(double x, double y, double &ReK, double &ImK) const
void voigt_c(double x, double y, double &ReK, double &ImK)
{
  double T[6];
  double C[6];
  double S[6];
  double y1,y2,y3;
  //double ReK = 0.0;
  double R,R2,D,D1,D2,D3,D4;
  int j;

  ReK = 0.0;
  ImK = 0.0;

  T[0] = .314240376;
  T[1] = .947788391;
  T[2] = 1.59768264;
  T[3] = 2.27950708;
  T[4] = 3.02063703;
  T[5] = 3.8897249;
  C[0] = 1.01172805;
  C[1] = -0.75197147;
  C[2] = 1.2557727e-2;
  C[3] = 1.00220082e-2;
  C[4] = -2.42068135e-4;
  C[5] = 5.00848061e-7;
  S[0] = 1.393237;
  S[1] = 0.231152406;
  S[2] = -0.155351466;
  S[3] = 6.21836624e-3;
  S[4] = 9.19082986e-5;
  S[5] = -6.27525958e-7;

  y1 = y+1.5;
  y2 = y1*y1;

  if((y > 0.85) || (fabs(x) < 18.1*y + 1.65)) {
    for(j=0;j<6;j++) {
      R = x-T[j];
      R2 = R*R;
      D = 1.0/(R2+y2);
      D1 = y1*D;
      D2 = R*D;

      R = x+T[j];
      R2 = R*R;
      D = 1.0/(R2+y2);
      D3 = y1*D;
      D4 = R*D;
      ReK = ReK+C[j]*(D1+D3)-S[j]*(D2-D4);
      ImK = ImK+C[j]*(D2+D4)+S[j]*(D1-D3);
    }
    return;
  }

  if( fabs(x) < 12.0)
    ReK = exp(-1.0*x*x);
  y3 = y+3.0;

  for(j=0;j<6;j++) {
    R = x-T[j];
    R2 = R*R;
    D = 1.0/(R2+y2);
    D1 = y1*D;
    D2 = R*D;
    ReK = ReK + y*(C[j]*(R*D2-1.5*D1)+S[j]*y3*D2)/(R2+2.25);

    R = x+T[j];
    R2 = R*R;
    D = 1.0/(R2+y2);
    D3 = y1*D;
    D4 = R*D;
    ReK = ReK + y*(C[j]*(R*D4-1.5*D3)-S[j]*y3*D4)/(R2+2.25);
    ImK = ImK + C[j]*(D2+D4)+S[j]*(D1-D3);
  }
  return;
}


//   To calculate the Faddeeva function with relative error less than 10^(-4).
//     (from http://www.atm.ox.ac.uk/user/wells/voigt.html)
// arguments:
//  x, y - Faddeeva/Voigt function arguments
// return value -- voigt
//
//double peak::humlik_(const double x, const double y) const
double humlik(const double x, const double y)
{

    static const double c[6] = { 1.0117281,     -0.75197147,      0.012557727,
                                0.010022008,   -2.4206814e-4,    5.0084806e-7 };
    static const double s[6] = { 1.393237,       0.23115241,     -0.15535147,
                                0.0062183662,   9.1908299e-5,   -6.2752596e-7 };
    static const double t[6] = { 0.31424038,     0.94778839,      1.5976826,
                                2.2795071,      3.020637,        3.8897249 };

    //const double rrtpi = 0.56418958; // 1/SQRT(pi)
    static const double rrtpi = 0.5641895835477563; // 1/SQRT(pi)

    static double a0, d0, d2, e0, e2, e4, h0, h2, h4, h6,
                 p0, p2, p4, p6, p8, z0, z2, z4, z6, z8;
    static double mf[6], pf[6], mq[6], pq[6], xm[6], ym[6], xp[6], yp[6];
    static double old_y = -1.;
    static bool rg1, rg2, rg3;
    static double xlim0, xlim1, xlim2, xlim3, xlim4;
    static double yq, yrrtpi;
    if (y != old_y) {
        old_y = y;
        yq = y * y;
        yrrtpi = y * rrtpi;
        rg1 = true, rg2 = true, rg3 = true;
        if (y < 70.55) {
            xlim0 = sqrt(y * (40. - y * 3.6) + 15100.);
            xlim1 = (y >= 8.425 ?  0. : sqrt(164. - y * (y * 1.8 + 4.3)));
            xlim2 = 6.8 - y;
            xlim3 = y * 2.4;
            xlim4 = y * 18.1 + 1.65;
            if (y <= 1e-6)
                xlim2 = xlim1 = xlim0;
        }
    }

    double abx = fabs(x);
    double xq = abx * abx;

    if (abx >= xlim0 || y >= 70.55)         // Region 0 algorithm
        return yrrtpi / (xq + yq);

    else if (abx >= xlim1) {            //  Humlicek W4 Region 1
        if (rg1) {  // First point in Region 1
            rg1 = false;
            a0 = yq + 0.5;  //Region 1 y-dependents
            d0 = a0 * a0;
            d2 = yq + yq - 1.;
        }
        return rrtpi / (d0 + xq * (d2 + xq)) * y * (a0 + xq);
    }

    else if (abx > xlim2) {  // Humlicek W4 Region 2
        if (rg2) {  //First point in Region 2
            rg2 = false;
            // Region 2 y-dependents
            h0 = yq * (yq * (yq * (yq + 6.) + 10.5) + 4.5) + 0.5625;
            h2 = yq * (yq * (yq * 4. + 6.) + 9.) - 4.5;
            h4 = 10.5 - yq * (6. - yq * 6.);
            h6 = yq * 4. - 6.;
            e0 = yq * (yq * (yq + 5.5) + 8.25) + 1.875;
            e2 = yq * (yq * 3. + 1.) + 5.25;
            e4 = h6 * 0.75;
        }
        return rrtpi / (h0 + xq * (h2 + xq * (h4 + xq * (h6 + xq))))
                 * y * (e0 + xq * (e2 + xq * (e4 + xq)));
    }

    else if (abx < xlim3) { // Humlicek W4 Region 3
        if (rg3) {  // First point in Region 3
            rg3 = false;
            //Region 3 y-dependents
            z0 = y * (y * (y * (y * (y * (y * (y * (y * (y * (y
                    + 13.3988) + 88.26741) + 369.1989) + 1074.409)
                    + 2256.981) + 3447.629) + 3764.966) + 2802.87)
                    + 1280.829) + 272.1014;
            z2 = y * (y * (y * (y * (y * (y * (y * (y * 5.  + 53.59518)
                    + 266.2987) + 793.4273) + 1549.675) + 2037.31)
                    + 1758.336) + 902.3066) + 211.678;
            z4 = y * (y * (y * (y * (y * (y * 10. + 80.39278) + 269.2916)
                    + 479.2576) + 497.3014) + 308.1852) + 78.86585;
            z6 = y * (y * (y * (y * 10. + 53.59518) + 92.75679)
                    + 55.02933) + 22.03523;
            z8 = y * (y * 5. + 13.3988) + 1.49646;
            p0 = y * (y * (y * (y * (y * (y * (y * (y * (y * 0.3183291
                    + 4.264678) + 27.93941) + 115.3772) + 328.2151) +
                    662.8097) + 946.897) + 919.4955) + 549.3954)
                    + 153.5168;
            p2 = y * (y * (y * (y * (y * (y * (y * 1.2733163 + 12.79458)
                    + 56.81652) + 139.4665) + 189.773) + 124.5975)
                    - 1.322256) - 34.16955;
            p4 = y * (y * (y * (y * (y * 1.9099744 + 12.79568)
                    + 29.81482) + 24.01655) + 10.46332) + 2.584042;
            p6 = y * (y * (y * 1.273316 + 4.266322) + 0.9377051)
                    - 0.07272979;
            p8 = y * 0.3183291 + 5.480304e-4;
        }
        return 1.7724538 / (z0 + xq * (z2 + xq * (z4 + xq * (z6 +
                xq * (z8 + xq)))))
                  * (p0 + xq * (p2 + xq * (p4 + xq * (p6 + xq * p8))));
    }

    else {  //  Humlicek CPF12 algorithm
        double ypy0 = y + 1.5;
        double ypy0q = ypy0 * ypy0;
        for (int j = 0; j <= 5; ++j) {
            double d = x - t[j];
            mq[j] = d * d;
            mf[j] = 1. / (mq[j] + ypy0q);
            xm[j] = mf[j] * d;
            ym[j] = mf[j] * ypy0;
            d = x + t[j];
            pq[j] = d * d;
            pf[j] = 1. / (pq[j] + ypy0q);
            xp[j] = pf[j] * d;
            yp[j] = pf[j] * ypy0;
        }
        double k = 0.;
        if (abx <= xlim4) // Humlicek CPF12 Region I
            for (int j = 0; j <= 5; ++j)
                k += c[j] * (ym[j]+yp[j]) - s[j] * (xm[j]-xp[j]);
        else {           // Humlicek CPF12 Region II
            double yf = y + 3.;
            for (int j = 0; j <= 5; ++j)
                k += (c[j] * (mq[j] * mf[j] - ym[j] * 1.5)
                         + s[j] * yf * xm[j]) / (mq[j] + 2.25)
                        + (c[j] * (pq[j] * pf[j] - yp[j] * 1.5)
                           - s[j] * yf * xp[j]) / (pq[j] + 2.25);
            k = y * k + exp(-xq);
        }
        return k;
    }
}


//     To calculate the Faddeeva function
//
//     and partial derivatives of the Voigt function for y>=0
//     (from http://www.atm.ox.ac.uk/user/wells/voigt.html)
// arguments:
//  x, y - Faddeeva/Voigt function arguments
//  k - voigt              -- output
//  l - Imaginary part     -- output
//  dkdx - dVoigt/dx       -- output
//  dkdy - dVoigt/dy       -- output
//
//void peak::humdev_(const double x, const double y,
//            double &k, double &l, double &dkdx, double &dkdy) const
void humdev(const double x, const double y,
            double &k, double &l, double &dkdx, double &dkdy)
{
    static const double c[6] = { 1.0117281,     -0.75197147,      0.012557727,
                                0.010022008,   -2.4206814e-4,    5.0084806e-7 };
    static const double s[6] = { 1.393237,       0.23115241,     -0.15535147,
                                0.0062183662,   9.1908299e-5,   -6.2752596e-7 };
    static const double t[6] = { 0.31424038,     0.94778839,      1.5976826,
                                2.2795071,      3.020637,        3.8897249 };

    //static const float rrtpi = 0.56418958; // 1/SQRT(pi)
    static const double rrtpi = 0.5641895835477563; // 1/SQRT(pi)
    static const double drtpi = 0.5641895835477563; // 1/SQRT(pi)

    /*static*/ double a0, b1, c0, c2, d0, d1, d2, e0, e2, e4, f1, f3, f5,
                 g0, g2, g4, g6, h0, h2, h4, h6, p0, p2, p4, p6, p8,
                 q1, q3, q5, q7, r0, r2, w0, w2, w4, z0, z2, z4, z6, z8,
                 mf[6], pf[6], mq[6], mt[6], pq[6], pt[6], xm[6], ym[6],
                 xp[6], yp[6];

    /*static*/ double old_y = -1.0;

    /*static*/ bool rgb, rgc, rgd;
    /*static*/ double yq, xlima, xlimb, xlimc, xlim4;

    if (y != old_y) {
        old_y = y;
        rgb = true, rgc = true, rgd = true;
        yq = y * y;
        xlima = 146.7 - y;
        xlimb = 24. - y;
        xlimc = 7.4 - y;
        xlim4 = y * 18.1 + 1.65;
    }

    double abx = fabs(x);
    double xq = abx * abx;

    if (abx > xlima)   //  Region A
    {
        double d = 1.0 / (xq + yq);
        d1 = d * rrtpi;
        k = d1 * y;
        l = d1 * x;
        d1 *= d;
        dkdx = -d1 * (y + y) * x;
        dkdy = d1 * (xq - yq);
    }
    else if (abx > xlimb) //  Region B
    {
        if (rgb) {
            rgb = false;
            a0 = yq + 0.5;
            b1 = yq - 0.5;
            d0 = a0 * a0;
            d2 = b1 + b1;
            c0 = yq * (1.0 - d2) + 1.5;
            c2 = a0 + a0;
            r0 = yq * (0.25 - yq * (yq + 0.5)) + 0.125;
            r2 = yq * (yq + 5.0) + 0.25;
        }
        double d = 1.0 / (d0 + xq * (d2 + xq));
        d1 = d * rrtpi;
        k = d1 * (a0 + xq) * y;
        l = d1 * (b1 + xq) * x;
        d1 *= d;
        dkdx = d1 * x * y * (c0 - (c2 + xq) * (xq + xq));
        dkdy = d1 * (r0 - xq * (r2 - xq * (b1 + xq)));
    }
    else
    {
        if (abx > xlimc)   // Region C
        {
            if (rgc)
            {
                rgc = false;
                h0 = yq * (yq * (yq * (yq + 6.0) + 10.5)
                        + 4.5) + 0.5625;
                h2 = yq * (yq * (yq * 4.0 + 6.0) + 9.)
                        - 4.5;
                h4 = 10.5 - yq * (6. - yq * 6.0);
                h6 = yq * 4.0 - 6.0;
                w0 = yq * (yq * (yq * 7.0 + 27.5)
                        + 24.25) + 1.875;
                w2 = yq * (yq * 15.0 + 3.0) + 5.25;
                w4 = yq * 9.0 - 4.5;
                f1 = yq * (yq * (yq + 4.5) + 5.25)
                    -  1.875;
                f3 = 8.25 - yq * (1.0 - yq * 3.0);
                f5 = yq * 3.0 - 5.5;
                e0 = y * (yq * (yq * (yq + 5.5) + 8.25)
                        + 1.875);
                e2 = y * (yq * (yq * 3.0 + 1.0) + 5.25);
                e4 = y * 0.75 * h6;
                g0 = y * (yq * (yq * (yq * 8.0 + 36.0)
                            + 42.0) + 9.0);
                g2 = y * (yq * (yq * 24.0 + 24.0) + 18.0);
                g4 = y * (yq * 24.0 - 12.0);
                g6 = y * 8.0;
            }
            double u = e0 + xq * (e2 + xq * (e4 + xq * y));
            double d = 1.0 / (h0 + xq * (h2 + xq * (h4 + xq
                                                    * (h6 + xq))));
            k = d * rrtpi * u;
            l = d * rrtpi * x * (f1 + xq * (f3 + xq * (f5 + xq)));
            double dudy = w0 + xq * (w2 + xq * (w4 + xq));
            double dvdy = g0 + xq * (g2 + xq * (g4 + xq * g6));
            dkdy = d * rrtpi * (dudy - d * u * dvdy);
        }
        else if (abx < 0.85)  // Region D
        {
            if (rgd)
            {
                rgd = false;
                z0 = y * (y * (y * (y * (y * (y * (y * (y * (y *
                        (y + 13.3988) + 88.26741) +
                        369.1989) + 1074.409) + 2256.981) +
                        3447.629) + 3764.966) +
                        2802.87) + 1280.829) + 272.1014;
                z2 = y * (y * (y * (y * (y * (y * (y * (y * 5.0
                        + 53.59518) + 266.2987)
                        + 793.4273) + 1549.675) + 2037.31)
                        + 1758.336) + 902.3066) + 211.678;
                z4 = y * (y * (y * (y * (y * (y * 10.0 + 80.39278)
                        + 269.2916) + 479.2576)
                        + 497.3014) + 308.1852) + 78.86585;
                z6 = y * (y * (y * (y * 10.0 + 53.59518)
                        + 92.75679) + 55.02933) + 22.03523;
                z8 = y * (y * 5.0 + 13.3988) + 1.49646;
                p0 = y * (y * (y * (y * (y * (y * (y * (y * (y *
                        0.3183291 + 4.264678) +
                        27.93941) + 115.3772) + 328.2151) +
                        662.8097) + 946.897) +
                        919.4955) + 549.3954) + 153.5168;
                p2 = y * (y * (y * (y * (y * (y * (y *
                        1.2733163 + 12.79458) + 56.81652) +
                        139.4665) + 189.773) +
                        124.5975) - 1.322256) - 34.16955;
                p4 = y * (y * (y * (y * (y * 1.9099744 + 12.79568) + 29.81482) +
                        24.01655) + 10.46332) + 2.584042;
                p6 = y * (y * (y * 1.273316 + 4.266322)
                        + 0.9377051) - 0.07272979;
                p8 = y * 0.3183291 + 5.480304e-4;
                q1 = y * (y * (y * (y * (y * (y * (y * (y * 0.3183291 + 4.26413) + 27.6294)
                         + 111.0528) + 301.3208) +
                        557.5178) + 685.8378) + 508.2585) +
                        173.2355;
                q3 = y * (y * (y * (y * (y * (y * 1.273316 +
                        12.79239) + 55.8865) +
                        130.8905) + 160.4013) + 100.7375) +
                        18.97431;
                q5 = y * (y * (y * (y * 1.909974 +
                        12.79239) + 28.8848) + 19.83766)
                        + 7.985877;
                q7 = y * (y * 1.273316 + 4.26413)
                        + 0.6276985;
            }
            double u = (p0 + xq * (p2 + xq * (p4 + xq * (p6 + xq * p8))))
                                                        * 1.7724538;
            double d = 1.0 / (z0 + xq * (z2 + xq * (z4 + xq
                                                * (z6 + xq * (z8 + xq)))));
            k = d * u;
            l = d * 1.7724538 * x * (q1 + xq * (q3 +
                    xq * (q5 + xq * (q7 + xq * 0.3183291))));
            dkdy = 2 * (x *  l + y * k - drtpi);
        }
        else     // Use CPF12
        {
            double ypy0 = y + 1.5;
            double ypy0q = ypy0 * ypy0;
            k = 0.0;
            l = 0.0;
            for (int j = 0; j <= 5; ++j) {
                mt[j] = x - t[j];
                mq[j] = mt[j] * mt[j];
                mf[j] = 1.0 / (mq[j] + ypy0q);
                xm[j] = mf[j] * mt[j];
                ym[j] = mf[j] * ypy0;
                pt[j] = x + t[j];
                pq[j] = pt[j] * pt[j];
                pf[j] = 1.0 / (pq[j] + ypy0q);
                xp[j] = pf[j] * pt[j];
                yp[j] = pf[j] * ypy0;
                l += c[j] * (xm[j] + xp[j]) + s[j] * (ym[j] - yp[j]);
            }
            if (abx <= xlim4)  // Humlicek CPF12 Region I
            {
                double yf1 = ypy0 + ypy0;
                double yf2 = ypy0q + ypy0q;
                dkdy = 0.0;
                for (int j = 0; j <= 5; ++j)
                {
                    double mfq = mf[j] * mf[j];
                    double pfq = pf[j] * pf[j];
                    k += c[j] * (ym[j] + yp[j]) - s[j] * (xm[j] - xp[j]);
                    dkdy += c[j] * (mf[j] + pf[j] - yf2 * (mfq + pfq))
                               + s[j] * yf1 * (mt[j] * mfq - pt[j] * pfq);
                }
            }
            else               //  Humlicek CPF12 Region II
            {
                double yp2y0 = y + 3.0;
                for (int j = 0; j <= 5; ++j)
                {
                    k += (c[j] * (mq[j] * mf[j] - ym[j] * 1.5)
                             + s[j] * yp2y0 * xm[j]) / (mq[j] + 2.25)
                          + (c[j] * (pq[j] * pf[j] - yp[j] * 1.5)
                            - s[j] * yp2y0 * xp[j]) / (pq[j] + 2.25);
                }
                k = y * k + exp(-xq);
                dkdy = 2 * (x * l + y * k - drtpi);
            }
        }
        dkdx = 2 * (y * l - x * k);
    }
}


// Voigt from A.B. McLean, C.E.J. Mitchell, and D.M. Swanston in
// "Implementation of an efficient analytical approximation to the Voigt
// function for photoemission lineshape analysis".
//
//double peak::vs_(double X, double Y) const
double vs(double X, double Y)
{

  static double A[4],B[4],C[4],D[4];
  double V=0;

  A[0]=-1.2150; B[0]= 1.2359;
  A[1]=-1.3509; B[1]= 0.3786;
  A[2]=-1.2150; B[2]=-1.2359;
  A[3]=-1.3509; B[3]=-0.3786;
  C[0]=-0.3085; D[0]= 0.0210;
  C[1]= 0.5906; D[1]=-1.1858;
  C[2]=-0.3085; D[2]=-0.0210;
  C[3]= 0.5906; D[3]= 1.1858;

  for(int i=0;i<=3;i++)
    V+=(C[i]*(Y-A[i])+D[i]*(X-B[i]))/(SQR(Y-A[i])+SQR(X-B[i]));
  return V;
}

//double peak::lorentz_(double x, double y) const
double lorentz(double x, double y)
{
  const double epsilon = 0.000001;
  const double sqrt_pi = 1.77245385;
  const double one_over_sqrt_pi = 0.5641895;
  if(y <= epsilon)
  {
    if(fabs(x) <= epsilon) return sqrt_pi;  //SQRT(pi)*DIRAC_DELTA_FUNCTION(x)
    else return 0.0;
  }
  double R = x/y;
  return one_over_sqrt_pi/(y*(1+R*R));
}


}; // namespace Lineshapes
