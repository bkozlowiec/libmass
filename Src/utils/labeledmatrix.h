//
//  labeledmatrix.h
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 3/23/17.
//
//

#ifndef labeledmatrix_h
#define labeledmatrix_h

#include <map>
#include <set>
#include <vector>

#include "../nr/nrutil.h"


/**
 * Container for a matrix with rows and columns accessible by string labels.
 */
class LabeledMatrix
{
public:
    LabeledMatrix();
    LabeledMatrix(int numRows, int numCols);
    
    LabeledMatrix & operator=(const LabeledMatrix & rhs)
    {
        if (this == &rhs)
        {
            return *this;
        }
        
        rowLabels_ = rhs.rowLabels_;
        colLabels_ = rhs.colLabels_;
        rowKey_ = rhs.rowKey_;
        colKey_ = rhs.colKey_;
        matrix_ = rhs.matrix_;
        
        return *this;
    }
    
    const double & operator()(const std::string & row, const std::string & col) const;
    double & operator()(const std::string & row, const std::string & col);
    
    const std::vector<std::string> & rowLabels() const { return rowLabels_; }
    const std::vector<std::string> & columnLabels() const { return colLabels_; }
    
    bool hasRow(const std::string & rowLabel) const;
    bool hasColumn(const std::string & colLabel) const;
    
    unsigned int rowIndex(const std::string & rowLabel) const;
    unsigned int columnIndex(const std::string & colLabel) const;
    
    const NRMat<double> & matrix() const { return matrix_; }
    
private:
    std::vector<std::string> rowLabels_;
    std::vector<std::string> colLabels_;
    std::map<std::string, int> rowKey_;
    std::map<std::string, int> colKey_;
    NRMat<double> matrix_;
};


/**
 * Tool for making a LabeledMatrix if you don't want to count up how many
 * rows and columns there are before inserting the values.
 */
class LabeledMatrixMaker
{
public:
    LabeledMatrixMaker();
    
    void set(const std::string & row, const std::string & col, double val);
    
    LabeledMatrix make() const;
    
private:
    std::set<std::string> rowSet_;
    std::set<std::string> colSet_;
    std::map<std::pair<std::string,std::string>, double> sparseMatrix_;
};

#endif /* labeledmatrix_h */
