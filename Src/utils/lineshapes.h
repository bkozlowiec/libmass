//
//  lineshapes.h
//  icosSPClib
//
//  Created by Paul Hansen on 11/13/15.
//
//

#ifndef lineshapes_h
#define lineshapes_h


namespace Lineshapes
{
/**
 * Evaluates k = Voigt(x,y).
 * Returns real and imaginary parts in ReK and ImK.
 */
void voigt_c(double x, double y, double &ReK, double &ImK); // The C version of voigt()
//double voigt_ic(double x, double y, double &ReK, double &ImK);
double humlik(const double x, const double y);
void humdev(const double x, const double y, double &k, double &l,
            double &dkdx, double &dkdy);
double vs(double normX, double normY);
double lorentz(double x, double y);
};

#endif /* lineshapes_h */
