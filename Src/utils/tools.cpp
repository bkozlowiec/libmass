#include "tools.h"
#include <numeric> // for std::accumulate
#include <vector>
#include <stdexcept>

namespace Tools
{

void add(const std::vector<double> & lhs, double rhs,
    std::vector<double> & outSum)
{
    if (outSum.size() != lhs.size())
        outSum.resize(lhs.size());
    
    for (std::size_t ii = 0; ii < lhs.size(); ii++)
        outSum[ii] = lhs[ii] + rhs;
}

double mean(const std::vector<double> &data)
{
    return mean(data, 0, data.size());
}

double mean(const std::vector<double> &data, int i0, int i1)
{
	double sum = 0;
	int ndata = data.size();
    
	if (ndata <= 0)
		std::cerr << "Empty vector in getAvg()";
    if (i1 > ndata)
        std::cerr << "i1 out of bounds";
    if (i0 < 0)
        std::cerr << "i0 out of bounds";
	for (int ii = i0; ii < i1; ii++)
	{
		sum += data[ii];
	}
	return sum / (i1 - i0);
}

// Average the most recent n data points.
//
double mean(std::deque<double> que, int numToAvg)
{
	double avg = 0.0;
	int nque = que.size();

	// No data yet!
	if (nque != 0)
	{
		if (numToAvg > nque)
		{
			numToAvg = nque;
		}
		for (int i = 0; i < numToAvg; i++)
		{
			avg += que.at (i);
		}
		avg = avg / numToAvg;
	}

	return avg;
}

void standardDeviation(const std::vector<double> &data, double &avg, double &stdDev)
{
	double sum = 0.0;
	avg = mean(data);
	if (data.size () > 2)
	{
		for (std::size_t i = 0; i < data.size (); i++)
		{
			sum += (data[i] - avg) * (data[i] - avg);
		}
		stdDev = sqrt (sum / (data.size () - 1));
	}
	else
	{
		stdDev = 0.0;
	}
}

Rate::RateID str2RateID (std::string str)
{
	Rate::RateID returnVal = Rate::UNDEFINED;
	std::transform (str.begin (), str.end (), str.begin (), ::tolower);

	if (str == "1000s")
	{
		returnVal = Rate::_1000s;
	}
	else if (str == "500s")
	{
		returnVal = Rate::_500s;
	}
	else if (str == "200s")
	{
		returnVal = Rate::_200s;
	}
	else if (str == "100s")
	{
		returnVal = Rate::_100s;
	}
	else if (str == "75s")
	{
		returnVal = Rate::_75s;
	}
	else if (str == "50s")
	{
		returnVal = Rate::_50s;
	}
	else if (str == "25s")
	{
		returnVal = Rate::_25s;
	}
	else if (str == "20s")
	{
		returnVal = Rate::_20s;
	}
	else if (str == "10s")
	{
		returnVal = Rate::_10s;
	}
	else if (str == "5s")
	{
		returnVal = Rate::_5s;
	}
	else if (str == "2s")
	{
		returnVal = Rate::_2s;
	}
	else if (str == "1s")
	{
		returnVal = Rate::_1s;
	}
	else if (str == "0.5s")
	{
		returnVal = Rate::_0_5s;
	}
	else if (str == "0.2s")
	{
		returnVal = Rate::_0_2s;
	}
	else if (str == "0.1s")
	{
		returnVal = Rate::_0_1s;
	}
	else if (str == "0.05s")
	{
		returnVal = Rate::_0_05s;
	}

	if(returnVal == Rate::UNDEFINED)
	{
		std::cerr << "Trying to use an undefined rate ID." << std::endl;
	}

	return returnVal;
}

std::string rateID2str (Rate::RateID rate)
{
	std::string returnVal = "Error in rateID2str";

	if (rate == Rate::_1000s)
	{
		returnVal = "1000s";
	}
	else if (rate == Rate::_500s)
	{
		returnVal = "500s";
	}
	else if (rate == Rate::_200s)
	{
		returnVal = "200s";
	}
	else if (rate == Rate::_100s)
	{
		returnVal = "100s";
	}
	else if (rate == Rate::_75s)
	{
		returnVal = "75s";
	}
	else if (rate == Rate::_50s)
	{
		returnVal = "50s";
	}
	else if (rate == Rate::_25s)
	{
		returnVal = "25s";
	}
	else if (rate == Rate::_20s)
	{
		returnVal = "20s";
	}
	else if (rate == Rate::_10s)
	{
		returnVal = "10s";
	}
	else if (rate == Rate::_5s)
	{
		returnVal = "5s";
	}
	else if (rate == Rate::_2s)
	{
		returnVal = "2s";
	}
	else if (rate == Rate::_1s)
	{
		returnVal = "1s";
	}
	else if (rate == Rate::_0_5s)
	{
		returnVal = "0.5s";
	}
	else if (rate == Rate::_0_2s)
	{
		returnVal = "0.2s";
	}
	else if (rate == Rate::_0_1s)
	{
		returnVal = "0.1s";
	}
	else if (rate == Rate::_0_05s)
	{
		returnVal = "0.05s";
	}

	return returnVal;
}

bool intervalContains_left(double left, double right, double pt)
{
    return (pt == left || (pt-left)*(pt-right) < 0.0);
}

bool intervalContains_right(double left, double right, double pt)
{
    return (pt == right || (pt-left)*(pt-right) < 0.0);
}

/**
 * Return true if left <= pt <= right or left >= pt >= right.
 */
bool intervalContains_closed(double left, double right, double pt)
{
    return (pt-left)*(pt-right) <= 0.0;
}


/*
void findElementsSpanning(const std::vector<double> & array,
    double x0, double x1,
    int & i0, int & i1,
    int startIndex)
{
    if (array.size() < 2)
        throw std::runtime_error("Array must have at least two elements");
    
    i0 = -1;
    i1 = -1;
    
}
*/

/**
 *
 */
void findElementsSpanning(const std::vector<double> & array,
    double x0, double x1,
    int & i0, int & i1,
    int startIndex)
{
    if (array.size() < 2)
        throw std::runtime_error("Array must have at least two elements");
    
    i0 = -1;
    i1 = -1;
    
    // If either x0 or x1 is not found in the range of array elements, return
    // -1 for both indices.  So intervals [x0,x1] partially intersecting
    // [array[first], array[last]] will not actually be analyzed.
    if (false == intervalContains_closed(array[startIndex], array[array.size()-1], x0) ||
        false == intervalContains_closed(array[startIndex], array[array.size()-1], x1))
    {
        return;
    }
    
    for (std::size_t ii = startIndex; ii < array.size()-1; ii++)
    {
        if (i0 == -1)
        {
            if (intervalContains_left(array[ii], array[ii+1], x0) ||
                intervalContains_left(array[ii], array[ii+1], x1))
                i0 = ii;
        }
        
        if (intervalContains_right(array[ii], array[ii+1], x0) ||
            intervalContains_right(array[ii], array[ii+1], x1))
        {
            i1 = ii+1;
        }
    }
}



RunningAverage::
RunningAverage() :
    mValues(1, 0.0),
    mEmptyValue(0.0),
    mCurrentSize(0),
    mCurrentIndex(0)
{
}

RunningAverage::
RunningAverage(int numSamples, double initialValue) :
    mValues(numSamples, 0.0),
    mEmptyValue(initialValue),
    mCurrentSize(0),
    mCurrentIndex(0)
{
}

void RunningAverage::
push(double val)
{
    mValues[mCurrentIndex++] = val;
    int nmValues = mValues.size();
    if (mCurrentIndex >= nmValues)
        mCurrentIndex = 0;
    if (mCurrentSize < nmValues)
        mCurrentSize++;
}

double RunningAverage::
mean() const
{
    if (mCurrentSize == 0)
        return mEmptyValue;
    else
    {
        double total = std::accumulate(mValues.begin(), mValues.end(), 0.0);
        return total / mCurrentSize;
    }
}

double RunningAverage::
emptyValue() const
{
    return mEmptyValue;
}


} // namespace Tools
