//
//  physconst_ini.h
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Dmitry Skvortsov on 01/12/17.
//
//

#ifndef physicalConsts_ini_h
#define physicalConsts_ini_h

#include <vector>
#include <string>
#include "../spc_implementation_data.h"

class PhysConstsINI
{
public:
	PhysConstsINI();
	PhysConstsINI(const std::string &fileName);
	PhysConstsINI(const std::vector<StructIsotopeConsts> &strAllIsotopes);
	~PhysConstsINI();

	double getMass(const std::string &isoName) const;
	std::string getMoleculeName(const std::string &isoName) const;
	std::vector<double> partitionFunctionCoefficents(const std::string &isoName) const;
	bool isWater(const std::string &isoName) const;
	double getAbundance(const std::string &isoName) const;

protected:
	void updatePhysConstsIni();
	void updatePhysConstsIni(const std::vector<StructIsotopeConsts> &strAllIsotopes);
    
	std::string fileName_;
	std::vector<StructIsotopeConsts> allIsotopes;
};

#endif /* physicalConsts_ini_h */
