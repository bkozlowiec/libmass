//
//  physicalConsts_ini.cpp
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Dmitry Skvortsov on 5/18/16.
//
//

#include "physicalConsts_ini.h"

#include <fstream>
#include <sstream>
#include <algorithm>


PhysConstsINI::PhysConstsINI()
{
}

PhysConstsINI::PhysConstsINI(const std::string & fileName)
{
	fileName_ = fileName;
	updatePhysConstsIni();
}

PhysConstsINI::PhysConstsINI(const std::vector<StructIsotopeConsts> &allIsotopes)
{
	updatePhysConstsIni(allIsotopes);
}

PhysConstsINI::~PhysConstsINI()
{
}


void PhysConstsINI::updatePhysConstsIni()
{
	StructIsotopeConsts isotopeConstants;
	std::string isoName(""), molName("");
	double mass=0.0, abundance=0.0, qc0=0.0, qc1=0.0, qc2=0.0, qc3=0.0, qc4=0.0, qc5=0.0;
	allIsotopes.clear();

//	structIsotopeConsts iso1 = { "O2_16", "O2", 31.98982923914, 0.99757,
//		4.6732, 0.70785, -5.4111e-005, 2.0673e-007, -5.6077e-011, 7.027e-015 };

//	allIsotopes.push_back(iso1);

	std::string str("");
	std::stringstream sstr("");
	std::ifstream phyConstsINIfile;
	phyConstsINIfile.open(fileName_.c_str(), std::ios::in);

	while (getline(phyConstsINIfile, str)) {

		if (str.length() < 1 || str.at(0) == '#') {

			// Skip blank lines or comments (lines starting with #)
		}
		else {
			std::string key("");
			std::stringstream ss(str);
			// Read a key and convert to uppercase
			ss >> key;
			std::transform(key.begin(), key.end(), key.begin(), ::toupper);
			if (key == "ISOTOPE")
			{
				ss >> isoName >> molName >> mass >> abundance
				   >> qc0 >> qc1 >> qc2 >> qc3 >> qc4 >> qc5;
				isotopeConstants.isotopeName = isoName;
				isotopeConstants.moleculeName = molName;
				isotopeConstants.molarMass_g = mass;
				isotopeConstants.abundance = abundance;
				isotopeConstants.partitionConst0 = qc0;
				isotopeConstants.partitionConst1 = qc1;
				isotopeConstants.partitionConst2 = qc2;
				isotopeConstants.partitionConst3 = qc3;
				isotopeConstants.partitionConst4 = qc4;
				isotopeConstants.partitionConst5 = qc5;
				allIsotopes.push_back(isotopeConstants);
			}
		}
	}
	phyConstsINIfile.close();
}

void PhysConstsINI::updatePhysConstsIni(const std::vector<StructIsotopeConsts> &strAllIsotopes)
{
	allIsotopes.clear();
	allIsotopes = strAllIsotopes;
}

double PhysConstsINI::getMass(const std::string &isoName) const
{
	for (std::size_t i = 0; i < allIsotopes.size(); i++)
	{
		if (allIsotopes[i].isotopeName == isoName)
		{
			return allIsotopes[i].molarMass_g;
		}
	}
	return -1.0;
}

double PhysConstsINI::getAbundance(const std::string &isoName) const
{
	for (std::size_t i = 0; i < allIsotopes.size(); i++)
	{
		if (allIsotopes[i].isotopeName == isoName)
		{
			return allIsotopes[i].abundance;
		}
	}
	return -1.0;
}

std::string PhysConstsINI::getMoleculeName(const std::string &isoName) const
{
	for (std::size_t i = 0; i < allIsotopes.size(); i++)
	{
		if (allIsotopes[i].isotopeName == isoName)
		{
			return allIsotopes[i].moleculeName;
		}
	}
	return "";
}

std::vector<double> PhysConstsINI::partitionFunctionCoefficents(const std::string &isoName) const
{
	std::vector<double> coeffs(6, 0.0);
	for (std::size_t i = 0; i < allIsotopes.size(); i++)
	{
		if (allIsotopes[i].isotopeName == isoName)
		{
			coeffs[0] = allIsotopes[i].partitionConst0;
			coeffs[1] = allIsotopes[i].partitionConst1;
			coeffs[2] = allIsotopes[i].partitionConst2;
			coeffs[3] = allIsotopes[i].partitionConst3;
			coeffs[4] = allIsotopes[i].partitionConst4;
			coeffs[5] = allIsotopes[i].partitionConst5;
			return coeffs;
		}
	}
	return coeffs;
}


bool PhysConstsINI::isWater(const std::string &isoName) const
{
	for (std::size_t i = 0; i < allIsotopes.size(); i++)
	{
		if (allIsotopes[i].isotopeName == isoName)
		{
			return allIsotopes[i].moleculeName == "H2O";
		}
	}
	return false;
}


