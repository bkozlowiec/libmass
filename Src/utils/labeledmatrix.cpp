//
//  labeledmatrix.cpp
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 3/23/17.
//
//

#include "labeledmatrix.h"

#include <stdexcept>

#include "tools.h"

LabeledMatrix::LabeledMatrix()
{
}

LabeledMatrix::LabeledMatrix(int numRows, int numCols) :
    rowLabels_(numRows),
    colLabels_(numCols),
    matrix_(0.0, numRows, numCols)
{
}


const double & LabeledMatrix::operator()(const std::string & row, const std::string & col) const
{
    std::map<std::string, int>::const_iterator itr;
    
    int rowIdx, colIdx;
    
    itr = rowKey_.find(row);
    if (itr == rowKey_.end())
    {
        throw std::runtime_error("Row key not found in const LabeledMatrix");
    }
    rowIdx = itr->second;
    
    itr = colKey_.find(col);
    if (itr == colKey_.end())
    {
        throw std::runtime_error("Column key not found in const LabeledMatrix");
    }
    colIdx = itr->second;
    
    if (rowIdx < 0 || rowIdx >= matrix_.nrows())
    {
        throw std::runtime_error("Row index out of bounds");
    }
    if (colIdx < 0 || colIdx >= matrix_.ncols())
    {
        throw std::runtime_error("Column index out of bounds");
    }
    
    return matrix_[rowIdx][colIdx];
}

double & LabeledMatrix::operator()(const std::string & rowLabel, const std::string & colLabel)
{
    int numRowLabels = rowKey_.size();
    int numColLabels = colKey_.size();
    
    std::map<std::string, int>::const_iterator itr;
    
    int rowIdx, colIdx;
    
    if (rowKey_.count(rowLabel) == 0)
    {
        // Need to add row index.
        if (numRowLabels >= matrix_.nrows())
        {
            // Oops, we have added enough rows already
            throw std::runtime_error("Cannot add another row label!");
        }
        rowIdx = numRowLabels;
        rowKey_[rowLabel] = rowIdx;
        rowLabels_[rowIdx] = rowLabel;
    }
    else
    {
        rowIdx = rowKey_[rowLabel];
    }
    
    if (colKey_.count(colLabel) == 0)
    {
        // Need to add row index.
        if (numColLabels >= matrix_.ncols())
        {
            // Oops, we have added enough rows already
            throw std::runtime_error("Cannot add another column label!");
        }
        colIdx = numColLabels;
        colKey_[colLabel] = colIdx;
        colLabels_[colIdx] = colLabel;
    }
    else
    {
        colIdx = colKey_[colLabel];
    }
    
    
    if (rowIdx < 0 || rowIdx >= matrix_.nrows())
    {
        throw std::runtime_error("Row index out of bounds");
    }
    if (colIdx < 0 || colIdx >= matrix_.ncols())
    {
        throw std::runtime_error("Column index out of bounds");
    }
    
    return matrix_[rowIdx][colIdx];
}

bool LabeledMatrix::hasRow(const std::string & rowLabel) const
{
    return (rowKey_.count(rowLabel) > 0);
}
bool LabeledMatrix::hasColumn(const std::string & colLabel) const
{
    return (colKey_.count(colLabel) > 0);
}

unsigned int LabeledMatrix::rowIndex(const std::string & rowLabel) const
{
    std::map<std::string, int>::const_iterator itr;
    itr = rowKey_.find(rowLabel);
    if (itr == rowKey_.end())
    {
        throw std::runtime_error("Row label not found");
    }
    return itr->second;
}

unsigned int LabeledMatrix::columnIndex(const std::string & colLabel) const
{
    std::map<std::string, int>::const_iterator itr;
    itr = colKey_.find(colLabel);
    if (itr == colKey_.end())
    {
        throw std::runtime_error("Column label not found");
    }
    return itr->second;
}



LabeledMatrixMaker::LabeledMatrixMaker()
{
}

void LabeledMatrixMaker::set(const std::string & row, const std::string & col, double val)
{
    rowSet_.insert(row);
    colSet_.insert(col);
    sparseMatrix_[std::make_pair(row,col)] = val;
}


/*
template<class T>
std::ostream & operator<<(std::ostream & str, const std::vector<T> & rhs)
{
    str << "[ ";
    for (int ii = 0; ii < rhs.size(); ii++)
    {
        str << rhs[ii] << " ";
    }
    str << "]";
    return str;
}
*/

LabeledMatrix LabeledMatrixMaker::make() const
{
    LabeledMatrix m(rowSet_.size(), colSet_.size());
    
    std::map<std::pair<std::string, std::string>, double>::const_iterator itr;
    for (itr = sparseMatrix_.begin(); itr != sparseMatrix_.end(); itr++)
    {
        const std::string & row = itr->first.first;
        const std::string & col = itr->first.second;
        double val = itr->second;
        
        m(row, col) = val;
    }
    
    /*
    std::cout << "Matrix rows:" << m.rowLabels() << "\n";
    std::cout << "Matrix cols:" << m.columnLabels() << "\n";
    std::cout << "Matrix:\n" << m.matrix() << "\n";
    */
    
    return m;
}




