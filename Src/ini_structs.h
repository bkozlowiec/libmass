//
//  ini_structs.h
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Dmitry Skvortsov on 9/26/16.
//
//
#ifndef ini_structs_h
#define ini_structs_h

#include <vector>
#include <string>


// TODO: Should StructPeaksIni contain a StructLaserRampArguments?
struct StructLaserRampArguments // SPCManager::initLaserRampArguments
{
	// Etalon: Parameters from EEPROM
	std::vector<double> etalonCoeffients;

	// Global shift in GHz
	double shift_GHz;

	// Full Ramp separation in two regions (LaserRamp and LaserOFF)
	int laserRampStart;
	int laserRampLength;
	int laserOffStart;
	int laserOffLength;

	/* Baseline Sample
	 * Typical:
	 * Baseline Sample 1 Start
	 * Baseline Sample 1 Length
	 * Baseline Sample 2 Start
	 * Baseline Sample 2 Length
	*/
	std::vector<int> baselineSample;
    
	// Fit window parameters
	// Typical:
	// Fitting Window Start
	// Fitting Window Length
	std::vector<int> fitWindow;
};

struct StructError // SPCManager::getError
{
	int errorCode;
	bool logToJournal;
	std::string logMessage;
};

struct StructBroadeningCoefficent // StructAbsorber
{
	std::string moleculeName;
	double coefficient;
};

struct StructAbsorber // StructStackConfig
{
	std::string moleculeName;
	double concentration_ppm;
	std::vector<StructBroadeningCoefficent> broadeningCoefficents;
};

struct StructStackConfig // SPCManager::initStack
{
	double length_m;
	std::vector<StructAbsorber> absorbers;
};

#endif /* ini_structs_h */
