//
//  ChamberConcentration.hpp
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 4/18/16.
//
//

#ifndef ChamberConcentration_hpp
#define ChamberConcentration_hpp

#include <map>
#include "utils/physicalConsts.h"
#include "utils/physicalConsts_ini.h"
#include "utils/tools.h"
#include "spectra/peaks_ini.h"
#include "utils/labeledmatrix.h"
#include "spc_implementation_data.h"

class Peak;


// TODO: move up to SPC interface level with the INI classes.
class ChamberGas
{
public:
    enum ChamberGasType
    {
        kIndirectAbsorber,
        kDirectAbsorberTrusted,
        kDirectAbsorberUntrusted
    };
    
    ChamberGas()
    {
    }
    
    ChamberGas(const std::string & name, double ppm, ChamberGasType type) :
        name_(name), ppm_(ppm), type_(type)
    {
    }
    
    const std::string & name() const { return name_; }
    double provisionalConcentration_ppm() const { return ppm_; }
    ChamberGasType type() const { return type_; }
    
    bool isDirect() const { return type_ != kIndirectAbsorber; }
    bool isIndirect() const { return type_ == kIndirectAbsorber; }
    bool isTrusted() const { return type_ == kDirectAbsorberTrusted; }
    bool isBackground() const { return type_ != kDirectAbsorberTrusted; }
    
private:
    std::string name_;
    double ppm_;
    ChamberGasType type_;
};


class ChamberConcentration
{
public:
    ChamberConcentration();
    
//    void clear();
    
    void init(const std::vector<ChamberGas> & gases, const LabeledMatrix & broadeningCoefficients);
    
    /**
     * Set the matrix of broadening coefficients used to obtain the effective
     * air concentration for each species.
     */
//    void setBroadeningCoefficients(const LabeledMatrix & matrix);
    
    //bool doesMeasureIsotope(std::string isotopeCode) const; // non-essential
//    bool doesMeasureMolecule(std::string moleculeCode) const;
    
    /**
     * Calculate the concentration for each isotope using the fitted peaks.
     * Guess the concentrations of indirect absorbers and uncalculated
     * absorbers using their initial proportions to force the total chamber
     * concentration to 1e6 ppm.
     *
     * (There are MANY ways this can be done, but whatever the implementation
     * of choice, please put it in here.)
     *
     * tauCorrected is so-named for ICOS applications when absorption in the
     * cavity should be taken into account.  In Ringdown application, when
     * there's high absorption across the spectral lines of interest, the
     * effective cavity length will seem smaller.  Do the correction before
     * calling this function.
     */
    void updateIsotopeConcentration(const std::vector<Peak*> peaks,
    	const PhysConstsINI & physConstsINI,
        double temperature_C,
        double pressure_torr,
        double tauCorrected);
    
    /**
     *
     */
    void updateEffectiveAirConcentration();
    
    // Weight the concentrations of all other species to treat their pressure
    // broadening effect as if it were due to just air.  "Air."
    double effectiveAirConcentration_ppm(const std::string & molName) const;
    
    double isotopeConcentration_ppm(const std::string & isotopeCode) const;
    double moleculeConcentration_ppm(const std::string & molName) const;
    
    double initialConcentration_ppm(const std::string & molName) const;

private:
    
    // Maps peak.molCode -> calculated concentration.
    // (NOT peak.isoCode)
//    std::map<std::string, Tools::RunningAverage> peak_ppm_;
    std::map<std::string, double> isotope_ppm_; // hash by peak isotope name
    std::map<std::string, double> molecule_ppm_; // hash by peak EV name
    std::map<std::string, double> initial_molecule_ppm_; // hash by peak EV name.  TODO: map<string, ChamberGas>?
    
    std::vector<ChamberGas> gases_;
    //std::vector<double> backgroundGasFraction_;
    
    // Maps peak.molCode -> effective air conc (from other species)
    std::map<std::string, double> effectiveAir_ppm_;
    
    // To initialize the r matrix, we need to get...
    // This is strictly from the STACK_GAS and FLANGE_GAS lines!!
    // This is from vector<StructAbsorber>.
    LabeledMatrix broadeningCoefficients_;
//    NRVec<double> molecule_ppm_; // corresponding to columns of rMatrix_
};



#endif /* ChamberConcentration_hpp */


