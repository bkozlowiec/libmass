//
//  linelock_ini.cpp
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Dmitry Skvortsov on 5/18/16.
//
//

#include "linelock_ini.h"

#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <iterator>
#include <string>


LinelockINI::LinelockINI()
{
}

LinelockINI::LinelockINI(const std::string & fileName) :
	fileName_(fileName)
{
	updateLinelockIni();
}

LinelockINI::LinelockINI(const StructLinelockIni & strLinelockIni)
{
	updateLinelockIni(strLinelockIni);
}

LinelockINI::~LinelockINI()
{
}

void LinelockINI::updateLinelockIni(const StructLinelockIni & strLinelockIni)
{
	clear();
	safetyGHzThreshold_ = strLinelockIni.safetyGHzThreshold;
	startGHzThreshold_ = strLinelockIni.startGHzThreshold;
	stopGHzThreshold_ = strLinelockIni.stopGHzThreshold;
	kelvinPerGHz_ = strLinelockIni.kelvinPerGHz;
	iterations2StopLineLock_ = strLinelockIni.iterations2StopLineLock;
	updateIntervalSec_ = strLinelockIni.updateIntervalSec;
	scaleAdjustmentFactorTEC_ = strLinelockIni.scaleAdjustmentFactorTEC;
	deltaTECmax_ = strLinelockIni.deltaTECmax;
	linelockPeakIndexes_.clear();
	linelockPeakIndexes_.reserve(strLinelockIni.linelockPeakIndexes.size());
	linelockPeakIndexes_ = strLinelockIni.linelockPeakIndexes;
	peakAreaThresholds_.clear();
	peakAreaThresholds_.reserve(strLinelockIni.peakAreaThresholds.size());
	peakAreaThresholds_ = strLinelockIni.peakAreaThresholds;
	goodnessOfFitMax_ = strLinelockIni.goodnessOfFitMax;
}


void LinelockINI::clear()
{
	safetyGHzThreshold_ = 5.0;
	startGHzThreshold_ = 0.25;
	stopGHzThreshold_ = 0.1;
	kelvinPerGHz_ = -0.032;
	iterations2StopLineLock_ = 3;
	updateIntervalSec_ = 1;
	scaleAdjustmentFactorTEC_ = 4;
	deltaTECmax_ = 0.75;
	linelockPeakIndexes_.clear();
	peakAreaThresholds_.clear();
	goodnessOfFitMax_ = 100.0;
}

void LinelockINI::updateLinelockIni()
{

	linelockPeakIndexes_.clear();
	linelockPeakIndexes_.reserve(100);
	peakAreaThresholds_.clear();
	peakAreaThresholds_.reserve(100);

	std::string str;
	std::stringstream sstr("");
	std::ifstream linelockFile;
	linelockFile.open(fileName_.c_str(), std::ios::in);

	while (getline(linelockFile, str)) {

		if (str.length() < 1 || str.at(0) == '#') {

			// Skip blank lines or comments (lines starting with #)
		}
		else {
			std::istringstream ss(str);
			// Read a key and convert to uppercase
			ss >> str;
			std::transform(str.begin(), str.end(), str.begin(), ::toupper);
			/*
			if (str == "LINELOCK_TYPE") {
			ss >> str;
			std::transform(str.begin(), str.end(), str.begin(), ::toupper);
			if (str == "GHZ") {
			shiftMode = GHZ;
			}
			else if (str == "POINTS") {
			shiftMode = POINTS;
			}
			}
			*/
			if (str == "GHZ_SAFETY_THRESHOLD")
			{
				ss >> safetyGHzThreshold_;
			}

			if (str == "GHZ_START_THRESHOLD")
			{
				ss >> startGHzThreshold_;
			}

			if (str == "GHZ_STOP_THRESHOLD")
			{
				ss >> stopGHzThreshold_;
			}

			if (str == "KELVIN_PER_GHZ")
			{
				ss >> kelvinPerGHz_;
			}
			/*
			if (str == "POINTS_START_THRESHOLD") {
			ss >> pointsThreshold_;
			}

			if (str == "POINTS_STOP_THRESHOLD") {
			ss >> pointsThreshold_;
			}

			if (str == "KELVIN_PER_POINT") {
			ss >> kelvinPerPoint;
			}
			*/
			if (str == "ITERATIONS2STOP")
			{
				ss >> iterations2StopLineLock_;
			}
			if (str == "UPDATE_INTERVAL_SEC")
			{
				ss >> updateIntervalSec_;
			}
			if (str == "TEC_SCALE_ADJUSTMENT")
			{
				ss >> scaleAdjustmentFactorTEC_;
			}
			if (str == "DELTA_TEC_MAX")
			{
				ss >> deltaTECmax_;
			}
			if (str == "LINELOCK_PEAK")
			{
				int indx;
				double min_area;
				ss >> indx >> min_area;
				linelockPeakIndexes_.push_back(indx);
				peakAreaThresholds_.push_back(min_area);
			}
			if (str == "GOODNESS_OF_FIT_MAX") {
				ss >> goodnessOfFitMax_;
			}
		}
	}
	linelockFile.close();
}
