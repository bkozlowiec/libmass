//
//  SingleSpectrumAnalyzer.cpp
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 4/6/16.
//
//

#include "singlespectrumanalyzer.h"

#include <stdexcept>
#include <cassert>

#include "ringdown/ringdown.h"
#include "spectra/spectrapeaks.h"
#include "rate/rate.h"
#include "spectra/peak.h"
#include "chamberconcentration.h"
#include "spectra/calibration_ini.h"
#include "spectra/peaks_ini.h"
#include "config_ini.h"
#include "cavityproperties.h"

#include "manylogs.h"
using namespace ManyLogs;

SingleSpectrumAnalyzer::SingleSpectrumAnalyzer(const PeaksINI & peaksINI,
        const CalibrationINI & calibrationINI,
        const ConfigINI & configINI,
		const LinelockINI & linelockINI) :
    spectrumFunction_(NULL),
    mainPeaksFunction_(NULL),
    flangeFunction_(NULL),
    lastFitResults_(), //-1),
    spectrumFitter_(peaksINI),
    previousSpectralShiftGHz_(0.0),
	reduced_chisq_(-999.0),
	standardDeviation_(1.0),
	power_at_end_fit_window(0.0),
	goodnessOfFit_(-1.0),
	etalonStretch_(1.0),
	peakZeroSHiftGHz_(0.0),
	isLinelockOOB_(false),
	isAreaNegative_(false),
	isPWNegative_(false),
	doCalculateInitialY_(false),
	doCalculateEstimatedY_(false),
	doCalculateFitY_(true)
{
    if (configINI.getLaserPathAlgo() == ConfigINI::RINGDOWN_LASER_PATH)
    {
        spectrumFunction_ = new ICOSSpectraPeaks(peaksINI, calibrationINI);
        mainPeaksFunction_ = new ICOSSpectraPeaks(peaksINI, calibrationINI);
        flangeFunction_ = new ICOSSpectraPeaks(peaksINI, calibrationINI);
    }
    else if (configINI.getLaserPathAlgo() == ConfigINI::SINGLE_PATH_LASER)
    {
        spectrumFunction_ = new OpenPathSpectraPeaks(peaksINI, calibrationINI);
        mainPeaksFunction_ = new OpenPathSpectraPeaks(peaksINI, calibrationINI);
        flangeFunction_ = new OpenPathSpectraPeaks(peaksINI, calibrationINI);
    }
    else
        throw std::runtime_error("Unknown laser path algorithm");
    
	lineLock_ = new LineLock();
	lineLock_->initLineLock(linelockINI);
    
    initChambers(configINI, peaksINI);
}

SingleSpectrumAnalyzer::~SingleSpectrumAnalyzer()
{
    if (spectrumFunction_ != NULL)
        delete spectrumFunction_;
    
    for (std::size_t ii = 0; ii < flangeConcentration_.size(); ii++)
        delete flangeConcentration_[ii];

	if (lineLock_ != NULL)
		delete lineLock_;
}

template<class T>
std::ostream & operator<<(std::ostream & str, const std::vector<T> & v)
{
    str << "[";
    for (int nn = 0; nn < v.size()-1; nn++)
        str << v[nn] << ", ";
    str << v[v.size()-1] << "]";
    return str;
}

void SingleSpectrumAnalyzer::fit(
    const PeaksINI & peaksINI,
    const ConfigINI & configINI,
    const PhysConstsINI & physConstsINI,
	const LinelockINI & linelockINI,
    const std::vector<double> & laserMinusOffset,
    const CavityProperties & mainCavity,
    const std::vector<CavityProperties> & flangeCavities)
{
    // call etalon function if it hasn't been called yet.
    // set some miscellaneous flags.
    spectrumFitter_.initFrequenciesAndRegions(peaksINI, laserMinusOffset.size());
    
    updateChambers(peaksINI, physConstsINI);

    // Maps from peak ID to peak* make it easier to set up master-slave
    // relationships on subsets of all peaks in the INI.
    //
    std::vector<const PeaksINI::PeakConstants*> allPeaks, flangePeaks;
    allPeaks = getAllPeaks(peaksINI);
    flangePeaks = getFlangePeaks(peaksINI);
    
    // Set initial peak parameters for the fit.
    //

    std::vector<double> peakAreas, peakDopplerWidths, peakPressureWidths, peakCenters;
    estimatePeakAreas(allPeaks, physConstsINI, mainCavity, flangeCavities, peakAreas);
    estimatePeakDopplerWidths(allPeaks, physConstsINI, mainCavity, flangeCavities, peakDopplerWidths);
    estimatePeakPressureWidths(allPeaks, physConstsINI, mainCavity, flangeCavities, peakPressureWidths);
    double peakShift = peaksINI.shift_GHz() - peaksINI.peak(peaksINI.zeroPeak()).center_GHz();
    estimatePeakCenters(allPeaks, peakShift, mainCavity, flangeCavities, peakCenters);
    
//    std::cerr << "\n** Centers:\n" << peakCenters << "\n";
//    std::cerr << "\n** Areas:\n" << peakAreas << "\n";
//    std::cerr << "\n** PWs:\n" << peakPressureWidths << "\n";
//    std::cerr << "\n** DWs:\n" << peakDopplerWidths << "\n";
    
    // setPeaks() totally blows away and resets the spectrumFunction, so it's ok
    // for the number of peaks to be changed here.  JUST CHECKED.  PCH170215
    spectrumFunction_->setPeaks(peaksINI, allPeaks, peakCenters, peakAreas,
        peakDopplerWidths, peakPressureWidths);
    
    // needs a bunch of parameters, but not actually the peak list

    doFit(peaksINI, configINI, linelockINI, spectrumFunction_, laserMinusOffset, mainCavity.pressure_torr());
//    log("postFitParams.m") << "post{" << fitIndex << "} = " << spectrumFunction_->getParams() << ";" << std::endl;

    std::vector<double> flangeParams = getBaselineAndFlangeParams(allPeaks, spectrumFunction_->getParams());
    flangeFunction_->setPeaks(peaksINI, flangePeaks, flangeParams);
    
//    log("flangeParams.m") << "flange{" << fitIndex << "} = " << flangeFunction_->getParams() << ";" << std::endl;
    
    // needs zero peak center, global shift, fit window.
    // the fit window stuff though—why is this even part of singlespectrumanalyzer...
    postFit(peaksINI, configINI, physConstsINI, linelockINI, spectrumFunction_, mainCavity, flangeCavities);
    
//    fitIndex++;
}



void SingleSpectrumAnalyzer::fitWithFitFlanges(
    const PeaksINI & peaksINI,
    const ConfigINI & configINI,
    const PhysConstsINI & physConstsINI,
	const LinelockINI & linelockINI,
	const std::vector<double> & laserMinusOffset,
    const CavityProperties & mainCavity,
    const std::vector<CavityProperties> & flangeCavities)
{
    // call etalon function if it hasn't been called yet.
    // set some miscellaneous flags.
    spectrumFitter_.initFrequenciesAndRegions(peaksINI, laserMinusOffset.size());
    
    updateChambers(peaksINI, physConstsINI);
    
    // Maps from peak ID to peak* make it easier to set up master-slave
    // relationships on subsets of all peaks in the INI.
    //
    std::vector<const PeaksINI::PeakConstants*> flangePeaks, mainPeaks;
    mainPeaks = getMainPeaks(peaksINI);
    flangePeaks = getFlangePeaks(peaksINI);
    
    // Set initial peak parameters for the fit—omitting flanges.
    //
    
    std::vector<double> peakAreas, peakDopplerWidths, peakPressureWidths, peakCenters;
    estimatePeakAreas(mainPeaks, physConstsINI, mainCavity, flangeCavities, peakAreas);
    estimatePeakDopplerWidths(mainPeaks, physConstsINI, mainCavity, flangeCavities, peakDopplerWidths);
    estimatePeakPressureWidths(mainPeaks, physConstsINI, mainCavity, flangeCavities, peakPressureWidths);
    double peakShift = peaksINI.shift_GHz() - peaksINI.peak(peaksINI.zeroPeak()).center_GHz();
    estimatePeakCenters(mainPeaks, peakShift, mainCavity, flangeCavities, peakCenters);
    
    mainPeaksFunction_->setPeaks(peaksINI, mainPeaks, peakCenters, peakAreas, peakDopplerWidths, peakPressureWidths);
    
    // Divide out the background peaks (flange peaks)
    const std::vector<double> & xGHz = spectrumFitter_.getFrequencies();
    std::vector<double> foregroundTransmission(xGHz.size());
    for (std::size_t ii = 0; ii < xGHz.size(); ii++)
    {
        foregroundTransmission[ii] = laserMinusOffset[ii] /
            flangeFunction_->fWithoutBaseline(xGHz[ii] - previousSpectralShiftGHz_, ii);
    }
    
    // needs a bunch of parameters, but not actually the peak list
    doFit(peaksINI, configINI, linelockINI, mainPeaksFunction_, foregroundTransmission, mainCavity.pressure_torr());
    
    // needs zero peak center, global shift, fit window.
    // the fit window stuff though—why is this even part of singlespectrumanalyzer...
    postFit(peaksINI, configINI, physConstsINI, linelockINI, mainPeaksFunction_, mainCavity, flangeCavities);
}



void SingleSpectrumAnalyzer::fitWithFixedFlanges(
    const PeaksINI & peaksINI,
    const ConfigINI & configINI,
    const PhysConstsINI & physConstsINI,
	const LinelockINI & linelockINI,
	const std::vector<double> & laserMinusOffset,
    const CavityProperties & mainCavity,
    const std::vector<CavityProperties> & flangeCavities)
{
    // call etalon function if it hasn't been called yet.
    // set some miscellaneous flags.
    spectrumFitter_.initFrequenciesAndRegions(peaksINI, laserMinusOffset.size());
    
    updateChambers(peaksINI, physConstsINI);
    
    // Maps from peak ID to peak* make it easier to set up master-slave
    // relationships on subsets of all peaks in the INI.
    //
    std::vector<const PeaksINI::PeakConstants*> flangePeaks, mainPeaks;
    mainPeaks = getMainPeaks(peaksINI);
    flangePeaks = getFlangePeaks(peaksINI);
    
    // Set initial peak parameters for the fit—omitting flanges.
    //
    
    std::vector<double> peakAreas, peakDopplerWidths, peakPressureWidths, peakCenters;
    double peakShift;
    
    estimatePeakAreas(mainPeaks, physConstsINI, mainCavity, flangeCavities, peakAreas);
    estimatePeakDopplerWidths(mainPeaks, physConstsINI, mainCavity, flangeCavities, peakDopplerWidths);
    estimatePeakPressureWidths(mainPeaks, physConstsINI, mainCavity, flangeCavities, peakPressureWidths);
    peakShift = peaksINI.shift_GHz() - peaksINI.peak(peaksINI.zeroPeak()).center_GHz();
    estimatePeakCenters(mainPeaks, peakShift, mainCavity, flangeCavities, peakCenters);
    mainPeaksFunction_->setPeaks(peaksINI, mainPeaks, peakCenters, peakAreas, peakDopplerWidths, peakPressureWidths);
    
    // Set initial peak parameters for just the flanges
    //
    
    estimatePeakAreas(flangePeaks, physConstsINI, mainCavity, flangeCavities, peakAreas);
    estimatePeakDopplerWidths(flangePeaks, physConstsINI, mainCavity, flangeCavities, peakDopplerWidths);
    estimatePeakPressureWidths(flangePeaks, physConstsINI, mainCavity, flangeCavities, peakPressureWidths);
    peakShift = peaksINI.shift_GHz() - peaksINI.peak(peaksINI.zeroPeak()).center_GHz();
    estimatePeakCenters(flangePeaks, peakShift, mainCavity, flangeCavities, peakCenters);
    flangeFunction_->setPeaks(peaksINI, flangePeaks, peakCenters, peakAreas, peakDopplerWidths, peakPressureWidths);
    
    // Divide out the background peaks (flange peaks)
    const std::vector<double> & xGHz = spectrumFitter_.getFrequencies();
    std::vector<double> foregroundTransmission(xGHz.size());
    for (std::size_t ii = 0; ii < xGHz.size(); ii++)
    {
        foregroundTransmission[ii] = laserMinusOffset[ii] /
            flangeFunction_->fWithoutBaseline(xGHz[ii] - previousSpectralShiftGHz_, ii);
    }
    
    // needs a bunch of parameters, but not actually the peak list
    doFit(peaksINI, configINI, linelockINI, mainPeaksFunction_, foregroundTransmission, mainCavity.pressure_torr());
    
    // needs zero peak center, global shift, fit window.
    // the fit window stuff though—why is this even part of singlespectrumanalyzer...
    postFit(peaksINI, configINI, physConstsINI, linelockINI, mainPeaksFunction_, mainCavity, flangeCavities);
}



void SingleSpectrumAnalyzer::doFit(
    const PeaksINI & peaksINI,
    const ConfigINI & configINI,
	const LinelockINI & linelockINI,
	SpectraPeaksBase * peaksFunction,
    const std::vector<double> & transmission,
    double pressure_torr)
{
	//bool doCalculateInitialY = false;
	//bool doCalculateEstimatedY = false;
	//bool doCalculateFitY = true;
	bool doEstimateBaseline = true;
	bool doHandlePeakShifts = false; // needed for original Linelock
	bool doEstimatePeakAmplitudes = false; // Dmitry: do NOT do this.It overwrites the SLAVE parameters
    
    spectrumFitter_.setSpectrumFunction(peaksFunction);
    
	//Peak positions for Linelock BEFORE fit
	lineLock_->setInitialPeakPositions(extractPeakPositionsForLinelock(linelockINI, peaksFunction));

    std::vector<double> initYs;
    std::vector<double> estYs;
    std::vector<double> fitYs;// mut.lock();
    spectrumFitter_.fitSpectra(
            peaksINI,
            transmission,
            initYs, estYs, fitYs,
			doCalculateInitialY_,  // calc initial guess spectra
			doCalculateEstimatedY_,  // calc estimated spectra
			doCalculateFitY_,  // calc fitted spectra
			doEstimateBaseline, // estimate baseline
			doHandlePeakShifts, // estimate peak positions
			doEstimatePeakAmplitudes, //estimate peak amplitudes
            configINI.getRate().getEstIters(),
            configINI.getRate().getNIters(),
            configINI.getRate().getMrqcofStride(),
            pressure_torr);// mut.unlock();
    lastFitResults_.setInitialModel(initYs);
    lastFitResults_.setEstimatedModel(estYs);
    lastFitResults_.setFinalModelFit(fitYs);
    
    // TODO: calculate shift of the zero peak, and carry to next fit for
    // shift estimation
}

void SingleSpectrumAnalyzer::postFit(
    const PeaksINI & peaksINI,
    const ConfigINI & configINI,
    const PhysConstsINI & physConstsINI,
	const LinelockINI & linelockINI,
	SpectraPeaksBase * peaksFunction,
    const CavityProperties & mainCavity,
    const std::vector<CavityProperties> & flangeCavities)
{
    // Calculate the amount of shift of the zero peak compared to the INI
    // values.
    double x0 = peaksINI.peak(peaksINI.zeroPeak()).shift_GHz() +
        peaksINI.shift_GHz();
    double x1 = peaksFunction->getPeak(peaksINI.zeroPeak()).getCenter();
    previousSpectralShiftGHz_ = x1 - x0;
	peakZeroSHiftGHz_ = x1;
    
//    std::cerr << "Zero peak shift is " << previousSpectralShiftGHz_ << "\n";
//    static int fitIndex = 1;
//    log("preFitParams.m") << "pre{" << fitIndex << "} = " << spectrumFunction_->getParams() << ";" << std::endl;
    
    // Save the concentrations
    // TODO: stuff some of this in a function somewhere, it's messy out here.
    double tauCorrected;
    if (configINI.getLaserPathAlgo() == ConfigINI::RINGDOWN_LASER_PATH)
    {
        tauCorrected = mainCavity.tau_us() * spectrumFitter_.tauAbsorptionCorrection();
    }
    else
    {
        tauCorrected = mainCavity.tau_us();
    }
    
    std::vector<Peak*> peaks;
    peaks.insert(peaks.end(),
        peaksFunction->getPeaks().begin(),
        peaksFunction->getPeaks().begin() + peaksINI.numForegroundPeaks());
    
    mainConcentration_.updateIsotopeConcentration(peaks, physConstsINI,
    	mainCavity.temperature_C(), mainCavity.pressure_torr(), tauCorrected);
    // flanges never need a conc update.
    
    
    // Accumulate plots into container here
    const std::vector<double> & xGHz = spectrumFitter_.getFrequencies();
    
    std::vector<double> baselineTemp;
    for (std::size_t ii = 0; ii < xGHz.size(); ii++)
    {
        baselineTemp.push_back(peaksFunction->baseline_f(xGHz[ii], ii));
    }
    
    lastFitResults_.setFrequencies(xGHz);
    lastFitResults_.setBaseline(baselineTemp);
    lastFitResults_.setFitWindow(peaksINI.fitWindow());

	// Save baseline value at the last point of the fit window.
	// This will serve as power indicator
	power_at_end_fit_window = baselineTemp[xGHz.size() - 1]; // TODO: put in lastFitResults_
    // TODO: NewSpectrumContainer::powerIndicator() should do this for us.

	// Get reduced ch^2 for Goodness of Fit calculation
	reduced_chisq_ = spectrumFitter_.getSpectraFitReducedChisq(); // TODO: put in lastFitResults_
	// Calculate Goodness of fit
	if (reduced_chisq_ >= 0.0 && standardDeviation_ != 0)
	{
		goodnessOfFit_ = std::sqrt(reduced_chisq_) / standardDeviation_; // TODO: put in lastFitResults_
	}
	lineLock_->updateLineLock(extractDataForLinelock(linelockINI, peaksFunction));
	linelockDeltaTEC_ = lineLock_->getDeltaTEC(); // TODO: put in lastFitResults_ ?
	etalonStretch_ = spectrumFitter_.getEtalonStretch();
	isLinelockOOB_ = lineLock_->getIsLineloclOBB();
	CheckForNegativeAreaAndPW(peaksFunction);
}

void SingleSpectrumAnalyzer::CheckForNegativeAreaAndPW(const SpectraPeaksBase * peaksFunction)
{
	// Perform Checks for only species we fit and want concentration from
	isAreaNegative_ = false;
	isPWNegative_ = false;
 	std::vector<Peak*> peaks;
	peaksFunction->getAllPeaks(peaks);
	for (std::size_t iPeak = 0; iPeak < peaks.size(); iPeak++)
	{
		if (peaks.at(iPeak)->wantConc())
		{
			isAreaNegative_ = isAreaNegative_ || (peaks[iPeak]->getArea() < 0);
			isPWNegative_ = isPWNegative_ || (peaks[iPeak]->getPressureWidth() < 0);
		}
	}
}

void SingleSpectrumAnalyzer::setEnableLinelock(bool enabled)
{
	if (lineLock_ != NULL)
	{
		lineLock_->setEnableState(enabled);
	}
}
void SingleSpectrumAnalyzer::setStandardDeviation(double standardDeviation)
{
	standardDeviation_ = standardDeviation;
}

std::vector<double> SingleSpectrumAnalyzer::extractPeakPositionsForLinelock(const LinelockINI & linelockINI, const SpectraPeaksBase * peaksFunction) const
{
	std::vector<double> peakPositions;
	if (!linelockINI.linelockPeakIndexes().empty())
	{
		int nLLPeaks = linelockINI.linelockPeakIndexes().size();
		peakPositions.clear();
		peakPositions.reserve(nLLPeaks);
		for (int i = 0; i < nLLPeaks; i++)
		{
			double peakPosition = peaksFunction->getPeak(linelockINI.linelockPeakIndexes()[i]).getCenter();
			peakPositions.push_back(peakPosition);
		}
	}
	return peakPositions;
}

LinelockData SingleSpectrumAnalyzer::extractDataForLinelock(const LinelockINI & linelockINI, const SpectraPeaksBase * peaksFunction) const
{
	LinelockData linelockData;
	std::vector<double> peakPositions;
	std::vector<double> peakAreas;
	if (!linelockINI.linelockPeakIndexes().empty())
	{
		int nLLPeaks = linelockINI.linelockPeakIndexes().size();
		peakPositions.clear();
		peakPositions.reserve(nLLPeaks);
		peakAreas.clear();
		peakAreas.reserve(nLLPeaks);
		for (int i = 0; i < nLLPeaks; i++)
		{
			double peakPosition = peaksFunction->getPeak(linelockINI.linelockPeakIndexes()[i]).getCenter();
			double peakArea = peaksFunction->getPeak(linelockINI.linelockPeakIndexes()[i]).getArea();
			peakPositions.push_back(peakPosition);
			peakAreas.push_back(peakArea);
		}
	}
	linelockData.setCurrentPeakPositions(peakPositions);
	linelockData.setCurrentPeakAreas(peakAreas);
	linelockData.setGoodnessOfFit(goodnessOfFit_);
	return linelockData;
}

void SingleSpectrumAnalyzer::initChambers(const ConfigINI & configINI, const PeaksINI & peaksINI)
{
    initChamber(mainConcentration_, configINI.stackGasProperties(), configINI.trustFitConcentration());
    
    flangeConcentration_.clear();
    for (int ff = 0; ff < configINI.numFlanges(); ff++)
    {
        flangeConcentration_.push_back(new ChamberConcentration());
        initChamber(*flangeConcentration_[ff], configINI.flangeGasProperties().at(ff),
            configINI.trustFitConcentration());
    }
}


//struct StructCavityGasIni
//{
//    LabeledMatrix broadeningCoefficients;
//    std::map<std::string, double> initialGasConcentration_ppm;
//};


void SingleSpectrumAnalyzer::initChamber(ChamberConcentration & chamber,
    const StructCavityGasIni & gasProperties,
    const std::map<std::string, bool> & isTrusted)
{
    std::vector<ChamberGas> gases;
    
    std::map<std::string, double>::const_iterator itr;
    for (itr = gasProperties.initialGasConcentration_ppm.begin();
        itr != gasProperties.initialGasConcentration_ppm.end(); itr++)
    {
        ChamberGas::ChamberGasType type;
        const std::string & molName = itr->first;
        
        std::map<std::string, bool>::const_iterator itr2 = isTrusted.find(molName);
        
        if (itr2 == isTrusted.end())
        {
            // Molecule is an indirect absorber
            type = ChamberGas::kIndirectAbsorber;
        }
        else if (itr2->second)
        {
            // Molecule is a trusted direct absorber
            type = ChamberGas::kDirectAbsorberTrusted;
        }
        else
        {
            // Molecule is an untrusted direct absorber
            type = ChamberGas::kDirectAbsorberUntrusted;
        }
        
        ChamberGas gas(itr->first, itr->second, type);
        gases.push_back(gas);
    }
    
    // The chamber should work entirely in terms of the gas concs right?
    // That is, the EV names...?
    
    chamber.init(gases, gasProperties.broadeningCoefficients);
}

/*
    Things I need to do:
    - distinguish which species' concentrations are fit
    - calculate the concentrations from peaks
    - calculate the remaining concentrations to come to 1e6 ppm
    - calculate the effective air concentrations
    
    
     I determine which peaks are fit just by reading all the wantConc columns.
     ChamberConcentration needs to maintain a molecule concentration vector
     that it can use for broadening calculations.
 
     foreach molecule
        molConc = initial conc
 
     foreach peak
        if peak.doWantConcentration()
            molConcs.insert(peak.moleculeCode)
*/

void SingleSpectrumAnalyzer::updateChambers(const PeaksINI & peaksINI,
		const PhysConstsINI & physConstsINI)
{
    
//    // Put each peak's isotope and molecule names into the appropriate
//    // chamber's conc maps.
//    for (int iPeak = 0; iPeak < peaksINI.numPeaks(); iPeak++)
//    {
//        ChamberConcentration* chamber;
//        const PeaksINI::PeakConstants & peak = peaksINI.peak(iPeak);
//        if (peak.isFlange())
//        {
//            if (peak.flangeIndex() < 0 || peak.flangeIndex() >= flangeConcentration_.size())
//            {
//                throw std::runtime_error("Peak flange index is out of range");
//            }
//            
//            chamber = flangeConcentration_[peak.flangeIndex()];
//        }
//        else
//        {
//            chamber = &mainConcentration_;
//        }
//        
//        
//        
//        // this is done by init() now.
////        if (false == chamber->doesMeasureIsotope(peak.isotopeCode()))
////        {
////            chamber->initIsotopeConcentration(peak);
////        }
//    }
    
    // commented out because we are removing the total concentration feature
//    mainConcentration_.updateTotalConcentration(physConstsINI);
//    for (std::size_t ff = 0; ff < flangeConcentration_.size(); ff++)
//        flangeConcentration_[ff]->updateTotalConcentration(physConstsINI);
}

// Check chamber setup preconditions:
// - all necessary ChamberConcentrations exist
// - all ChamberConcentrations have values/defaults for each isotope
void SingleSpectrumAnalyzer::checkChamberInitialization(const PeaksINI & peaksINI) const
{
    for (int iPeak = 0; iPeak < peaksINI.numPeaks(); iPeak++)
    {
        const PeaksINI::PeakConstants & peak = peaksINI.peak(iPeak);
        if (peak.isFlange())
        {
            assert(peak.flangeIndex() < static_cast<int>(flangeConcentration_.size()));
//            assert(flangeConcentration_[peak.flangeIndex()]->
//                doesMeasureIsotope(peak.isotopeCode()));
        }
        else
        {
//            assert(mainConcentration_.doesMeasureIsotope(peak.isotopeCode()));
        }
    }

// don't do this, just use gwConc when water is not measured.  right?
//    assert(mainConcentration_->doesMeasureMolecule(_H2O_T));
//    for (int iFlange = 0; iFlange < flangeConcentration_.size(); iFlange++)
//    {
//        assert(flangeConcentration_[iFlange]->doesMeasureMolecule(_H2O_T));
//    }
}


double SingleSpectrumAnalyzer::choosePressure(double instrument_torr, double default_torr) const
{
    double P_torr;
    
    // If the instrument is providing a valid pressure, use that, else use
    // the provisional value that was read previously from INIs.
    if (instrument_torr > 0.0)
        P_torr = instrument_torr;
    else
        P_torr = default_torr;
    return P_torr;
}

double SingleSpectrumAnalyzer::chooseTemperature(double instrument_C, double default_C) const
{
    double T_celsius;
    
    // If the instrument is providing a valid temperature, use that, else use
    // the provisional value that was read previously from INIs.
    if (instrument_C > -273.15)
        T_celsius = instrument_C;
    else
        T_celsius = default_C;
    return T_celsius;
}


void SingleSpectrumAnalyzer::engineeringValues(
    std::map<std::string, double> & outEVs,
    const PhysConstsINI & physConstsINI,
    const CavityProperties & mainCavity,
    bool doCorrectTau) const
{
    const LevmarPeakFit & fitter = spectrumFitter_; // silliness from moving function into here
    const SpectraPeaksBase & spectrum = *fitter.getSpectra();
    double tauCorrected;
    
    // In Ringdown application, when there's high absorption across the
    // spectral lines of interest, the effective cavity length will seem
    // smaller.
    //
    // Tau correction uses the baseline and the model fit.
    if (doCorrectTau)
    {
        tauCorrected = mainCavity.tau_us() * fitter.tauAbsorptionCorrection();
    }
    else
    {
        tauCorrected = mainCavity.tau_us();
    }
    
    std::map<std::string, double> speciesConcs =
        ConcentrationCalculator::speciesConcentrations(
            spectrum,
            physConstsINI,
            mainCavity.temperature_C(),
            mainCavity.pressure_torr(),
            tauCorrected);
    
    std::map<std::string, double>::iterator itr;
    for (itr = speciesConcs.begin (); itr != speciesConcs.end (); itr++)
    {
        outEVs[itr->first] = itr->second;
    }
}

void SingleSpectrumAnalyzer::getBaselineParameters(std::vector<double> & outParams) const
{
    const SpectraPeaksBase & spectrum = *spectrumFitter_.getSpectra();
    outParams = spectrum.getBaselineParams();
}

void SingleSpectrumAnalyzer::getPeakParameters(std::vector<double> & outParams) const
{
    const SpectraPeaksBase & spectrum = *spectrumFitter_.getSpectra();
    outParams = spectrum.getPeakParams();
}


std::vector<const PeaksINI::PeakConstants*> SingleSpectrumAnalyzer::getAllPeaks(
    const PeaksINI & peaksINI) const
{
    std::vector<const PeaksINI::PeakConstants*> peaks;
    peaks.reserve(peaksINI.numPeaks());
    
    for (int ii = 0; ii < peaksINI.numPeaks(); ii++)
    {
        const PeaksINI::PeakConstants & p = peaksINI.peak(ii);
        peaks.push_back(&p);
    }
    return peaks;
}

std::vector<const PeaksINI::PeakConstants*> SingleSpectrumAnalyzer::getFlangePeaks(
    const PeaksINI & peaksINI) const
{
    std::vector<const PeaksINI::PeakConstants*> peaks;
    peaks.reserve(peaksINI.numBackgroundPeaks());
    
    for (int ii = 0; ii < peaksINI.numPeaks(); ii++)
    {
        const PeaksINI::PeakConstants & p = peaksINI.peak(ii);
        if (p.isFlange())
            peaks.push_back(&p);
    }
    return peaks;
}

std::vector<const PeaksINI::PeakConstants*> SingleSpectrumAnalyzer::getMainPeaks(
    const PeaksINI & peaksINI) const
{
    std::vector<const PeaksINI::PeakConstants*> peaks;
    peaks.reserve(peaksINI.numForegroundPeaks());
    
    for (int ii = 0; ii < peaksINI.numPeaks(); ii++)
    {
        const PeaksINI::PeakConstants & p = peaksINI.peak(ii);
        if (false == p.isFlange())
            peaks.push_back(&p);
    }
    return peaks;
}


std::vector<double> SingleSpectrumAnalyzer::getBaselineAndFlangeParams(
    const std::vector<const PeaksINI::PeakConstants*> & peakConsts,
    const std::vector<double> & params) const
{
    std::vector<double> outParams;
    
    outParams.insert(outParams.end(), params.begin(), params.begin()+4); // baseline
    
    for (std::size_t ii = 0; ii < peakConsts.size(); ii++)
    if (peakConsts[ii]->isFlange())
    {
        int iBegin = 4 + 4*ii; // skip 4 baseline params, span 4 peak params
        int iEnd = 4 + 4*(ii+1);
        outParams.insert(outParams.end(), params.begin()+iBegin, params.begin()+iEnd);
    }
    
    return outParams;
}

// AREA
// Peak area depends on isotope concentration (number of molecules
// with a peak at the expected location).
double SingleSpectrumAnalyzer::estimateIsotopeConcentrationForArea(
    const PeaksINI::PeakConstants & peak) const
{
    double isoConc;
    const ChamberConcentration* chamber = getChamber(peak);
    
    const double TINY_PPM = 1e-3;
    double previous_self_ppm = chamber->isotopeConcentration_ppm(peak.isotopeCode());
    if (previous_self_ppm > TINY_PPM)
    {
        isoConc = previous_self_ppm;
    }
    else
    {
        isoConc = chamber->initialConcentration_ppm(peak.molName());
    }
    
    return isoConc;
}

// PRESSURE WIDTH
// Use total concentration of own molecule and of water for the
// pressure-broadening calculations.
double SingleSpectrumAnalyzer::estimateIsotopeConcentrationForPW(
    const PeaksINI::PeakConstants & peak) const
{
    double isoConc;
    const ChamberConcentration* chamber = getChamber(peak);
    
    isoConc = chamber->moleculeConcentration_ppm(peak.molName());
    
    return isoConc;
}
// PRESSURE WIDTH
// Use total concentration of own molecule and of water for the
// pressure-broadening calculations.
double SingleSpectrumAnalyzer::estimateEffectiveAirConcentrationForPW(
    const PeaksINI::PeakConstants & peak) const
{
    double effectiveAirConc;
    const ChamberConcentration* chamber = getChamber(peak);
    
    // Model:
    //
    // The chamber knows of direct absorbers and indirect absorbers.
    // - Direct absorbers (molecules not isotopes) have peaks that we fit.
    // - Indirect absorbers don't have peaks that we fit.
    // However we must estimate concentrations of all these materials,
    // for each isotope (sometimes?), regardless of whether there are peaks or
    // not, because all species contribute to the broadening of spectral peaks
    // (this is handled ultimately as an air broadening term for each peak).
    //
    // 1. Direct absorbers with wantConc = Y for at least one peak: the chamber
    // uses the peak width to calculate the isotope concentration.
    //
    // 2. Direct absorbers with wantConc = N for all peaks: the chamber assumes
    // that the peak conc calculation is unreliable and uses the background
    // concentration calculation (defined below).
    //
    // 3. Indirect absorbers: the chamber uses the background concentration
    // calculation.
    //
    // Background concentration calculation: the INI/system config must supply
    // a provisional concentration for ALL isotopes (direct and indirect).
    // Unless a species conc is calculated from peak widths, it's a background
    // species.  Background species concentrations are assumed to keep a fixed
    // ratio during operation of the instrument (the ratio is set by their
    // provisional concentrations from config).  The proportion of background
    // species is calculated by the chamber after all peak concentrations so
    // as to get the total concentration to sum to 1e6 ppm.
    
    //double cAir_old = 1e6 - chamber->moleculeConcentration_ppm(peak.evName());
    effectiveAirConc = chamber->effectiveAirConcentration_ppm(peak.molName());
    
    return effectiveAirConc;
}

void SingleSpectrumAnalyzer::estimatePeakAreas(const std::vector<const PeaksINI::PeakConstants*> & peaks,
	const PhysConstsINI & physConstsINI,
    const CavityProperties & mainCavity,
    const std::vector<CavityProperties> & flangeCavities,
    std::vector<double> & outPeakAreas) const
{
    outPeakAreas = std::vector<double>(peaks.size());
    int iArea = 0;
    
    for (std::size_t ii = 0; ii < peaks.size(); ii++)
    {
        const PeaksINI::PeakConstants & peak = *peaks[ii];
        
        double isoConc_ppm = estimateIsotopeConcentrationForArea(peak);
        
        double peakTau_us;
        double peakT_C;
        double peakP_torr;
        if (peaks[ii]->isFlange())
        {
            unsigned int flangeIndex = peaks[ii]->flangeIndex();
            
            peakTau_us = flangeCavities.at(flangeIndex).tau_us();
            peakT_C = flangeCavities.at(flangeIndex).temperature_C();
            peakP_torr = flangeCavities.at(flangeIndex).pressure_torr();
        }
        else
        {
            peakTau_us = mainCavity.tau_us();
            peakT_C = mainCavity.temperature_C();
            peakP_torr = mainCavity.pressure_torr();
        }
        
        double peakT_kelvin = peakT_C + 273.15;
		std::vector<double> qCoeffs = physConstsINI.partitionFunctionCoefficents(peak.isotopeCode());
        double areaEstimate = PhysicalConsts::peakArea(
        	qCoeffs,
            peakT_kelvin,
            peakP_torr,
            peak.epp(),
            peak.adjustedLineStrength(),
            isoConc_ppm,
            peakTau_us);
        
        outPeakAreas[iArea++] = areaEstimate;
    }
}

void SingleSpectrumAnalyzer::estimatePeakDopplerWidths(const std::vector<const PeaksINI::PeakConstants*> & peaks,
	const PhysConstsINI & physConstsINI,
    const CavityProperties & mainCavity,
    const std::vector<CavityProperties> & flangeCavities,
	std::vector<double> & outPeakDopplerWidths) const
{
    outPeakDopplerWidths = std::vector<double>(peaks.size());
    int iOut = 0;
    
    
    for (std::size_t ii = 0; ii < peaks.size(); ii++)
    {
        const PeaksINI::PeakConstants & peak = *peaks[ii];
        
        double peakT_C, peakP_torr;
        if (peaks[ii]->isFlange())
        {
            unsigned int flangeIndex = peaks[ii]->flangeIndex();
            
            peakT_C = flangeCavities.at(flangeIndex).temperature_C();
            peakP_torr = flangeCavities.at(flangeIndex).pressure_torr();
        }
        else
        {
            peakT_C = mainCavity.temperature_C();
            peakP_torr = mainCavity.pressure_torr();
        }
        
        double peakT_kelvin = peakT_C + 273.15;
        
        double dopplerWidthEstimate = PhysicalConsts::peakDopplerWidth(
        	physConstsINI.getMass(peak.isotopeCode()),
            peak.center_GHz() + peak.press_shift_GHz() * peakP_torr / 760.0,
            peakT_kelvin);
        
        outPeakDopplerWidths[iOut++] = dopplerWidthEstimate;
    }
}

void SingleSpectrumAnalyzer::estimatePeakPressureWidths(const std::vector<const PeaksINI::PeakConstants*> & peaks,
	const PhysConstsINI & physConstsINI,
    const CavityProperties & mainCavity,
    const std::vector<CavityProperties> & flangeCavities,
    std::vector<double> & outPeakPressureWidths) const
{
    outPeakPressureWidths = std::vector<double>(peaks.size());
    
    // TODO: the R matrix for each chamber should be known, or easily accessible
    // with row/column map lookup.
    //
    // First put a loop here to calculate the effective air conc for each
    // molecule in each chamber.  It can go here for now I think.
    //
    // ... or it can go in
    
    for (std::size_t iPeak = 0; iPeak < peaks.size(); iPeak++)
    {
        const PeaksINI::PeakConstants & peak = *peaks[iPeak];
        //std::string moleculeName = physConstsINI.getMoleculeName(peak.isotopeCode());
        // TODO: first calculate the estimated concentrations for all gases.
        // what this function does is get the main chamber OR flange from the
        // peak, then read the total concentration from the chamber and return
        // it.  this is fine as long as the chamber concentration includes the
        // concentration of the gas whether it be a direct absorber (measured)
        // or an indirect absorber (should be estimated using the fixed ratios
        // between indirect absorber concentrations, and the summed concentration
        // of all measured species, to replace the 1-conc part).
        //
        // Actually at this stage we need to take basically a matrix of
        // the gammas, species broadening coefficients, and multiply the vector
        // of concentrations.  BUT the gamma matrix is per-peak!  It has the
        // form
        //
        // gamma(species2, species1, peakIdx) = gamma(air, species1, peakIdx) * R(species2, species1)
        //
        // and R is just some table that we calculate from first principles.
        //
        //
        // The notion of getting a concentration for a PEAK is now starting to
        // get a little clunky maybe.  For each peak's broadening coeff,
        // we will want a vector of all gas concentrations in the chamber
        // and a vector of all gamma(s2,s1) for that peak.  Then dot them.
        //
        // So for each peak we need gamma(s2,s1) for all other species s2.
        //
        // Considering how this gets used, precalculating conc(s2)*R(s2,s1) will
        // give us the pieces for an effective air concentration for species1.
        // What we should do is precalculate
        //      conc(s2)*R(s2,s1)
        //      conc(s3)*R(s3,s1)
        //      conc(s4)*R(s4,s1)
        //      conc(s5)*R(s5,s1)
        //      . . .
        //      conc(s1)*R(s1,s2)
        //      conc(s3)*R(s3,s2)
        //      . . .
        // and so on, at the top of this function.  It's a matrix.  Then
        //
        
        
        double peakT_C, peakP_torr;
        if (peak.isFlange())
        {
            unsigned int flangeIndex = peak.flangeIndex();
            
            peakT_C = flangeCavities.at(flangeIndex).temperature_C();
            peakP_torr = flangeCavities.at(flangeIndex).pressure_torr();
        }
        else
        {
            peakT_C = mainCavity.temperature_C();
            peakP_torr = mainCavity.pressure_torr();
        }
        
        double peakT_kelvin = peakT_C + 273.15;
        
        // TODO: send in the effective air concentration.
        
        double selfConc_ppm = estimateIsotopeConcentrationForPW(peak);
        double effectiveAirConc_ppm = estimateEffectiveAirConcentrationForPW(peak); // TODO: is this an ok way to proceed
        
        double pressureWidthEstimate = PhysicalConsts::peakPressureWidth_new(
            selfConc_ppm,
            effectiveAirConc_ppm,
            peak.gammaSelf(),
            peak.gammaAir(),
            peak.tExponent(),
            peakT_kelvin,
            peakP_torr);
        
        outPeakPressureWidths[iPeak] = pressureWidthEstimate;
    }
}

void SingleSpectrumAnalyzer::estimatePeakCenters(const std::vector<const PeaksINI::PeakConstants*> & peaks,
    double originGHz,
    const CavityProperties & mainCavity,
    const std::vector<CavityProperties> & flangeCavities,
    std::vector<double> & outPeakCenters) const
{
    outPeakCenters = std::vector<double>(peaks.size());
	int iOut = 0;
	double peakP_torr;
	for (std::size_t ii = 0; ii < peaks.size(); ii++)
	{
		const PeaksINI::PeakConstants & peak = *peaks[ii];
        
        if (peak.isFlange())
        {
            peakP_torr = flangeCavities.at(peak.flangeIndex()).pressure_torr();
        }
        else
        {
            peakP_torr = mainCavity.pressure_torr();
        }
        
		double centerEstimate_GHz = peak.center_GHz()
			+ peak.shift_GHz()
			+ originGHz
			+ peak.press_shift_GHz() * peakP_torr / 760.0;

		outPeakCenters[iOut++] = centerEstimate_GHz;
	}
}



const ChamberConcentration* SingleSpectrumAnalyzer::getChamber(
    const PeaksINI::PeakConstants & peak) const
{
    const ChamberConcentration* chamber;
    if (peak.isFlange())
        chamber = flangeConcentration_[peak.flangeIndex()];
    else
        chamber = &mainConcentration_;
    
    return chamber;
}

ChamberConcentration* SingleSpectrumAnalyzer::getChamber(
    const PeaksINI::PeakConstants & peak)
{
    ChamberConcentration* chamber;
    if (peak.isFlange())
        chamber = flangeConcentration_[peak.flangeIndex()];
    else
        chamber = &mainConcentration_;
    
    return chamber;
}


