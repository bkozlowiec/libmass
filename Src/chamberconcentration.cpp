//
//  ChamberConcentration.cpp
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 4/18/16.
//
//

#include <cassert>

#include "chamberconcentration.h"
#include "spectra/concentrationcalculator.h"
#include "spectra/peak.h"
#include "nr/extramath.h"


ChamberConcentration::
ChamberConcentration()
{
}

void ChamberConcentration::init(const std::vector<ChamberGas> & gases, const LabeledMatrix & broadeningCoefficients)
{
    broadeningCoefficients_ = broadeningCoefficients;
    gases_ = gases;
    
	for (std::size_t gg = 0; gg < gases_.size(); gg++)
	{
		molecule_ppm_[gases_[gg].name()] = gases_[gg].provisionalConcentration_ppm();
		initial_molecule_ppm_[gases[gg].name()] = gases_[gg].provisionalConcentration_ppm();
	}
	updateEffectiveAirConcentration();
    /*
    double totalBackgroundConc_ppm = 0.0;
    for (std::size_t gg = 0; gg < gases_.size(); gg++)
    {
        if (gases_[gg].isBackground())
        {
            totalBackgroundConc_ppm += gases_[gg].provisionalConcentration_ppm();
        }
        
        molecule_ppm_[gases_[gg].name()] = gases_[gg].provisionalConcentration_ppm();
        initial_molecule_ppm_[gases[gg].name()] = gases_[gg].provisionalConcentration_ppm();
    }
    
    backgroundGasFraction_ = std::vector<double>(gases_.size(), 0.0);
    for (std::size_t gg = 0; gg < gases_.size(); gg++)
    {
        if (gases_[gg].isBackground())
        {
            backgroundGasFraction_[gg] = gases_[gg].provisionalConcentration_ppm() / totalBackgroundConc_ppm;
        }
    }
    
    updateEffectiveAirConcentration();
//    std::cerr << "Effective air conc map: size " << effectiveAir_ppm_.size() << ".\n";

 */
}

void ChamberConcentration::updateIsotopeConcentration(
    const std::vector<Peak*> peaks,
    const PhysConstsINI & physConstsINI,
    double temperature_C,
    double pressure_torr,
    double tauCorrected)
{
    // Presently we scan all the peaks and if they have wantConc() == true then
    // we calculate the conc and push it into the iso averager.
    //
    // This does not test to see if more than one peak has wantConc() == true.
    
    for (std::size_t ii = 0; ii < peaks.size(); ii++)
    {
		const std::string & isoCode = peaks[ii]->getIsotopeCode();
		const std::string & molName = peaks[ii]->getMolName();
		// correct untrusted concentrations
		for (std::size_t gg = 0; gg < gases_.size(); gg++)
		{
			const ChamberGas & gas = gases_[gg];
			if (gas.name() == molName)
			{
				if (gas.isTrusted())
				{
					double ppm = ConcentrationCalculator::gasConcentration(*peaks[ii],
						physConstsINI.partitionFunctionCoefficents(isoCode),
						temperature_C, pressure_torr, tauCorrected);
					isotope_ppm_[isoCode] = ppm;
					molecule_ppm_[molName] = ppm;
				}
				else
				{
					isotope_ppm_[isoCode] = gases_[gg].provisionalConcentration_ppm();
					molecule_ppm_[molName] = gases_[gg].provisionalConcentration_ppm();
				}
			}
		}
    }

	updateEffectiveAirConcentration();
	/*
    double nonBackgroundConcentration_ppm = 0.0;
    for (std::size_t gg = 0; gg < gases_.size(); gg++)
    {
        const ChamberGas & gas = gases_[gg];
        if (gas.isTrusted())
        {
            nonBackgroundConcentration_ppm += molecule_ppm_[gas.name()];
        }
    }
    
    double backgroundConcentration_ppm = 1e6 - nonBackgroundConcentration_ppm;
    
    // Now fill in the background concentrations
    for (std::size_t gg = 0; gg < gases_.size(); gg++)
    {
        const ChamberGas & gas = gases_[gg];
        if (gas.isBackground())
        {
            molecule_ppm_[gas.name()] = backgroundConcentration_ppm * backgroundGasFraction_[gg];
        }
    }
    
    // Lastly... DOUBLE CHECK THE CONCS
    double totalConc_ppm = 0.0;
    std::map<std::string, double>::const_iterator itr;
    for (itr = molecule_ppm_.begin(); itr != molecule_ppm_.end(); itr++)
    {
        totalConc_ppm += itr->second;
    }
    
    std::cerr << "Total ppm now " << totalConc_ppm << ".\n";
    
    updateEffectiveAirConcentration();
    */
}


void ChamberConcentration::updateEffectiveAirConcentration()
{
    effectiveAir_ppm_.clear();
    
    // 1. Make the concentration vector for the RHS
    // We only need to read off the gases present in the columns of the
    // broadening matrix.
    
    NRVec<double> allConcentrations_ppm(0.0, broadeningCoefficients_.matrix().ncols());
	double sumOfTrustedConcetrations = 0;
	double sumOfUnTrustedConcetrations = 0;
    for (std::size_t gg = 0; gg < gases_.size(); gg++)
    {
        const ChamberGas & gas = gases_[gg];
        if (broadeningCoefficients_.hasColumn(gas.name()))
        {
			unsigned int rowIndex = broadeningCoefficients_.columnIndex(gas.name());
			double conc = molecule_ppm_[gas.name()];
			if (gas.isTrusted())
				sumOfTrustedConcetrations += conc;
			else
				sumOfUnTrustedConcetrations += conc;
            allConcentrations_ppm[rowIndex] = conc;
        }
    }

	// 2. Correct (scale) inderect absorbtion concentrations and
	// untrusted direct absorbers concentrations to get total 100%
	double scaleFactor = 1.0;
	if (sumOfUnTrustedConcetrations > 0.0)
	{
		scaleFactor = fabs((1e6 - sumOfTrustedConcetrations) / sumOfUnTrustedConcetrations);
	}

	for (std::size_t gg = 0; gg < gases_.size(); gg++)
	{
		const ChamberGas & gas = gases_[gg];
		if (broadeningCoefficients_.hasColumn(gas.name()))
		{
			if (!gas.isTrusted())
			{
				unsigned int rowIndex = broadeningCoefficients_.columnIndex(gas.name());
				allConcentrations_ppm[rowIndex] *= scaleFactor;
			}
		}
	}
    
    // 3. Get the effective air concentrations and stuff 'em in the map
    
    NRVec<double> airConcentrations_ppm = broadeningCoefficients_.matrix() * allConcentrations_ppm;
    
    for (std::size_t gg = 0; gg < gases_.size(); gg++)
    {
        const ChamberGas & gas = gases_[gg];
        if (gas.isDirect())
        {
            unsigned int rowIndex = broadeningCoefficients_.rowIndex(gas.name());
            effectiveAir_ppm_[gas.name()] = airConcentrations_ppm[rowIndex];
        }
    }
}

double ChamberConcentration::effectiveAirConcentration_ppm(const std::string & molName) const
{
//    std::cerr << "My matrix:\n";
//    std::cerr << broadeningCoefficients_.matrix() << "\n";
//    std::cerr << ".\n";
    std::map<std::string, double>::const_iterator itr;
    itr = effectiveAir_ppm_.find(molName);
    
    if (itr != effectiveAir_ppm_.end())
    {
        return itr->second;
    }
    else
    {
        throw std::runtime_error(std::string("Chamber does not have effective air concentration for ") + molName);
    }
}

double ChamberConcentration::isotopeConcentration_ppm(const std::string & isotopeCode) const
{
    std::map<std::string, double>::const_iterator itr;
    itr = isotope_ppm_.find(isotopeCode);
    
    if (itr != isotope_ppm_.end())
    {
        return itr->second;
    }
    else
    {
        return -1.0;
    }
}

double ChamberConcentration::moleculeConcentration_ppm(const std::string & molName) const
{
    std::map<std::string, double>::const_iterator itr;
    itr = molecule_ppm_.find(molName);
    
    if (itr != molecule_ppm_.end())
    {
        return itr->second;
    }
    else
    {
        return -1.0;
    }
}

double ChamberConcentration::initialConcentration_ppm(const std::string & molName) const
{
    std::map<std::string, double>::const_iterator itr;
    itr = initial_molecule_ppm_.find(molName);
    
    if (itr != initial_molecule_ppm_.end())
    {
        return itr->second;
    }
    else
    {
        return -1.0;
    }
}

//
//double ChamberConcentration::moleculeConcentration(const std::string & molName) const
//{
//    //int molIndex = rMatrix_.
//}

