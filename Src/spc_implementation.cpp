/*
 * spc_interface.cpp
 *
 *  Created on: May 26, 2015
 *      Author: hsingh
 */

#include <stdexcept>

#include "spc_implementation.h"
#include "utils/physicalConsts.h"
#include "cavityproperties.h"
#include <cassert>

SPCInterface* SPCFactory::createInterface()
{
    return new SPCImplementation();
}

SPCInterface::~SPCInterface ()
{
}

SPCImplementation::SPCImplementation () :
    numLasers_(1),
    ringDown_(),
    laserOffset_()
{
}

SPCImplementation::~SPCImplementation ()
{
    for (std::size_t ii = 0; ii < analyzers_.size(); ii++)
        delete analyzers_[ii];
    for (std::size_t ii = 0; ii < peaksINIs_.size(); ii++)
        delete peaksINIs_[ii];
    for (std::size_t ii = 0; ii < calibrationINIs_.size(); ii++)
        delete calibrationINIs_[ii];
	for (std::size_t ii = 0; ii < linelockINIs_.size(); ii++)
		delete linelockINIs_[ii];
	if (configINI_)
		delete configINI_;
	if (physConstsINI_)
		delete physConstsINI_;
}

int SPCImplementation::initSPC (const std::string & iniFilePath, unsigned char numLasers)
{
	std::string peakPrefixStr("peaks_");
	std::string calPrefixStr("calibration_");
	std::string linelockPrefixStr("linelock_");
    std::string configINI_path = iniFilePath + "/" + "spc_config.ini";
    std::string physConstsINI_path = iniFilePath + "/" + "phys_consts.ini";

    configINI_ = new ConfigINI(configINI_path);
    physConstsINI_ = new PhysConstsINI(physConstsINI_path);

	// SPC is programmed to work with 2 lasers
    if (numLasers > SPCImplementation::maxLasers)
    {
        return -1;
    }
    numLasers_ = numLasers;
    
    for (unsigned int laserIdx = 0; laserIdx < numLasers_; laserIdx++)
    {
        const double FIXED_TAU = -1;
        RingDown* ringDown = new RingDown(
            configINI_->getDacRate(),
            configINI_->getSparseFactor(),
            configINI_->getRingdownClip()[laserIdx],
            FIXED_TAU);
        ringDown_.push_back(ringDown);
        
        std::stringstream streamIdx;
        streamIdx << laserIdx;
        
        // Read the peak INIs for the first time
        peaksINIs_.push_back(new PeaksINI(
            iniFilePath + "/" + peakPrefixStr + streamIdx.str() + ".ini"));
        calibrationINIs_.push_back(new CalibrationINI(
            iniFilePath + "/" + calPrefixStr + streamIdx.str() + ".ini"));
		linelockINIs_.push_back(new LinelockINI(
			iniFilePath + "/" + linelockPrefixStr + streamIdx.str() + ".ini"));

        analyzers_.push_back(new SingleSpectrumAnalyzer(
            *peaksINIs_[laserIdx],
            *calibrationINIs_[laserIdx],
            *configINI_,
            *linelockINIs_[laserIdx]));
        
        if (laserIdx != ConfigINI::FIRST_LASER)
            std::cerr << "Saying it's the first laser but it ain't.\n";
    }
    
    return 0;
}

int SPCImplementation::initSPC(const StructConfigIni & configIni,
    const std::vector<StructIsotopeConsts> & physConstsIni,
    const StructPeaksIni & peaksIni,
    const StructCalibrationIni & calibrationIni,
    const StructLinelockIni & linelockIni)
{
	// Copy objects (instead of reading from INI)
	configINI_ = new ConfigINI(configIni);
	physConstsINI_ = new PhysConstsINI(physConstsIni);
	for (unsigned int laserIdx = 0; laserIdx < numLasers_; laserIdx++)
	{
		peaksINIs_.push_back(new PeaksINI(peaksIni));
		calibrationINIs_.push_back(new CalibrationINI(calibrationIni));
		linelockINIs_.push_back(new LinelockINI(linelockIni));
	}

	for (unsigned int laserIdx = 0; laserIdx < numLasers_; laserIdx++)
	{
		const double FIXED_TAU = -1;
		RingDown* ringDown = new RingDown(
			configINI_->getDacRate(),
			configINI_->getSparseFactor(),
			configINI_->getRingdownClip()[laserIdx],
			FIXED_TAU);
		ringDown_.push_back(ringDown);

		analyzers_.push_back(new SingleSpectrumAnalyzer(
			*peaksINIs_[laserIdx],
			*calibrationINIs_[laserIdx],
			*configINI_,
			*linelockINIs_[laserIdx]));

		if (laserIdx != ConfigINI::FIRST_LASER)
			std::cerr << "Saying it's the first laser but it ain't.\n";
	}
	return 0;
}

unsigned int SPCImplementation::getLaserRampStartIdx() const
{
	return configINI_->getLaserRampStart();
}

unsigned int SPCImplementation::getLaserRampLength() const
{
	return configINI_->getLaserRampLength();
}

unsigned int SPCImplementation::getLaserOFFStartIdx() const
{
	return configINI_->getLaserOffStart();
}

unsigned int SPCImplementation::getLaserOFFLength() const
{
	return configINI_->getLaserOffLength();
}

unsigned int SPCImplementation::getRDFitCnt() const
{
	return configINI_->getNumRingdownsToFit();
}

int SPCImplementation::computeSpectrum(
    double instrumentTemperature_C,
    double instrumentPressure_torr,
    const std::vector<std::vector<double> > & laserRampData,
    const std::vector<std::vector<double> > & laserRingdownData,
    const std::vector<int> & p_rdCnt)
{
    laserRingdownMinusOffset_ = std::vector<std::vector<double> >(numLasers_);
    laserOffset_ = std::vector<double>(numLasers_);
    
	std::vector<double> avgTau(numLasers_, 0.0);
    
    // Calculate tau and the offset.
    for (unsigned int laserIdx = 0; laserIdx < numLasers_; laserIdx++)
    {
    	double standardDeviation;
        ringDown_[laserIdx]->setRDTrace(laserRingdownData[laserIdx], p_rdCnt[laserIdx]);
        
        if (ConfigINI::SINGLE_PATH_LASER == configINI_->getLaserPathAlgo())
        {
            avgTau[laserIdx] = 1e6*configINI_->getCavityLength() / Constants::speedOfLight;
			Tools::standardDeviation(laserRingdownData[laserIdx], laserOffset_[laserIdx], standardDeviation);
			analyzers_[laserIdx]->setStandardDeviation(standardDeviation);
        }
        else if (ConfigINI::RINGDOWN_LASER_PATH == configINI_->getLaserPathAlgo())
        {
            double avgTauStdDev_unused, avgOffsetStdDev_unused;
            int nTau_unused = 0;
            ringDown_.at(laserIdx)->fitExp(avgTau[laserIdx],
                avgTauStdDev_unused, laserOffset_[laserIdx],
                avgOffsetStdDev_unused, nTau_unused);
        }
        else
        {
            throw std::runtime_error("Unimplemented laser path algorithm");
        }
    }
    
    // Subtract off the offset voltage.
    laserRampMinusOffset_ = std::vector<std::vector<double> >(numLasers_);
    for (unsigned int laserIdx = 0; laserIdx < numLasers_; laserIdx++)
    {
        Tools::add(laserRampData[laserIdx], -laserOffset_[laserIdx],
            laserRampMinusOffset_[laserIdx]);
        Tools::add(laserRingdownData[laserIdx], -laserOffset_[laserIdx],
            laserRingdownMinusOffset_[laserIdx]);
    }
    
    // can we get the loop out of the function, pull it out here, to avoid
    // looping inside all these nice functions... it's so hard to test them
    // this way?
	performFit(laserRampMinusOffset_, avgTau,
        instrumentTemperature_C, instrumentPressure_torr,
        std::vector<double>(), std::vector<double>());
    
	int returnCode = -1;
	return returnCode;
}

int SPCImplementation::computeSpectrum(
    double instrumentTemperature_C,
    double instrumentPressure_torr,
    const std::vector<double> & flangeTemperature_C,
    const std::vector<double> & flangePressure_torr,
	const std::vector<std::vector<double> > & laserRampData,
	const std::vector<std::vector<double> > & laserRingdownData,
	const std::vector<int> & p_rdCnt)
{
	laserRingdownMinusOffset_ = std::vector<std::vector<double> >(numLasers_);
	laserOffset_ = std::vector<double>(numLasers_);

	std::vector<double> avgTau(numLasers_, 0.0);

	// Calculate tau and the offset.
	for (unsigned int laserIdx = 0; laserIdx < numLasers_; laserIdx++)
	{
		double standardDeviation;
		ringDown_[laserIdx]->setRDTrace(laserRingdownData[laserIdx], p_rdCnt[laserIdx]);

		if (ConfigINI::SINGLE_PATH_LASER == configINI_->getLaserPathAlgo())
		{
			avgTau[laserIdx] = 1e6*configINI_->getCavityLength() / Constants::speedOfLight;
			Tools::standardDeviation(laserRingdownData[laserIdx], laserOffset_[laserIdx], standardDeviation);
			analyzers_[laserIdx]->setStandardDeviation(standardDeviation);
			//if (flangeTemperature_C.size() > 0)
			//{
			//	peaksINIs_[laserIdx]->updateFlangeTP(flangeTemperature_C, flangePressure_torr);
			//}
		}
		else if (ConfigINI::RINGDOWN_LASER_PATH == configINI_->getLaserPathAlgo())
		{
			double avgTauStdDev_unused, avgOffsetStdDev_unused;
			int nTau_unused = 0;
			ringDown_.at(laserIdx)->fitExp(avgTau[laserIdx],
				avgTauStdDev_unused, laserOffset_[laserIdx],
				avgOffsetStdDev_unused, nTau_unused);
		}
		else
		{
			throw std::runtime_error("Unimplemented laser path algorithm");
		}
	}

	// Subtract off the offset voltage.
	laserRampMinusOffset_ = std::vector<std::vector<double> >(numLasers_);
	for (unsigned int laserIdx = 0; laserIdx < numLasers_; laserIdx++)
	{
		Tools::add(laserRampData[laserIdx], -laserOffset_[laserIdx],
			laserRampMinusOffset_[laserIdx]);
		Tools::add(laserRingdownData[laserIdx], -laserOffset_[laserIdx],
			laserRingdownMinusOffset_[laserIdx]);
	}
    
	// can we get the loop out of the function, pull it out here, to avoid
	// looping inside all these nice functions... it's so hard to test them
	// this way?
	performFit(laserRampMinusOffset_,
        avgTau,
		instrumentTemperature_C, instrumentPressure_torr,
        flangeTemperature_C, flangePressure_torr);

	int returnCode = -1;
	return returnCode;
}


// return the raw signal of detector output for laser ramp and ringdown
std::vector<double> SPCImplementation::getRawSignal (unsigned int laserIdx) const
{
	std::vector<double> returnVec;

	if (laserIdx < numLasers_)
	{
        unsigned int totalPoints = laserRampMinusOffset_[laserIdx].size() +
            laserRingdownMinusOffset_[laserIdx].size();
        
        double offset = laserOffset_[laserIdx];
        
        returnVec.resize(totalPoints);
        
        int jj = 0;
        for (std::size_t ii = 0; ii < laserRampMinusOffset_[laserIdx].size(); ii++)
        {
            returnVec[jj] = laserRampMinusOffset_[laserIdx][ii] + offset;
            jj++;
        }
        for (std::size_t ii = 0; ii < laserRingdownMinusOffset_[laserIdx].size(); ii++)
        {
            returnVec[jj] = laserRampMinusOffset_[laserIdx][ii] + offset;
            jj++;
        }
	}

	return returnVec;
}

std::vector<double> SPCImplementation::getBaselineFit (unsigned int laserIdx) const
{
	std::vector<double> returnVec;
	std::vector<int> fitWindow; //to store the fit window to plot

	if (laserIdx <= (numLasers_ - 1))
	{
        const NewSpectrumContainer & fitContainer = analyzers_[laserIdx]->getFitContainer();
        const std::vector<double> & baseline = fitContainer.baseline();

		//get the desired base line fit data for plotting based on fit window
		// fitWindow[0] denotes the start index
		// fitWindow[1] denotes the length of data
        fitWindow = fitContainer.fitWindow();

		//copy the section of baseline fit based on the fit window
		// also add the baseline offset
        double offset = laserOffset_[laserIdx];
		for (int idx = fitWindow[0]; idx < fitWindow[0] + fitWindow[1]; idx++)
		{
			returnVec.push_back(baseline[idx] + offset);
		}
	}

	return returnVec;
}

std::vector<double> SPCImplementation::getRingdownFit (unsigned int laserIdx) const
{
	std::vector<double> returnVec;

	if (laserIdx <= (numLasers_ - 1))
	{
		switch(configINI_->getLaserPathAlgo())
		{
			case ConfigINI::SINGLE_PATH_LASER:
				returnVec = ringDown_[laserIdx]->getRDTrace ();
				break;

			case ConfigINI::RINGDOWN_LASER_PATH:
			default:
				returnVec = ringDown_[laserIdx]->getRDFit ();
		}
	}

	return returnVec;
}


std::vector<unsigned int> SPCImplementation::getSamplePts(unsigned int laserIdx) const
{
	std::vector<unsigned int> returnVec;

	if (laserIdx <= (numLasers_ - 1))
	{
        const NewSpectrumContainer & fitContainer = analyzers_[laserIdx]->getFitContainer();
        unsigned int totalPoints = fitContainer.baseline().size()
            + laserRingdownMinusOffset_[laserIdx].size();
        
		for (unsigned int idx = 0; idx < totalPoints; idx++)
		{
			returnVec.push_back (idx);
		}
	}

	return returnVec;
}

unsigned int SPCImplementation::getBaselineFitStart (unsigned int laserIdx) const
{
	std::vector<int> fitWindow; //to store the fit window to plot

	if (laserIdx <= (numLasers_ - 1))
	{
        const NewSpectrumContainer & fitContainer = analyzers_[laserIdx]->getFitContainer();
		//get the desired base line fit data for plotting based on fit window
		// fitWindow[0] denotes the start index
		// fitWindow[1] denotes the length of data
        fitWindow = fitContainer.fitWindow();
	}

	return fitWindow[0];
}

unsigned int SPCImplementation::getRingdownFitStart (unsigned int laserIdx) const
{
	unsigned int returnVal;
    
    const NewSpectrumContainer & fitContainer = analyzers_[laserIdx]->getFitContainer();
    
    returnVal = fitContainer.baseline().size() +
					ringDown_[laserIdx]->getRDFitFirstPoint();

	return returnVal;
}

std::vector<double> SPCImplementation::getMeasuredData (unsigned int laserIdx) const
{
	std::vector<double> returnVec;
	std::vector<int> fitWindow; //to store the fit window to plot

	if (laserIdx <= (numLasers_ - 1))
	{
        const NewSpectrumContainer & fitContainer = analyzers_[laserIdx]->getFitContainer();
		//get the desired base line fit data for plotting based on fit window
		// fitWindow[0] denotes the start index
		// fitWindow[1] denotes the length of data
        fitWindow = fitContainer.fitWindow();
        
        const std::vector<double> & yy = laserRampMinusOffset_[laserIdx];
        const std::vector<double> & baseline = fitContainer.baseline();

		//compute the values for graph for the fitWindow
        for (int idx = fitWindow[0]; idx < fitWindow[0] + fitWindow[1]; idx++)
		{
            returnVec.push_back( (baseline[idx] - yy[idx]) / baseline[idx] );
		}
	}

	return returnVec;
}

std::vector<double> SPCImplementation::getPeakFit (unsigned int laserIdx) const
{
	std::vector<double> returnVec;
	std::vector<double> fitVec; //to get the fitted values
	std::vector<int> fitWindow; //to store the fit window to plot

	if (laserIdx <= (numLasers_ - 1))
	{
		if (analyzers_[laserIdx]->getDoCalculateFitY() == true)
		{
			const NewSpectrumContainer & fitContainer = analyzers_[laserIdx]->getFitContainer();
			// fitWindow[0] denotes the start index
			// fitWindow[1] denotes the length of data
			fitWindow = fitContainer.fitWindow();

			fitVec = fitContainer.finalModelFit();
			const std::vector<double> & baseline = fitContainer.baseline();

			//compute the values for graph for the fitWindow
			for (int idx = fitWindow[0]; idx < fitWindow[0] + fitWindow[1]; idx++)
			{
				returnVec.push_back((baseline[idx] - fitVec[idx]) / baseline[idx]);
			}
		}
	}
	return returnVec;
}

std::vector<double> SPCImplementation::getFitResidual(unsigned int laserIdx) const
{
	std::vector<double> returnVec;
	std::vector<double> fitVec;
	std::vector<int> fitWindow; //to store the fit window to plot

	if (laserIdx <= (numLasers_ - 1))
	{
		if (analyzers_[laserIdx]->getDoCalculateFitY() == true)
		{
			const NewSpectrumContainer & fitContainer = analyzers_[laserIdx]->getFitContainer();
			//get the desired base line fit data for plotting based on fit window
			// fitWindow[0] denotes the start index
			// fitWindow[1] denotes the length of data
			fitWindow = fitContainer.fitWindow();

			const std::vector<double> & yy = laserRampMinusOffset_[laserIdx];
			const std::vector<double> & baseline = fitContainer.baseline();
			fitVec = fitContainer.finalModelFit();

			//compute the values for graph for the fitWindow
			for (int idx = fitWindow[0]; idx < fitWindow[0] + fitWindow[1]; idx++)
			{
				returnVec.push_back((fitVec[idx] - yy[idx]) / baseline[idx]);
			}
		}
	}
	return returnVec;
}


std::vector<double> SPCImplementation::getInitialGuessFit(unsigned int laserIdx) const
{
	std::vector<double> returnVec;
	std::vector<double> initVec; //to get the initial values
	std::vector<int> fitWindow; //to store the fit window to plot

	if (laserIdx <= (numLasers_ - 1))
	{
		if (analyzers_[laserIdx]->getDoCalculateInitialY() == true)
		{
			const NewSpectrumContainer & fitContainer = analyzers_[laserIdx]->getFitContainer();
			// fitWindow[0] denotes the start index
			// fitWindow[1] denotes the length of data
			fitWindow = fitContainer.fitWindow();

			initVec = fitContainer.initialModel();
			const std::vector<double> & baseline = fitContainer.baseline();

			//compute the values for graph for the fitWindow
			for (int idx = fitWindow[0]; idx < fitWindow[0] + fitWindow[1]; idx++)
			{
				returnVec.push_back((baseline[idx] - initVec[idx]) / baseline[idx]);
			}
		}
	}
	return returnVec;
}

std::vector<double> SPCImplementation::getEstimatedFit(unsigned int laserIdx) const
{
	std::vector<double> returnVec;
	std::vector<double> estVec; //to get the Estimated values
	std::vector<int> fitWindow; //to store the fit window to plot

	if (laserIdx <= (numLasers_ - 1))
	{
		if (analyzers_[laserIdx]->getDoCalculateEstimatedY() == true)
		{
			const NewSpectrumContainer & fitContainer = analyzers_[laserIdx]->getFitContainer();
			// fitWindow[0] denotes the start index
			// fitWindow[1] denotes the length of data
			fitWindow = fitContainer.fitWindow();

			estVec = fitContainer.initialModel();
			const std::vector<double> & baseline = fitContainer.baseline();

			//compute the values for graph for the fitWindow
			for (int idx = fitWindow[0]; idx < fitWindow[0] + fitWindow[1]; idx++)
			{
				returnVec.push_back((baseline[idx] - estVec[idx]) / baseline[idx]);
			}
		}
	}
	return returnVec;
}

std::vector<double> SPCImplementation::getMeasuredDataWithBL(unsigned int laserIdx) const
{
	std::vector<double> returnVec;
	std::vector<int> fitWindow; //to store the fit window to plot

	if (laserIdx <= (numLasers_ - 1))
	{
        const NewSpectrumContainer & fitContainer = analyzers_[laserIdx]->getFitContainer();
		//get the desired base line fit data for plotting based on fit window
		// fitWindow[0] denotes the start index
		// fitWindow[1] denotes the length of data
        fitWindow = fitContainer.fitWindow();

        const std::vector<double> & yy = laserRampMinusOffset_[laserIdx];

		//compute the values for graph for the fitWindow
        for (int idx = fitWindow[0]; idx < fitWindow[0] + fitWindow[1]; idx++)
		{
            returnVec.push_back(yy[idx]);
		}
	}

	return returnVec;
}
std::vector<double> SPCImplementation::getInitialGuessFitWithBL(unsigned int laserIdx) const
{
	std::vector<double> returnVec;
	std::vector<double> initVec; //to get the initial values
	std::vector<int> fitWindow; //to store the fit window to plot

	if (laserIdx <= (numLasers_ - 1))
	{
		if (analyzers_[laserIdx]->getDoCalculateInitialY() == true)
		{
			const NewSpectrumContainer & fitContainer = analyzers_[laserIdx]->getFitContainer();
			// fitWindow[0] denotes the start index
			// fitWindow[1] denotes the length of data
			fitWindow = fitContainer.fitWindow();

			initVec = fitContainer.initialModel();

			//compute the values for graph for the fitWindow
			for (int idx = fitWindow[0]; idx < fitWindow[0] + fitWindow[1]; idx++)
			{
				returnVec.push_back(initVec[idx]);
			}
		}
	}
	return returnVec;
}
std::vector<double> SPCImplementation::getEstimatedFitWithBL(unsigned int laserIdx) const
{
	std::vector<double> returnVec;
	std::vector<double> estVec; //to get the Estimated values
	std::vector<int> fitWindow; //to store the fit window to plot

	if (laserIdx <= (numLasers_ - 1))
	{
		if (analyzers_[laserIdx]->getDoCalculateEstimatedY() == true)
		{
			const NewSpectrumContainer & fitContainer = analyzers_[laserIdx]->getFitContainer();
			// fitWindow[0] denotes the start index
			// fitWindow[1] denotes the length of data
			fitWindow = fitContainer.fitWindow();

			estVec = fitContainer.initialModel();

			//compute the values for graph for the fitWindow
			for (int idx = fitWindow[0]; idx < fitWindow[0] + fitWindow[1]; idx++)
			{
				returnVec.push_back(estVec[idx]);
			}
		}
	}
	return returnVec;
}
std::vector<double> SPCImplementation::getPeakFitWithBL(unsigned int laserIdx) const
{
	std::vector<double> returnVec;
	std::vector<double> fitVec; //to get the fitted values
	std::vector<int> fitWindow; //to store the fit window to plot

	if (laserIdx <= (numLasers_ - 1))
	{
		if (analyzers_[laserIdx]->getDoCalculateFitY() == true)
		{
			const NewSpectrumContainer & fitContainer = analyzers_[laserIdx]->getFitContainer();
			// fitWindow[0] denotes the start index
			// fitWindow[1] denotes the length of data
			fitWindow = fitContainer.fitWindow();

			fitVec = fitContainer.finalModelFit();

			//compute the values for graph for the fitWindow
			for (int idx = fitWindow[0]; idx < fitWindow[0] + fitWindow[1]; idx++)
			{
				returnVec.push_back(fitVec[idx]);
			}
		}
	}
	return returnVec;
}


std::vector<double> SPCImplementation::getGHzPts (unsigned int laserIdx) const
{
	std::vector<double> returnVec;
	std::vector<double> ghzVec; //total frequency points
	std::vector<int> fitWindow; //to store the fit window to plot

	if (laserIdx <= (numLasers_ - 1))
	{
        const NewSpectrumContainer & fitContainer = analyzers_[laserIdx]->getFitContainer();
		//get the desired base line fit data for plotting based on fit window
		// fitWindow[0] denotes the start index
		// fitWindow[1] denotes the length of data
        fitWindow = fitContainer.fitWindow();

		//get all frequency point equivalent to laser ramp
        ghzVec = fitContainer.frequencies();

		// copy the section of frequency point based on the fit window
		for (int idx = 0; idx < fitWindow[1]; idx++)
		{
			returnVec.push_back(ghzVec[fitWindow[0] + idx]);
		}
	}

	return returnVec;
}

std::map<std::string, double> SPCImplementation::getEVs() const
{
    return engineeringValues_;
}

std::vector<double> SPCImplementation::getBaselineParameters(unsigned int laserIdx) const
{
    std::vector<double> params;
    analyzers_[laserIdx]->getBaselineParameters(params);
    
    return params;
}

std::vector<double> SPCImplementation::getPeakParameters(unsigned int laserIdx) const
{
    std::vector<double> params;
    analyzers_[laserIdx]->getPeakParameters(params);
    
    return params;
}

double SPCImplementation::getGoodnessOfFit(unsigned int laserIdx) const
{
	return analyzers_[laserIdx]->getGoodnessOfFit();
}
double SPCImplementation::getLinelockDeltaTEC(unsigned int laserIdx) const
{
	return analyzers_[laserIdx]->getLinelockDeltaTEC();
}

void SPCImplementation::setEnableLinelock(unsigned int laserIdx, bool Enabled)
{
    //std::cout <<"Rcvd linelock state >> " << Enabled << std::endl;
	analyzers_[laserIdx]->setEnableLinelock(Enabled);
}

double SPCImplementation::getLaserPowerArbUnits(unsigned int laserIdx) const
{
    return analyzers_[laserIdx]->getLaserPowerArbUnits();
}

double SPCImplementation::getEtalonStretchFactor(unsigned int laserIdx) const
{
	return analyzers_[laserIdx]->getEtalonStretchFactor();
}

double SPCImplementation::getDetectorNoise(unsigned int laserIdx) const
{
	return analyzers_[laserIdx]->getStandardDeviation();
}

bool SPCImplementation::getIsLinelockOOB(unsigned int laserIdx) const
{
	return analyzers_[laserIdx]->getIsLinelockOOB();
}

bool SPCImplementation::getIsAreaNegative(unsigned int laserIdx) const
{
	return analyzers_[laserIdx]->isAreaNegative();
}

bool SPCImplementation::getIsPWNegative(unsigned int laserIdx) const
{
	return analyzers_[laserIdx]->isPWNegative();
}

double SPCImplementation::getPeakZeroShiftGHz(unsigned int laserIdx) const
{
	return analyzers_[laserIdx]->getPeakZeroShiftGHz();
}

bool SPCImplementation::getDoCalculateInitialY(unsigned int laserIdx) const
{
	return analyzers_[laserIdx]->getDoCalculateInitialY();
}

bool SPCImplementation::getDoCalculateEstimatedY(unsigned int laserIdx) const
{
	return analyzers_[laserIdx]->getDoCalculateEstimatedY();
}

bool SPCImplementation::getDoCalculateFitY(unsigned int laserIdx) const
{
	return analyzers_[laserIdx]->getDoCalculateFitY();
}

void SPCImplementation::setDoCalculateInitialY(unsigned int laserIdx, bool enable)
{
	analyzers_[laserIdx]->setDoCalculateInitialY(enable);
}

void SPCImplementation::setDoCalculateEstimatedY(unsigned int laserIdx, bool enable)
{
	analyzers_[laserIdx]->setDoCalculateEstimatedY(enable);
}

void SPCImplementation::setDoCalculateFitY(unsigned int laserIdx, bool enable)
{
	analyzers_[laserIdx]->setDoCalculateFitY(enable);
}

void SPCImplementation::performFit(
    const std::vector<std::vector<double> > & laserMinusOffset,
    const std::vector<double> & tau,
    double instrumentTemperature_C,
    double instrumentPressure_torr,
    const std::vector<double> & flangeT_C,
    const std::vector<double> & flangeP_torr)
{
    engineeringValues_.clear();
    bool doCorrectTau = (configINI_->getLaserPathAlgo() == ConfigINI::RINGDOWN_LASER_PATH);

    thread_local std::vector<int> sFitIndex(numLasers_, 0);

    // THINGS WITH laserIdx:
    // sFitIndex
    // analyzers_
    // linelockINIs_
    // laserMinusOffset
    // tau
    //
    // Tempting to come up with a class that holds all the per-laser things.
    //
    // Things without laserIdx:
    // configINI_
    // physConstsINI_
    // T, C
    
    
    // TODO: find good place for these?
    
    // This is a sort of stupid hack.  We just want to pick a good default T and P
    // for the flanges, in case their instrument T and P are giving bad readings.
    // flangeFallbackTP just exists to find a backup T and P for flanges.
    CavityProperties flangeFallbackTP(instrumentTemperature_C, instrumentPressure_torr, 0.0,
        configINI_->getDefaultTemperature_C(), configINI_->getDefaultPressure_torr());
    
    std::vector<CavityProperties> flangeCavities(flangeT_C.size());
    for (std::size_t ff = 0; ff < flangeCavities.size(); ++ff)
    {
        double flangeTau = configINI_->getFlangeLength().at(ff) / (1e-6 * Constants::speedOfLight);
        flangeCavities[ff] = CavityProperties(flangeT_C[ff], flangeP_torr[ff], flangeTau,
            flangeFallbackTP.temperature_C(), flangeFallbackTP.pressure_torr());
    }

    for (std::size_t laserIdx = 0; laserIdx < numLasers_; ++laserIdx)
    {
        CavityProperties mainCavity(instrumentTemperature_C, instrumentPressure_torr, tau[laserIdx],
            configINI_->getDefaultTemperature_C(), configINI_->getDefaultPressure_torr());
        if (peaksINIs_[laserIdx]->doFitFlanges())
        {
            if (sFitIndex[laserIdx] == 0)
            {
                //std::lock_guard<std::mutex> lck(mut);
                // Whenever the counter is set to zero we do the full fit
                //std::cerr << "Fitting all peaks\n";
                analyzers_[laserIdx]->fit(*peaksINIs_[laserIdx],
                    *configINI_,
                    *physConstsINI_,
					*linelockINIs_[laserIdx],
                    laserMinusOffset[laserIdx],
                    mainCavity,
                    flangeCavities);
            }
            else
            {
                //std::cerr << "Fitting foreground peaks with background from fit\n";
                analyzers_[laserIdx]->fitWithFitFlanges(*peaksINIs_[laserIdx],
                    *configINI_,
                    *physConstsINI_,
					*linelockINIs_[laserIdx],
                    laserMinusOffset[laserIdx],
                    mainCavity,
                    flangeCavities);
            }

            // Increment the fit counter and check if it should be reset.
            // The reason we don't use "sFitIndex % fitPeriod == 0" is so the
            // interval between fits is uniform even when sFitIndex overflows
            // INT_MAX.
            ++sFitIndex[laserIdx];
            if (sFitIndex[laserIdx] >= peaksINIs_[laserIdx]->flangeFitPeriod())
                sFitIndex[laserIdx] = 0;
        }
        else
        {
            //std::cerr << "Fitting foreground peaks with background from INI\n";
            analyzers_[laserIdx]->fitWithFixedFlanges(*peaksINIs_[laserIdx],
                *configINI_,
                *physConstsINI_,
				*linelockINIs_[laserIdx],
                laserMinusOffset[laserIdx],
                mainCavity,
                flangeCavities);
        }
        
        analyzers_[laserIdx]->engineeringValues(engineeringValues_,
        	*physConstsINI_,
            mainCavity,
            doCorrectTau);
    }
}


