/*--------------------- Copyright (c) ABB inc, 2015 --------------------------
 *                                 Source code
 * This software is the property of ABB inc and shall be considered and treated
 * as proprietary information.  Refer to the "Source Code License Agreement"
 *-----------------------------------------------------------------------------
 */

#ifndef __LINELOCK_H
#define __LINELOCK_H

#include <vector>
#include <fstream>
#include <chrono>
#include "linelock_ini.h"
#include "linelock_data.h"

/**
 * @class com_abb::cross_stack::LineLock
 * @brief This class implements the Linelock algorithm which
 * calculate the difference between the predicted and measured
 * line centers of the spectra peaks.
 *
 * @author Dmitry Skvortsov <dmitry.skvortsov@us.abb.com>
 */
class LineLock
{
	public:

		/**
		 * @brief Default constructor of LineLock algorithm
		 */
		LineLock();

		/**
		 * @brief Default destructor of LineLock algorithm
		 */
		~LineLock();

		/**
		* @Initializes Linelock with params from linelockINI
		*/
		void initLineLock(const LinelockINI & linelockINI);

		/**
		* This method initialize the internal class members
		* of the linelock feature.
		* <More to add>
		*
		* @return Error codes generated during initialization
		* NO_ERROR Successful initialization
		*
		*/
		void setInitialPeakPositions(std::vector<double> initPeakPositions);

		/**
		* This method calculates the adjustment value for TEC.
		* Call this method after successful spc_interface::computeSpectrum().
		* Read updated deltaTEC value with getDeltaTEC()
		*
		* @return Error codes generated during calculation
		* NO_ERROR Successful computation
		*/
		void updateLineLock(const LinelockData & linelockData);

		/**
		 * This method returns the computed delta for TEC in [K]
		 *
		 * @return deltaTEC value
		 */
		double getDeltaTEC();

		/**
		* This method returns the computed deltaGHz in [GHz]
		*
		* @return deltaGHz value
		*/
		double getDeltaGHz();

		/**
		* This method returns the Enable state of the linelock
		*
		* @return isLineLockEnabled value
		*/
		bool getEnableState();

		/**
		* This method returns the Started state of the linelock
		*
		* @return isStarted value
		*/
		bool getIsStarted();

		/**
		* This method sets the Enable state of the linelock
		*
		* @set isLineLockEnabled value
		*/
		void setEnableState(bool newEnableState);

		/**
		* This method returns the OutOfBounds state of the linelock
		*
		* @return isLinelockOOB_ value
		*/
		bool getIsLineloclOBB() { return isLinelockOOB_; }


	private:
		/**
		* Variable to become true after at least one call to updateLineLock()
		*/
		bool isOnce_;

		/**
		*  Maximum change of TEC temperature per iteration [Kelvin]
		*/
		double deltaTECmax_;

		/**
		* Maximum deviation of line postion [GHz] where the Linelock will start.
		* If deviation is larger than this Linelock will not start
		*/
		double safetyGHzThreshold_;

		/**
		* Maximum deviation of line postion [GHz] before Linelock will start
		*/
		double startGHzThreshold_;

		/**
		* Maximum deviation of line positions [GHz] before Linelock will stop
		*/
		double stopGHzThreshold_;

		/**
		* Number of succesfull (|deltaGHz| < stopGHzThreshold) iteration before stopping linelock
		*/
		int iterations2StopLineLock_; //one iteration is OK

		/**
		* HW related coefficent. Depends on the laser (can be positive or negative)
		*/
		double kelvinPerGHz_;

		/**
		*  magic number from Roy's code. We just do not want to approach final value too quick
		*/
		double scaleAdjustmentFactorTEC_;

		/**
		* Holds desired line positions. THis is set during initialization of linelock
		*/
		std::vector<double> desiredPeakPositions_;

		/**
		* Holds minimal values for Areas of the peaks
		* if peak Area is smaller than threshold this peak position is ignored
		*/
		std::vector<double> peakAreaThresholds_;

		/**
		* Variable to hold state of the linelock algorithm (true = started the linelock procedure, false = not started)
		*/
		bool isStarted_;

		/**
		* Variable to hold the number of succesful reads of the linepositions before stopping Linelock algorithm
		*/
		int counterStopLineLock_;

		/**
		* Variable to hold the number of seconds before next update. It is useful to limit TEC update rate to slower than > 1 Hz
		*/
		double updateIntervalSec_;

		/**
		* Tracking time since last call to UpdateLineLock() method
		*/

		std::chrono::time_point<std::chrono::system_clock> lastUpdateTime_;
		std::chrono::time_point<std::chrono::system_clock> currentTime_;

		/**
		 * Variable to hold the calculated drift in GHz
		 * i.e. difference between line centers
		 */
		double deltaGHz_;

		/**
		* Variable to hold the calculated delta for TEC in K
		* i.e. how much the TEC has to change
		*/
		double deltaTEC_;

		/**
		* Variable to hold state of thewhole linelock algorithm
		*/
		bool isEnabled_;

		/**
		* Method to perform calculation and update deltaGHz from currentPeakPositions and desiredPeakPositions
		*/
		bool calculateDeltaGHz(const LinelockData & linelockData);

		/**
		* Method to perform calculation and update deltaTEC from deltaGHz and KperGHz
		*/
		void calculateDeltaTEC();

		/**
		* Variable to hold Max value of goodness of fit
		* Linelock will not recalculate deltaTEC if currentGOF > goodnessOfFitMax_
		*/
		double goodnessOfFitMax_;

		/**
		* Variable to hold OutOfBounds event for linelock
		*/
		bool isLinelockOOB_;

};

#endif /* __LINELOCK_H */
