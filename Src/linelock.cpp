/*--------------------- Copyright (c) ABB inc, 2015 --------------------------
 *                                 Source code
 * This software is the property of ABB inc and shall be considered and treated
 * as proprietary information.  Refer to the "Source Code License Agreement"
 *-----------------------------------------------------------------------------
 */

#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <cmath>

#include "linelock.h"


/**
* @brief Default constructor of LineLock algorithm
*/
LineLock::LineLock()
{
	deltaGHz_ = 0.0;
	deltaTEC_ = 0.0;
	counterStopLineLock_ = 0;
	lastUpdateTime_ = std::chrono::system_clock::now();
	currentTime_ = lastUpdateTime_;
	isStarted_ = false;
	isOnce_ = false;
	isEnabled_ = false;
	goodnessOfFitMax_ = 10;
	isLinelockOOB_ = false;
};

/**
* @brief Default destructor of LineLock algorithm
*/
LineLock::~LineLock()
{
	desiredPeakPositions_.clear();
}

void LineLock::initLineLock(const LinelockINI & linelockINI)
{
	deltaTECmax_ = linelockINI.deltaTECmax();
	safetyGHzThreshold_ = linelockINI.safetyGHzThreshold();
	startGHzThreshold_ = linelockINI.startGHzThreshold();
	stopGHzThreshold_ = linelockINI.stopGHzThreshold();
	iterations2StopLineLock_ = linelockINI.iterations2StopLineLock();
	kelvinPerGHz_ = linelockINI.kelvinPerGHz();
	updateIntervalSec_ = linelockINI.updateIntervalSec();
	scaleAdjustmentFactorTEC_ = linelockINI.scaleAdjustmentFactorTEC();
	goodnessOfFitMax_ = linelockINI.goodnessOfFitMax();
	if (!(linelockINI.peakAreaThresholds()).empty())
	{
		peakAreaThresholds_.clear();
		peakAreaThresholds_.reserve(linelockINI.peakAreaThresholds().size());
		peakAreaThresholds_ = std::vector<double>(linelockINI.peakAreaThresholds());
	}
}

void LineLock::setInitialPeakPositions(std::vector<double> initPeakPositions)
{
	if (!initPeakPositions.empty())
	{
		desiredPeakPositions_.clear();
		desiredPeakPositions_.resize(initPeakPositions.size());
		desiredPeakPositions_ = std::vector<double>(initPeakPositions);
	}
}


void LineLock::updateLineLock(const LinelockData & linelockData)
{
	deltaTEC_ = 0;
	if (isEnabled_
		&& isOnce_
		&& !desiredPeakPositions_.empty()
		&& !linelockData.getCurrentPeakPositions().empty()
		&& linelockData.getCurrentPeakPositions().size() == desiredPeakPositions_.size()
		&& linelockData.getGoodnessOfFit() < goodnessOfFitMax_)
	{
		currentTime_ = std::chrono::system_clock::now();
		std::chrono::duration<double> wctduration = currentTime_ - lastUpdateTime_;
		if (wctduration.count() > updateIntervalSec_)
		{
			lastUpdateTime_ = currentTime_;
			/*
			//for testing
			//std::vector<double> test(desiredPeakPositions_.size());
			//for (int j = 0; j < desiredPeakPositions_.size(); j++)
			//{
			//	test[j] = desiredPeakPositions_[j] + (rand() / (double)RAND_MAX) * 2 * 25 - 25;
			//}
			//bool isValid = calculateDeltaGHz(test);
			*/
			bool isValid = calculateDeltaGHz(linelockData);
			if (isValid)
			{
				if (fabs(deltaGHz_) > safetyGHzThreshold_)
				{
					isLinelockOOB_ = true;
				}
				else
				{
					isLinelockOOB_ = false;
				}
				if ((!isStarted_) && (fabs(deltaGHz_) > startGHzThreshold_) && (!isLinelockOOB_))
				{
					isStarted_ = true;
				}
				if (isStarted_)
				{
					if ((fabs(deltaGHz_) > stopGHzThreshold_) && (!isLinelockOOB_))
					{
						calculateDeltaTEC();
						counterStopLineLock_ = 0;
						//ApplyDeltaTEC(); One can do it here or outside this routine.
					}
					else
					{
						deltaTEC_ = 0;
						counterStopLineLock_ += 1;
						if (counterStopLineLock_ >= iterations2StopLineLock_) //wait for iterations to make sure valuies are stable
						{
							//when done adjusting TEC set isStarted to false
							isStarted_ = false;
							counterStopLineLock_ = 0;
						}
					}
				}
			}
		}
	}
	// For debug
	// std::cout << "Started = " << isStarted << std::endl;
	// std::cout << "dGHz = " << deltaGHz << " GHz" << std::endl;
	// std::cout << "dTEC = " << deltaTEC << " K" << std::endl;
	// std::cout << "counterStopLineLock = " << counterStopLineLock << std::endl;

	isOnce_ = true;
	// TODO add error handling
}

/**
* This method returns the computed delta for TEC in [K]
*
* @return deltaTEC value
*/
double LineLock::getDeltaTEC()
{
	return this->deltaTEC_;
}

double LineLock::getDeltaGHz()
{
	return this->deltaGHz_;
}

/**
* This method returns the Enable state of the linelock
*
* @return isLineLockEnabled value
*/
bool LineLock::getEnableState()
{
	return isEnabled_;
}

bool LineLock::getIsStarted()
{
	return isStarted_;
}

/**
* This method sets the Enable state of the linelock
*
* @set isLineLockEnabled value
*/
void LineLock::setEnableState(bool newEnableState)
{
	isEnabled_ = newEnableState;
}

bool LineLock::calculateDeltaGHz(const LinelockData & linelockData)
{
	// TODO : need validation for calculations here.
	// e.g. 1) if Goodness of fit is bad - do not calculate and return false

	// Create vector of differences in GHz
	std::vector<double> diffGHz;
	diffGHz.reserve(desiredPeakPositions_.size());
	for (std::size_t ii = 0; ii<desiredPeakPositions_.size(); ii++)
	{
		if (linelockData.getCurrentPeakAreas()[ii] >= peakAreaThresholds_[ii])
		{
			diffGHz.push_back(linelockData.getCurrentPeakPositions()[ii] - desiredPeakPositions_[ii]);
		}
	}
	// Use simple (Max+Min)/2 deviation of all lines
	if (!diffGHz.empty())
	{
		deltaGHz_ = (*std::min_element(diffGHz.begin(), diffGHz.end()) + *std::max_element(diffGHz.begin(), diffGHz.end())) / 2.0;
		return true;
	}
	else
	{
		deltaGHz_ = 0;
		return false;
	}
}

void LineLock::calculateDeltaTEC()
{
	double prelimDeltaTEC = kelvinPerGHz_ * deltaGHz_ / scaleAdjustmentFactorTEC_;
	if (fabs(prelimDeltaTEC) > deltaTECmax_)
	{
		deltaTEC_ = deltaTECmax_;
	}
	else
	{
		deltaTEC_ = prelimDeltaTEC;
	}
}



