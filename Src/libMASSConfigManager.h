//
//  libMASSConfigManager.h
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Dmitry Skvortsov on 10/12/2016.
//
//

#ifndef libMASSConfigManager_h
#define libMASSConfigManager_h

#include <string>
#include <vector>

#include "ini_structs.h"
#include "spectra/peaks_ini.h"
#include "config_ini.h"
#include "spectra/calibration_ini.h"
#include "utils/physicalConsts_ini.h"
#include "spc_interface.h"

class SPCImplementation;

class LibMASSConfigManager : public SPCManager
{
public:
        LibMASSConfigManager(const std::string & iniFilePath, const std::string & analyzerType);
    
    virtual ~LibMASSConfigManager();
    
    // Sets peak self_ppm in strPeaksINI_
    // Sets strConfigINI_.cavityLength_
	virtual void initStack(const StructStackConfig & stack); // "stack" is cavity length, gases & initial concs
    
    // Appends flange peaks to strPeaksINI_.peaks_; sets slaveMasters_ too
	virtual void initFlanges(const std::vector<StructStackConfig> & flanges); // flanges is idx, length, gases & initial concs
    
    // Sets etalonCoefficients_ and shift_ in strPeaksINI_
	virtual void initLaserRampArguments(const StructLaserRampArguments & strEEPROM); // EEProm is etalon coeffs and shift
    
    // Appends a bunch of slaving to strPeaksINI_
	virtual void setMode(int mode);
    
	virtual const StructPeaksIni & getPeaksINI() const { return strPeaksINI_; };
	virtual const StructCalibrationIni & getCalibrationINI() const { return strCalibrationINI_; };
	virtual const StructConfigIni & getConfigINI() const { return strConfigINI_; };
	virtual const StructLinelockIni & getLinelockINI() const { return strLinelockINI_; };
	virtual const std::vector<StructIsotopeConsts> & getPhysConstsINI() const { return physConstsINI_; };
	virtual const std::vector<StructError> & getErrors();
	virtual const std::string & getVersion() const { return iniVersion_; };

    virtual SPCInterface* getInterface();

private:
    std::string mConfDirectory; // ONLY for createInterface().  think about it.
    
    bool hasSPCInterface() const;
    void createInterface();
    void clearInterface();
    void clearInterfaceIfNeeded();
    
    SPCImplementation* mSPCInterface;
    
	void readCrossStackINI(const std::string & filepath);
	void createCalibrationINI();
	void readPhysConstsINI(const std::string & filepath);
	const std::string & getMoleculeName(const std::string & isoName) const;
	std::string analyzerType_;
	void checkForErrors();
    
    
	int numPeaks_;

	StructPeaksIni strPeaksINI_;
	StructCalibrationIni strCalibrationINI_;
	StructConfigIni strConfigINI_;
	StructLinelockIni strLinelockINI_;
	std::vector<StructIsotopeConsts> physConstsINI_;
	std::vector<StructError> Errors_;
	std::string iniVersion_;

	enum ErrorCodes
	{
		// Excessive Chi2
		badGoodnessOfFit = 9000,
		//Negative fit pressure width
		negativePressureWidth = 9001,
		//Negative fit area
		negativeArea = 9002,
		//Excessive etalon stretch
		badEtalonStretch = 9003,
		// Peak center OOB
		linelockCenterOOB = 9004,
		//Detector offset StDev too large
		badDetectorNoise = 9005
	};

};



#endif /* LibMASSConfigManager_h */
