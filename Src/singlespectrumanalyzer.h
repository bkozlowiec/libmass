//
//  SingleSpectrumAnalyzer.h
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 4/6/16.
//
//

#ifndef SingleSpectrumAnalyzer_hpp
#define SingleSpectrumAnalyzer_hpp

#include <map>
#include <vector>

#include "chamberconcentration.h"
#include "spectra/levmarpeakfit.h"
#include "spectra/spectrafitcontainer.h"
#include "utils/tools.h"
#include "utils/physicalConsts.h"
#include "linelock.h"
#include "utils/physicalConsts_ini.h"

class Peak;
class SpectraPeaksBase;
class PeaksINI;
class CalibrationINI;
class ConfigINI;
class ChamberConcentration;
class LinelockINI;
class CavityProperties;

class SingleSpectrumAnalyzer
{
public:
    SingleSpectrumAnalyzer(const PeaksINI & peaksINI,
        const CalibrationINI & calibrationINI,
        const ConfigINI & configINI,
		const LinelockINI & linelockINI);;
    ~SingleSpectrumAnalyzer();
    
    /**
     * peaksINI must be re-read before this call, yo
     */
    void fit(const PeaksINI & peaksINI,
        const ConfigINI & configINI,
        const PhysConstsINI & physConstsINI,
		const LinelockINI & linelockINI,
        const std::vector<double> & laserMinusOffset,
        const CavityProperties & mainCavity,
        const std::vector<CavityProperties> & flangeCavities);

    /**
     *
     */
    void fitWithFitFlanges(
        const PeaksINI & peaksINI,
        const ConfigINI & configINI,
        const PhysConstsINI & physConstsINI,
		const LinelockINI & linelockINI,
        const std::vector<double> & laserMinusOffset,
        const CavityProperties & mainCavity,
        const std::vector<CavityProperties> & flangeCavities);
    
    /**
     *
     */
    void fitWithFixedFlanges(
        const PeaksINI & peaksINI,
        const ConfigINI & configINI,
        const PhysConstsINI & physConstsINI,
		const LinelockINI & linelockINI,
        const std::vector<double> & laserMinusOffset,
        const CavityProperties & mainCavity,
        const std::vector<CavityProperties> & flangeCavities);
    
    const SpectraPeaksBase* getSpectrumFunction() const { return spectrumFunction_; }
    const NewSpectrumContainer & getFitContainer() const { return lastFitResults_; }
    const LevmarPeakFit & getSpectrumFitter() const { return spectrumFitter_; }
	double getLinelockDeltaTEC() const { return linelockDeltaTEC_; }
	bool getIsLinelockOOB() const { return isLinelockOOB_; }
	void setEnableLinelock(bool enabled);
	double getLaserPowerArbUnits(void) const { return power_at_end_fit_window; }
	bool isAreaNegative() const { return isAreaNegative_; }
	bool isPWNegative() const { return isPWNegative_; }

    /**
     * Calculate engineering values (concentrations to report) and store them
     * in the given map.  Will overwrite existing EVs with the same key.
     */
    void engineeringValues(std::map<std::string, double> & outEVs,
    	const PhysConstsINI & physConstsINI,
        const CavityProperties & mainCavity,
        bool doCorrectTau) const;
    
    /**
     * Get the peak parameters from the last spectral fit.
     * Each peak has four parameters: center, area, pressure width, and 
     * doppler width, in order.  The parameter vector puts these in order,
     *
     * params = {C, A, PW, DW, C, A, PW, DW, ... }
     */
    void getPeakParameters(std::vector<double> & outParams) const;
    
    /**
     * Get the baseline parameters from the last spectral fit.
     % Baseline parameters are a 4-element vector.
     */
    void getBaselineParameters(std::vector<double> & outParams) const;

	/**
	* Set standard deviation for goodness of the fit calculations
	*/
	void setStandardDeviation(double standardDeviation);

	/**
	* Get standard deviation for error condition calculations
	*/
	double getStandardDeviation() const { return standardDeviation_; }

	/**
	* Get goodness of the fit fit.
	*/
	double getGoodnessOfFit() const { return goodnessOfFit_; };

	/**
	* Returns etalon stretch factor for current fit.
	*/
	double getEtalonStretchFactor() const { return etalonStretch_; };

	/**
	* Returns etalon stretch factor for current fit.
	*/
	double getPeakZeroShiftGHz() const { return peakZeroSHiftGHz_; };

	/**
	* Returns TRUE is Fitter will calculate Initial Guess spectrum.
	*/
	bool getDoCalculateInitialY() const { return doCalculateInitialY_; };

	/**
	* Returns TRUE is Fitter will calculate Estimated spectrum.
	*/
	bool getDoCalculateEstimatedY() const { return doCalculateEstimatedY_; };

	/**
	* Returns TRUE is Fitter will calculate Fitted spectrum.
	*/
	bool getDoCalculateFitY() const { return doCalculateFitY_; };

	/**
	* Enables calculation of the Initial Guess spectrum.
	*/
	void setDoCalculateInitialY(bool enable)  { doCalculateInitialY_ = enable; };

	/**
	* Enables calculation of the  Estimated spectrum.
	*/
	void setDoCalculateEstimatedY(bool enable) { doCalculateEstimatedY_ = enable; };

	/**
	* Enables calculation of the  Fitted spectrum.
	*/
	void setDoCalculateFitY(bool enable) { doCalculateFitY_ = enable; };
private:

    void doFit(
        const PeaksINI & peaksINI,
        const ConfigINI & configINI,
		const LinelockINI & linelockINI,
        SpectraPeaksBase * peaksFunction,
        const std::vector<double> & transmission,
        double pressure_torr);
    
    void postFit(
        const PeaksINI & peaksINI,
        const ConfigINI & configINI,
        const PhysConstsINI & physConstsINI,
		const LinelockINI & linelockINI,
        SpectraPeaksBase * peaksFunction,
        const CavityProperties & mainCavity,
        const std::vector<CavityProperties> & flangeCavities);

	// for Linelock
	std::vector<double> extractPeakPositionsForLinelock(const LinelockINI & linelockINI, const SpectraPeaksBase * peaksFunction) const;
	LinelockData extractDataForLinelock(const LinelockINI & linelockINI, const SpectraPeaksBase * peaksFunction) const;


    void initChambers(const ConfigINI & configINI, const PeaksINI & peaksINI);
    void initChamber(ChamberConcentration & chamber,
        const StructCavityGasIni & gasProperties,
        const std::map<std::string, bool> & isTrusted);
    
    /**
     * Make sure all the chambers (main and flanges) have been allocated.
     * Make sure they have values for all isotopes and molecules mentioned in
     * the INI.  Make sure they have a total concentration defined for water
     * in case no water peaks are explicitly given.
     */
    void updateChambers(const PeaksINI & peaksINI,
    		const PhysConstsINI & physConstsINI);
    
    /**
     * Check that all flanges referred to in peaksINI are allocated,
     * and that isotopes corresponding to each peak are measured in the
     * chamber concentration maps.  Check also that water is present in the
     * total concentration maps so it can be used for water broadening even
     * if water peaks are not measured.
     */
    void checkChamberInitialization(const PeaksINI & peaksINI) const;
    
    /**
     * Return instrument pressure (torr) if it is valid (> 0.0), else return
     * the default value.
     */
    double choosePressure(double instrument_torr, double default_torr) const;
    
    /**
     * Return instrument temperature (celsius) if it is valid (> -273.15), else
     * return the default value.
     */
    double chooseTemperature(double instrument_C, double default_C) const;
    
private:
    std::vector<const PeaksINI::PeakConstants*> getAllPeaks(
        const PeaksINI & peaksINI) const;
    std::vector<const PeaksINI::PeakConstants*> getFlangePeaks(
        const PeaksINI & peaksINI) const;
    std::vector<const PeaksINI::PeakConstants*> getMainPeaks(
        const PeaksINI & peaksINI) const;
    
    /**
     * Extract the baseline parameters and flange peak parameters from a vector
     * of all fitted parameters.
     */
    std::vector<double> getBaselineAndFlangeParams(
        const std::vector<const PeaksINI::PeakConstants*> & peakConsts,
        const std::vector<double> & params) const;
    
    double estimateIsotopeConcentrationForArea(const PeaksINI::PeakConstants & peak) const;
    double estimateIsotopeConcentrationForPW(const PeaksINI::PeakConstants & peak) const;
    double estimateEffectiveAirConcentrationForPW(const PeaksINI::PeakConstants & peak) const;
    
    void estimatePeakAreas(const std::vector<const PeaksINI::PeakConstants*> & peaks,
    	const PhysConstsINI & physConstsINI,
        const CavityProperties & mainCavity,
        const std::vector<CavityProperties> & flangeCavities,
    	std::vector<double> & outPeakAreas) const;
    
    void estimatePeakDopplerWidths(const std::vector<const PeaksINI::PeakConstants*> & peaks,
    	const PhysConstsINI & physConstsINI,
        const CavityProperties & mainCavity,
        const std::vector<CavityProperties> & flangeCavities,
    	std::vector<double> & outPeakDopplerWidths) const;
    
    void estimatePeakPressureWidths(const std::vector<const PeaksINI::PeakConstants*> & peaks,
    	const PhysConstsINI & physConstsINI,
        const CavityProperties & mainCavity,
        const std::vector<CavityProperties> & flangeCavities,
        std::vector<double> & outPeakPressureWidths) const;
    
    void estimatePeakCenters(const std::vector<const PeaksINI::PeakConstants*> & peaks,
        double originGHz,
        const CavityProperties & mainCavity,
        const std::vector<CavityProperties> & flangeCavities,
        std::vector<double> & outPeakCenters) const;
    
    /**
     * Return the concentration record for the chamber responsible for the peak.
     * Const version.
     */
    const ChamberConcentration* getChamber(const PeaksINI::PeakConstants & peak) const;
    
    /**
     * Return the concentration record for the chamber responsible for the peak.
     * Non-const version.
     */
    ChamberConcentration* getChamber(const PeaksINI::PeakConstants & peak);
    
	void CheckForNegativeAreaAndPW(const SpectraPeaksBase * peaksFunction);

private:
    SpectraPeaksBase* spectrumFunction_;
    SpectraPeaksBase* mainPeaksFunction_;
    SpectraPeaksBase* flangeFunction_;
    NewSpectrumContainer lastFitResults_;
    LevmarPeakFit spectrumFitter_;
    
    double previousSpectralShiftGHz_;
    
    ChamberConcentration mainConcentration_; // TODO: must re-init when INI changes (needs gConc)
    std::vector<ChamberConcentration*> flangeConcentration_;
    double reduced_chisq_;
	double standardDeviation_;
	double power_at_end_fit_window;
	double goodnessOfFit_;
	// For Linelock
	LineLock *lineLock_; // QUESTION: why a pointer? PCH170215
	double linelockDeltaTEC_; // QUESTION: TEC is what? PCH170215
	double etalonStretch_;
	double peakZeroSHiftGHz_;
	bool isLinelockOOB_;
	bool isAreaNegative_;
	bool isPWNegative_;

	bool doCalculateInitialY_;
	bool doCalculateEstimatedY_;
	bool doCalculateFitY_;
};







#endif /* SingleSpectrumAnalyzer_hpp */
