/*---------------------- Copyright (c) ABB inc, 2015 ------------------------
 *                                 Source code
 * This software is the property of ABB and should be considered and 
 * treated as proprietary information.  Refer to the "Source Code License 
 * Agreement"
 *---------------------------------------------------------------------------
 */


#ifndef SPCIMPL_H_
#define SPCIMPL_H_

#include <vector>
#include <map>
#include <string>
#include "spc_interface.h"
#include "spc_implementation_data.h"
#include "ringdown/ringdown.h"
#include "utils/physicalConsts.h"
#include "spectra/spectrapeaks.h"
#include "spectra/levmarpeakfit.h"
#include "spectra/spectrafitcontainer.h"
#include "rate/rate.h"
#include "spectra/peaks_ini.h"
#include "config_ini.h"
#include "singlespectrumanalyzer.h"
#include "ini_structs.h"
#include "libMASSConfigManager.h"
#include "utils/physicalConsts_ini.h"

/**
 * @class SPCImplementation
 * @brief Implement the SPCInterface.
 *
 * Header file for the SPC implementation.  The abstract base class SPCInterface
 * shields client code from details of the member data in this class.
 *
 * See SPCInterface for description of interface methods.
 */
class SPCImplementation : public SPCInterface
{
public:
    // TEMPORARY PCH170222
    SPCImplementation();
    
    virtual ~SPCImplementation ();
    
    int initSPC(const StructConfigIni & configIni,
        const std::vector<StructIsotopeConsts> & physConstsIni,
        const StructPeaksIni & peaksIni,
        const StructCalibrationIni & calibrationIni,
        const StructLinelockIni & linelockIni);

protected:
    //
    // All protected methods are declared in SPCInterface
    //

    // call #2 (after createInterface())  PCH151006
    virtual int initSPC (const std::string & iniFilePath, unsigned char numLasers);
    

    // calls #3 (after initSPC) PCH151006
    unsigned int getLaserRampStartIdx() const;
    unsigned int getLaserRampLength() const;
    unsigned int getLaserOFFStartIdx() const;
    unsigned int getLaserOFFLength() const;
    unsigned int getRDFitCnt() const;
    
    // call #4 (getLaser*() can be done prior) PCH151006
    int computeSpectrum (double instrumentTemperature_C,
                         double instrumentPressure_torr,
                         const std::vector<std::vector<double> > & laserRampData,
                         const std::vector<std::vector<double> > & laserRingdownData,
                         const std::vector<int> & p_rdCnt);

	// call #4 (getLaser*() can be done prior) PCH151006
	int computeSpectrum(double instrumentTemperature_C,
                        double instrumentPressure_torr,
                        const std::vector<double> & flangeTemperature_C,
                        const std::vector<double> & flangePressure_torr,
						const std::vector<std::vector<double> > & laserRampData,
						const std::vector<std::vector<double> > & laserRingdownData,
						const std::vector<int> & p_rdCnt);

    // calls #5 (after computeSpectrum()) PCH151006
    //computed vectors to return
    
    std::vector<double> getRawSignal(unsigned int laserIdx) const;
    std::vector<double> getBaselineFit(unsigned int laserIdx) const;
    std::vector<double> getRingdownFit(unsigned int laserIdx) const;

    /**
     * Return 0, 1, 2, ..., spectrum size + ringdown size
     */
    std::vector<unsigned int> getSamplePts(unsigned int laserIdx) const;
    unsigned int getBaselineFitStart(unsigned int laserIdx) const;
    unsigned int getRingdownFitStart(unsigned int laserIdx) const;
    std::vector<double> getMeasuredData(unsigned int laserIdx) const;
    std::vector<double> getPeakFit(unsigned int laserIdx) const;
	std::vector<double> getFitResidual(unsigned int laserIdx) const;
	std::vector<double> getInitialGuessFit(unsigned int laserIdx) const;
	std::vector<double> getEstimatedFit(unsigned int laserIdx) const;

    std::vector<double> getMeasuredDataWithBL(unsigned int laserIdx) const;
	std::vector<double> getInitialGuessFitWithBL(unsigned int laserIdx) const;
	std::vector<double> getEstimatedFitWithBL(unsigned int laserIdx) const;
    std::vector<double> getPeakFitWithBL(unsigned int laserIdx) const;

    std::vector<double> getGHzPts(unsigned int laserIdx) const;
    std::map<std::string, double> getEVs() const;
    std::vector<double> getBaselineParameters(unsigned int laserIdx) const;
    std::vector<double> getPeakParameters(unsigned int laserIdx) const;
    double getGoodnessOfFit(unsigned int laserIdx) const;
	double getLaserPowerArbUnits(unsigned int laserIdx) const;
	double getLinelockDeltaTEC(unsigned int laserIdx) const;
	void setEnableLinelock(unsigned int laserId, bool Enabled);
	double getEtalonStretchFactor(unsigned int laserIdx) const;
	double getPeakZeroShiftGHz(unsigned int laserIdx) const;
	bool getIsLinelockOOB(unsigned int laserIdx) const;
	bool getIsAreaNegative(unsigned int laserIdx) const;
	bool getIsPWNegative(unsigned int laserIdx) const;
	double getDetectorNoise(unsigned int laserIdx) const;
	bool getDoCalculateInitialY(unsigned int laserIdx) const;
	bool getDoCalculateEstimatedY(unsigned int laserIdx) const;
	bool getDoCalculateFitY(unsigned int laserIdx) const;
	void setDoCalculateInitialY(unsigned int laserIdx, bool enable);
	void setDoCalculateEstimatedY(unsigned int laserIdx, bool enable);
	void setDoCalculateFitY(unsigned int laserIdx, bool enable);
private:
	// Configuration manager for new crossstack.ini
//	LibMASSConfigManager* spcManager_;

    /**
     * Do all the calculations
     */
    void performFit(const std::vector<std::vector<double> > & laserMinusOffset,
        const std::vector<double> & tau,
        double instrumentTemperature_C,
        double instrumentPressure_torr,
        const std::vector<double> & flangeT_C,
        const std::vector<double> & flangeP_torr);
    
private:

    // Maximum number of lasers allowed by this code
    static const int maxLasers = 2;

    // Laser ramp settings
    unsigned int numLasers_;

    std::vector<SingleSpectrumAnalyzer*> analyzers_;
    
    // Ringdown and settings
    std::vector<RingDown*> ringDown_;
    
    ConfigINI* configINI_;
    PhysConstsINI* physConstsINI_;
    std::vector<PeaksINI*> peaksINIs_; // one per laser
    std::vector<CalibrationINI*> calibrationINIs_; // one per laser
	std::vector<LinelockINI*> linelockINIs_; // one per laser
    
    std::vector<double> laserOffset_;
    std::vector<std::vector<double> > laserRampMinusOffset_;
    std::vector<std::vector<double> > laserRingdownMinusOffset_;
    
    // EVs (Engineering Values)
    // Really might change this eventually.
    std::map<std::string, double> engineeringValues_;

    //static std::mutex mut;
};



#endif /* SPCIMPL_H_ */
