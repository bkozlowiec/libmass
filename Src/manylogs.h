//
//  manylogs.hpp
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 4/26/16.
//
//

#ifndef manylogs_h
#define manylogs_h

#include <ostream>
#include <string>

namespace ManyLogs
{
    std::ostream & log(const std::string & fileName);
};


#endif /* manylogs_hpp */
