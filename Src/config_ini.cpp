//
//  config_ini.cpp
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 2/17/16.
//
//
#include <stdexcept>
#include "config_ini.h"

#include <fstream>

#include "utils/tools.h"

ConfigINI::ConfigINI()
{
}

ConfigINI::ConfigINI(const std::string & fileName) :
    fileName_(fileName),
    dacRate_(1250000),
    sparseFactor_(8),
    rdClip_(2,-1),  // need to read from dm7520.INI [legacy comment]
    laserRampStart_(0),
    laserRampLength_(0),
    laserOffStart_(0),
    laserOffLength_(0),
    laserPathAlgo_(RINGDOWN_LASER_PATH),
    numRDToFit_(1),
    configRate_(),
    cavityLength_(0.0),
    flangeLength_(),
    defaultTemperature_C_(-274.0),
    defaultPressure_torr_(-1.0)
{
    read();
}

ConfigINI::ConfigINI(const StructConfigIni & strConfIni)
{
	updateConfigINI(strConfIni);
}

ConfigINI::~ConfigINI()
{
}

void ConfigINI::updateConfigINI(const StructConfigIni & strConfIni)
{
	laserRampStart_ = strConfIni.laserRampStart;
	laserRampLength_ = strConfIni.laserRampLength;
	laserOffStart_ = strConfIni.laserOffStart;
	laserOffLength_ = strConfIni.laserOffLength;
	laserPathAlgo_ = strConfIni.laserPathAlgo;
	Rate::RateID id = Tools::str2RateID("1s");
	configRate_ = Rate(id, 300, 1, strConfIni.fitIterations, strConfIni.fitIterations, 1, 1);

	dacRate_ = 4000000;
	sparseFactor_ = 1;
	rdClip_ = { 24, -1 };
    
    
	cavityLength_ = strConfIni.cavityLength_m;
    flangeLength_ = strConfIni.flangeLength_m;
    defaultTemperature_C_ = strConfIni.celsius;
    defaultPressure_torr_ = strConfIni.torr;

    stackGasProperties_ = strConfIni.stackGasProperties;
    flangeGasProperties_ = strConfIni.flangeGasProperties;
    trustFitConcentration_ = strConfIni.trustFitConcentration;
}


void ConfigINI::updateConfigINI()
{
	std::ifstream spcConfigFile;
	spcConfigFile.open (fileName_.c_str());
	std::string rlstr;	//to read a line from the file stream

	if (spcConfigFile.is_open ())
	{
		while (getline (spcConfigFile, rlstr))
		{
			if ((rlstr.length () > 1) && (rlstr.at (0) != '#'))
			{
				std::stringstream ss;
				ss << rlstr;
				std::string key; //to store key from the line
				std::string value;

				ss >> std::uppercase >> key;
				if (key.compare ("LASER_RAMP_START") == 0)
				{
					ss >> this->laserRampStart_;
				}
				else if (key.compare ("LASER_RAMP_LENGTH") == 0)
				{
					ss >> this->laserRampLength_;
				}
				else if (key.compare ("LASER_OFF_START") == 0)
				{
					ss >> this->laserOffStart_;
				}
				else if (key.compare ("LASER_OFF_LENGTH") == 0)
				{
					ss >> this->laserOffLength_;
				}
				else if (key.compare ("CAVITY_TEMPERATURE") == 0)
				{
                    throw std::runtime_error("CAVITY_TEMPERATURE was in spc_config for alpha and should be removed.");
					//ss >> this->temperature_;
				}
				else if (key.compare ("CAVITY_PRESSURE") == 0)
				{
                    throw std::runtime_error("CAVITY_PRESSURE was in spc_config for alpha and should be removed.");
					//ss >> this->pressure_;
				}
                else if (key.compare ("CAVITY_LENGTH") == 0)
                {
                    ss >> this->cavityLength_;
                }
			}
		}
	}
}

void ConfigINI::read()
{
	std::ifstream spcConfigFile;
	spcConfigFile.open (fileName_.c_str());
	std::string rlstr;	//to read a line from the file stream
    
	if (spcConfigFile.is_open ())
	{
		while (getline (spcConfigFile, rlstr))
		{
			if ((rlstr.length () > 1) && (rlstr.at (0) != '#'))
			{
				std::stringstream ss;
				ss << rlstr;
				std::string key; //to store key from the line

				ss >> std::uppercase >> key;
				if (key.compare ("LASER_RAMP_START") == 0)
				{
					ss >> this->laserRampStart_;
				}
				else if (key.compare ("LASER_RAMP_LENGTH") == 0)
				{
					ss >> this->laserRampLength_;
				}
				else if (key.compare ("LASER_OFF_START") == 0)
				{
					ss >> this->laserOffStart_;
				}
				else if (key.compare ("LASER_OFF_LENGTH") == 0)
				{
					ss >> this->laserOffLength_;
				}
                else if (key.compare ("CAVITY_LENGTH") == 0)
                {
                    ss >> this->cavityLength_;
                }
				else if (key.compare ("LASER_PATH") == 0)
				{
					ss >> this->laserPathAlgo_;
				}
				else if (key.compare ("DAC_RATE") == 0)
				{
					ss >> this->dacRate_;
				}
				else if (key.compare ("NUM_RD_TO_FIT") == 0)
				{
					ss >> this->numRDToFit_;
				}
				else if (key.compare ("RD0_CLIP") == 0)
				{
					ss >> this->rdClip_[0];
				}
				else if (key.compare ("RD1_CLIP") == 0)
				{
					ss >> this->rdClip_[1];
				}
				else if (key.compare ("SPARSE") == 0)
				{
					ss >> this->sparseFactor_;
				}
				else if (key.compare ("SLOW") == 0)
				{
                    int sweeps, fits, estIters, iters, stride, rdFits;
                    std::string str, curr, riStr, plotOnDemand, icon;
					ss >> curr >> riStr >> sweeps >> fits >> estIters
					   >> iters >> stride >> rdFits;
                    
					Rate::RateID id = Tools::str2RateID(riStr);
					configRate_ = Rate (id, sweeps, fits, estIters, iters, stride, rdFits);
				}
			}
		}
		spcConfigFile.close();
	}
}
