//
//  extramath.h
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 3/19/17.
//
//

#ifndef extra_math_h
#define extra_math_h

#include "nrutil.h" // templated matrix and vector classes: NRMat, NRVec


// why be ambitious.  not my fault nobody did this already.
//template<class T>
//NRVec<T> operator+(const NRVec<T> & lhs, const NRVec<T> & rhs);
//
//template<class T>
//NRVec<T> operator-(const NRVec<T> & lhs, const NRVec<T> & rhs);
//
//template<class T>
//NRVec<T> operator*(const NRVec<T> & lhs, const NRVec<T> & rhs);
//
//template<class T>
//NRVec<T> operator/(const NRVec<T> & lhs, const NRVec<T> & rhs);


template<class T>
NRVec<T> operator*(const NRMat<T> & lhs, const NRVec<T> & rhs);



#include "extramath-inl.h"

#endif /* extra_math_h */
