#ifndef _NRR_H_
#define _NRR_H_
#include <fstream>
#include <complex>
#include <vector>
#include "nrutil.h"
#include "nrtypes.h"
//#include "spectrapeaks.h"

class SpectraPeaksBase;

void mrqcof(Vec_I_DP &x, Vec_I_DP &y,  Vec_I_DP &a,
            Vec_I_BOOL &ia, Mat_O_DP &alpha, Vec_O_DP &beta, DP &chisq, int stride,
            int startPt, SpectraPeaksBase& spectra);
void mrqmin(Vec_I_DP &x, Vec_I_DP &y, Vec_IO_DP &a,
            Vec_I_BOOL &ia, Mat_O_DP &covar, Mat_O_DP &alpha, DP &chisq,
            DP &alamda, int stride, int startPt, SpectraPeaksBase& spectra);

void covsrt(Mat_IO_DP &covar, Vec_I_BOOL &ia, const int mfit);
void gaussj(Mat_IO_DP &a, Mat_IO_DP &b);

void fitexy(Vec_I_DP &x, Vec_I_DP &y, Vec_I_DP &sigx, Vec_I_DP &sigy,
            DP &a, DP &b, DP &siga, DP &sigb, DP &chi2, DP &q);
void avevar(Vec_I_DP &data, DP &ave, DP &var);
void fit(Vec_I_DP &x, Vec_I_DP &y, Vec_I_DP &sig, const bool mwt, DP &a,
         DP &b, DP &siga, DP &sigb, DP &chi2, DP &q);
DP gammq(const DP a, const DP x);
DP chixy(const DP bang);
void mnbrak(DP &ax, DP &bx, DP &cx, DP &fa, DP &fb, DP &fc,
            DP func(const DP));
void shft3(DP &a, DP &b, DP &c, const DP d);
DP brent(const DP ax, const DP bx, const DP cx, DP f(const DP),
         const DP tol, DP &xmin);
DP zbrent(DP func(const DP), const DP x1, const DP x2, const DP tol);
void gser(DP &gamser, const DP a, const DP x, DP &gln);
void gcf(DP &gammcf, const DP a, const DP x, DP &gln);
DP gammln(const DP xx);

// From the original NR C code
void mrqmin(Vec_I_DP &x, Vec_I_DP &y, Vec_I_DP &sig, Vec_IO_DP &a,
            Vec_I_BOOL &ia, Mat_O_DP &covar, Mat_O_DP &alpha, DP &chisq,
            void funcs(const DP, Vec_I_DP &, DP &, Vec_O_DP &), DP &alamda);
void mrqcof(Vec_I_DP &x, Vec_I_DP &y, Vec_I_DP &sig, Vec_I_DP &a,
            Vec_I_BOOL &ia, Mat_O_DP &alpha, Vec_O_DP &beta, DP &chisq,
            void funcs(const DP, Vec_I_DP &,DP &, Vec_O_DP &));
void fEtalon(const DP x, Vec_I_DP &a, DP &y, Vec_O_DP &dyda);
void fgauss(const DP x, Vec_I_DP &a, DP &y, Vec_O_DP &dyda);

void convlv(Vec_I_DP &data, Vec_I_DP &respns, const int isign,
    Vec_O_DP &ans);
void realft(Vec_IO_DP &data, const int isign);
void four1(Vec_IO_DP &data, const int isign);
void savgol(Vec_O_DP &c, const int np, const int nl, const int nr,
    const int ld, const int m);
void ludcmp(Mat_IO_DP &a, Vec_O_INT &indx, DP &d);
void lubksb(Mat_I_DP &a, Vec_I_INT &indx, Vec_IO_DP &b);

// Qt Version ----------------------------------------------------------------
void linearLeastSquaresFit(std::vector<double> &x, std::vector<double> &y,
         double &a, double &b, double &siga, double &sigb,
         double &chi2);
std::vector<double> savitzkyGolay(std::vector<double> data, int np, int nl, int nr, int ld, int m);

#endif
