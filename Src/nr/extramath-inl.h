//
//  extramath-inl.h
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 3/19/17.
//
//

#ifndef extra_math_inl_h
#define extra_math_inl_h

#include <stdexcept>

//template <class T>
//class NRVec {
//private:
//	int nn;	// size of array. upper index is nn-1
//	T *v;
//public:
//	NRVec();
//	explicit NRVec(int n);		// Zero-based array
//	NRVec(const T &a, int n);	//initialize to constant value
//	NRVec(const T *a, int n);	// Initialize to array
//	NRVec(const NRVec &rhs);	// Copy constructor
//	NRVec & operator=(const NRVec &rhs);	//assignment
//	NRVec & operator=(const T &a);	//assign a to every element
//	inline T & operator[](const int i);	//i'th element
//	inline const T & operator[](const int i) const;
//	inline int size() const;
//	~NRVec();
//};

//template <class T>
//class NRMat {
//private:
//	int nn;
//	int mm;
//	T **v;
//public:
//	NRMat();
//	NRMat(int n, int m);			// Zero-based array
//	NRMat(const T &a, int n, int m);	//Initialize to constant
//	NRMat(const T *a, int n, int m);	// Initialize to array
//	NRMat(const NRMat &rhs);		// Copy constructor
//	NRMat & operator=(const NRMat &rhs);	//assignment
//	NRMat & operator=(const T &a);		//assign a to every element
//	inline T* operator[](const int i);	//subscripting: pointer to row i
//	inline const T* operator[](const int i) const;
//	inline int nrows() const;
//	inline int ncols() const;
//	~NRMat();
//};

template<class T>
NRVec<T> operator*(const NRMat<T> & matrix, const NRVec<T> & vector)
{
    if (vector.size() != matrix.ncols())
    {
        throw std::runtime_error("Num rows of RHS does not match num cols of LHS");
    }
    
    NRVec<T> result(0.0, matrix.nrows());
    
    int numCols = matrix.ncols(); // not sure if compiler needs help or not
    int numRows = matrix.nrows();
    for (int cc = 0; cc < numCols; cc++)
    {
        for (int rr = 0; rr < numRows; rr++)
        {
            result[rr] += matrix[rr][cc] * vector[cc];
        }
    }
    
    return result;
}

#endif /* extra_math_inl_h */
