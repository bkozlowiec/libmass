#ifndef _NR_H_
#define _NR_H_
#include <fstream>
#include <complex>
#include "nrutil.h"
#include "nrtypes.h"
//#include "icosfit.h"

using namespace std;


namespace NR {

//void covsrt(Mat_IO_DP &covar, Vec_I_BOOL &ia, const int mfit);
//void fgauss(const DP x, Vec_I_DP &a, DP &y, Vec_O_DP &dyda,Vec_I_DP &Cs);
//void gaussj(Mat_IO_DP &a, Mat_IO_DP &b);

/**
 * Compute the matrix [\alpha] and vector \beta.
 * Calls the user function funcs(x, a, out_y, out_dyda).
 *
 * @param x data x-axis
 * @param y data y-axis
 * @param a parameters
 * @param ia flags indicating which parameters to fit
 * @param alpha linearized fitting matrix (proportional to the Hessian)
 * @param beta a vector proportional to the gradient
 * @param chisq value of the merit function
 * @param funcs user-provided function calculating y(x) and dydx(x)
 * @param Cs NOT SURE PCH151020
 */
/*
void mrqcof(Vec_I_DP &x, Vec_I_DP &y,  Vec_I_DP &a,
	Vec_I_BOOL &ia, Mat_O_DP &alpha, Vec_O_DP &beta, DP &chisq,
	void funcs(const DP, Vec_I_DP &,DP &, Vec_O_DP &,Vec_I_DP &),
	Vec_I_DP &Cs);
*/
/**
 * Perform one iteration of Marquardt's method.
 *
 * It is first called once with alamda < 0, which signals the routine to
 * initialize.  alamda is set on the first and all subsequent calls to the
 * suggested value of lambda (a fudge factor) for the next iteration.
 * a and chisq are always given back as the best parameter and merit function
 * values so far.  alpha and covar will be set to the curvature and covariance
 * matrices for the converged parameter values.  alpha, a and chisq should not
 * be modified between calls; alamda shouldn't be either except to set it to
 * zero for the final call.
 */
/*
void mrqmin(Vec_I_DP &x, Vec_I_DP &y, Vec_IO_DP &a,
	Vec_I_BOOL &ia, Mat_O_DP &covar, Mat_O_DP &alpha, DP &chisq,
	void funcs(const DP, Vec_I_DP &, DP &, Vec_O_DP &,Vec_I_DP &),
	DP &alamda,Vec_I_DP &Cs);
	*/
}

#endif /* _NR_H_ */
