#define NRANSI

#include <cmath>
#include <stdio.h>
#include <limits>
#include <atomic>
#include "nrr.h"
#include "../spectra/spectrapeaks.h"

#define PI 3.14159265

//////////////////////////////////////////////////////////////////////
//
//	The source codes below are the property of Numerical Recipes Inc.
//	Legally purchased and licensed through
//	Numerical Recipes Transaction # 1061016664_32540
//	Download # 841196491
//	by Kenneth Ricci on behalf of Los Gatos Research Inc
//	on August 15, 2003.
//
//	These codes, from the book Numerical Recipes in C
//	(Cambridge University Press), Copyright (C) 1987-2002
//	by Numerical Recipes Software, are used by permission.
//    Use of these codes other than as an integral part
//    of ICOS.exe requires an additional license from Numerical Recipes
//	Software.  Further distribution in any form is prohibited.
//
////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
void mrqmin(Vec_I_DP &x, Vec_I_DP &y, Vec_IO_DP &a,
        Vec_I_BOOL &ia, Mat_O_DP &covar, Mat_O_DP &alpha, DP &chisq, DP &alamda,
    int stride, int startPt, SpectraPeaksBase& spectra)
{
    // Nonlinear least-squares fit, Marquardt's method
    thread_local int mfit;
    thread_local DP ochisq;
    int j,k,l;
    int ma=a.size();
    /*static*/ Mat_DP *oneda_p;
    /*static*/ Vec_DP *atry_p,*beta_p,*da_p;
    if (alamda < 0.0)
    {
        mfit=0;
        for (j=0;j<ma;j++)
            if (ia[j]) mfit++;
        
        atry_p = new Vec_DP(ma);
        beta_p = new Vec_DP(ma);
        da_p = new Vec_DP(ma);
        oneda_p = new Mat_DP(mfit,1);
        alamda=0.001;
        
        mrqcof(x,y,a,ia,alpha,*beta_p,chisq,stride,startPt,spectra);
        ochisq = chisq;
        
        for (j=0;j<ma;j++)
            (*atry_p)[j]=a[j];
    }
    Mat_DP &oneda = *oneda_p;
    Vec_DP &atry = *atry_p, &beta = *beta_p, &da = *da_p;
    Mat_DP temp(mfit,mfit);
    for (j=0;j<mfit;j++) {
            for (k=0;k<mfit;k++) covar[j][k]=alpha[j][k];
            covar[j][j]=alpha[j][j]*(1.0+alamda);
            for (k=0;k<mfit;k++) temp[j][k]=covar[j][k];
            oneda[j][0]=beta[j];
    }
    gaussj(temp,oneda);
    for (j=0;j<mfit;j++) {
            for (k=0;k<mfit;k++) covar[j][k]=temp[j][k];
            da[j]=oneda[j][0];
    }
    if (alamda == 0.0) {
            covsrt(covar,ia,mfit);
            covsrt(alpha,ia,mfit);
            delete oneda_p; delete da_p;
            delete beta_p; delete atry_p;
            return;
    }
    for (j=0,l=0;l<ma;l++)
    {
        if (ia[l])
            atry[l]=a[l]+da[j++];
    }

    mrqcof(x,y,atry,ia,covar,da,chisq,stride,startPt,spectra);

    if (chisq < ochisq)
    {
        alamda *= 0.1;
        ochisq=chisq;
        for (j=0;j<mfit;j++)
        {
            for (k=0;k<mfit;k++)
                alpha[j][k]=covar[j][k];
            beta[j]=da[j];
        }
        for (l=0;l<ma;l++)
            a[l]=atry[l];
    }
    else
    {
        alamda *= 10.0;
        chisq=ochisq;
    }
}

template<class T>
std::ostream & operator<<(std::ostream & str, const std::vector<T> & v)
{
    str << "[";
    for (int nn = 0; nn < v.size()-1; nn++)
        str << v[nn] << ", ";
    str << v[v.size()-1] << "]";
    return str;
}

//////////////////////////////////////////////////////////////////////
void mrqcof(Vec_I_DP &x, Vec_I_DP &y, Vec_I_DP &a,
        Vec_I_BOOL &ia, Mat_O_DP &alpha, Vec_O_DP &beta, DP &chisq, int stride,
    int startPt, SpectraPeaksBase& spectra)
{
    int i,j,k,l,m,mfit=0;
    DP ymod,wt,dy;

    int ndata=x.size();
    int ma=a.size();
    /*static*/ Vec_DP dyda(ma);
    for (j=0;j<ma;j++)
        if (ia[j]) mfit++;
    for (j=0;j<mfit;j++) {
        for (k=0;k<=j;k++) alpha[j][k]=0.0;
        beta[j]=0.0;
    }
    chisq=0.0;

    //std::vector<std::vector<double> > jacobian(x.size(), std::vector<double>(a.size()));
    
    spectra.setParams(a);
    for (i=0;i<ndata;i+=stride) {
        spectra.funcs(x[i],i+startPt,a,ymod,dyda,ia);
        
        //for (int pp = 0; pp < a.size(); pp++) // save the Jacobian at one freq
        //    jacobian[i][pp] = dyda[pp];
        
        dy=y[i]-ymod;
        for (j=0,l=0;l<ma;l++) {
            if (ia[l]) {
                wt=dyda[l];
                for (k=0,m=0;m<l+1;m++)
                    if (ia[m]) alpha[j][k++] += wt*dyda[m];
                beta[j++] += dy*wt;
            }
        }
        chisq += dy*dy;
    }
    
//    std::cout << "jacobian = " << jacobian << "\n";
    
    for (j=1;j<mfit;j++)
        for (k=0;k<j;k++) alpha[k][j]=alpha[j][k];
}

//////////////////////////////////////////////////////////////////////

void covsrt(Mat_IO_DP &covar, Vec_I_BOOL &ia, const int mfit)
{
        int i,j,k;

        int ma=ia.size();
        for (i=mfit;i<ma;i++)
                for (j=0;j<i+1;j++)
            covar[i][j]=covar[j][i]=0.0;
        k=mfit-1;
        for (j=ma-1;j>=0;j--) {
                if (ia[j]) {
                    //std::cout << "mfit ijk:" << mfit << ":" << i << ":" << j << ":" << k;
                        for (i=0;i<ma;i++) SWAP(covar[i][k],covar[i][j]);
                        for (i=0;i<ma;i++) SWAP(covar[k][i],covar[j][i]);
                        k--;
                }
        }
}

//////////////////////////////////////////////////////////////////////

void gaussj(Mat_IO_DP &a, Mat_IO_DP &b)
{
        int i,icol=0,irow=0,j,k,l,ll;
        DP big,dum,pivinv;
        const double epsilon = 1.0e-10;

        int n=a.nrows();
        int m=b.ncols();
        /*static*/ Vec_INT indxc(n),indxr(n),ipiv(n);
        for (j=0;j<n;j++) ipiv[j]=0;
        for (i=0;i<n;i++) {
                big=0.0;
                for (j=0;j<n;j++)
                        if (ipiv[j] != 1)
                                for (k=0;k<n;k++) {
                                        if (ipiv[k] == 0) {
                                                if (fabs(a[j][k]) >= big) {
                                                        big=fabs(a[j][k]);
                                                        irow=j;
                                                        icol=k;
                                                }
                                        }
                                }
                ++(ipiv[icol]);
                if (irow != icol) {
                        for (l=0;l<n;l++) SWAP(a[irow][l],a[icol][l]);
                        for (l=0;l<m;l++) SWAP(b[irow][l],b[icol][l]);
                }
                indxr[i]=irow;
                indxc[i]=icol;

                if (fabs(a[icol][icol]) <= epsilon)
                  a[icol][icol] = epsilon*   //PREVENT SINGULAR MATRIX
                    ( (a[icol][icol]<0) ? -1.0 : 1.0 );

                pivinv=1.0/a[icol][icol];
                a[icol][icol]=1.0;
                for (l=0;l<n;l++) a[icol][l] *= pivinv;
                for (l=0;l<m;l++) b[icol][l] *= pivinv;
                for (ll=0;ll<n;ll++)
                        if (ll != icol) {
                                dum=a[ll][icol];
                                a[ll][icol]=0.0;
                                for (l=0;l<n;l++) a[ll][l] -= a[icol][l]*dum;
                                for (l=0;l<m;l++) b[ll][l] -= b[icol][l]*dum;
                        }
        }
        for (l=n-1;l>=0;l--) {
                if (indxr[l] != indxc[l])
                        for (k=0;k<n;k++)
                                SWAP(a[k][indxr[l]],a[k][indxc[l]]);
        }
}

//#include <cmath>
//#include "nr.h"
//using namespace std;

Vec_DP *xx_p,*yy_p,*sx_p,*sy_p,*ww_p;
DP aa,offs;

void fitexy(Vec_I_DP &x, Vec_I_DP &y, Vec_I_DP &sigx, Vec_I_DP &sigy,
        DP &a, DP &b, DP &siga, DP &sigb, DP &chi2, DP &q)
{
    //Vec_DP *xx_p,*yy_p,*sx_p,*sy_p,*ww_p;
    //DP aa,offs;

        int j;
        const DP POTN=1.571000,BIG=1.0e30,ACC=1.0e-3;
        //const DP PI=3.141592653589793238;
        //double POTN=1.571000,BIG=1.0e30,ACC=1.0e-3;
        //double Pi=3.141592653589793238;
        DP amx,amn,varx,vary,ang[7],ch[7],scale,bmn,bmx,d1,d2,r2,
                dum1,dum2,dum3,dum4,dum5;

        int ndat=x.size();
        xx_p=new Vec_DP(ndat);
        yy_p=new Vec_DP(ndat);
        sx_p=new Vec_DP(ndat);
        sy_p=new Vec_DP(ndat);
        ww_p=new Vec_DP(ndat);
        Vec_DP &xx=*xx_p, &yy=*yy_p;
        Vec_DP &sx=*sx_p, &sy=*sy_p, &ww=*ww_p;
        avevar(x,dum1,varx);
        avevar(y,dum1,vary);
        scale=sqrt(varx/vary);
        for (j=0;j<ndat;j++) {
                xx[j]=x[j];
                yy[j]=y[j]*scale;
                sx[j]=sigx[j];
                sy[j]=sigy[j]*scale;
                ww[j]=sqrt(SQR(sx[j])+SQR(sy[j]));
        }
        fit(xx,yy,ww,true,dum1,b,dum2,dum3,dum4,dum5);
        offs=ang[0]=0.0;
        ang[1]=atan(b);
        ang[3]=0.0;
        ang[4]=ang[1];
        ang[5]=POTN;
        for (j=3;j<6;j++) ch[j]=chixy(ang[j]);
        mnbrak(ang[0],ang[1],ang[2],ch[0],ch[1],ch[2],chixy);
        chi2=brent(ang[0],ang[1],ang[2],chixy,ACC,b);
        chi2=chixy(b);
        a=aa;
        q=gammq(0.5*(ndat-2),chi2*0.5);
        r2=0.0;
        for (j=0;j<ndat;j++) r2 += ww[j];
        r2=1.0/r2;
        bmx=BIG;
        bmn=BIG;
        offs=chi2+1.0;
        for (j=0;j<6;j++) {
                if (ch[j] > offs) {
                        d1=fabs(ang[j]-b);
                        while (d1 >= PI) d1 -= PI;
                        d2=PI-d1;
                        if (ang[j] < b)
                                SWAP(d1,d2);
                        if (d1 < bmx) bmx=d1;
                        if (d2 < bmn) bmn=d2;
                }
        }
        if (bmx < BIG) {
                bmx=zbrent(chixy,b,b+bmx,ACC)-b;
                amx=aa-a;
                bmn=zbrent(chixy,b,b-bmn,ACC)-b;
                amn=aa-a;
                sigb=sqrt(0.5*(bmx*bmx+bmn*bmn))/(scale*SQR(cos(b)));
                siga=sqrt(0.5*(amx*amx+amn*amn)+r2)/scale;
        } else sigb=siga=BIG;
        a /= scale;
        b=tan(b)/scale;
        delete ww_p; delete sy_p; delete sx_p; delete yy_p; delete xx_p;
}

void avevar(Vec_I_DP &data, DP &ave, DP &var)
{
        DP s,ep;
        int j;

        int n=data.size();
        ave=0.0;
        for (j=0;j<n;j++) ave += data[j];
        ave /= n;
        var=ep=0.0;
        for (j=0;j<n;j++) {
                s=data[j]-ave;
                ep += s;
                var += s*s;
        }
        var=(var-ep*ep/n)/(n-1);
}

// Fit x[] vs y[] to a straight line y=ax+b minimizing chi^2.
// sig[] is the sigmas of y[].
// If mwt = false, sig[] is not used.
// q is the chi^2 probability, a metric for judging the fit. If mwt=false,
// q always returns 1.0.
//
void fit( Vec_I_DP &x, Vec_I_DP &y, Vec_I_DP &sig, const bool mwt, DP &a,
        DP &b, DP &siga, DP &sigb, DP &chi2, DP &q)
{
        int i;
        DP wt,t,sxoss,sx=0.0,sy=0.0,st2=0.0,ss,sigdat;

        int ndata=x.size();
        b=0.0;
        if (mwt) {
                ss=0.0;
                for (i=0;i<ndata;i++) {
                        wt=1.0/SQR(sig[i]);
                        ss += wt;
                        sx += x[i]*wt;
                        sy += y[i]*wt;
                }
        } else {
                for (i=0;i<ndata;i++) {
                        sx += x[i];
                        sy += y[i];
                }
                ss=ndata;
        }
        sxoss=sx/ss;
        if (mwt) {
                for (i=0;i<ndata;i++) {
                        t=(x[i]-sxoss)/sig[i];
                        st2 += t*t;
                        b += t*y[i]/sig[i];
                }
        } else {
                for (i=0;i<ndata;i++) {
                        t=x[i]-sxoss;
                        st2 += t*t;
                        b += t*y[i];
                }
        }
        b /= st2;
        a=(sy-sx*b)/ss;
        siga=sqrt((1.0+sx*sx/(ss*st2))/ss);
        sigb=sqrt(1.0/st2);
        chi2=0.0;
        q=1.0;
        if (!mwt) {
                for (i=0;i<ndata;i++)
                        chi2 += SQR(y[i]-a-b*x[i]);
                sigdat=sqrt(chi2/(ndata-2));
                siga *= sigdat;
                sigb *= sigdat;
        } else {
                for (i=0;i<ndata;i++)
                        chi2 += SQR((y[i]-a-b*x[i])/sig[i]);
                if (ndata>2) q=gammq(0.5*(ndata-2),0.5*chi2);
        }
}

DP gammq(const DP a, const DP x)
{
        DP gamser,gammcf,gln;

        if (x < 0.0 || a <= 0.0)
                std::cerr << "Invalid arguments in routine gammq" << std::endl;
        if (x < a+1.0) {
                gser(gamser,a,x,gln);
                return 1.0-gamser;
        } else {
                gcf(gammcf,a,x,gln);
                return gammcf;
        }
}

void shft3(DP &a, DP &b, DP &c, const DP d)
{
    a=b;
    b=c;
    c=d;
}
void mnbrak(DP &ax, DP &bx, DP &cx, DP &fa, DP &fb, DP &fc,
        DP func(const DP))
{
        const DP GOLD=1.618034,GLIMIT=100.0,TINY=1.0e-20;
        DP ulim,u,r,q,fu;

        fa=func(ax);
        fb=func(bx);
        if (fb > fa) {
                SWAP(ax,bx);
                SWAP(fb,fa);
        }
        cx=bx+GOLD*(bx-ax);
        fc=func(cx);
        while (fb > fc) {
                r=(bx-ax)*(fb-fc);
                q=(bx-cx)*(fb-fa);
                u=bx-((bx-cx)*q-(bx-ax)*r)/
                        (2.0*SIGN(MAX(fabs(q-r),TINY),q-r));
                ulim=bx+GLIMIT*(cx-bx);
                if ((bx-u)*(u-cx) > 0.0) {
                        fu=func(u);
                        if (fu < fc) {
                                ax=bx;
                                bx=u;
                                fa=fb;
                                fb=fu;
                                return;
                        } else if (fu > fb) {
                                cx=u;
                                fc=fu;
                                return;
                        }
                        u=cx+GOLD*(cx-bx);
                        fu=func(u);
                } else if ((cx-u)*(u-ulim) > 0.0) {
                        fu=func(u);
                        if (fu < fc) {
                                shft3(bx,cx,u,u+GOLD*(u-cx));
                                shft3(fb,fc,fu,func(u));
                        }
                } else if ((u-ulim)*(ulim-cx) >= 0.0) {
                        u=ulim;
                        fu=func(u);
                } else {
                        u=cx+GOLD*(cx-bx);
                        fu=func(u);
                }
                shft3(ax,bx,cx,u);
                shft3(fa,fb,fc,fu);
        }
}

DP chixy(const DP bang)
{
        const DP BIG=1.0e30;
        int j;
        DP ans,avex=0.0,avey=0.0,sumw=0.0,b;

        Vec_DP &xx=*xx_p, &yy=*yy_p;
        Vec_DP &sx=*sx_p, &sy=*sy_p, &ww=*ww_p;
        int nn=xx.size();
        b=tan(bang);
        for (j=0;j<nn;j++) {
                ww[j] = SQR(b*sx[j])+SQR(sy[j]);
                sumw += (ww[j]=(ww[j] < 1.0/BIG ? BIG : 1.0/ww[j]));
                avex += ww[j]*xx[j];
                avey += ww[j]*yy[j];
        }
        avex /= sumw;
        avey /= sumw;
        aa=avey-b*avex;
        for (ans = -offs,j=0;j<nn;j++)
                ans += ww[j]*SQR(yy[j]-aa-b*xx[j]);
        return ans;
}


DP brent(const DP ax, const DP bx, const DP cx, DP f(const DP),
        const DP tol, DP &xmin)
{
        const int ITMAX=100;
        const DP CGOLD=0.3819660;
        const DP ZEPS=numeric_limits<DP>::epsilon()*1.0e-3;
        int iter;
        DP a,b,d=0.0,etemp,fu,fv,fw,fx;
        DP p,q,r,tol1,tol2,u,v,w,x,xm;
        DP e=0.0;

        a=(ax < cx ? ax : cx);
        b=(ax > cx ? ax : cx);
        x=w=v=bx;
        fw=fv=fx=f(x);
        for (iter=0;iter<ITMAX;iter++) {
                xm=0.5*(a+b);
                tol2=2.0*(tol1=tol*fabs(x)+ZEPS);
                if (fabs(x-xm) <= (tol2-0.5*(b-a))) {
                        xmin=x;
                        return fx;
                }
                if (fabs(e) > tol1) {
                        r=(x-w)*(fx-fv);
                        q=(x-v)*(fx-fw);
                        p=(x-v)*q-(x-w)*r;
                        q=2.0*(q-r);
                        if (q > 0.0) p = -p;
                        q=fabs(q);
                        etemp=e;
                        e=d;
                        if (fabs(p) >= fabs(0.5*q*etemp) || p <= q*(a-x) || p >= q*(b-x))
                                d=CGOLD*(e=(x >= xm ? a-x : b-x));
                        else {
                                d=p/q;
                                u=x+d;
                                if (u-a < tol2 || b-u < tol2)
                                        d=SIGN(tol1,xm-x);
                        }
                } else {
                        d=CGOLD*(e=(x >= xm ? a-x : b-x));
                }
                u=(fabs(d) >= tol1 ? x+d : x+SIGN(tol1,d));
                fu=f(u);
                if (fu <= fx) {
                        if (u >= x) a=x; else b=x;
                        shft3(v,w,x,u);
                        shft3(fv,fw,fx,fu);
                } else {
                        if (u < x) a=u; else b=u;
                        if (fu <= fw || w == x) {
                                v=w;
                                w=u;
                                fv=fw;
                                fw=fu;
                        } else if (fu <= fv || v == x || v == w) {
                                v=u;
                                fv=fu;
                        }
                }
        }
        std::cerr << "Too many iterations in brent" << std::endl;;
        xmin=x;
        return fx;
}


DP zbrent(DP func(const DP), const DP x1, const DP x2, const DP tol)
{
        const int ITMAX=100;
        // EPS defined in SpectraT.h
        //const DP EPS=numeric_limits<DP>::epsilon();
        int iter;
        DP a=x1,b=x2,c=x2,d=0,e=0,min1,min2;
        DP fa=func(a),fb=func(b),fc,p,q,r,s,tol1,xm;

        if ((fa > 0.0 && fb > 0.0) || (fa < 0.0 && fb < 0.0))
                std::cerr << "Root must be bracketed in zbrent" << std::endl;
        fc=fb;
        for (iter=0;iter<ITMAX;iter++) {
                if ((fb > 0.0 && fc > 0.0) || (fb < 0.0 && fc < 0.0)) {
                        c=a;
                        fc=fa;
                        e=d=b-a;
                }
                if (fabs(fc) < fabs(fb)) {
                        a=b;
                        b=c;
                        c=a;
                        fa=fb;
                        fb=fc;
                        fc=fa;
                }
                tol1=2.0*EPS*fabs(b)+0.5*tol;
                xm=0.5*(c-b);
                if (fabs(xm) <= tol1 || fb == 0.0) return b;
                if (fabs(e) >= tol1 && fabs(fa) > fabs(fb)) {
                        s=fb/fa;
                        if (a == c) {
                                p=2.0*xm*s;
                                q=1.0-s;
                        } else {
                                q=fa/fc;
                                r=fb/fc;
                                p=s*(2.0*xm*q*(q-r)-(b-a)*(r-1.0));
                                q=(q-1.0)*(r-1.0)*(s-1.0);
                        }
                        if (p > 0.0) q = -q;
                        p=fabs(p);
                        min1=3.0*xm*q-fabs(tol1*q);
                        min2=fabs(e*q);
                        if (2.0*p < (min1 < min2 ? min1 : min2)) {
                                e=d;
                                d=p/q;
                        } else {
                                d=xm;
                                e=d;
                        }
                } else {
                        d=xm;
                        e=d;
                }
                a=b;
                fa=fb;
                if (fabs(d) > tol1)
                        b += d;
                else
                        b += SIGN(tol1,xm);
                        fb=func(b);
        }
        std::cerr << "Maximum number of iterations exceeded in zbrent" << std::endl;
        return 0.0;
}

void gser(DP &gamser, const DP a, const DP x, DP &gln)
{
        const int ITMAX=100;
        // EPS defined in SpectraT.h
        //const DP EPS=numeric_limits<DP>::epsilon();
        int n;
        DP sum,del,ap;

        gln=gammln(a);
        if (x <= 0.0) {
                if (x < 0.0)
                    std::cerr << "x less than 0 in routine gser" << std::endl;
                gamser=0.0;
                return;
        } else {
                ap=a;
                del=sum=1.0/a;
                for (n=0;n<ITMAX;n++) {
                        ++ap;
                        del *= x/ap;
                        sum += del;
                        if (fabs(del) < fabs(sum)*EPS) {
                                gamser=sum*exp(-x+a*log(x)-gln);
                                return;
                        }
                }
                std::cerr << "a too large, ITMAX too small in routine gser" << std::endl;
                return;
        }
}

void gcf(DP &gammcf, const DP a, const DP x, DP &gln)
{
        const int ITMAX=100;
        // EPS defined in SpectraT.h
        // const DP EPS=numeric_limits<DP>::epsilon();
        const DP FPMIN=numeric_limits<DP>::min()/EPS;
        int i;
        DP an,b,c,d,del,h;

        gln=gammln(a);
        b=x+1.0-a;
        c=1.0/FPMIN;
        d=1.0/b;
        h=d;
        for (i=1;i<=ITMAX;i++) {
                an = -i*(i-a);
                b += 2.0;
                d=an*d+b;
                if (fabs(d) < FPMIN) d=FPMIN;
                c=b+an/c;
                if (fabs(c) < FPMIN) c=FPMIN;
                d=1.0/d;
                del=d*c;
                h *= del;
                if (fabs(del-1.0) <= EPS) break;
        }
        if (i > ITMAX)
            std::cerr << "a too large, ITMAX too small in gcf" << std::endl;
        gammcf=exp(-x+a*log(x)-gln)*h;
}

DP gammln(const DP xx)
{
        int j;
        DP x,y,tmp,ser;
        static const DP cof[6]={76.18009172947146,-86.50532032941677,
                24.01409824083091,-1.231739572450155,0.1208650973866179e-2,
                -0.5395239384953e-5};

        y=x=xx;
        tmp=x+5.5;
        tmp -= (x+0.5)*log(tmp);
        ser=1.000000000190015;
        for (j=0;j<6;j++) ser += cof[j]/++y;
        return -tmp+log(2.5066282746310005*ser/x);
}

void mrqmin(Vec_I_DP &x, Vec_I_DP &y, Vec_I_DP &sig, Vec_IO_DP &a,
        Vec_I_BOOL &ia, Mat_O_DP &covar, Mat_O_DP &alpha, DP &chisq,
        void funcs(const DP, Vec_I_DP &, DP &, Vec_O_DP &), DP &alamda) {
        static int mfit=0;
        static DP ochisq=0;
        int j,k,l;

        int ma=a.size();
        static Mat_DP *oneda_p=0;
        static Vec_DP *atry_p=0,*beta_p=0,*da_p=0;
        if (alamda < 0.0) {
                atry_p = new Vec_DP(ma);
                beta_p = new Vec_DP(ma);
                da_p = new Vec_DP(ma);
                mfit=0;
                for (j=0;j<ma;j++)
                        if (ia[j]) mfit++;
                oneda_p = new Mat_DP(mfit,1);
                alamda=0.001;
                mrqcof(x,y,sig,a,ia,alpha,*beta_p,chisq,funcs);
                ochisq=chisq;
                for (j=0;j<ma;j++) (*atry_p)[j]=a[j];
        }
        Mat_DP &oneda=*oneda_p;
        Vec_DP &atry=*atry_p,&beta=*beta_p,&da=*da_p;
        Mat_DP temp(mfit,mfit);
        for (j=0;j<mfit;j++) {
                for (k=0;k<mfit;k++) covar[j][k]=alpha[j][k];
                covar[j][j]=alpha[j][j]*(1.0+alamda);
                for (k=0;k<mfit;k++) temp[j][k]=covar[j][k];
                oneda[j][0]=beta[j];
        }
        gaussj(temp,oneda);
        for (j=0;j<mfit;j++) {
                for (k=0;k<mfit;k++) covar[j][k]=temp[j][k];
                da[j]=oneda[j][0];
        }
        if (alamda == 0.0) {
                covsrt(covar,ia,mfit);
                covsrt(alpha,ia,mfit);
                delete oneda_p;
                delete da_p;
                delete beta_p;
                delete atry_p;
                return;
        }
        for (j=0,l=0;l<ma;l++)
                if (ia[l]) {
                    //std::cerr << "j da " << j << " " << da[j] << std::endl;
                    atry[l]=a[l]+da[j++];
                }
        mrqcof(x,y,sig,atry,ia,covar,da,chisq,funcs);
        if (chisq < ochisq) {
                alamda *= 0.1;
                ochisq=chisq;
                for (j=0;j<mfit;j++) {
                        for (k=0;k<mfit;k++) alpha[j][k]=covar[j][k];
                                beta[j]=da[j];
                }
                for (l=0;l<ma;l++) a[l]=atry[l];
        } else {
                alamda *= 10.0;
                chisq=ochisq;
        }
}

void mrqcof(Vec_I_DP &x, Vec_I_DP &y, Vec_I_DP &sig, Vec_I_DP &a,
        Vec_I_BOOL &ia, Mat_O_DP &alpha, Vec_O_DP &beta, DP &chisq,
        void funcs(const DP, Vec_I_DP &,DP &, Vec_O_DP &)) {
        int i,j,k,l,m,mfit=0;
        DP ymod,wt,sig2i,dy;

        int ndata=x.size();
        int ma=a.size();
        Vec_DP dyda(ma);
        for (j=0;j<ma;j++)
                if (ia[j]) mfit++;
        for (j=0;j<mfit;j++) {
                for (k=0;k<=j;k++) alpha[j][k]=0.0;
                beta[j]=0.0;
        }
        chisq=0.0;
        for (i=0;i<ndata;i++) {
                funcs(x[i],a,ymod,dyda);
                sig2i=1.0/(sig[i]*sig[i]);
                dy=y[i]-ymod;
                //std::cerr << "dy " << dy << std::endl;
                for (j=0,l=0;l<ma;l++) {
                        if (ia[l]) {
                                wt=dyda[l]*sig2i;
//                                std::cout << "l wt dyda sig2i" << m << ":" << wt << ":" << dyda[l]
//                                        << ":" << sig2i;
                                for (k=0,m=0;m<l+1;m++)
                                        if (ia[m]) {
                                            //std::cerr << "x i dyda " << x[i] << " " << i << " " << dyda[m] << std::endl;

                                            alpha[j][k++] += wt*dyda[m];
                                        }
                                beta[j++] += dy*wt;
                        }
                }
                chisq += dy*dy*sig2i;
        }
        for (j=1;j<mfit;j++)
                for (k=0;k<j;k++) alpha[k][j]=alpha[j][k];
}

// Derivative of etalon transform function
//
//    y = a + b*x + c*x/ln(x) + d*ln(x)/x + e/x
//
// or
//
//    y = a[0] + a[1]*x + a[2]*x/ln(x) + a[3]*ln(x)/x + a[4]/x
//
// where x is in DAQ points and y is frequency in GHz.
//
void fEtalon(const DP x, Vec_I_DP &a, DP &y, Vec_O_DP &dyda) {

    y = a[0] + a[1]*x + a[2]*x/log(x) + a[3]*log(x)/x + a[4]/x;
    dyda[0] = 1;
    dyda[1] = x;
    dyda[2] = x/log(x);
    dyda[3] = log(x)/x;
    dyda[4] = 1/x;
}

// Derivative of a gaussian line shape
//
void fgauss(const DP x, Vec_I_DP &a, DP &y, Vec_O_DP &dyda)
{
        int i;
        DP fac,ex,arg;

        int na=a.size();
        y=0.0;
        for (i=0;i<na-1;i+=3) {
//            std::cout << "xa1a2:" << x << " " << a[1] << " " << a[2];
                arg=(x-a[i+1])/a[i+2];
                ex=exp(-arg*arg);
                fac=a[i]*ex*2.0*arg;
                y += a[i]*ex;
                dyda[i]=ex;
                dyda[i+1]=fac/a[i+2];
                dyda[i+2]=fac*arg/a[i+2];
        }
}

#undef NRANSI

// Qt Version ---------------------------------------------------------------
// Fit x[] vs y[] to a straight line y=bx+a minimizing chi^2.
//
//void linearLeastSquaresFit(QVector<double> &x, QVector<double> &y, //HO
void linearLeastSquaresFit(std::vector<double> &x, std::vector<double> &y,
         double &a, double &b, double &siga, double &sigb,
         double &chi2)
{
    int i;
    double t,sxoss,sx=0.0,sy=0.0,st2=0.0,ss,sigdat;
    int ndata=x.size();
    b=0.0;
    for (i=0;i<ndata;i++) {
        sx += x[i];
        sy += y[i];
    }
    ss=ndata;
    sxoss=sx/ss;
    for (i=0;i<ndata;i++) {
        t=x[i]-sxoss;
        st2 += t*t;
        b += t*y[i];
    }
    b /= st2;
    a=(sy-sx*b)/ss;
    siga=sqrt((1.0+sx*sx/(ss*st2))/ss);
    sigb=sqrt(1.0/st2);
    chi2=0.0;
    for (i=0;i<ndata;i++)
        chi2 += (y[i]-a-b*x[i]) * (y[i]-a-b*x[i]);
    sigdat=sqrt(chi2/(ndata-2));
    siga *= sigdat;
    sigb *= sigdat;
}

void convlv(Vec_I_DP &data, Vec_I_DP &respns, const int isign,
    Vec_O_DP &ans)
{
    int i,no2;
    DP mag2,tmp;

    int n=data.size();
    int m=respns.size();
    Vec_DP temp(n);
    temp[0]=respns[0];
    for (i=1;i<(m+1)/2;i++) {
        temp[i]=respns[i];
        temp[n-i]=respns[m-i];
    }
    for (i=(m+1)/2;i<n-(m-1)/2;i++)
        temp[i]=0.0;
    for (i=0;i<n;i++)
        ans[i]=data[i];
    realft(ans,1);
    realft(temp,1);
    no2=n>>1;
    if (isign == 1) {
        for (i=2;i<n;i+=2) {
            tmp=ans[i];
            ans[i]=(ans[i]*temp[i]-ans[i+1]*temp[i+1])/no2;
            ans[i+1]=(ans[i+1]*temp[i]+tmp*temp[i+1])/no2;
        }
        ans[0]=ans[0]*temp[0]/no2;
        ans[1]=ans[1]*temp[1]/no2;
    } else if (isign == -1) {
        for (i=2;i<n;i+=2) {
            if ((mag2=SQR(temp[i])+SQR(temp[i+1])) == 0.0)
                std::cout << "Deconvolving at response zero in convlv";
            tmp=ans[i];
            ans[i]=(ans[i]*temp[i]+ans[i+1]*temp[i+1])/mag2/no2;
            ans[i+1]=(ans[i+1]*temp[i]-tmp*temp[i+1])/mag2/no2;
        }
        if (temp[0] == 0.0 || temp[1] == 0.0)
            std::cout << "Deconvolving at response zero in convlv";
        ans[0]=ans[0]/temp[0]/no2;
        ans[1]=ans[1]/temp[1]/no2;
    } else
		std::cout << "No meaning for isign in convlv";
    realft(ans,-1);
}

void realft(Vec_IO_DP &data, const int isign)
{
    int i,i1,i2,i3,i4;
    DP c1=0.5,c2,h1r,h1i,h2r,h2i,wr,wi,wpr,wpi,wtemp,theta;

    int n=data.size();
    theta=3.141592653589793238/DP(n>>1);
    if (isign == 1) {
        c2 = -0.5;
        four1(data,1);
    } else {
        c2=0.5;
        theta = -theta;
    }
    wtemp=sin(0.5*theta);
    wpr = -2.0*wtemp*wtemp;
    wpi=sin(theta);
    wr=1.0+wpr;
    wi=wpi;
    for (i=1;i<(n>>2);i++) {
        i2=1+(i1=i+i);
        i4=1+(i3=n-i1);
        h1r=c1*(data[i1]+data[i3]);
        h1i=c1*(data[i2]-data[i4]);
        h2r= -c2*(data[i2]+data[i4]);
        h2i=c2*(data[i1]-data[i3]);
        data[i1]=h1r+wr*h2r-wi*h2i;
        data[i2]=h1i+wr*h2i+wi*h2r;
        data[i3]=h1r-wr*h2r+wi*h2i;
        data[i4]= -h1i+wr*h2i+wi*h2r;
        wr=(wtemp=wr)*wpr-wi*wpi+wr;
        wi=wi*wpr+wtemp*wpi+wi;
    }
    if (isign == 1) {
        data[0] = (h1r=data[0])+data[1];
        data[1] = h1r-data[1];
    } else {
        data[0]=c1*((h1r=data[0])+data[1]);
        data[1]=c1*(h1r-data[1]);
        four1(data,-1);
    }
}

void four1(Vec_IO_DP &data, const int isign)
{
    int n,mmax,m,j,istep,i;
    DP wtemp,wr,wpr,wpi,wi,theta,tempr,tempi;

    int nn=data.size()/2;
    n=nn << 1;
    j=1;
    for (i=1;i<n;i+=2) {
        if (j > i) {
            SWAP(data[j-1],data[i-1]);
            SWAP(data[j],data[i]);
        }
        m=nn;
        while (m >= 2 && j > m) {
            j -= m;
            m >>= 1;
        }
        j += m;
    }
    mmax=2;
    while (n > mmax) {
        istep=mmax << 1;
        theta=isign*(6.28318530717959/mmax);
        wtemp=sin(0.5*theta);
        wpr = -2.0*wtemp*wtemp;
        wpi=sin(theta);
        wr=1.0;
        wi=0.0;
        for (m=1;m<mmax;m+=2) {
            for (i=m;i<=n;i+=istep) {
                j=i+mmax;
                tempr=wr*data[j-1]-wi*data[j];
                tempi=wr*data[j]+wi*data[j-1];
                data[j-1]=data[i-1]-tempr;
                data[j]=data[i]-tempi;
                data[i-1] += tempr;
                data[i] += tempi;
            }
            wr=(wtemp=wr)*wpr-wi*wpi+wr;
            wi=wi*wpr+wtemp*wpi+wi;
        }
        mmax=istep;
    }
}

void savgol(Vec_O_DP &c, const int np, const int nl, const int nr,
    const int ld, const int m)
{
    int j,k,imj,ipj,kk,mm;
    DP d,fac,sum;

    if (np < nl+nr+1 || nl < 0 || nr < 0 || ld > m || nl+nr < m)
        std::cout << "bad args in savgol";
    Vec_INT indx(m+1);
    Mat_DP a(m+1,m+1);
    Vec_DP b(m+1);
    for (ipj=0;ipj<=(m << 1);ipj++) {
        sum=(ipj ? 0.0 : 1.0);
        for (k=1;k<=nr;k++) sum += pow(DP(k),DP(ipj));
        for (k=1;k<=nl;k++) sum += pow(DP(-k),DP(ipj));
        mm=MIN(ipj,2*m-ipj);
        for (imj = -mm;imj<=mm;imj+=2) a[(ipj+imj)/2][(ipj-imj)/2]=sum;
    }
    ludcmp(a,indx,d);
    for (j=0;j<m+1;j++) b[j]=0.0;
    b[ld]=1.0;
    lubksb(a,indx,b);
    for (kk=0;kk<np;kk++) c[kk]=0.0;
    for (k = -nl;k<=nr;k++) {
        sum=b[0];
        fac=1.0;
        for (mm=1;mm<=m;mm++) sum += b[mm]*(fac *= k);
        kk=(np-k) % np;
        c[kk]=sum;
    }
}
void ludcmp(Mat_IO_DP &a, Vec_O_INT &indx, DP &d)
{
    const DP TINY=1.0e-20;
    int i=0,imax=0,j=0,k=0;
    DP big,dum,sum,temp;

    int n=a.nrows();
    Vec_DP vv(n);
    d=1.0;
    for (i=0;i<n;i++) {
        big=0.0;
        for (j=0;j<n;j++)
            if ((temp=fabs(a[i][j])) > big) big=temp;
        if (big == 0.0)
            std::cout << "Singular matrix in routine ludcmp";
        vv[i]=1.0/big;
    }
    for (j=0;j<n;j++) {
        for (i=0;i<j;i++) {
            sum=a[i][j];
            for (k=0;k<i;k++) sum -= a[i][k]*a[k][j];
            a[i][j]=sum;
        }
        big=0.0;
        for (i=j;i<n;i++) {
            sum=a[i][j];
            for (k=0;k<j;k++) sum -= a[i][k]*a[k][j];
            a[i][j]=sum;
            if ((dum=vv[i]*fabs(sum)) >= big) {
                big=dum;
                imax=i;
            }
        }
        if (j != imax) {
            for (k=0;k<n;k++) {
                dum=a[imax][k];
                a[imax][k]=a[j][k];
                a[j][k]=dum;
            }
            d = -d;
            vv[imax]=vv[j];
        }
        indx[j]=imax;
        if (a[j][j] == 0.0) a[j][j]=TINY;
        if (j != n-1) {
            dum=1.0/(a[j][j]);
            for (i=j+1;i<n;i++) a[i][j] *= dum;
        }
    }
}
void lubksb(Mat_I_DP &a, Vec_I_INT &indx, Vec_IO_DP &b)
{
    int i,ii=0,ip,j;
    DP sum;

    int n=a.nrows();
    for (i=0;i<n;i++) {
        ip=indx[i];
        sum=b[ip];
        b[ip]=b[i];
        if (ii != 0)
            for (j=ii-1;j<i;j++) sum -= a[i][j]*b[j];
        else if (sum != 0.0)
            ii=i+1;
        b[i]=sum;
    }
    for (i=n-1;i>=0;i--) {
        sum=b[i];
        for (j=i+1;j<n;j++) sum -= a[i][j]*b[j];
        b[i]=sum/a[i][i];
    }
}

// Use Savitzky-Golay digital filter to smooth spectral data.
// This type of filter has the advantage of not distorting lineshapes
// as much as other types of filters.
// See Ch. 13 & 14 of Numerical Recipes in C/C++.
//
//QVector<double> savitzkyGolay(QVector<double> data, int np, int nl, int nr, int ld, int m) { //HO
std::vector<double> savitzkyGolay(std::vector<double> data, int np, int nl, int nr, int ld, int m) {
    int fftSize = 2;
    int j = 0;
    int ndata = data.size();
    double lastData = 0;
    while(fftSize < ndata)
        fftSize *= 2;

    Vec_IO_DP nrRawData(fftSize);
    Vec_IO_DP nrSmoothedData(fftSize);
    for(int i=0; i<fftSize; i++)
        nrRawData[i] = nrSmoothedData[i] = 0.0;
//    QVector<double> smoothedData(data.size()); //HO
    std::vector<double> smoothedData(ndata);
    for(; j<ndata; j++)
        nrRawData[j] = data[j];
    lastData = data[j-1];
    for(; j<nrRawData.size(); j++)
        nrRawData[j] = lastData;
    Vec_IO_DP c(np);
    savgol(c,c.size(),nl,nr,ld,m);
    convlv(nrRawData,c,+1,nrSmoothedData);
    for(int j=0; j<ndata; j++)
        smoothedData[j] = nrSmoothedData[j];
    return smoothedData;
}



