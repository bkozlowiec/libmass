//
//  linelock_data.h
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Dmitry Skvortsov on 5/18/16.
//
//

#ifndef linelock_data_h
#define linelock_data_h

#include <vector>
#include <string>

class LinelockData
{
public:
	LinelockData();
	~LinelockData();

	void clear();

	int numPeaks() const
	{
		return currentPeakPositions_.size();
	}

	std::vector<double> getCurrentPeakPositions() const
	{
		return currentPeakPositions_;
	}
	std::vector<double> getCurrentPeakAreas() const
	{
		return currentPeakAreas_;
	}
	double getGoodnessOfFit() const
	{
		return goodnessOfFit_;
	}
	void setCurrentPeakPositions(const std::vector<double> & peakPositions);
	void setCurrentPeakAreas(const std::vector<double> & peakAreas);
	void setGoodnessOfFit(const double & goodnessOfFit);

protected:
	/**
	*  Peak Positions.
	*/
	std::vector<double> currentPeakPositions_;
	/**
	*  Peak Areas.
	*/
	std::vector<double> currentPeakAreas_;
	/**
	*  Goofness of Fit.
	*/
	double goodnessOfFit_;
};

#endif /* linelock_data_h */
