//
//  peaks_ini.hpp
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 2/13/16.
//
//

#ifndef peaks_ini_hpp
#define peaks_ini_hpp

#include <stdio.h>
#include <vector>
#include <map>
#include <string>

#include "../utils/physicalConsts.h"
#include "etalon.h"
#include "../spc_implementation_data.h"

class PeaksINI
{
public:
//    /**
//     * Representation of CONC lines in INI file.
//     *
//     * CONC lines specify how the fitter will produce concentration guesses to
//     * initialize the curve fitter.  Concentration estimates go into area and
//     * pressure width estimation for each peak.
//     */
//    class IsotopeConcentration
//    {
//    public:
//        IsotopeConcentration() {}
//        IsotopeConcentration(std::string isotopeCode, double expectedConc_ppm,
//            bool doUsePreviousConcentration, int numValuesToAverage);
//        
//        std::string isotopeCode() const { return isotopeCode_; }
//        //std::string moleculeCode() const { return moleculeCode_; }
//        
//        /**
//         * Expected concentration of this isotopologue, e.g. 18H2O
//         */
//        double isotopeConcentration_ppm() const { return isotope_ppm_; }
//        
//        /**
//         * Sum of concentrations of this and other isotopologues of the same
//         * molecule.
//         */
//        double moleculeConcentration_ppm() const { return total_ppm_; }
//        
//        void setMoleculeConcentration_ppm(double ppm)
//        {
//            total_ppm_ = ppm;
//        }
//        
//        bool doUsePrevious() const { return doUsePrev_; }
//        int numValuesToAverage() const { return numValuesToAvg_; }
//        
//    private:
//        std::string isotopeCode_;
//        //molecule_t moleculeCode_;
//        double isotope_ppm_;
//        double total_ppm_;
//        bool doUsePrev_;
//        int numValuesToAvg_;
//    };
    
    /**
     * Representation of PEAK lines in INI file
     */
    class PeakConstants
    {
    public:
        PeakConstants() {}
        PeakConstants(
            int index,
            double gammaAir,
            double gammaWater,
            double gammaSelf,
            double tExponent,
            double self_ppm,
            double water_ppm,
            double shiftGHz,
            double lineStrength,
            double lineStrengthFudgeFactor,
            bool wantConc,
            double epp,
            double rangeGHz,
            double microns,
            const std::string & isotopeCode,
            const std::string & molName,
            const std::string & evName,
            const std::string & fitFlagString,
            double press_shift_cm1);

        PeakConstants(
            int index,
            double gammaAir,
            double gammaWater,
            double gammaSelf,
            double tExponent,
            double self_ppm,
            double water_ppm,
            double shiftGHz,
            double lineStrength,
            double lineStrengthFudgeFactor,
            bool wantConc,
            double epp,
            double rangeGHz,
            double microns,
            const std::string & isotopeCode,
            const std::string & molName,
            const std::string & evName,
            const std::string & fitFlagString,
            double press_shift_cm1,
            int flangeIndex);
        
        /**
         * INI file index of peak.  The index is how master-slave relationships
         * are tabulated.
         */
        int index() const { return index_; }
        
        /**
         * Return air broadening coefficient in GHz/torr
         */
        double gammaAir() const { return gammaAir_; }
        
        /**
         * Return water broadening coefficient in GHz/torr
         */
        double gammaWater() const { return gammaWater_; }
        
        /**
         * Return self broadening coefficient in GHz/torr
         */
        double gammaSelf() const { return gammaSelf_; }
        
        /**
         * Exponent in pressure-width temperature dependence formula
         */
        double tExponent() const { return tExp_; }
        
        /**
         * Guess PPM for starting the fitter.  It's a per-peak value because
         * if the instrument temperature is wrong then the fitter will not
         * see the same PPM for each peak; characterizing the error may require
         * giving different initial PPM to each peak.  So don't assume that the
         * PPM for each peak of a given isotope will be the same either before
         * or after fitting!!!
         */
        double concentrationEstimate_ppm() const { return selfConcentration_; }
//        double waterConcentrationEstimate_ppm() const { return waterConcentration_; }
        double shift_GHz() const { return ghzShift_; }
        double lineStrength() const { return lineStrength_; }
        double adjustedLineStrength() const { return lineStrength_ * lsf_; }
        double lineStrengthFudgeFactor() const { return lsf_; }
        bool doWantConcentration() const { return wantConc_; }
        
        /**
         * Epsilon-prime-prime (epp)
         */
        double epp() const { return epp_; }
        
        /**
         * Width in GHz of search window for peak position
         */
        double range_GHz() const { return range_; }
        double center_GHz() const
        {
            return PhysicalConsts::micronsToGHz(microns_);
        }
        double microns() const { return microns_; }
        const std::string & isotopeCode() const { return isotopeCode_; }
		const std::string & molName() const { return molName_; }
        const std::string & evName() const { return evName_; }
        const std::string & fitFlagString() const { return fitFlagString_; }
        std::vector<bool> fitFlags() const;
        
		double press_shift_cm1() const { return press_shift_cm1_; }

		double press_shift_GHz() const
		{
			return PhysicalConsts::wavenumberToGHz(press_shift_cm1_);
		}
        // Flange things
        bool isFlange() const;
        int flangeIndex() const;

        // Master-slave things
        const std::vector<int> & xSlaves() const { return xSlaves_; }
        const std::vector<int> & areaSlaves() const { return areaSlaves_; }
        const std::vector<int> & dopplerWidthSlaves() const { return dopplerWidthSlaves_; }
        const std::vector<int> & pressureWidthSlaves() const { return pressureWidthSlaves_; }
        std::vector<int> & xSlaves() { return xSlaves_; }
        std::vector<int> & areaSlaves() { return areaSlaves_; }
        std::vector<int> & dopplerWidthSlaves() { return dopplerWidthSlaves_; }
        std::vector<int> & pressureWidthSlaves() { return pressureWidthSlaves_; }
        
        void setXSlaves(const std::vector<int> & v)
        {
            xSlaves_ = v;
        }
        void setAreaSlaves(const std::vector<int> & v)
        {
            areaSlaves_ = v;
        }
        void setDopplerwidthSlaves(const std::vector<int> & v)
        {
            dopplerWidthSlaves_ = v;
        }
        void setPressureWidthSlaves(const std::vector<int> & v)
        {
            pressureWidthSlaves_ = v;
        }
        
    private:
        int index_;
        double gammaAir_, gammaWater_, gammaSelf_;
        double tExp_;
        double selfConcentration_, waterConcentration_;
        double ghzShift_;
        double lineStrength_;
        double lsf_;
        bool wantConc_;
        double epp_;
        double range_;
        double microns_;
        std::string isotopeCode_;
        std::string molName_;
        std::string evName_;
        std::string fitFlagString_;
        double press_shift_cm1_;
        int flangeIndex_;
        
        
        std::vector<int> xSlaves_, areaSlaves_, dopplerWidthSlaves_,
            pressureWidthSlaves_;
    };
    
public:
    PeaksINI();
    PeaksINI(const std::string & fileName);
	PeaksINI(const StructPeaksIni & strPeaksIni);
    ~PeaksINI();
    
    void clear();
    void updatePeaksINI();
	void updatePeaksINI(const StructPeaksIni & strPeaksIni);
	//void updateFlangeTP(const std::vector<double> & flangeTemp_C,
    //    const std::vector<double> & flangePressure_torr);

    /**
     * Pressure used for estimation.  Will be used in fitting when instrument
     * pressure is not available.
     */
    //double torr() const { return torr_; }
    
    /**
     * Temperature used for estimation.  Will be used in fitting when instrument
     * temperature is not available.
     */
    //double celsius() const { return celsius_; }
    
    int numPeaks() const { return peaks_.size(); }
    int numBackgroundPeaks() const { return peaks_.size() - iFirstBackgroundPeak_; }
    int numForegroundPeaks() const { return iFirstBackgroundPeak_; }
    
    bool doFitFlanges() const { return flangeFitPeriod_ > 0; }
    int flangeFitPeriod() const { return flangeFitPeriod_; }
    
    const PeakConstants & peak(int ii) const { return peaks_[ii]; }
    
	const std::vector<int> & getLinelockPeakIndexes() const { return linelockPeakIndexes_; }

    /**
     * Index of peak that is centered on the zero of the spectra GHz x-axis.
     */
    int zeroPeak() const { return zeroIdx_; }
    
    /** 
     * Real peaks are usually shifted somewhat from the fit model.  Set this
     * to a non-zero value to move the initial guess model to match the real
     * peak.
     */
    double shift_GHz() const { return shift_; }
    
    // ---------------- Baseline
    //
    //
    bool doEstimateBaseline() const
    {
        return (blLmEst_.size() > 0);
    }
    
    int numBaselineFitIterations() const
    {
        if (doEstimateBaseline())
            return blLmEst_[0];
        else
            return 0;
    }
    
    int baselineFitStride() const
    {
        if (doEstimateBaseline())
            return blLmEst_[1];
        else
            return 0;
    }
    
    const std::vector<int> & baselineSampleRanges() const
    {
        return blSample_;
    }
    
    int getBaselineModel() const
    {
        return blModel_;
    }
    
    const std::vector<double> & baselineParameters() const
    {
        return baselineParameters_;
    }
    
    const std::vector<bool> & baselineFlags() const
    {
        return baselineFlags_;
    }
    
    // ---------------- Fitting
    //
    //
    const std::vector<int> & fitWindow() const
    {
        return fitWindow_;
    }
    
    bool doEnableFit() const
    {
        return enableFit_;
    }
    
    int getFitAlgorithm() const
    {
        return fitAlgorithm_;
    }
    
    // ---------------- Etalon
    //
    //
    const Etalon & etalon() const
    {
        return etalon_;
    }
    
protected:
    
    std::string fileName_;
    std::string peaksIniLabel_;
    
    bool plotEnabled_;
    
    int zeroIdx_;
    double shift_;
    //double celsius_;
    //double torr_;
    Etalon etalon_;
    
    bool enableFit_;
    int fitAlgorithm_;
    std::vector<double> lowerBounds_; // TODO: unused PCH170215
    std::vector<double> upperBounds_; // TODO: unused PCH170215
    
    int flangeFitPeriod_;
    
    double badChisq_;
    bool resetEveryFit_;
    
    int blModel_;
    std::vector<double> baselineParameters_;
    std::vector<bool> baselineFlags_;
    std::vector<bool> baselineFeedForwardFlags_;
    std::vector<int> blSample_;
    std::vector<int> blLmEst_;
    
    std::vector<PeakConstants> peaks_;
    int iFirstBackgroundPeak_;
    
    std::vector<int> fitWindow_;

	std::vector<int> linelockPeakIndexes_;
};


std::ostream & operator<<(std::ostream & str, const PeaksINI::PeakConstants & p);


#endif /* peaks_ini_hpp */
