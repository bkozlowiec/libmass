#ifndef CONCENTRATIONCALCULATOR_H
#define CONCENTRATIONCALCULATOR_H

#include <cmath>
#include <map>
#include "peak.h"
#include "spectrapeaks.h"
#include "../utils/physicalConsts.h"
#include "../utils/physicalConsts_ini.h"

namespace c = Constants;

class ConcentrationCalculator
{
public:
//    static std::map<std::string,double> gasConcentrations(SpectraPeaksBase&, double celsius, double pressure, double tau);
    
    /**
     * Return concentration of every peak with wantConc() == true, labeled by
     * isotope.  If wantConc() is true for peaks that have the same molecule_t,
     * the last one analyzed will return its concentration in the map.
     */
    static std::map<std::string,double> speciesConcentrations(const SpectraPeaksBase &,
    		const PhysConstsINI & physConstsINI,
    		double celsius, double pressure, double tau);
    
    static double gasConcentration(const Peak & peak, const std::vector<double> & qCoeffs,
    		double celsius, double pressure, double tau);

private:
    ConcentrationCalculator() {}
    
    static double gasConcentration_( const double area,
                            const double temperature,
                            const std::vector<double> & qCoeffs,
                            const double epp,
                            const double pressure,
                            const double tau,
                            const double lineStrength,
                            const double lsFudge );

};

#endif // CONCENTRATIONCALCULATOR_H
