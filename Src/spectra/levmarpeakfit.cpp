#include "levmarpeakfit.h"

#include <numeric>
#include <stdexcept>
#include <cassert>


LevmarPeakFit::LevmarPeakFit(const PeaksINI & peaksINI) :
    spectraFunc_(NULL),
    etalonStretchDeltaOriginalGHz_(0.0),
    shiftFitWindow_(false),
    recentSpectralShifts_(1, 0.0),
    haveReadICOStruct_(false)
{
	//Etalon Stretching reqire modifications to Etalon object
	etalon_ = peaksINI.etalon();
}

LevmarPeakFit::LevmarPeakFit(SpectraPeaksBase* s) :
    spectraFunc_(s),
    etalonStretchDeltaOriginalGHz_(0.0),
    shiftFitWindow_(false),
    recentSpectralShifts_(1, 0.0),
    haveReadICOStruct_(false)
{
}

void LevmarPeakFit::fitSpectra(
    const PeaksINI & peaksINI,
    const std::vector<double>& inVoltage,
    std::vector<double>& outInitYs,
    std::vector<double>& outEstYs,
    std::vector<double>& outFitYs,
    bool doCalculateInitialY,
    bool doCalculateEstimatedY,
    bool doCalculateFitY,
    bool doEstimateBaseline,
    bool doHandlePeakShifts,
    bool doEstimatePeakAmplitudes,
    int numAmplitudeEstimateIterations,
    int numFitIterations,
    int mrqcofStride,
    double pressure_torr)
{
    chisq_ = -999.0; //remains neg. if fit not attempted
    reduced_chisq_ = -999.0; //remains neg. if fit not attempted

    // Skew basis func based upon tau. Skew of voigts not implemented yet.
    //spectraFunc_->skew();

    if (doCalculateInitialY)
    {
        outInitYs = getSnapshotOfFit();
    }

    if (peaksINI.doEstimateBaseline())
    {
        spectraFunc_->getBaseline()->setParams(nrLoopBl(
            peaksINI.numBaselineFitIterations(),
            peaksINI.baselineFitStride(),
            inVoltage));
    }
    else
    {
		// baselineEst_ function calculates linear fit to the Baseline fit regions.
		// This will affect first two baseline parameters
		// We do not want to change these first two parameters if the FitFlags are set to False
		std::vector<bool> blFitFlags = spectraFunc_->getBaseline()->getFitFlags();
		if (blFitFlags[0] && blFitFlags[1])
		{
			bool isBinFit = spectraFunc_->getBaseline()->isBinFit();
			spectraFunc_->getBaseline()->setParams(baselineEst_(isBinFit, inVoltage));
		}
    }

    // Ray: if the laser were rock-stable or could be locked to a fringe then
    // all this stuff wouldn't be necessary
    // Alok: the new laser, we don't know how stable it is
    // Ray: depends on linewidth too.  We adjust the laser on the fly too...
    if (doHandlePeakShifts)
    {
        updatePeakShifts(peaksINI.fitWindow()[0],
            peaksINI.fitWindow()[1],
            inVoltage);
        spectraFunc_->shiftPeaks(recentSpectralShifts_.mean());
    }
    if (doEstimatePeakAmplitudes)
    {
        estPeakAmplitudes_(etalon_, numAmplitudeEstimateIterations, inVoltage);
    }
    if (doCalculateEstimatedY)
    {
        outEstYs = getSnapshotOfFit();
    }

    if (numFitIterations > 0 && peaksINI.doEnableFit())
    {
        switch(peaksINI.getFitAlgorithm())
        {
            case 0:
                nrLoop(numFitIterations, mrqcofStride, inVoltage);
                break;
            default:
                std::cout << "No fit algorithm specified";
        }
    }

    if(doCalculateFitY)
    {
        outFitYs = getSnapshotOfFit();
    }

	// Etalon stretching for the next fit
	if (etalon_.getStretchEnabled() == true)
	{
		std::vector<int> idx = etalon_.getStretchPeakIndexes();
		double nu0f = spectraFunc_->getPeak(idx[0]).getCenter();
		double nu1f = spectraFunc_->getPeak(idx[1]).getCenter();
		double shift0 = peaksINI.peak(idx[0]).shift_GHz();
		double shift1 = peaksINI.peak(idx[1]).shift_GHz();
		double p_shift0 = peaksINI.peak(idx[0]).press_shift_GHz();
		double p_shift1 = peaksINI.peak(idx[1]).press_shift_GHz();
		double deltaf = (nu1f - shift1 - p_shift1 * pressure_torr / 760.0)
			- (nu0f - shift0 - p_shift0 * pressure_torr / 760.0);
		double cur_stretch_factor = etalon_.getStretchFactor();
		double updated_stretch_factor = cur_stretch_factor * etalonStretchDeltaOriginalGHz_ / deltaf;
		etalon_.setStretchFactor(updated_stretch_factor);
    }
}

// Get a crude estimate of the spectra baseline by sampling data points in two
// regions of the spectra (between peaks of course) and estimate the slope and
// offset of the baseline.  A full fit of the baseline is done in mrqmin() but
// before we do that, we need to crudely align the estimated peak centers with
// the real spectra, and before we can do that, we need baseline subtracted
// signal so we can determine where the peaks are centered.  In
// icosfit_shared.cpp, this routine is known as Two_Region_Baseline().
//
// Tips for using this method:
// If you are using a straight polynomial baseline function
// (e.g. a0 + a1*x + a2*x*x + a3*x*x*x) you want to select baseline sampling
// regions far apart to get a better average slope and offset.
// However, if you are using the correlate polynomial function
// a0 + a1*x + a1*a2*x*x, the estimated slope and offset will actually
// produce a worse initial guess because the initial slope will change
// the curvature. The best way to estimate the baseline is to sample two
// regions as close to the beginning of the laser sweep as possible, where
// the x^2 term is smallest.
//
std::vector<double> LevmarPeakFit::baselineEst_(bool binFit,
    const std::vector<double> & inVoltage) const
{
    std::vector<int> start = getBaselineRegionStart(); // could be int[2] PCH151024
    std::vector<int> length = getBaselineRegionLength(); // could be int[2] PCH151024
    
    // Find center frequencies and avg signal of regions 0 & 1
    
    double x0 = FitXs_.at(start.at(0) + length.at(0)/2);
    double x1 = FitXs_.at(start.at(1) + length.at(1)/2);
    
    // For baseline bin fit
    if(binFit) {
        x0 = start.at(0) + length.at(0)/2;
        x1 = start.at(1) + length.at(1)/2;
    }

    // Mean y for each region
    //
    
    double mean_y0 = Tools::mean(inVoltage, start[0], start[0]+length[0]);
    double mean_y1 = Tools::mean(inVoltage, start[1], start[1]+length[1]);
    
    // Find the slope and the avg. for the intercept
    // FIXME slope calculation should fail noisily instead of returning 0.0 PCH151019
    double slope = ( fabs(x1-x0) > EPS ) ? (mean_y1-mean_y0)/(x1-x0) : 0.0;
    double intercept0 = mean_y0 - slope*x0;
    double intercept1 = mean_y1 - slope*x1;
    double intercept = (intercept0 + intercept1)/ 2.0;

    std::vector<double> baselineParams(2,0.0);
    baselineParams[0] = intercept;
    baselineParams[1] = slope;
    return baselineParams;
}

//void LevmarPeakFit::estNoise_(void)
//{
//    // Est RMS noise using the regions for baseline estimation
//    //
//    double d;
//    std::vector<int> start = getBaselineRegionStart(); // or int[2] PCH151024
//    std::vector<int> length = getBaselineRegionLength(); // also length 2 PCH151024
//    
//    // FIXME two redundant code blocks.  Do this better.  Might not even see that
//    // it was happening.  D.R.Y.  PCH151024
//    
//    // REVIEW x always refers to a frequency it seems, why isn't it called something less generic PCH151024
//    // REVIEW 1e-10 is a magic constant PCH151024
//    double sum = 0.0;//, noiseMin = 1e10, noiseMax = -1e10;
//    for(int i=0; i<length.at(0); i++)
//    {
//        int idx = i + start.at(0);
//        d = debaselinedY(idx, 1e-10);
//        sum += d*d;
//    }
//    
//    // Calculate the sum of squares of a sort of relative noise measurement.
//    // Each point is (offset + baseline - averaged raw signal)/baseline.
//    //
//    // REVIEW: physical significance of RMS relative error is what? curious PCH151024
//    for(int i=0; i<length.at(1); i++) {
//        int idx = i + start.at(1);
//        d = debaselinedY(idx, 1e-10);
//        sum += d*d;
//    }
//    noiseEst_ = sqrt(sum/(length.at(0)+length.at(1)));
//}

void LevmarPeakFit::updatePeakShifts(int fitWindowStart, int fitWindowEnd,
    const std::vector<double> & inVoltage)
{
    // loop through the peaks in the fit spectrum and look for peaks that
    // have a non-NULL range setting.  Get the ranges and the initial peak centers
    // parameter. Range and center are in GHz. Once we have the list of new
    // centers for each peak, we can calc an avg. shift and apply the shift
    // to all peaks in the spectra.  The reason I built in the ability
    // to look at several peaks is that I want the code to be robust enough
    // to handle cases where the pressumed dominant peak is weak or absent.
    // In the case where it will always be there, we can just define that
    // one peak with a range.
    //
    std::vector<Peak*> peaksWithRange;
    spectraFunc_->getPeaksWithRange(peaksWithRange);
    if(peaksWithRange.size() == 0)
        return; // abort if there aren't any peaks to estimate
    
    std::vector<double> peakShifts_GHz;
    
//    // Dmitry suggestion:
//    std::vector<int> fitWindow = spectraFunc_->getFitWindow();
//    int start_ind = fitWindow.at(0);
    
    for(std::size_t iPeak = 0; iPeak < peaksWithRange.size(); iPeak++)
    {
        const double center = peaksWithRange.at(iPeak)->getCenter();
        const double range = peaksWithRange.at(iPeak)->getRange();
        
        int startWindowPt, endWindowPt;
        int iStartSearch = fitWindowStart;
        Tools::findElementsSpanning(FitXs_, center - range/2.0, center + range/2.0,
            startWindowPt, endWindowPt, iStartSearch);
        
        if (startWindowPt == -1)
        {
            std::ostringstream ss;
            ss << "Peak (" << peaksWithRange.at(iPeak)->id() << ") ("
                << peaksWithRange.at(iPeak)->getMicrons() << " um, "
                << 1.0e4 / peaksWithRange.at(iPeak)->getMicrons() << " cm-1) at "
                << center << " GHz with range " << range << " GHz "
                << "is not contained in ramp frequencies from "
                << FitXs_[iStartSearch] << " to " << FitXs_[FitXs_.size()-1] << ".";
            throw std::runtime_error(ss.str());
        }
        
        // Find the actual peak maximum position and determine its shift
        // relative to the center X frequency from the INI file.
        double maxY = 0.0;
        int iMax = -1;
        for (int xpt = startWindowPt; xpt <= endWindowPt; xpt++)
        {
            double y = debaselinedY(xpt, 1e-10, inVoltage); // doesn't need fancy debaselining
            if (y > maxY)
            {
                maxY = y;
                iMax = xpt;
            }
        }
        if (iMax == -1)
            throw std::runtime_error("Debaselined y is never > 0");
        
        peakShifts_GHz.push_back(FitXs_.at(iMax) - center); // NEEDED BELOW
    }
    
    if (peakShifts_GHz.size() > 0)
    {
        double meanShift = std::accumulate(
            peakShifts_GHz.begin(), peakShifts_GHz.end(), 0.0) / peakShifts_GHz.size();
        recentSpectralShifts_.push(meanShift);
    }
    
    // Shift the fit window to follow the shift in spectra due to laser drift.
    // This important to keep a constant del measurement in ch4iso.
    //
    if(shiftFitWindow_)
    {
        std::cout << "Sliding fit window disabled.";
        /*
        spectraFitStartPt_ = origSpectraFitStartPt_;
        double dghz = FitXs_[spectraFitStartPt_+1] - FitXs_[spectraFitStartPt_];
        int newSpectraFitStartPt_ = spectraFitStartPt_ - (int)(meanShift/dghz);
        spectraFitStartPt_ = newSpectraFitStartPt_;
        pS_->FitX1.at(laserIdx_) = newSpectraFitStartPt_;
        */
    }
}

// Estimate peak/basis amplitude before fitting.  We iterate n times.
// Ignore peaks whose centers are outside the fit window because the peak
// center could be in the ringdown section of the laser ramp.
//
//
// Ray explains:
//The number of Levenberg-Marquardt fit iterations is fixed to 5.  In most cases
//the fit parameters are good enough after 5 iterations. We don't want to run
//more than 5 because fit have to complete the fit in about 50ms and the system
//has a low performance CPU. If the initial guess parameters are too far off,
//5 fit iterations may not be enough so we estimate the parameters by looking
//at the initial fit model and comparing it to the spectra in a few key points.
//These points are ideally the peak maxima but as we haven't yet done the full
//spectral fit, we only know the approximate location of the peak maxima.
//Comparing these points to the estimated baseline to get an approximate peak
//amplitude, and then comparing these data to the model, we then scale the
//model to better match the spectra. Normally one pass through the estimator
//is good enough but if the initial guess is really far of the scaling may
//under- or overshoot. Subsequent estimations will clean this up.  As the
//estimator takes almost no time at all, we just iterate the estimator 5
//times just to be safe.

void LevmarPeakFit::estPeakAmplitudes_(const Etalon & etalon, int numIterations,
    const std::vector<double> & inVoltage)
{
    bool inFitWindow = true;
    std::vector<Peak*> peaks;
    spectraFunc_->getAllPeaks(peaks);
    
    for (int iteration = 0; iteration < numIterations; iteration++)
    {
        for (std::size_t iPeak = 0; iPeak < peaks.size(); iPeak++)
        {
            double centerGHz = peaks.at(iPeak)->getCenter();
            int centerPt = etalon.getPoint(centerGHz, FitXs_);

            if (centerPt < spectraFitStartPt_ ||
                centerPt > (spectraFitStartPt_ + spectraFitLengthPt_))
                inFitWindow = false;
            else
                inFitWindow = true;

            if (inFitWindow && fabs(spectraFunc_->baseline_f(centerGHz,centerPt)) > 1e-10)
            {
//  Feng 08/26/09 : NH3 peak estimation is out of the fitting window!!
//            if(fabs(spectraFunc_->baseline_f(centerGHz,centerpt)) > 1e-10) {
                // get the y of the real data
                double realY = debaselinedY(centerGHz, centerPt, 1e-10, inVoltage);
                
                // Just need to check that realY > 1e-10 and I can do away with debaselinedY.

                // Get the y of the predicted spectra.
                // Note that this flips the peaks upside-down.
                double predictedY = ( + spectraFunc_->baseline_f(centerGHz,centerPt) - (spectraFunc_->f(centerGHz,centerPt)))/
                    spectraFunc_->baseline_f(centerGHz,centerPt);
                
                double currentArea = peaks.at(iPeak)->getArea();
                if (realY > 0.0 && predictedY > 0.0)
                {
                    std::cerr << "Make this not overwrite slave params!\n";
                    peaks.at(iPeak)->setArea(currentArea*(realY/predictedY));
                }
                else
                {
                    //peaks.at(i)->setParam(amp*0.01, AREA);
                    //peaks.at(i)->setArea(amp*0.01);
                }
            }
        }
    }
}

//// Adjust one or more etalon coefficients or fudge factors to compensate for
//// changes in the laser tuning rate. We compute the adjustment by looking at
//// the spacing between fitted peaks and compare them with the HITRAN spacing.
////
//// In this version, for Bob's N2O instrument, we are looking at one CO and one
//// N2O peak.
////
//double levmarPeakFit::etalonDeviation(void) const
//{
//    std::vector<Peak*> pwr;
//    spectraFunc_->getAllPeaks(pwr);
//    double delFittedCenters =
//        pwr.at(1)->getParam(CENTER) - pwr.at(0)->getParam(CENTER);
//    double delHITRANCenters =
//        pwr.at(1)->getOrigParam(CENTER) - pwr.at(0)->getOrigParam(CENTER);
//    return delFittedCenters/delHITRANCenters;
//}

std::vector<double> LevmarPeakFit::getSnapshotOfFit(void) const
{
    std::vector<double> spectra;
    for(std::size_t i=0; i<FitXs_.size(); i++)
        spectra.push_back(spectraFunc_->f(FitXs_.at(i),i,wells1999));
    return spectra;
}

void LevmarPeakFit::setBaselineRegions(int s0, int l0, int s1, int l1)
{
    baselineRegion0Start_ = s0;
    baselineRegion1Start_ = s1;
    baselineRegion0Length_ = l0;
    baselineRegion1Length_ = l1;
}

// REVIEW: function only used internally and only packs two integers into
// a std::vector.  Does not appear to be meaningfully used as a vector later.
// PCH151024
std::vector<int> LevmarPeakFit::getBaselineRegionStart(void) const
{
    std::vector<int> v;
    v.push_back(baselineRegion0Start_);
    v.push_back(baselineRegion1Start_);
    return v;
}

std::vector<int> LevmarPeakFit::getBaselineRegionLength(void) const
{
    std::vector<int> v;
    v.push_back(baselineRegion0Length_);
    v.push_back(baselineRegion1Length_);
    return v;
}

void LevmarPeakFit::setSpectraRegion(int iStart, int numPoints)
{
    origSpectraFitStartPt_ = spectraFitStartPt_ = iStart;
    spectraFitLengthPt_ = numPoints;
}

int LevmarPeakFit::getSpectraStart(void) const
{
    return spectraFitStartPt_;
}

int LevmarPeakFit::getSpectraLength(void) const
{
    return spectraFitLengthPt_;
}

double LevmarPeakFit::debaselinedY(int xpt, double tinyBaseline,
    const std::vector<double> & inVoltage) const
{
    double xghz = FitXs_.at(xpt);
    double baselineValue = spectraFunc_->baseline_f(xghz, xpt);
    double yOut;
    
    if (fabs(baselineValue) < tinyBaseline)
        yOut = 0.0;
    else
        yOut = (baselineValue - inVoltage[xpt]) / baselineValue;
    return yOut;
}

double LevmarPeakFit::debaselinedY(double xghz, int xpt, double tinyBaseline,
    const std::vector<double> & inVoltage) const
{
    double baselineValue = spectraFunc_->baseline_f(xghz, xpt);
    double yOut;
    
    if (fabs(baselineValue) < tinyBaseline)
        yOut = 0.0;
    else
        yOut = (baselineValue - inVoltage[xpt]) / baselineValue;
    return yOut;
}

void LevmarPeakFit::initFrequenciesAndRegions(
    const PeaksINI & peaksINI,
    int numFrequencies)
{
	// have to recalculate X if etalon stretch is enabled
	if (etalon_.getStretchEnabled() == true)
	{
		etalon_.getFrequencies_GHz(numFrequencies, FitXs_);
	}
    if(!haveReadICOStruct_)
    {
		// Etalon stretching origin
		std::vector<int> idx = etalon_.getStretchPeakIndexes();
		double nu0 = peaksINI.peak(idx[0]).center_GHz() + peaksINI.peak(idx[0]).shift_GHz();
		double nu1 = peaksINI.peak(idx[1]).center_GHz() + peaksINI.peak(idx[1]).shift_GHz();
		etalonStretchDeltaOriginalGHz_ = nu1 - nu0;

		double origin = peaksINI.shift_GHz() - peaksINI.peak(peaksINI.zeroPeak()).center_GHz();
		double nu0_start = nu0 + origin;
		etalon_.setStretchNu0(nu0_start);

		// Calculate X coordinate
    	etalon_.getFrequencies_GHz(numFrequencies, FitXs_);
        
        std::vector<int> blSample = peaksINI.baselineSampleRanges();
        std::vector<int> fitWindow = peaksINI.fitWindow();
        
        if (blSample.size() < 4)
        {
            throw std::runtime_error("Baseline must have at least two sample segments");
        }
        if (fitWindow.size() < 2)
        {
            throw std::runtime_error("Fit window must be a 2-element vector");
        }
        
        setBaselineRegions(blSample.at(0), blSample.at(1),
                           blSample.at(2), blSample.at(3));
        setSpectraRegion(fitWindow.at(0), fitWindow.at(1));
        
        haveReadICOStruct_ = true;
    }
}

//std::mutex LevmarPeakFit::mut;
// Spectra fitter
//
void LevmarPeakFit::nrLoop(int nIters, int stride,
    const std::vector<double> & inY)
{
    int numParams = spectraFunc_->getParams().size();
    DP alamda, chisq;//,ochisq;
    Mat_DP covar(0.0, numParams, numParams);
    Mat_DP alpha(0.0, numParams, numParams);
    Vec_DP params(0.0, numParams);
    Vec_BOOL fitFlags(false, numParams);
    std::vector<double> parameters_STL(numParams);
    std::vector<bool> paramFitFlags_STL(numParams);
    
    parameters_STL = spectraFunc_->getParams();
    for(int i = 0; i < numParams; i++)
        params[i] = parameters_STL[i];
    
    paramFitFlags_STL = spectraFunc_->getFitFlags();
    for(int i = 0; i < numParams; i++)
        fitFlags[i] = paramFitFlags_STL[i];

    int length = getSpectraLength();
    int startPt = getSpectraStart();
    Vec_DP Xs(length),Ys(length);
    for(int j = 0; j < length; j++)
    {
        Xs[j] = FitXs_.at(j + startPt);
        Ys[j] = inY[j + startPt];
    }
    alamda = -1;// mut.lock();
    mrqmin(Xs, Ys, params, fitFlags, covar, alpha, chisq, alamda, stride, startPt, (*(spectraFunc_)));
    for(int i = 0; i < nIters; i++)
    {
        mrqmin(Xs, Ys, params, fitFlags, covar, alpha, chisq, alamda, stride, startPt, (*(spectraFunc_)));
    }

    alamda=0.0;
    mrqmin(Xs, Ys, params, fitFlags, covar,alpha,chisq,alamda,stride,startPt,(*(spectraFunc_)));// mut.unlock();

    for(int i = 0; i < numParams; i++)
        parameters_STL[i] = params[i];
    spectraFunc_->setParams(parameters_STL);
    chisq_ = chisq;
	int N_variables = std::count(paramFitFlags_STL.begin(), paramFitFlags_STL.end(), true);
	int DoF = length - N_variables;
    reduced_chisq_ = chisq / (DoF - 1);
}

// Baseline fit only version of nrLoop(). While this is a "fitter" it is used
// as a baseline estimator since the gas peaks are not fitted. This code is
// a potential improvement over the former method of estimating the baseline
// slope and intercept which can't handle baselines with the form
// a0 + a1*x + a1*a2*x*x. The baseline fit flags are the same as used in the
// regular spectra fit and are defined in peaks_#.ini.
//
// The fit region is the same as the regular spectra fit which includes all
// the peaks. I tried setting up the fit to only use the baseline between
// peaks but mrqmin() fails with gaps in the data. This code seems to work
// best with a large stride (3-10) and an initial guess of the gas
// concentrations is not far off from typical values.
//
std::vector<double> LevmarPeakFit::nrLoopBl(int nIters, int stride,
    const std::vector<double> & inVoltage)
{
//    int num_params = cacheParams().size();
    int num_params = spectraFunc_->getParams().size();
    
    // alamda: Marquardt's fudge factor lambda.  Misspelled in the book!  PCH151020
    // chisq: the merit function, a weighted sum of squared errors.
    DP alamda,chisq;
    
    Mat_DP covar(0.0,num_params,num_params);
    Mat_DP alpha(0.0,num_params,num_params);
    
    // a: parameters to find.
    Vec_DP params(0.0,num_params);
    
    // ia: parameters to fit; set all to zero, and we'll turn the baseline
    // parameters on a few lines down.
    Vec_BOOL fitFlags(false,num_params);
    
    std::vector<double> parameters_STL(num_params);
    std::vector<bool> paramFitFlags_STL(num_params);
    
    // Set the initial parameters for the fitting.
    parameters_STL = spectraFunc_->getParams();
    for(int i=0; i<num_params; i++)
        params[i] = parameters_STL[i];
    
    // Put the baseline fit flags into ia.  They will be the only nonzero
    // flags, so only the baseline parameters will be fit.  PCH151020
    std::vector<bool> blFitFlags = spectraFunc_->getBaseline()->getFitFlags();

    for(std::size_t i=0; i<blFitFlags.size(); i++)
        fitFlags[i] = blFitFlags[i];
    
    // For the fit X and Y vectors, grab just the laser ramp portion of the
    // time traces (corresponding to the spectrum we measure).  PCH151020
    int length = getSpectraLength();
    int startPt = getSpectraStart();
    Vec_DP Xs(length),Ys(length);
    
    for(int jj = 0; jj < length; jj++)
    {
        Xs[jj] = FitXs_.at(jj+startPt);
        Ys[jj] = inVoltage[jj + startPt];
    }
    
    // Set lambda = -1 to signal that this is the first iteration.
    // Run mrqmin for the first time.
    alamda = -1;
    mrqmin(Xs,Ys,params,fitFlags,covar,alpha,chisq,alamda,stride,startPt,(*(spectraFunc_)));

    // Run a fixed number of iterations of mrqmin (mrqmin performs one step).
    for(int i=0; i<nIters; i++) {
        mrqmin(Xs,Ys,params,fitFlags,covar,alpha,chisq,alamda,stride,startPt,(*(spectraFunc_)));
    }

    // Set lambda = 0 for the final iteration of mrqmin.
    alamda=0.0;
    mrqmin(Xs,Ys,params,fitFlags,covar,alpha,chisq,alamda,stride,startPt,(*(spectraFunc_)));

    // Return the baseline parameters, which are packed into the first elements
    // of the parameter vector "a".  PCH151020
    std::vector<double> blParams;
    for(std::size_t i=0; i<blFitFlags.size(); i++)
        blParams.insert(blParams.end(), params[i]);
    return blParams;
}

// Tau correction compensates for the cases where there is an absorber at the
// laser frequency where we are measuring ringdown.  In congested spectra such
// as water vapor and CH4 isotope, this problem is unavoidable.  The absorber
// will result in a fitted tau that is too fast, i.e. the mirrors appear more
// lossy and the derived effective path length is too short.
//
// To compensate, we measure the fitted absorption at the last DAQ point in
// the fit. Ideally this point is the last point just before the laser is shut
// off.
//
// The formula for the correction is as follows.
//  bl   - fitted baseline
//  Y    - fitted transmission
//  abs  - absorption
//  GA   - generalized absorption term from ringdown forumlas
//  tau  - tau from ringdown fit of decay curve
//  tau' - corrected tau
//
//  abs = (bl-Y)/bl
//  GA = abs/(1-abs)
//  tau' = tau * (1+GA)
//
// Ref: D. Baer, et.al. Appl. Phys. B Vol. 75, pg 261-265 (2002).
//
double LevmarPeakFit::tauAbsorptionCorrection(void) const
{
    int lastFitPt =  getSpectraStart() + getSpectraLength() - 1;
    double xghz = FitXs_.at(lastFitPt);

    double lastBaselineValue = spectraFunc_->baseline_f(xghz,lastFitPt);
    if(lastBaselineValue < 1.0e-7)
        lastBaselineValue = 1.0e-7;

    double lastY = spectraFunc_->f(xghz,lastFitPt);

    double absorption = (lastBaselineValue - lastY) / lastBaselineValue;
    if( fabs(absorption-1.0) < 1.0e-8 )
        absorption = 1.0e-8;
    
    double GA = absorption/(1.0-absorption);

    //tauCorrection_ = 1.0 + GA;
    double tauCorrectionFactor = 1.0 + GA;

    return tauCorrectionFactor;
}
