//
//  calibration_ini.hpp
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 2/13/16.
//
//

#ifndef calibration_ini_hpp
#define calibration_ini_hpp

#include <vector>
#include <string>
#include "../spc_implementation_data.h"

class CalibrationINI
{
public:
    CalibrationINI();
    CalibrationINI(const std::string & fileName);
	CalibrationINI(const StructCalibrationIni & structCalIni);
    ~CalibrationINI();
    
    void clear();
    void updateCalibrationIni();
	void updateCalibrationIni(const StructCalibrationIni & structCalIni);
    
    int numPeaks() const
    {
        return lineStrengthFudge_.size();
    }
    
    // Each peak that is used to determine a gas concentration has to be
    // first measured with a known concentration of gas.  After measuring
    // with the standard, we determine the 'cal gas coefficient', the factor
    // that lets us translate the concentration calculated in gasConc.cpp
    // into the desired units.  Each peak, not just each gas, needs its own
    // value.  calConc_ is the species concentration in the calibration
    // (reference) gas.
    
    double lineStrengthFudge(int ii) const // unused PCH170215
    {
        return lineStrengthFudge_[ii];
    }
    const std::vector<double> & areaCoefficients(int ii) const // unused PCH170215
    {
        return areaCoefficients_[ii];
    }
    double calibrationConcentration(int ii) const // unused PCH170215
    {
        return calConcv_[ii];
    }
    time_t calibrationTime(int ii) const // unused PCH170215
    {
        return timev_[ii];
    }
    
protected:
    std::string fileName_;
    
    std::vector<double> lineStrengthFudge_;
    std::vector<std::vector<double> > areaCoefficients_;
    std::vector<double> calConcv_; // unused PCH170215
    std::vector<time_t> timev_;
};

#endif /* calibration_ini_hpp */
