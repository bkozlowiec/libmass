// This class replaces the SpectraT.h template and fixes the type to PEAKS.
// We will need to create a separate class for the BASIS type when we
// integrate chemometric fits into the new code.
//
// This refactoring was necessary because I want to use the signal/slot
// mechanism to allow the levmar code access to the spectra func(). The
// method of passing around the spectra object in the old template was cumbersome.
// Furthermore, the template has become cumbersome and trying to keep both
// peak and basis fitting methods consistent kludgy at best.
//

#ifndef SPECTRAPEAKS_H
#define SPECTRAPEAKS_H

#include <cmath>
#include <vector>
#include <map>
#include <cstdio>
#include <float.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
#include "../utils/physicalConsts.h"
#include "../nr/nrutil.h"
#include "../nr/nrtypes.h"
#include "baseline.h"
#include "peak.h"
#include "etalon.h"
#include "peaks_ini.h"
#include "calibration_ini.h"
#include "../utils/tools.h"

#define EPS 1.0E-10

namespace c = Constants;

class SpectraPeaksBase
{
public:
    explicit SpectraPeaksBase(const PeaksINI & peaksINI,
        const CalibrationINI & calINI);
    virtual ~SpectraPeaksBase();
    
    /**
     *
     */
    void setPeaks(
        const PeaksINI & peaksINI, // JUST for the baseline
        const std::vector<const PeaksINI::PeakConstants*> & peaks,
        const std::vector<double> & centers,
        const std::vector<double> & areas,
        const std::vector<double> & dopplerWidths,
        const std::vector<double> & pressureWidths);
    
    /**
     * 
     */
    void setPeaks(
        const PeaksINI & peaksINI, // JUST for the baseline
        const std::vector<const PeaksINI::PeakConstants*> & peaks,
        const std::vector<double> & initialParams);
    
    // REVIEW: f(), df(), baseline_f() and funcs() just need peaks_ and
    // baseline_.  They do the job that you would EXPECT spectrapeaks to do.
    // All the other stuff looks like setup cruft to me. PCH151117
    //
    // The thing is, to extend the ICOS code to more than one transfer function,
    // all that has to change is the content of f() and df() and funcs().
    // Even the caching seems to be handled by peaks.
    
    // TODO: to get around possible speed issues, provide virtual f() and df()
    // and funcs() evaluations that can accept a range of frequencies.  Then
    // there won't be the hit of a virtual function call for each frequency
    // point—should that even end up mattering.  PCH151117
    
    /**
     * Calculate transfer function
     *
     * @return transfer function
     */
    virtual double f(double x,int xpt) const = 0;
    
    /**
     * Calculate transfer function
     *
     * @return transfer function
     */
    virtual double f(double x,int xpt, lineshape ls) const = 0;
    
    /**
     * Cache derivatives in each peak, and return
     * value of transfer function, just like f().
     *
     * REVIEW this function may be unused; it's not used in funcs() for instance
     * PCH151027
     *
     * @return transfer function
     */
    virtual double df(double x,int xpt) = 0;
    
    /**
     * Evaluate peak function with baseline = 1.0.
     */
    virtual double fWithoutBaseline(double x, int xpt) const = 0;
    
    /**
     * Call baseline_->f(x, xpt)
     */
    double baseline_f(double x,int xpt);
    
    Baseline* getBaseline(void) { return baseline_; }
    
    /**
     * Const accessor for the parameters.
     */
    std::vector<double> getParams() const;
    
    /**
     *
     */
    std::vector<double> getBaselineParams() const;
    
    /**
     *
     */
    std::vector<double> getPeakParams() const;
        
    std::vector<std::string> getParamNames(void);
    std::vector<bool> getPrintFlag(void);
    void setParams(Vec_I_DP&);
    void setParams(std::vector<double>&);
    std::vector<bool> getFitFlags(void) const;
    
    /**
     * The function called in mrqcof() to evaluate the spectral function and its
     * derivatives.  Evaluates one frequency point at a time.
     *
     * @param x: Frequency (GHz)
     * @param xPt: index corresponding to frequency (could be used for baseline)
     * @param a: parameters (UNUSED)
     * @param y: OUTPUT value y(x)
     * @param dyda: OUTPUT value dy(x)/da
     * @param ia: indices to fit (UNUSED)
     */
    virtual void funcs(const DP x, const int xPt, Vec_I_DP& a, DP& y,
            Vec_O_DP& dyda, Vec_I_BOOL& ia) =  0;
    
    void getPeaksWithRange(std::vector<Peak*>&) const;
    void getAllPeaks(std::vector<Peak*>&) const;
    
    const std::vector<Peak*> & getPeaks() const { return peaks_; }
    
    /**
     * Const accessors.  PCH151113
     */
    const Peak & getPeak(int index) const;
    int numPeaks() const { return peaks_.size(); }
    
    void shiftPeaks(double del_x);
    void resetBaseline(void);
    void zeroOutNegativePeaks(void);
    //void skew(void);
    
    friend std::ostream & operator<<(std::ostream & str, const SpectraPeaksBase & rhs);
    
protected:
    
    void assignMasterSlave(const std::vector<const PeaksINI::PeakConstants*> & peaks);
    
// Ray explains badChisq_:
//    This is an attempt to identify when the spectral fit is bad, say when a
//    laser has drifted too far or there is an interfering peak from an
//    unidentified contaminant.  We don't use it as we don't have an easy or
//    reliable way to determine a threshold when the fit has gone from good to
//    bad with just this number.  In test runs where we force a bad fit by
//    making the laser drift, we can see the fit data go bad long before
//    chisq looks bad.

    void deletePeaks();
    
    // Objects used to simulate spectra.  We use voigt::voigt so we
    // can distinguish between the voigt class and the voigt function
    // in nr_specific_source.h.
    //
    // Once baseline_ and peaks_ are initialized, they contain ALL the
    // information needed for simulating spectra, and all the other stuff
    // in the class is for setup I think...
    Baseline* baseline_;
    std::vector<Peak*> peaks_;

    // setParams() uses a small, local post_dvector to construct a vector
    // of parameters to send.  setParams() is called many times in the inner
    // loop of mrqmin(). After some profiling, I found that memory allocation
    // of local post_dvector was hurting the fit execution speed by +10%.
    //
    // To gain the speed back, I create an object data member, paramsToSend_,
    // that persists during the entire object life time. Memory is only
    // allocated once, in the constructor.
    std::vector<double> paramsLocalBuffer_;
};


class ICOSSpectraPeaks : public SpectraPeaksBase
{
public:
    explicit ICOSSpectraPeaks(
        const PeaksINI & peaksINI,
        const CalibrationINI & calINI);
    /**
     * Calculate baseline/(1 + sum of peaks)
     *
     * @return baseline/(1 + sum of peaks)
     */
    virtual double f(double x,int xpt) const;
    
    /**
     * Calculate baseline/(1 + sum of peaks)
     *
     * @return baseline/(1 + sum of peaks)
     */
    virtual double f(double x,int xpt, lineshape ls) const;
    
    /**
     * Evaluate peak function with baseline = 1.0.
     */
    virtual double fWithoutBaseline(double x, int xpt) const;
    
    /**
     * Cache derivatives in each peak, and return
     * baseline/(1 + sum of peaks), just like f().
     *
     * REVIEW this function may be unused; it's not used in funcs() for instance
     * PCH151027
     *
     * @return baseline/(1 + sum of peaks)
     */
    virtual double df(double x,int xpt);
    
    /**
     * The function called in mrqcof() to evaluate the spectral function and its
     * derivatives.  Evaluates one frequency point at a time.
     *
     * Model: y(x) = baseline / ( 1 + sum (Voigt lineshapes) )
     *
     * @param x: Frequency (GHz)
     * @param xPt: index corresponding to frequency (could be used for baseline)
     * @param a: parameters (UNUSED)
     * @param y: OUTPUT value y(x)
     * @param dyda: OUTPUT value dy(x)/da
     * @param ia: indices to fit (UNUSED)
     */
    virtual void funcs(const DP x, const int xPt, Vec_I_DP& a, DP& y,
            Vec_O_DP& dyda, Vec_I_BOOL& ia);

    
private:
};

class OpenPathSpectraPeaks : public SpectraPeaksBase
{
public:
    explicit OpenPathSpectraPeaks(
        const PeaksINI & peaksINI,
        const CalibrationINI & calINI);
    
    /**
     * Calculate baseline * exp(-sum of peaks)
     *
     * @return baseline * exp(-sum of peaks)
     */
    virtual double f(double x,int xpt) const;
    
    /**
     * Calculate baseline * exp(-sum of peaks)
     *
     * @return baseline * exp(-sum of peaks)
     */
    virtual double f(double x,int xpt, lineshape ls) const;
    
    /**
     * Cache derivatives in each peak, and return
     * baseline * exp(-sum of peaks), just like f().
     *
     * REVIEW this function may be unused; it's not used in funcs() for instance
     * PCH151027
     *
     * @return baseline * exp(-sum of peaks)
     */
    virtual double df(double x,int xpt);
    
    /**
     * Evaluate peak function with baseline = 1.0.
     */
    virtual double fWithoutBaseline(double x, int xpt) const;
    
    /**
     * The function called in mrqcof() to evaluate the spectral function and its
     * derivatives.  Evaluates one frequency point at a time.
     *
     * Model: y(x) = baseline * exp(-sum of Voigt lineshapes)
     *
     * @param x: Frequency (GHz)
     * @param xPt: index corresponding to frequency (could be used for baseline)
     * @param a: parameters (UNUSED)
     * @param y: OUTPUT value y(x)
     * @param dyda: OUTPUT value dy(x)/da
     * @param ia: indices to fit (UNUSED)
     */
    virtual void funcs(const DP x, const int xPt, Vec_I_DP& a, DP& y,
            Vec_O_DP& dyda, Vec_I_BOOL& ia);
private:
};


#endif // SPECTRAPEAKS_H
