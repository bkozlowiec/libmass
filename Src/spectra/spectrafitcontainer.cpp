 #include "spectrafitcontainer.h"



NewSpectrumContainer::
NewSpectrumContainer() //:
//    laserIdx_(0)
{
}

//NewSpectrumContainer::
//NewSpectrumContainer(int laserIdx) :
//    laserIdx_(laserIdx)
//{
//}

void NewSpectrumContainer::
setFrequencies(const std::vector<double> & freqs)
{
    xGHz_ = freqs;
}

void NewSpectrumContainer::
setBaseline(const std::vector<double> & baseline)
{
    baseline_ = baseline;
}

void NewSpectrumContainer::
setInitialModel(const std::vector<double> & initialModel)
{
    initialModel_ = initialModel;
}

void NewSpectrumContainer::
setEstimatedModel(const std::vector<double> & estimatedModel)
{
    estimatedModel_ = estimatedModel;
}

void NewSpectrumContainer::
setFinalModelFit(const std::vector<double> & finalModelFit)
{
    finalModelFit_ = finalModelFit;
}

//void NewSpectrumContainer::
//setLaserIndex(int laserIdx)
//{
//    laserIdx_ = laserIdx;
//}

void NewSpectrumContainer::
setFitWindow(const std::vector<int> & fitWindow)
{
    fitWindow_ = fitWindow;
}




NewRingdownContainer::NewRingdownContainer() :
    laserIdx_(0)
{
}

NewRingdownContainer::NewRingdownContainer(int laserIdx) :
    laserIdx_(laserIdx)
{
}

void NewRingdownContainer::
setModelTrace(const std::vector<double> & modelTrace)
{
    ringdownModel_ = modelTrace;
}

void NewRingdownContainer::
setTau(double tau)
{
    tau_ = tau;
}

void NewRingdownContainer::
setOffset(double offset)
{
    offset_ = offset;
}

void NewRingdownContainer::
setLaserIndex(int laserIdx)
{
    laserIdx_ = laserIdx;
}




SpectraFitContainer::SpectraFitContainer (std::vector<std::string> spectrumNames)
{
	initVectors_ (spectrumNames);
}

void SpectraFitContainer::initVectors_ (std::vector<std::string> spectrumNames)
{
	for (std::size_t i = 0; i < spectrumNames.size (); i++)
	{
		OneFitContainer ofc (spectrumNames[i]);
		fits[spectrumNames[i]] = ofc;
	}

	tau.resize (2);
	offset.resize (2);
	ringdownTrace.resize (2);
	laserRampTrace.resize (2);
	firstPassWinner.resize (2);
	secondPassWinner.resize (2);
	thirdPassWiner.resize (2);
	firstPassWinner[0] = "000";
	firstPassWinner[1] = "100";
}

void SpectraFitContainer::setSpectralData (int laserIdx, double tauIn, double offsetIn,
                                           std::vector<double> rdTraceIn,
                                           std::vector<double> laserRampTraceIn)
{
	tau[laserIdx] = tauIn;
	offset[laserIdx] = offsetIn;
	ringdownTrace[laserIdx] = rdTraceIn;
	laserRampTrace[laserIdx] = laserRampTraceIn;
}

void SpectraFitContainer::setFitData (int laserIdx, int passIdx, int fitIdx,
                                      std::vector<int> fitWindow, std::vector<double> xghz,
                                      std::vector<double> baseline, std::vector<double> initFit,
                                      std::vector<double> estFit, std::vector<double> fit)
{
	std::string key = makeKey (laserIdx, passIdx, fitIdx);
	fits[key].fitWindow = fitWindow;
	fits[key].xghz = xghz;
	fits[key].baseline = baseline;
	fits[key].initFit = initFit;
	fits[key].estFit = estFit;
	fits[key].fit = fit;
}

std::vector<int> SpectraFitContainer::getFitWindow (int laserIdx, int passIdx, int fitIdx)
{
	std::string key = makeKey (laserIdx, passIdx, fitIdx);
	return fits[key].fitWindow;
}

std::vector<double> SpectraFitContainer::getXghz (int laserIdx, int passIdx, int fitIdx)
{
	std::string key = makeKey (laserIdx, passIdx, fitIdx);
	return fits[key].xghz;
}

std::vector<double> SpectraFitContainer::getBaseline (int laserIdx, int passIdx, int fitIdx)
{
	std::string key = makeKey (laserIdx, passIdx, fitIdx);
	return fits[key].baseline;
}

std::vector<double> SpectraFitContainer::getInitFit (int laserIdx, int passIdx, int fitIdx)
{
	std::string key = makeKey (laserIdx, passIdx, fitIdx);
	return fits[key].initFit;
}

std::vector<double> SpectraFitContainer::getEstFit (int laserIdx, int passIdx, int fitIdx)
{
	std::string key = makeKey (laserIdx, passIdx, fitIdx);
	return fits[key].estFit;
}

std::vector<double> SpectraFitContainer::getFit (int laserIdx, int passIdx, int fitIdx)
{
	std::string key = makeKey (laserIdx, passIdx, fitIdx);
	return fits[key].fit;
}

void SpectraFitContainer::setWinner (int laserIdx, int passIdx, int fitIdx)
{
	std::string winner = makeKey (laserIdx, passIdx, fitIdx);
	switch (passIdx)
	{
		case 0:
			firstPassWinner[laserIdx] = winner;
			break;
		case 1:
			secondPassWinner[laserIdx] = winner;
			break;
		case 2:
			thirdPassWiner[laserIdx] = winner;
			break;
		default:
			std::cerr << "Pass limit exceeded in SpectraFitContainer()";
			break;
	}
}

std::vector<int> SpectraFitContainer::getWinner (int laserIdx, int passIdx)
{
	std::string winner ("000");
	switch (passIdx)
	{
		case 0:
			winner = firstPassWinner[laserIdx];
			break;
		case 1:
			winner = secondPassWinner[laserIdx];
			break;
		case 2:
			winner = thirdPassWiner[laserIdx];
			break;
		default:
			std::cerr << "Pass limit exceeded in SpectraFitContainer()";
			break;
	}
	return splitKey (winner);
}

// Assume that there will be a no more than 2 lasers, no more than 9 fit passes
// and no more than 9 fits per pass, the unique key for each fit is a 3 digit
// number/string ordered by laserIdx, passIdx, fitIdx.
//
std::string SpectraFitContainer::makeKey (int laserIdx, int passIdx, int fitIdx)
{
	std::stringstream key;
	key << laserIdx << passIdx << fitIdx;

	return key.str ();
}

std::vector<int> SpectraFitContainer::splitKey (std::string key)
{
	std::vector<int> indicies;
	//std::stringList strList = key.split(QRegExp("."));
	for (std::size_t idx = 0; idx < key.length (); idx++)
	{
		indicies.push_back (atoi(&key[idx]));
	}

	return indicies;
}

// ============================================================================
//
OneFitContainer::OneFitContainer (void)
{
	laserIdx = 0;
}

OneFitContainer::OneFitContainer (std::string label)
{
	fitLabel = label;
	laserIdx = atoi(&fitLabel[0]); // 1st char id's this as laser 0 or 1
	resetVectors ();
}

void OneFitContainer::resetVectors ()
{
	fitWindow.clear ();
	ringdownData.clear ();
	ringdownFit.clear ();
	baseline.clear ();
	initFit.clear ();
	estFit.clear ();
	fit.clear ();
}

