#include "spectrapeaks.h"
#include <exception>
#include <stdexcept>
#include <cassert>

SpectraPeaksBase::SpectraPeaksBase(
    const PeaksINI & peaksINI,
    const CalibrationINI & calINI)
{
//    peaksIniLabel_ = "";
//    badChisq_ = 10000.0; // high default essentially disables check for ini's that don't use it
    
    baseline_ = new Baseline(peaksINI.getBaselineModel(),
        peaksINI.baselineParameters(), peaksINI.baselineFlags());
    
    paramsLocalBuffer_.assign(NUM_PEAKPARAM, 0.0);
}

SpectraPeaksBase::
~SpectraPeaksBase()
{
}



void SpectraPeaksBase::setPeaks(
    const PeaksINI & peaksINI,
    const std::vector<const PeaksINI::PeakConstants*> & peaks,
    const std::vector<double> & centers,
    const std::vector<double> & areas,
    const std::vector<double> & dopplerWidths,
    const std::vector<double> & pressureWidths)
{
    deletePeaks();
    
    baseline_ = new Baseline(peaksINI.getBaselineModel(),
        peaksINI.baselineParameters(), peaksINI.baselineFlags());
    
    for (std::size_t iPeak = 0; iPeak < peaks.size(); iPeak++)
    {
        const PeaksINI::PeakConstants & peak = *peaks[iPeak];
        
        // Wells 1999 is a voigt with the best balance of speed and accuracy.
        const lineshape LINESHAPECODE = wells1999;
        std::string peaksIniLabel_unused = "";
        
        Peak* p = new Peak(peaksIniLabel_unused,
                iPeak,
                centers[iPeak],
                areas[iPeak],
                dopplerWidths[iPeak],
                pressureWidths[iPeak],
                peak.range_GHz(),
                peak.isotopeCode(),
				peak.molName(),
                peak.evName(),
                LINESHAPECODE,
                peak.epp(),
                peak.adjustedLineStrength(),
                peak.lineStrengthFudgeFactor(),
                peak.doWantConcentration(),
                peak.gammaAir(),
                peak.gammaSelf(),
                peak.gammaWater(),
                peak.tExponent(),
                peak.fitFlags(),
                peak.microns(),
                peak.press_shift_cm1());
        peaks_.push_back(p);
    }
    
    assignMasterSlave(peaks);
}


    
void SpectraPeaksBase::setPeaks(
    const PeaksINI & peaksINI,
    const std::vector<const PeaksINI::PeakConstants*> & peaks,
    const std::vector<double> & initialParams)
{
    deletePeaks();
    
    baseline_ = new Baseline(peaksINI.getBaselineModel(),
        peaksINI.baselineParameters(), peaksINI.baselineFlags());
    
    for (std::size_t iPeak = 0; iPeak < peaks.size(); iPeak++)
    {
        const PeaksINI::PeakConstants & peak = *peaks[iPeak];
        
        // Wells 1999 is a voigt with the best balance of speed and accuracy.
        const lineshape LINESHAPECODE = wells1999;
        std::string peaksIniLabel_unused = "";
    
        // Extract the peak parameters from the parameter vector.
        // There are 4 baseline parameters to skip.
        double pCenter, pArea, pDopplerWidth, pPressureWidth;
        pCenter = initialParams.at(4 + 4*iPeak + CENTER);
        pArea = initialParams.at(4 + 4*iPeak + AREA);
        pPressureWidth = initialParams.at(4 + 4*iPeak + PW);
        pDopplerWidth = initialParams.at(4 + 4*iPeak + DW);
        
        Peak* p = new Peak(peaksIniLabel_unused,
                iPeak,
                pCenter,
                pArea,
                pDopplerWidth,
                pPressureWidth,
                peak.range_GHz(),
                peak.isotopeCode(),
				peak.molName(),
                peak.evName(),
                LINESHAPECODE,
                peak.epp(),
                peak.adjustedLineStrength(),
                peak.lineStrengthFudgeFactor(),
                peak.doWantConcentration(),
                peak.gammaAir(),
                peak.gammaSelf(),
                peak.gammaWater(),
                peak.tExponent(),
                peak.fitFlags(),
                peak.microns(),
                peak.press_shift_cm1());
        peaks_.push_back(p);
    }
    
    assignMasterSlave(peaks);
}

// Index   Parameter
// -----------------
//   0     X (position)
//   1     A (area)
//   2     D (doppler width)
//   3     P (pressure width)
//
// Values
// ------
//   F     Fit this parameter, initial value from first principles
//   .     Don't fit, value computed from first principles
//   f     Fit this paramter, initial from feed forward
//   -     Don't fit, fixed value from feed forward
//
//std::vector<bool> SpectraPeaksBase::strToFitFlags_(std::string flags) const
//{
//    std::vector<bool> fitFlagVec;
//    fitFlagVec.assign(4,false);
//
//    if (flags.size() < 4 || (
//                (flags.find("X") != std::string::npos) ||
//                (flags.find("A") != std::string::npos) ||
//                (flags.find("D") != std::string::npos) ||
//                (flags.find("P") != std::string::npos) ||
//                (flags.find("N") != std::string::npos)) )
//    {
//
//        if(flags.find("X") != std::string::npos)
//            fitFlagVec[0] = true;
//
//        if(flags.find("A") != std::string::npos)
//            fitFlagVec[1] = true;
//
//        if(flags.find("D") != std::string::npos)
//            fitFlagVec[2] = true;
//
//        if(flags.find("P") != std::string::npos)
//            fitFlagVec[3] = true;
//    }
//    else
//    {
//        if((flags[0] == 'F') || (flags[0] == 'f'))
//            fitFlagVec[0] = true;
//
//        if((flags[1] == 'F') || (flags[1] == 'f'))
//            fitFlagVec[1] = true;
//
//        if((flags[2] == 'F') || (flags[2] == 'f'))
//            fitFlagVec[2] = true;
//
//        if((flags[3] == 'F') || (flags[3] == 'f'))
//            fitFlagVec[3] = true;
//    }
//
//    return fitFlagVec;
//}

// Index   Parameter
// -----------------
//   0     X (position)
//   1     A (area)
//   2     D (doppler width)
//   3     P (pressure width)
//
// Values
// ------
//   F     Fit this parameter, initial value from first principles
//   .     Don't fit, value computed from first principles
//   f     Fit this paramter, initial from feed forward
//   -     Don't fit, fixed value from feed forward
//

//std::vector<bool> SpectraPeaksBase::strToFitFlagsV2_(std::string flags) const
//{
//    std::vector<bool> fitFlagVec;
//
//    if(flags.size() < 4 || (flags.find("X") != std::string::npos)) {
//        std::cout << "match: " <<flags;
//    }
//
//    if((flags[0] == 'F') || (flags[0] == 'f'))
//        fitFlagVec[0] = true;
//
//    if((flags[1] == 'F') || (flags[1] == 'f'))
//        fitFlagVec[1] = true;
//
//    if((flags[2] == 'F') || (flags[2] == 'f'))
//        fitFlagVec[2] = true;
//
//    if((flags[3] == 'F') || (flags[3] == 'f'))
//        fitFlagVec[3] = true;
//
//    return fitFlagVec;
//}

void SpectraPeaksBase::deletePeaks()
{
    if(peaks_.size() > 0) {
        for(std::size_t i=0; i<peaks_.size(); i++)
            delete peaks_.at(i);
        delete baseline_;
    }
    peaks_.clear();
}

//void SpectraPeaksBase::setPeaks(
//    const PeaksINI & peaksINI,
//    const std::vector<double> & concForPW,
//    const std::vector<double> & waterConcForPW,
//    const std::vector<double> & concForArea,
//    const std::vector<double> & T_celsius,
//    const std::vector<double> & P_torr,
//    const std::vector<double> & estimatedTau,
//    bool doAssignMasterSlave,
//    bool onlyForeground)
//{
//    using namespace PhysicalConsts;
//    
//    deletePeaks();
//    
//    baseline_ = new Baseline(peaksINI.getBaselineModel(),
//        peaksINI.baselineParameters(), peaksINI.baselineFlags());
//    
//    // TODO: another loop over flanges for flange implementation
//    // REVIEW: How do we keep track of flange peaks turning on/off during this
//    // fit?
//    
//    for (int iPeak = 0; iPeak < peaksINI.numPeaks(); iPeak++)
//    {
//        const PeaksINI::PeakConstants & peak = peaksINI.peak(iPeak);
//        
//        double T_kelvin = T_celsius[iPeak] + 273.15;
//        // ----- Concentrations
//        // Previous concentration is INI concentration for the first fit, and
//        // a running average thereafter.
//        //
//        // We would prefer to use the previous concentration in the estimators,
//        // but when it becomes very small it causes errors in the area estimator
//        // so for small concentrations we use the INI value for area estimation.
//        //
//        
//        // ----- Center frequency estimator
//        // (The entire estimate comes from the INI.)
//        
//        double centerEstimate_GHz = peaksINI.peak(iPeak).center_GHz()
//            + peaksINI.peak(iPeak).shift_GHz()
//            - peaksINI.peak(peaksINI.zeroPeak()).center_GHz()
//            + peaksINI.shift_GHz();
//        
//        // ----- Doppler width estimator
//        //
//        
//        double dopplerWidthEstimate = peakDopplerWidth(
//            peak.isotopeCode(),
//            peak.center_GHz(),
//            T_kelvin);
//        
//        // ----- Pressure width estimator
//        // Use the previously-measured concentration unless doUsePreviousConc
//        // is false.
//        //
//        // Pressure widths are calculated with TOTAL concentrations.  Look them
//        // up using the peak's moleculeCode().
//        //
//        
//        double pressureWidthEstimate = peakPressureWidth(
//            concForPW[iPeak],
//            waterConcForPW[iPeak],
//            peak.isotopeCode(),
//            peak.gammaAir(),
//            peak.gammaSelf(),
//            peak.gammaWater(),
//            peak.tExponent(),
//            T_kelvin,
//            P_torr[iPeak]);
//        
//        // ----- Area estimator
//        //
//        // Areas are calculated with ISOTOPE concentrations.  Look them up using
//        // the peak's isotopeCode().
//        
//        
//        double areaEstimate = PhysicalConsts::peakArea(
//            peak.isotopeCode(),
//            T_kelvin,
//            P_torr[iPeak],
//            peak.epp(),
//            peak.adjustedLineStrength(),
//            concForArea[iPeak],
//            estimatedTau[iPeak]);
//        
//        
//        // ----- Create and push new peak
//        //
//        //
//        
//        // Wells 1999 is a voigt with the best balance of speed and accuracy.
//        const lineshape LINESHAPECODE = wells1999;
//        
//        std::string peaksIniLabel_unused = "";
//        
//        if (false == onlyForeground || false == peak.isFlange())
//        {
//            Peak* p = new Peak(peaksIniLabel_unused,
//                    iPeak,
//                    centerEstimate_GHz,
//                    areaEstimate,
//                    dopplerWidthEstimate,
//                    pressureWidthEstimate,
//                    peak.range_GHz(),
//                    peak.isotopeCode(),
//                    peak.evName(),
//                    LINESHAPECODE,
//                    peak.epp(),
//                    peak.adjustedLineStrength(),
//                    peak.lineStrengthFudgeFactor(),
//                    peak.doWantConcentration(),
//                    peak.gammaAir(),
//                    peak.gammaSelf(),
//                    peak.gammaWater(),
//                    peak.tExponent(),
//                    peak.fitFlags(),
//                    peak.microns());
//            
//            peaks_.push_back(p);
//        }
//    }
//    
//    // ----- Assign peak master-slave relationships
//    //
//    
//    if (doAssignMasterSlave)
//    {
//        if (onlyForeground)
//            assignForegroundMasterSlave(peaks_, peaksINI);
//        else
//            assignMasterSlave(peaks_, peaksINI);
//    }
//}


//void SpectraPeaksBase::setForegroundPeaks(
//    const PeaksINI & peaksINI,
//    const std::vector<double> & concForPW,
//    const std::vector<double> & waterConcForPW,
//    const std::vector<double> & concForArea,
//    const std::vector<double> & T_celsius,
//    const std::vector<double> & P_torr,
//    const std::vector<double> & estimatedTau,
//    bool doAssignMasterSlave)
//{
//    bool ONLY_FOREGROUND = true;
//    setPeaks(peaksINI, concForPW, waterConcForPW, concForArea,
//        T_celsius, P_torr, estimatedTau, doAssignMasterSlave,
//        ONLY_FOREGROUND);
//}
//    
//void SpectraPeaksBase::setBackgroundPeaks(
//    const PeaksINI & peaksINI,
//    const SpectraPeaksBase & copyFrom)
//{
//    for (int ii = peaksINI.numForegroundPeaks(); ii < copyFrom.numPeaks(); ii++)
//    {
//        assert(peaksINI.peak(ii).isFlange());
//        Peak* p = new Peak(copyFrom.getPeak(ii));
//        peaks_.push_back(p);
//    }
//}


void SpectraPeaksBase::assignMasterSlave(const std::vector<const PeaksINI::PeakConstants*> & peakConsts)
{
    assert(peaks_.size() == peakConsts.size()); // peakConsts should match peaks_
    
    // Make the master-slave map
    std::map<int, Peak*> peakMap;
    for (std::size_t ii = 0; ii < peakConsts.size(); ii++)
        peakMap[peakConsts[ii]->index()] = peaks_[ii];
    
    for (std::size_t masterIdx = 0; masterIdx < peakConsts.size(); masterIdx++)
    {
        const PeaksINI::PeakConstants & master = *peakConsts[masterIdx];
        const std::vector<int> & xSlaves = master.xSlaves();
        const std::vector<int> & areaSlaves = master.areaSlaves();
        const std::vector<int> & dopplerWidthSlaves = master.dopplerWidthSlaves();
        const std::vector<int> & pressureWidthSlaves = master.pressureWidthSlaves();
        
        for (std::size_t jj = 0; jj < xSlaves.size(); jj++)
        if (peakMap.count(xSlaves[jj]))
            peaks_[masterIdx]->addXSlavePeak(peakMap[xSlaves[jj]]);
        for (std::size_t jj = 0; jj < areaSlaves.size(); jj++)
        if (peakMap.count(areaSlaves[jj]))
            peaks_[masterIdx]->addAreaSlavePeak(peakMap[areaSlaves[jj]]);
        for (std::size_t jj = 0; jj < dopplerWidthSlaves.size(); jj++)
        if (peakMap.count(dopplerWidthSlaves[jj]))
            peaks_[masterIdx]->addDWSlavePeak(peakMap[dopplerWidthSlaves[jj]]);
        for (std::size_t jj = 0; jj < pressureWidthSlaves.size(); jj++)
        if (peakMap.count(pressureWidthSlaves[jj]))
            peaks_[masterIdx]->addPWSlavePeak(peakMap[pressureWidthSlaves[jj]]);
    }
    
}

//void SpectraPeaksBase::assignMasterSlave(std::vector<Peak*> & peaks,
//    const PeaksINI & peaksINI) const
//{
//    for (int masterIdx = 0; masterIdx < peaksINI.numPeaks(); masterIdx++)
//    {
//        const PeaksINI::PeakConstants & master = peaksINI.peak(masterIdx);
//        const std::vector<int> & xSlaves = master.xSlaves();
//        const std::vector<int> & areaSlaves = master.areaSlaves();
//        const std::vector<int> & dopplerWidthSlaves = master.dopplerWidthSlaves();
//        const std::vector<int> & pressureWidthSlaves = master.pressureWidthSlaves();
//        
//        for (int jj = 0; jj < xSlaves.size(); jj++)
//            peaks[masterIdx]->addXSlavePeak(peaks_[xSlaves[jj]]);
//        for (int jj = 0; jj < areaSlaves.size(); jj++)
//            peaks[masterIdx]->addAreaSlavePeak(peaks_[areaSlaves[jj]]);
//        for (int jj = 0; jj < dopplerWidthSlaves.size(); jj++)
//            peaks[masterIdx]->addDWSlavePeak(peaks_[dopplerWidthSlaves[jj]]);
//        for (int jj = 0; jj < pressureWidthSlaves.size(); jj++)
//            peaks[masterIdx]->addPWSlavePeak(peaks_[pressureWidthSlaves[jj]]);
//    }
//}
//
//void SpectraPeaksBase::assignForegroundMasterSlave(std::vector<Peak*> & peaks,
//    const PeaksINI & peaksINI) const
//{
//    // Only add relationships between foreground peaks.
//    int numFG = peaksINI.numForegroundPeaks();
//    
//    for (int masterIdx = 0; masterIdx < numFG; masterIdx++)
//    {
//        const PeaksINI::PeakConstants & master = peaksINI.peak(masterIdx);
//        assert(false == master.isFlange());
//        
//        const std::vector<int> & xSlaves = master.xSlaves();
//        const std::vector<int> & areaSlaves = master.areaSlaves();
//        const std::vector<int> & dopplerWidthSlaves = master.dopplerWidthSlaves();
//        const std::vector<int> & pressureWidthSlaves = master.pressureWidthSlaves();
//        
//        for (int jj = 0; jj < xSlaves.size(); jj++)
//        if (xSlaves[jj] < numFG)
//            peaks[masterIdx]->addXSlavePeak(peaks_[xSlaves[jj]]);
//        for (int jj = 0; jj < areaSlaves.size(); jj++)
//        if (areaSlaves[jj] < numFG)
//            peaks[masterIdx]->addAreaSlavePeak(peaks_[areaSlaves[jj]]);
//        for (int jj = 0; jj < dopplerWidthSlaves.size(); jj++)
//        if (dopplerWidthSlaves[jj] < numFG)
//            peaks[masterIdx]->addDWSlavePeak(peaks_[dopplerWidthSlaves[jj]]);
//        for (int jj = 0; jj < pressureWidthSlaves.size(); jj++)
//        if (pressureWidthSlaves[jj] < numFG)
//            peaks[masterIdx]->addPWSlavePeak(peaks_[pressureWidthSlaves[jj]]);
//    }
//}

void SpectraPeaksBase::resetBaseline(void)
{
    baseline_->resetParams();
}

// The fit doesn't do well with very weak peaks; sometimes they come out
// with negative areas (e.g. negative concentrations).  Call this method
// after a fit to set the area of those mal-fitted peaks to zero.
//
void SpectraPeaksBase::zeroOutNegativePeaks(void)
{
    for(std::size_t i=0; i<peaks_.size(); i++)
        if(peaks_.at(i)->getParam(AREA) < 0.0)
            peaks_.at(i)->setParam(0.0,AREA);
}

std::vector<double> SpectraPeaksBase::getParams() const
{
    std::vector<double> p = baseline_->getParams();
    
    for (std::size_t pp = 0; pp < peaks_.size(); pp++)
    {
        std::vector<double> peakParams = peaks_[pp]->getParams();
        p.insert(p.end(), peakParams.begin(), peakParams.end());
    }
    return p;
}

void SpectraPeaksBase::setParams(Vec_I_DP& a)
{
    int idx = 0;

    // Send baseline parameters
    //
    for(int ii = 0; ii < baseline_->getNumParams(); ii++)
    {
        // TODO: paramsLocalBuffer_ should be EXPLICITLY allocated big enough for the baseline params OR peak params!!!
        paramsLocalBuffer_.at(ii) = a[idx];
        idx++;
    }
    baseline_->setParams(paramsLocalBuffer_);
    
    for(std::size_t iPeak = 0; iPeak < peaks_.size(); iPeak++)
    {
        for(int ii = 0; ii < NUM_PEAKPARAM; ii++)
        {
            paramsLocalBuffer_[ii] = a[idx];
            idx++;
        }
        peaks_[iPeak]->setParams(paramsLocalBuffer_);
    }
}

void SpectraPeaksBase::setParams(std::vector<double>& a)
{
    int idx = 0;

    // Send baseline parameters
    //
    //int numParamsToSend = numParamsPerElement_[0];
    for(int ii = 0; ii < baseline_->getNumParams(); ii++)
    {
        paramsLocalBuffer_[ii] = a[idx];
        idx++;
    }
    baseline_->setParams(paramsLocalBuffer_);
    
    for (std::size_t iPeak = 0; iPeak < peaks_.size(); iPeak++)
    {
        for (int ii = 0; ii < NUM_PEAKPARAM; ii++)
        {
            paramsLocalBuffer_[ii] = a[idx];
            idx++;
        }
        peaks_[iPeak]->setParams(paramsLocalBuffer_);
    }
}

std::vector<std::string> SpectraPeaksBase::getParamNames(void)
{
    std::vector<std::string> outNames;

    // Get the baseline parameters
    //
    std::vector<std::string> paramNames = baseline_->getParamNames(); //HT till foreach

//    if (false == peaksIniLabel_.empty())
//    {
//        for(int ii = 0; ii < paramNames.size(); ii++)
//            paramNames[ii] += "_" + peaksIniLabel_; //TODO for loops can be merged
//    }
    for(std::size_t idx = 0; idx < paramNames.size(); idx++)
    {
    	outNames.push_back(paramNames[idx]);
    }
    
    // Loop through the vector of peaks, get the parameters for each peak,
    // and stick them onto the end of a_.
    //
    for(std::size_t iPeak = 0; iPeak < peaks_.size(); iPeak++)
    {
        paramNames.clear();
        std::stringstream iPeakString;
        iPeakString << iPeak;
        std::string temp = "_" + iPeakString.str(); // + peaksIniLabel_;
        std::vector<std::string> paramNames = peaks_.at(iPeak)->getParamNames(temp);
        for(std::size_t iPeakParam = 0; iPeakParam < paramNames.size(); iPeakParam++)
        {
        	outNames.push_back(paramNames[iPeakParam]);
        }
    }
    return outNames;
}


std::vector<double> SpectraPeaksBase::getBaselineParams() const
{
    return baseline_->getParams();
}

std::vector<double> SpectraPeaksBase::getPeakParams() const
{
    std::vector<double> p;
    
    for (std::size_t pp = 0; pp < peaks_.size(); pp++)
    {
        std::vector<double> peakParams = peaks_[pp]->getParams();
        p.insert(p.end(), peakParams.begin(), peakParams.end());
    }
    return p;
}

std::vector<bool> SpectraPeaksBase::getPrintFlag(void) {
    std::vector<bool> printFlag;

    // Get the baseline parameters
    //
    for(std::size_t i=0; i<baseline_->getParams().size(); i++)
        printFlag.push_back(true);

    for(std::size_t i=0; i<peaks_.size(); i++) {
        for(std::size_t j=0; j<peaks_.at(i)->getParams().size(); j++)
            printFlag.push_back(peaks_.at(i)->printFlag());
    }
    return printFlag;
}

// --- Fit flag methods ----------------------------------------------------
//
std::vector<bool> SpectraPeaksBase::getFitFlags(void) const
{
    std::vector<bool> fitFlags;

    // Get the baseline parameters
    //
    std::vector<bool> toAppend = baseline_->getFitFlags();
    fitFlags.insert(fitFlags.end(), toAppend.begin(), toAppend.end());
    
    // Loop through the vector of peaks, get the parameters for each peak,
    // and stick them onto the end of a_.
    //
    for(std::size_t iPeak = 0; iPeak < peaks_.size(); iPeak++)
    {
        toAppend = peaks_.at(iPeak)->getFitFlags(); //HT
        fitFlags.insert(fitFlags.end(), toAppend.begin(), toAppend.end());
    }
    return fitFlags;
}


// Displace all the peak centers by del_x.  Used for recentering the initial
// guess spectra before entering the mrqmin() fit.
//
void SpectraPeaksBase::shiftPeaks(double del_x)
{
    for(std::size_t i=0; i<peaks_.size(); i++)
        peaks_.at(i)->setParam( del_x + peaks_.at(i)->getParam(CENTER), CENTER);
}

void SpectraPeaksBase::getPeaksWithRange(std::vector<Peak*>& inVec) const
{
    std::vector<Peak*> pwr;
    for(std::size_t i=0; i<peaks_.size(); i++)
        if(peaks_.at(i)->getRange() > 0.0)
            pwr.push_back(peaks_.at(i));
    inVec = pwr;
}

void SpectraPeaksBase::getAllPeaks(std::vector<Peak*>& inVec) const
{
    inVec = peaks_;
}

const Peak & SpectraPeaksBase::getPeak(int index) const
{
    if (index < 0 || index >= numPeaks())
        throw(std::runtime_error("Index out of range"));
    
    Peak* p = peaks_.at(index);
    if (NULL == p)
        throw(std::runtime_error("Cannot dereference NULL peak pointer"));
    
    return *p;
}

double SpectraPeaksBase::baseline_f(double x, int xpt)
{
    return baseline_->f(x,xpt);
}


// ----------------------------- Print!

std::ostream & operator<<(std::ostream & stream, const SpectraPeaksBase & rhs)
{
    // Things I (PCH) might want to print out:
    //
    //  algorithm (NR, levmar unbounded, levmar box)
    //  upper and lower bounds
    //  reset every fit?
    //  levmar fit window start index, ghz vector, fit flags, bad chisq
    //  celsius, torr, tau, tau0, daqTime (?)
    //  baseline stuff
    //  peak vec (DEFINITELY print these)
    //
    
    for (std::size_t pp = 0; pp < rhs.peaks_.size(); pp++)
        stream << "\t" << pp << ": " << *rhs.peaks_[pp] << "\n";
    
    return stream;
}




// -----------------------------------------------------------------------------

OpenPathSpectraPeaks::OpenPathSpectraPeaks(const PeaksINI & peaksINI,
    const CalibrationINI & calINI) :
      SpectraPeaksBase(peaksINI, calINI)
{
//    std::cout << "Creating OpenPathSpectraPeaks\n";
}

double OpenPathSpectraPeaks::f(double x, int xpt) const
{
    // numerator = baseline
    // denominator = 1 + sum of peaks
    // y(x) = numerator/denominator
    //
    // NEW: y(x) = baseline * exp(-(sum of peaks)) PCH151117
    double baseline = baseline_->f(x,xpt);
    
    double peakSum = 0.0;
    
    for (std::size_t ii = 0; ii < peaks_.size(); ii++)
        peakSum += peaks_.at(ii)->f(x);
    
    double y = baseline * exp(-peakSum);
    return y;
}

double OpenPathSpectraPeaks::f(double x, int xpt, lineshape ls) const
{
    // numerator = baseline
    // denominator = 1 + sum of peaks
    // y(x) = numerator/denominator
    //
    // NEW: y(x) = baseline * exp(-(sum of peaks)) PCH151117
    double baseline = baseline_->f(x,xpt);
    
    double peakSum = 0.0;
    
    for (std::size_t ii = 0; ii < peaks_.size(); ii++)
        peakSum += peaks_.at(ii)->f(x, ls);
    
    double y = baseline * exp(-peakSum);
    return y;
}

double OpenPathSpectraPeaks::df(double x, int xpt)
{
    // numerator = baseline
    // denominator = 1 + sum of peaks
    // y(x) = numerator/denominator
    //
    // NEW: y(x) = baseline * exp(-(sum of peaks)) PCH151117
    
    double baseline = baseline_->f(x,xpt);
    
    double peakSum = 0.0;
    
    // TODO: this notation is so busted, need to stop calling df() to get the
    // value of f().  Completely misleading.  PCH151118
    for (std::size_t ii = 0; ii < peaks_.size(); ii++)
        peakSum += peaks_.at(ii)->df(x);
    
    double y = baseline * exp(-peakSum);
    return y;
}


// TODO: this function does what f() and df() do, it should not be copypasta. PCH151109
// this is the function with Voigt functions that is called repeatedly
// in levmarpeakfit.cpp.  Gives values and derivatives.
// -----------------------------------------------------------------------------
void OpenPathSpectraPeaks::funcs(const DP x, const int xpt, Vec_I_DP& /*a*/, DP& y,
    Vec_O_DP& dyda, Vec_I_BOOL& /*ia*/) {
    // CENTER, AMPLITUDE, DOPPLER_WIDTH, PRESSURE_WIDTH macros defined in
    // peak.h and included with spectra.h.
    //
    // Ordered definition of a and dyda:
    //  a[0..3] cubic baseline
    //  a[4..7] center, amplitude, doppler width, pressure width of peak 1
    //  a[8..11] "                                                " peak 2
    //  a[11..14] continue pattern for n peaks
    //http://forums.qrz.com/forumdisplay.php?7-Ham-Radio-Gear-For-Sale

    // Use mrqmin()'s parameters for this nth iteration to determine the
    // derivatives.
    //
    // Move setParams(a) call to mrqcof() in nrr.cpp since a only changes
    // once per spectra, not every x. This saves around 20% of the inner loop
    // execution time.
    //setParams(a);

    // calc denom = 1.0 + sum(peak(x)) and denom^2
    //
    
    // NEW: y(x) = baseline * exp(-(sum of peaks)) PCH151117
    //
    // Dy = Dbaseline * exp(-(sum of peaks)) + baseline * exp(-(sum of peaks)) * sum( - Dpeaks)
    
    
    double baseline = baseline_->f(x,xpt);
    
    double peakSum = 0.0;
    for (std::size_t ii = 0; ii < peaks_.size(); ii++)
        peakSum += peaks_.at(ii)->df(x);
    
    double expMinusSumOfPeaks = exp(-peakSum);
    y = baseline * expMinusSumOfPeaks;
    
    std::vector<double> Dbaseline = baseline_->dfda(x,xpt);
    std::vector<double> DsumOfVoigts(Dbaseline.size() + 4*peaks_.size(), 0.0);
    
    int daIdx = 4; // the first non-baseline parameter is #4
    for (std::size_t ii = 0; ii < peaks_.size(); ii++)
    {
        // Differentiate in order: center, area, doppler width, pressure width
        DsumOfVoigts[daIdx++] = peaks_.at(ii)->getdVdx();
//        std::cerr << DsumOfVoigts[daIdx-1] << "\n";
        DsumOfVoigts[daIdx++] = peaks_.at(ii)->getdVda();
//        std::cerr << DsumOfVoigts[daIdx-1] << "\n";
        DsumOfVoigts[daIdx++] = peaks_.at(ii)->getdVddw();
//        std::cerr << DsumOfVoigts[daIdx-1] << "\n";
        DsumOfVoigts[daIdx++] = peaks_.at(ii)->getdVdpw();
//        std::cerr << DsumOfVoigts[daIdx-1] << "\n";
    }
    
    for (std::size_t ii = 0; ii < Dbaseline.size(); ii++)
    {
        dyda[ii] = Dbaseline.at(ii) * expMinusSumOfPeaks +
                baseline * expMinusSumOfPeaks * (-1.0) * DsumOfVoigts.at(ii);
    }
    
    for (int ii = Dbaseline.size(); ii < dyda.size(); ii++)
    {
        dyda[ii] = baseline * expMinusSumOfPeaks * (-1.0) * DsumOfVoigts.at(ii);
    }
    
    for (std::size_t ii = 0; ii < peaks_.size(); ii++)
    {
        peaks_.at(ii)->resetDFFlag();
    }
}

double OpenPathSpectraPeaks::fWithoutBaseline(double x, int xpt) const
{
    // numerator = baseline
    // denominator = 1 + sum of peaks
    // y(x) = numerator/denominator
    //
    // NEW: y(x) = baseline * exp(-(sum of peaks)) PCH151117
    
    double peakSum = 0.0;
    
    for (std::size_t ii = 0; ii < peaks_.size(); ii++)
        peakSum += peaks_.at(ii)->f(x);
    
    double y = exp(-peakSum);
    return y;
}

// -----------------------------------------------------------------------------

ICOSSpectraPeaks::ICOSSpectraPeaks(const PeaksINI & peaksINI,
        const CalibrationINI & calINI) :
        SpectraPeaksBase(peaksINI, calINI)
{
    std::cout << "Creating ICOSSpectraPeaks\n";
}


// -----------------------------------------------------------------------------
double ICOSSpectraPeaks::f(double x, int xpt) const
{
    // numerator = baseline
    // denominator = 1 + sum of peaks
    // y(x) = numerator/denominator
    //
    // NEW: y(x) = baseline * exp(-(sum of peaks)) PCH151117
    double num = baseline_->f(x,xpt);
    double denom = 1.0;
    for(std::size_t i=0; i<peaks_.size(); i++)
        denom += peaks_.at(i)->f(x);
    return num/denom;
}

double ICOSSpectraPeaks::f(double x, int xpt, lineshape ls) const
{
    // numerator = baseline
    // denominator = 1 + sum of peaks
    // y(x) = numerator/denominator
    //
    // NEW: y(x) = baseline * exp(-(sum of peaks)) PCH151117
    double num = baseline_->f(x,xpt);
    double denom = 1.0;
    for(std::size_t i=0; i<peaks_.size(); i++)
        denom += peaks_.at(i)->f(x,ls);
    return num/denom;
}

double ICOSSpectraPeaks::df(double x, int xpt)
{
    // numerator = baseline
    // denominator = 1 + sum of peaks
    // y(x) = numerator/denominator
    //
    // NEW: y(x) = baseline * exp(-(sum of peaks)) PCH151117
    double num = baseline_->f(x,xpt);
    double denom = 1.0;
    for(std::size_t i=0; i<peaks_.size(); i++)
        // Calling df() will cache the derivatives as necessary and return f.
        // FIXME df() should return void or something else; follow with getV()
        // for clarity... but also why do we refer to V and also to f?  PCH151027
        denom += peaks_.at(i)->df(x); // df() returns V, just like f() PCH151027
    return num/denom;
}


// TODO: this function does what f() and df() do, it should not be copypasta. PCH151109
// this is the function with Voigt functions that is called repeatedly
// in levmarpeakfit.cpp.  Gives values and derivatives.
// -----------------------------------------------------------------------------
void ICOSSpectraPeaks::funcs(const DP x, const int xpt, Vec_I_DP& /*a*/, DP& y,
    Vec_O_DP& dyda, Vec_I_BOOL& /*ia*/) {
    // CENTER, AMPLITUDE, DOPPLER_WIDTH, PRESSURE_WIDTH macros defined in
    // peak.h and included with spectra.h.
    //
    // Ordered definition of a and dyda:
    //  a[0..3] cubic baseline
    //  a[4..7] center, amplitude, doppler width, pressure width of peak 1
    //  a[8..11] "                                                " peak 2
    //  a[11..14] continue pattern for n peaks

    // Use mrqmin()'s parameters for this nth iteration to determine the
    // derivatives.
    //
    // Move setParams(a) call to mrqcof() in nrr.cpp since a only changes
    // once per spectra, not every x. This saves around 20% of the inner loop
    // execution time.
    //setParams(a);

    // calc denom = 1.0 + sum(peak(x)) and denom^2
    //
    double denom = 1.0;
    for(std::size_t i=0; i<peaks_.size(); i++)
        // Cache peak derivatives and return V, aka f().  PCH151027
        // REVIEW deceptive function name df() does not return deriv. PCH151027
        denom += peaks_.at(i)->df(x);

    double denom_squared = denom * denom;
    double baseline = baseline_->f(x,xpt);
    y = baseline / denom;
    
    std::vector<double> dfda = baseline_->dfda(x,xpt);
    for(std::size_t i=0; i<dfda.size(); i++)
        dyda[i] = dfda.at(i)/denom;

    int daIdx = 4;
    double coeff = -baseline / denom_squared;
    for(std::size_t i=0; i<peaks_.size(); i++) {
        dyda[daIdx] = coeff * peaks_.at(i)->getdVdx(); // d/d(center)
        daIdx++;

        dyda[daIdx] = coeff * peaks_.at(i)->getdVda(); // d/d(area)
        daIdx++;

        dyda[daIdx] = coeff * peaks_.at(i)->getdVddw(); // d/d(doppler width)
        daIdx++;

        dyda[daIdx] = coeff * peaks_.at(i)->getdVdpw(); // d/d(pressure width)
        daIdx++;
    }
    for(std::size_t i=0; i<peaks_.size(); i++) {
        peaks_.at(i)->resetDFFlag();
    }
}


double ICOSSpectraPeaks::fWithoutBaseline(double x, int xpt) const
{
    // numerator = baseline
    // denominator = 1 + sum of peaks
    // y(x) = numerator/denominator
    //
    // NEW: y(x) = baseline * exp(-(sum of peaks)) PCH151117
    double num = 1.0;
    double denom = 1.0;
    for(std::size_t i=0; i<peaks_.size(); i++)
        denom += peaks_.at(i)->f(x);
    return num/denom;
}
