// SpectraFitContainer
//
// With the addition of n-by-n region fitting, passing the plot data from
// the fitting code to the plotting code has become very cumbersome. This
// class will contain all that data along with sufficent information
// to make displaying multiple fits on a single plot easy.
//
#ifndef SPECTRAFITCONTAINER_H
#define SPECTRAFITCONTAINER_H

#include <iostream>
#include <vector>
#include <string>
#include <stdlib.h>
#include <sstream>
#include <map>


/**
 * Hold all results from the fitting process.
 */
class NewSpectrumContainer
{
public:
    NewSpectrumContainer();
//    NewSpectrumContainer(int laserIdx);
    
    void setFrequencies(const std::vector<double> & freqs);
    void setBaseline(const std::vector<double> & baseline);
    void setInitialModel(const std::vector<double> & initialModel);
    void setEstimatedModel(const std::vector<double> & estimatedModel);
    void setFinalModelFit(const std::vector<double> & finalModelFit);
    void setFitWindow(const std::vector<int> & fitWindow);
//    void setLaserIndex(int laserIdx);
    
    const std::vector<double> & frequencies() const { return xGHz_; }
    const std::vector<double> & baseline() const { return baseline_; }
    const std::vector<double> & initialModel() const { return initialModel_; }
    const std::vector<double> & estimatedModel() const { return estimatedModel_; }
    const std::vector<double> & finalModelFit() const { return finalModelFit_; }
    const std::vector<int> & fitWindow() const { return fitWindow_; }
//    int laserIndex() const { return laserIdx_; }
private:
    // These span the entire laser ramp region and are all the same length.
    std::vector<double> xGHz_;              // from the etalon function
    std::vector<double> baseline_;
    std::vector<double> initialModel_;
    std::vector<double> estimatedModel_;
    std::vector<double> finalModelFit_;
    
    std::vector<int> fitWindow_;
    
    // Laser index stored for easy identification purposes
//    int laserIdx_;
};

/**
 * The need for this class is extremely questionable.
 *
 * Holds tau, offset, and the values of the ringdown fit exponential.
 * This is NOT the measured ringdown data.
 */
class NewRingdownContainer
{
public:
    NewRingdownContainer();
    NewRingdownContainer(int laserIdx);
    
    void setModelTrace(const std::vector<double> & modelTrace);
    void setTau(double tau); // microseconds
    void setOffset(double offset);
    void setLaserIndex(int laserIdx);
    
    const std::vector<double> & modelTrace() const { return ringdownModel_; }
    double tau() const { return tau_; } // microseconds
    double offset() const { return offset_; }
    int laserIndex() const { return laserIdx_; }
private:
    std::vector<double> ringdownModel_;
    double tau_; // microseconds
    double offset_; // need to see where this is used.
    
    int laserIdx_;
};


// ============================================================================
// This encapsulates the data and results for one fit.
class OneFitContainer {
public:
    OneFitContainer();
    OneFitContainer(std::string label);
    void resetVectors(void);
    int getLaserIdx(void) { return laserIdx; }

    std::string fitLabel; // e.g. "010"
    int laserIdx; // Is this data for laser 0 or 1 (i.e. first or second laser)

    std::vector<double> ringdownData;
    std::vector<double> ringdownFit;

    std::vector<double> xghz;
    std::vector<double> baseline;
    std::vector<double> initFit;
    std::vector<double> estFit;
    std::vector<double> fit;

    std::vector<int> fitWindow; // fitWindow[0] = start pt, fitWindow[1] = length

private:
};

/**
 * Store the analyzed results of all spectral fits.
 *
 * This class stores time constants tau, offsets, separated ringdown and
 * spectral traces, baselines and fits and all that.
 *
 * The results are held for each laser, each pass and each fit index.
 *
 * Hardcoded for two lasers and three passes!!!
 */
class SpectraFitContainer
{
public:
    SpectraFitContainer(std::vector<std::string> spectrumNames);
    void setSpectralData(int laserIdx,
                         double tau,
                         double offset,
                         std::vector<double> rdTrace,
                         std::vector<double> laserRampTrace);
    double getTau(int laserIdx) { return tau[laserIdx]; }
    double getOffset(int laserIdx) { return offset[laserIdx]; }
    std::vector<double> getRDTrace(int laserIdx) { return ringdownTrace[laserIdx]; }
    std::vector<double> getLaserRampTrace(int laserIdx) { return laserRampTrace[laserIdx]; }

    void setFitData(int laserIdx,
                    int passIdx,
                    int fitIdx,
                    std::vector<int> fitWindow,
                    std::vector<double> xghz,
                    std::vector<double> baseline,
                    std::vector<double> initFit,
                    std::vector<double> estFit,
                    std::vector<double> bestFit);

    std::vector<int> getFitWindow(int laserIdx, int passIdx, int fitIdx);
    std::vector<double> getXghz(int laserIdx, int passIdx, int fitIdx);
    std::vector<double> getBaseline(int laserIdx, int passIdx, int fitIdx);
    std::vector<double> getInitFit(int laserIdx, int passIdx, int fitIdx);
    std::vector<double> getEstFit(int laserIdx, int passIdx, int fitIdx);
    std::vector<double> getFit(int laserIdx, int passIdx, int fitIdx);

    //void initContainers(std::vector<std::string> list);
    //void resetContainers(void);

    void setWinner(int laserIdx, int passIdx, int fitIdx);
    std::vector<int> getWinner(int laserIdx, int passIdx);
    
    // makeKey and splitKey are never used outside the class
    static std::string makeKey(int laserIdx, int passIdx, int fitIdx);
    static std::vector<int> splitKey(std::string key);

private:
    /**
     * This is the constructor.
     */
    void initVectors_(std::vector<std::string> list);

    std::map<std::string,OneFitContainer> fits;

    // Ringdown fit results for laser 0 and laser 1
    std::vector<double> tau;
    std::vector<double> offset;

    // Ringdown trace (the plot)
    std::vector< std::vector<double> > ringdownTrace;

    // Spectra to fit from laser 0 and laser 1
    std::vector< std::vector<double> > laserRampTrace;

    // Keep track of the fit "winners"
    // firstPassWinner[0] : key of first pass laser 0 winner
    // firstPassWinner[1] : key of first pass laser 1 winner
    //
    std::vector<std::string> firstPassWinner;
    std::vector<std::string> secondPassWinner;
    std::vector<std::string> thirdPassWiner;

};

#endif // SPECTRAFITCONTAINER_H
