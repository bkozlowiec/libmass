//
//  calibration_ini.cpp
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 2/13/16.
//
//

#include "calibration_ini.h"

#include <fstream>
#include <sstream>
#include <string>

CalibrationINI::CalibrationINI()
{
}

CalibrationINI::CalibrationINI(const std::string & fileName) :
    fileName_(fileName)
{
	updateCalibrationIni();
}

CalibrationINI::CalibrationINI(const StructCalibrationIni & srtCalIni)
{
	updateCalibrationIni(srtCalIni);
}

CalibrationINI::~CalibrationINI()
{
}

void CalibrationINI::updateCalibrationIni(const StructCalibrationIni & structCalIni)
{
	lineStrengthFudge_.clear();
	lineStrengthFudge_.reserve(structCalIni.lineStrengthFudge.size());
	lineStrengthFudge_ = structCalIni.lineStrengthFudge;
	areaCoefficients_.clear();
	areaCoefficients_.reserve(structCalIni.lineStrengthFudge.size());
	calConcv_.clear();
	calConcv_.reserve(structCalIni.lineStrengthFudge.size());
	timev_.clear();
	timev_.reserve(structCalIni.lineStrengthFudge.size());
}

void CalibrationINI::clear()
{
    lineStrengthFudge_.clear();
    areaCoefficients_.clear();
    calConcv_.clear();
    timev_.clear();
}

void CalibrationINI::updateCalibrationIni()
{
    int idx;
    time_t time;
    double lsf, ac0, ac1, ac2, calConc;
    
    std::ifstream calibrationFile;
    calibrationFile.open(fileName_.c_str());
    std::string rlstr;
    if (calibrationFile.is_open())
    {
        while ( getline (calibrationFile, rlstr) )
        {
            if(rlstr.length() < 1 || rlstr.at(0) == '#') {
                // Skip blank lines or comments (lines starting with #)
            }else{
                std::stringstream ss;
                ss << rlstr;
                std::string str;

                // Read a key and convert to uppercase
                ss >> std::uppercase >> str;
//                std::transform(str.begin(), str.end(), str.begin(), std::toupper);
                //str = str.toUpper();
                if(str.compare("PEAK") == 0) {
                    std::vector<double> d;
                    ss >> idx >> lsf >> ac0 >> ac1 >> ac2 >> calConc >> time;
                    lineStrengthFudge_.push_back(lsf);
                    calConcv_.push_back(calConc);
                    timev_.push_back(time);
                    d.push_back(ac0);
                    d.push_back(ac1);
                    d.push_back(ac2);
                    areaCoefficients_.push_back(d);
                }
            }
        }
        calibrationFile.close();
    }

}
//
//void CalibrationINI::write() const
//{
//    std::ofstream file;
//    file.open(fileName_);
//
//    // Header
//    //
//    file << "#\tIDX LSF AC0  AC1  AC2\tREF[PPM]\tDATE\n";
//    file << "#------------------------------------------------------\n";
//
////    PEAK	 4 8.29528925e-01 0 1 0  2.34000000e+00 1415255929 (11/05/2014)
//
//    for(int i=0; i < numPeaks(); i++) {
//        std::stringstream outStream;
//        double lsf = peaks_.at(i)->getLSFudge();
////        double width = 12; // unused
////        double numDigits = 8; // unused
//
//        std::stringstream lsfStrStream;
//        lsfStrStream.precision(8);
//        lsfStrStream.width(12);
//        lsfStrStream << std::scientific << lsf;
//        std::string lsfStrS = lsfStrStream.str();
//
//        double calConc = peaks_.at(i)->getCalConc();
//        std::stringstream calConcStrStream;
//        calConcStrStream.precision(8);
//        calConcStrStream.width(12);
//        calConcStrStream << std::scientific << calConc;
//        std::string  calConcStrS = calConcStrStream.str();
//
//        outStream << "PEAK" << '\t';
//        outStream.width(2);
//        outStream << i ;
//        outStream.width(0);
//        outStream << " " << lsfStrS << " ";
//
//        for(int j=0; j< areaCoefficients_.at(i).size(); j++)
//        {
//            outStream << areaCoefficients_.at(i).at(j) << " ";
//        }
//
//         outStream << " " << calConcStrS << " "
//            << peaks_.at(i)->getCalTime() << " ("
//            << peaks_.at(i)->getLocalCalTimeStr(1) << ")\n";
//         std::string outStr = outStream.str();
//
//        file << outStr;
//    }
//
//    file.close();
//}
