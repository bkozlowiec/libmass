//---------------------------------------------------------------------------
//
// There are several references for the voigt functions and voigt derivatives
// used in this code.
//
// XGUAPROF_1999
// -------------
// auth. unk., "XGAUPROF: A GIPSY program to fit parameters of a Gauss function,
// the Gauss-Hermite series or a Voigt line-shape to data from GIPSY sets or
// ASCII files". Unpublished manuscript. http://www.astro.rug.nl/~gipsy/
// xgauprof/xgauprof.ps. Contains formulas for the analytic derivatives
// used in this code.
//
// HUMLICEK_1999
// -------------
// J. Humlicek, "Journal of Quantitative Spectroscopy & Radiative Transfer",
// vol 21 (1979).

// WELLS_1999
// ----------
// R. J. Wells, "Rapid Approximation to the Voigt/Faddeeva Function and its
// Derivatives", JQSRT 62 (1999), pp. 29-48. Manuscript and code is available
// at http://www.atm.ox.ac.uk/user/wells/voigt.html. This approximation of
// the voigt function is significantly faster the the Humlicek code but
// accurate enough for our purposes.
//

#include <cmath>
#include <iostream>
#include <sstream>
#include <cassert>
#include <stdexcept>
#include "../utils/physicalConsts.h"
#include "peak.h"
#include "../utils/lineshapes.h"


//---------------------------------------------------------------------------
// Class expects the following units:
// GHz - center, area, dopplerWidth, pressureWidth
// cm-1 - epp (E", ground state energy)
//
// Update:
// PW changed to use air and self broadening formulation from HITRAN
//
Peak::Peak(std::string peakIniName,
    int idx,
    double center, double area, double dopplerWidth, double pressureWidth,
    double range,
    std::string isotopeCode,
    std::string molName,
    std::string evName,
    lineshape ls, double epp, double lineStrength, double lsf, bool wantConc,
    double gAir, double gSelf, double gH2O, double tExp,
    std::vector<bool> fitFlags,
    double microns, double press_shift_cm1) :
    microns_(microns),
    range_(range),
    isotopeCode_(isotopeCode),
    molName_(molName),
    evName_(evName),
    lineshape_(ls), epp_(epp), lineStrength_(lineStrength),
    lsFudge_(lsf), wantConc_(wantConc),
    gammaAir_(gAir), gammaSelf_(gSelf), gammaH2O_(gH2O), tExp_(tExp),
    V_(0.0), dVda_(0.0), dVdx_(0.0), dVddw_(0.0), dVdpw_(0.0),
    isXSlave_(false), isAreaSlave_(false), isDWSlave_(false),
    isPWSlave_(false), masterSlaveAreaRatio_(1.0),
    dfComputed_(false), press_shift_cm1(press_shift_cm1)

{
    _peakId_ = "ini:" + peakIniName + " idx:";
    std::stringstream idxStr;
    idxStr << idx;
    _peakId_.append(idxStr.str());

    params_.assign(NUM_PEAKPARAM, 0);
    
    params_[CENTER] = center;
    params_[AREA] = area;
    params_[DW] = dopplerWidth;
    params_[PW] = pressureWidth;
    
    fitFlags_.assign(NUM_PEAKPARAM, false);
    fitFlags_[CENTER] = fitFlags.at(CENTER);
    fitFlags_[AREA] = fitFlags.at(AREA);
    fitFlags_[DW] = fitFlags.at(DW);
    fitFlags_[PW] = fitFlags.at(PW);
    
    xSlavePeaks_.clear();
    areaSlavePeaks_.clear();
    dwSlavePeaks_.clear();
    pwSlavePeaks_.clear();
}


Peak::Peak(const Peak & p) :
    microns_(p.microns_),
    range_(p.range_),
    isotopeCode_(p.isotopeCode_),
    molName_(p.molName_),
    evName_(p.evName_),
    _peakId_(p._peakId_),
    lineshape_(p.lineshape_),
    epp_(p.epp_),
    lineStrength_(p.lineStrength_),
    lsFudge_(p.lsFudge_),
    wantConc_(p.wantConc_),
    gammaAir_(p.gammaAir_),
    gammaSelf_(p.gammaSelf_),
    gammaH2O_(p.gammaH2O_),
    tExp_(p.tExp_),
    params_(p.params_),
    fitFlags_(p.fitFlags_),
    xSlavePeaks_(),
    areaSlavePeaks_(),
    dwSlavePeaks_(),
    pwSlavePeaks_(),
    isXSlave_(false),
    isAreaSlave_(false),
    isDWSlave_(false),
    isPWSlave_(false),
    dfComputed_(false),
    press_shift_cm1(p.press_shift_cm1)
{
}


// --- Parameter and descriptor methods -------------------------------------
//
//


// Do not call these directly. They are used by
// add<...>SlavePeak()
//
void Peak::setXSlave(void) { isXSlave_ = true; }
void Peak::setAreaSlave(void) { isAreaSlave_ = true; }
void Peak::setDWSlave(void) { isDWSlave_ = true; }
void Peak::setPWSlave(void) { isPWSlave_ = true; }

// These 4 methods are only called by a master peak.
//
void Peak::addXSlavePeak(Peak* p)
{
    xSlavePeaks_.push_back(p);
    p->setXSlave();
    p->setCenterOffset(p->getCenter() - getCenter());
}

void Peak::addAreaSlavePeak(Peak* p)
{
    areaSlavePeaks_.push_back(p);
    p->setAreaSlave();
    p->setAreaRatio(p->getArea() / getArea());
//    p->setAreaRatio(p->originalArea_ / originalArea_);
}

void Peak::addDWSlavePeak(Peak* p)
{
    dwSlavePeaks_.push_back(p);
    p->setDWSlave();
    p->setDWRatio(p->getDopplerWidth() / getDopplerWidth());
    //p->setDWRatio(p->dopplerWidthCoefficient_ / dopplerWidthCoefficient_);
}

void Peak::addPWSlavePeak(Peak* p)
{
    pwSlavePeaks_.push_back(p);
    p->setPWSlave();
    p->setPWRatio(p->getPressureWidth() / getPressureWidth());
}

void Peak::setParams(std::vector<double>& a)
{
    if(!isXSlave_)
        params_[CENTER] = a.at(CENTER);
    if(!isAreaSlave_)
        params_[AREA] = a.at(AREA);
    if(!isDWSlave_)
        params_[DW] = a.at(DW);
    if(!isPWSlave_)
        params_[PW] = a.at(PW);
    setSlaveParams_();
}

// Does not need concentration values
void Peak::setSlaveParams_(void)
{
    for (std::size_t i=0; i<xSlavePeaks_.size(); i++)
    {
        //double delx = xSlavePeaks_.at(i)->getOrigParam(CENTER) - oa_.at(CENTER);
//        double delx = xSlavePeaks_.at(i)->originalCenter_ - originalCenter_;
        double delx = xSlavePeaks_.at(i)->getCenterOffset();
        xSlavePeaks_.at(i)->setParam( delx + params_.at(CENTER), CENTER );
    }
    for (std::size_t i=0; i<areaSlavePeaks_.size(); i++)
        areaSlavePeaks_.at(i)->setParam( areaSlavePeaks_.at(i)->getAreaRatio() * params_.at(AREA), AREA );
    for (std::size_t i=0; i<dwSlavePeaks_.size(); i++)
        dwSlavePeaks_.at(i)->setParam( dwSlavePeaks_.at(i)->getDWRatio() * params_.at(DW), DW );
    for (std::size_t i=0; i<pwSlavePeaks_.size(); i++)
        pwSlavePeaks_.at(i)->setParam( pwSlavePeaks_.at(i)->getPWRatio() * params_.at(PW), PW );
}

std::vector<double> Peak::getParams() const
{
    return params_;
}

std::vector<std::string> Peak::getParamNames(std::string num) const
{
    std::vector<std::string> n;
    if(fitFlags_.at(CENTER))
        n.push_back(evName_ + num + "_CT");
    else
        n.push_back(evName_ + num + "_CF");

    if(fitFlags_.at(AREA))
        n.push_back(evName_ + num + "_AT");
    else
        n.push_back(evName_ + num + "_AF");

    if(fitFlags_.at(DW))
        n.push_back(evName_ + num + "_DT");
    else
        n.push_back(evName_ + num + "_DF");

    if(fitFlags_.at(PW))
        n.push_back(evName_ + num + "_PT");
    else
        n.push_back(evName_ + num + "_PF");
    return n;
}

double Peak::getParam(int paramName, bool /*adjust*/) const
{
    double param = 0;
    switch( paramName ) {
    case CENTER:
        param = params_.at(CENTER);
        break;
    case AREA:
        param = params_.at(AREA);
        //if(adjust)
        //    param *= ac_.at(0)/param + ac_.at(1) + ac_.at(2)*param;
        break;
    case DW:
        param = params_.at(DW);
        break;
    case PW:
        param = params_.at(PW);
        break;
    default:
        std::cout << "Param type undefined.";
        break;
    }
    return param;
}

void Peak::setParam(double a, int paramName) {
    switch( paramName ) {
    case CENTER:
        params_[CENTER] = a;
        break;
    case AREA:
        params_[AREA] = a;
        break;
    case DW:
        params_[DW] = a;
        break;
    case PW:
        params_[PW] = a;
        break;
    default:
        std::cout << "Param type undefined.";
        break;
    }
}

// --- Fit flag methods ---------------------------------------------------
//
void Peak::setFitFlags(std::vector<bool> ia)
{
    fitFlags_[CENTER] = ia.at(CENTER);
    fitFlags_[AREA] = ia.at(AREA);
    fitFlags_[DW] = ia.at(DW);
    fitFlags_[PW] = ia.at(PW);
}

std::vector<bool> Peak::getFitFlags(void) const
{
    return fitFlags_;
}

// If SNR of this peak (as estimated in nrFitFactoryT.h) is very low, we need
// turn-off parameters in the fit to keep the fit from blowing up.  Without this
// peaks amplitudes and positions can jump around as the fit tries to find
// the peak lost in the noise.  Ideally, the determination of which paramteters
// fix should be set in nrFitFactory as this more properly belongs in the
// fit domain and not in the peak definition.  However, nrFitFactory is generic
// with respect to the objects it is fitting.  What I mean is that peak.cpp
// has 4 fit flags and basis.cpp has 3 resulting in the necessity of using RTTI
// for nrFitFactory to know how it should change the fit flags. Using RTTI is
// messy and is not recommended according to the references I have.
//
// Our heuristic is that if the peak is barely visible, we fix both width terms
// and the position. If it is lost in the noise, nothing is fitted and we
// use the estimated amplitude.
//
void Peak::setFitFlagsForLowSNR(double snr)
{
    std::vector<bool> ia;
//    ia.fill(false,4);  //HO
    ia.assign(4, false);
    if(snr <=5.0 && snr > 3.0) {
        ia[CENTER] = ia[AREA] = true;
        setFitFlags(ia);
        //setFitFlags(true,true,false,false);
    }else{
        ia[AREA] = true;
        setFitFlags(ia);
        //setFitFlags(false,true,false,false);
    }
}

// --- Peak plotting and fitting methods -------------------------------------
//
// Formulas can be referenced to XGAUPROF_1999. Where solitary number appears
// in a comment, this is a cross reference to the formula in the above
// document.
//
// TODO consolidate with f(x, shape), this is redundant.  PCH151027
double Peak::f(const double x) const
{
    // Calc. master peak
    //
    
    // k, l: real and imaginary parts of Voigt function PCH151025
    double k, l, d1, d2, d3;
    double xCenter = params_.at(CENTER);
    double area = params_.at(AREA);
    double inverseDopplerWidth = 1.0/params_.at(DW);
    double pressureWidth = params_.at(PW);

    double normX = sqrt_ln2 * (x - xCenter)*inverseDopplerWidth;     // 1.5
    double normY = sqrt_ln2 * pressureWidth * inverseDopplerWidth;         // 1.5
    double scale = sqrt_ln2_pi * area * inverseDopplerWidth;    // 1.4
    
    if(lineshape_ == wells1999) {
        Lineshapes::humdev( normX, normY, k, d1, d2, d3 );
        //humlik_(normX, normY);
    }else if(lineshape_ == humlicek1979) {
        Lineshapes::voigt_c(normX,normY,k,l);
    }else if(lineshape_ == mclean1997) {
        k = Lineshapes::vs(normX,normY);
    }else if(lineshape_ == lorentz) {
        k = Lineshapes::lorentz(normX,normY);
    }
    else
    {
        throw std::runtime_error("Unknown lineshape");
    }
    return scale * k;                           // 1.4
}

double Peak::f(const double x, const lineshape shape) const
{
    // Calc. master peak
    //
    double k, l, d1, d2, d3;
    double xo = params_.at(CENTER);
    double area = params_.at(AREA);
    double idw = 1.0/params_.at(DW);
    double pw = params_.at(PW);

    double normX = sqrt_ln2 * (x - xo)*idw;     // 1.5
    double normY = sqrt_ln2 * pw * idw;         // 1.5
    double scale = sqrt_ln2_pi * area * idw;    // 1.4

    if(shape == wells1999) {
        Lineshapes::humdev( normX, normY, k, d1, d2, d3 );
        //humlik_(normX, normY);
    }else if(shape == humlicek1979) {
        Lineshapes::voigt_c(normX,normY,k,l);
    }else if(shape == mclean1997) {
        k = Lineshapes::vs(normX,normY);
    }else if(shape == lorentz) {
        k = Lineshapes::lorentz(normX,normY);
    }
    else
    {
        throw std::runtime_error("Unknown lineshape");
    }
    return scale * k;                           // 1.4
}

// How we tie/constrain parameters:
// In many spectra, we will have more than one peak of a given species but
// at least some will be weak and difficult to fit. If we fit the small
// peaks as independent entities, the fitted may have large errors and
// may introduce noise into the fit of the larger, desired peak. In this
// case we may want to constrain parameters of the smaller peak relative
// to a larger peak. For example, we may fixed the frequency spacing between
// peaks, or their intensity.
//
// We define a master peak and its slaves. The master is fitted explicity
// but the slaves are only fitted implicitly. When the mrqmin() fit routine
// needs to compute the derivatives for the purpose of parameter optimization,
// each peak is computed in turn. If we have masters and slaves, when it's
// the master's turn to compute derivatives, it does its own and those of
// the slaves it owns. When it's a slave's turn, it only computes the
// derivatives of those parameters not owned by a master.
//
// Computing derivatives of slaves is relatively straight forward. Let us
// say we have two H2O peaks, with the same ground state energy, and that
// one is very weak. We choose to constrain the relative size of the two
// peaks.
//
// So if the spectra is modeled as
//
//      T(v) = T_master(v) + T_slave(v)
//
//      T(v) = (A0/gd0)*sqrt(ln2/pi)*K(x0,y0) + (A1/gd1)*sqrt(ln2/pi)*K(x1,y1)
//
// where T(v) is the transmission as a function of frequency v, A0 is peak 0
// area, gd0 is the doppler width of peak 0, x0 is the normalized line
// position of peak 0, and y0 is the normalized line width of peak 0.
//
// If we fix the relative areas of the two peaks, the slave peak area can
// be written as the master peak area multiplied by a constant r.
//
//      T(v) = (A0/gd0)*sqrt(ln2/pi)*K(x0,y0) + (r*A0/gd1)*sqrt(ln2/pi)*K(x1,y1)
//
// Computing the derivative of the total transmission with respect to A0
// yields
//
//      dT(v)/dA0 = (1/gd0)*sqrt(ln2/pi)*K(x1,y1) + (r/gd1)*sqrt(ln2/pi)*K(x1,y1)
//
//      dT(v)/dA0 = dT_master(v)/dA + r*dT_slave(v)/dA
//
// dT_*(v)/dA are are the derivatives that come out of humdev_() for each
// peak.
//
// When the code wants to compute slave derivatives we have
//
//      dT(v)/dA1 = 0
//
// because this code is skipped as the master has already computed the
// derivative for this peak.
//
// The master/slave relationship of the other parameters are treated likewise
// but not derived here because the forumlas are too long for the comments
// field.
//
// Formulas can be referenced to XGAUPROF_1999. Where solitary number appears
// in a comment, this is a cross reference to the formula in the above
// document.
//
double Peak::df(const double x)
{
    if(dfComputed_)
        return V_;
    
    double k, l, dkdx, dkdy;  // k = re{Voigt}, l = im{Voigt}
    double x_center = params_.at(CENTER);
    double area = params_.at(AREA);
    double inverse_dw = 1.0/params_.at(DW); // inverse doppler width
    double pw = params_.at(PW);

    // Input to all four lineshape models is a normalized and recentered
    // complex frequency X + iY.
    //
    // X: freq. relative to center freq, divided by doppler width
    // Y: \propto pressure width / doppler width
    //
    // PCH151027
    double X_normalized = sqrt_ln2 * (x - x_center) * inverse_dw;      // 1.5
    double Y_normalized = sqrt_ln2 * pw * inverse_dw;                  // 1.5
    double scaleFactor = sqrt_ln2_pi * area * inverse_dw;              // 1.4
    
    if(lineshape_ == wells1999) {
        Lineshapes::humdev( X_normalized, Y_normalized, k, l, dkdx, dkdy );
    }else if(lineshape_ == humlicek1979) {
        Lineshapes::voigt_c(X_normalized,Y_normalized,k,l);
        dkdx = -2.0*X_normalized*k;
        dkdy = -2.0/sqrt(pi) + 2.0*Y_normalized*k;
    }else if(lineshape_ == mclean1997) {
        k = Lineshapes::vs(X_normalized,Y_normalized);
        dkdx = -2.0*X_normalized*k;
        dkdy = -2.0/sqrt(pi) + 2.0*Y_normalized*k;
    }else if(lineshape_ == lorentz) {
        k = Lineshapes::lorentz(X_normalized,Y_normalized);
        dkdx = -2.0*X_normalized*k;
        dkdy = -2.0/sqrt(pi) + 2.0*Y_normalized*k;
    }else{
    	k = l = dkdx = dkdy = 0.0;
    }

    // Cache the partial derivatives and the peak value itself. PCH151027
    
    dVda_ = inverse_dw * sqrt_ln2_pi * k;                              // 4.14
    dVdx_ = scaleFactor * dkdx * -inverse_dw * sqrt_ln2;               // 4.15
    dVdpw_ = scaleFactor * dkdy * inverse_dw * sqrt_ln2;               // 4.16
    dVddw_ = -scaleFactor * inverse_dw *
        (k + X_normalized*dkdx + Y_normalized*dkdy);                   // 4.17
    V_ = scaleFactor * k;
    dfComputed_ = true;

    // If this is a master peak and it owns slaves, get slave
    // derivatives for each tied parameter, scale it appropriately,
    // and add it to the master peak derivative.
    //
    // Note we are probably calling df() too many times so
    // we'll need to rethink how we do our slave peak bookkeeping
    // if the performance is poor.
    for(std::size_t i=0; i<xSlavePeaks_.size(); i++) {
        xSlavePeaks_.at(i)->df(x);
        dVdx_ += xSlavePeaks_.at(i)->getdVdx();
    }
    for(std::size_t i=0; i<areaSlavePeaks_.size(); i++) {
        areaSlavePeaks_.at(i)->df(x);
        dVda_ += areaSlavePeaks_.at(i)->getdVda() * areaSlavePeaks_.at(i)->getAreaRatio();
    }
    for(std::size_t i=0; i<dwSlavePeaks_.size(); i++) {
        dwSlavePeaks_.at(i)->df(x);
        dVddw_ += dwSlavePeaks_.at(i)->getdVddw() * dwSlavePeaks_.at(i)->getDWRatio();
    }
    for(std::size_t i=0; i<pwSlavePeaks_.size(); i++) {
        pwSlavePeaks_.at(i)->df(x);
        dVdpw_ += pwSlavePeaks_.at(i)->getdVdpw() * pwSlavePeaks_.at(i)->getPWRatio();
    }
    return V_;
}


// uses tExp_
//double Peak::getPWTemperatureCoefficient(double P_torr, double T_celsius) const
//{
//    return PhysicalConsts::getPWTemperatureCoefficient(T_celsius + 273.15, P_torr, tExp_);
//}


// Friendship is magic

std::ostream& operator<<(std::ostream & str, const Peak & rhs)
{
    str << "center " << rhs.getCenter()
        << " area " << rhs.getArea()
        << " dw " << rhs.getDopplerWidth()
        << " pw " << rhs.getPressureWidth();
    return str;
}

