//---------------------------------------------------------------------------

#ifndef peakH
#define peakH
//---------------------------------------------------------------------------
#include <ctime>
#include "../utils/physicalConsts.h"
#include <vector>

using namespace Constants;

//
// For spectra, we don't need the imaginary components of the voigt function.
// define NO_IMAGINARY to save a few cpu cycles
//
#define NO_IMAGINARY

// This enum replaces the #defines for CENTER, AREA, etc.
// The last element is a bit of a trick I learned.  I want to know the number of
// elements so that I can loop through the four attributes for each peak when
// constructing arrays to pass to mrqmin().  However, there is no method that
// returns the number of elements.  So what I do is define an extra element
// and put it at the end of the list. It's default value, 4 in this case,
// tells me that there are four peak parameters. It would be the same as
// using a '#define NUM_PEAKPARAM' but this idiom is a little cleaner, IMHO.
//
enum peakParam {
    CENTER,
    AREA,
    DW,
    PW,
    NUM_PEAKPARAM
};

// Choose the line shape function.
//
// Voigts:
// humlicek1979: C version of the code from J. Humlicek JQSRT 21 (1979) pp 301.
// wells1999: C version of the code from R. Wells JQSRT 62 (1999), pp 29-48.
//
// Notes:
// 1. The choice of function doesn't necessarily determine the method of calculating
// derivatives for the purpose of fitting spectra.  However, in the current
// implementation, of the fit, we take two point derivatives when using
// humlicek1979 and analytic derivatives when using wells1999.  Well's humdev()
// automatically computes derivatives so we go ahead and use them.
//
// 2. There is only one configurable lineshape choice.  If the spectra contains
// many peaks, and we desire to use a computationally cheaper lineshape for
// uninteresting peaks (for example, fast_ch4 models some little peaks with
// lorentzians), this enum will define the peaks of interest and the others
// will have to be hardcoded.
//
enum lineshape {
    humlicek1979,
    wells1999,
    mclean1997,
    lorentz
};

//---------------------------------------------------------------------------
class Peak
{
public:
    Peak(std::string peakIniName,
        int idx,
        double center, double area, double dopplerWidth, double pressureWidth,
        double range=0.0,
        std::string isotopeCode="", std::string molName="",
        std::string evName="", lineshape ls=wells1999, double epp=0.0,
        double lineStrength=0.0, double lsf=1.0, bool wantConc=false,
        double gAir=0.05, double gSelf=0.05, double gH2O=0.05, double tExp=0.5,
        std::vector<bool> fitFlags=std::vector<bool>(4,false),
        double microns = 0.0, double press_shift_cm1 = 0.0);
    
    Peak(const Peak & copyMe);
    
    /**
     * Calculate (but do not cache) the peak value at a given frequency.
     *
     * @return Voigt lineshape at frequency x.
     */
    double f(double x) const; // get real component of voigt()
    double f(const double x, lineshape ls) const;
    
    /**
     * Calculate and cache the partial derivatives of f with respect to
     * amplitude, center frequency, doppler width and pressure width.  If
     * the values have been calculated already (so dfComputed_ is true),
     * it will not repeat the calculation.
     *
     * The partial derivatives can be accessed through getdVda(), getdVdx(),
     * getdVddw(), and getdVdpw().
     * 
     * @return f(x), the scaled real part of the Voigt function.
     * FIXME: this is misleading; should use df() then getV() instead. PCH151027
     */
    double df(const double x);
    //double df(double x, double da, peakParam idx);

    void setParams(std::vector<double>&);
    //void setParams(double center_GHz, double area, double dopplerWidth,
    //    double pressureWidth);
    
    /**
     * Return the parameters: [center area DW PW].
     */
    std::vector<double> getParams() const;
    
    std::vector<std::string> getParamNames(std::string) const;

    void setFitFlags(std::vector<bool>);
    void setFitFlagsForLowSNR(double);
    std::vector<bool> getFitFlags(void) const;
    
    /**
     * Return the code for the molecular isotope, e.g. _NNO18
     */
    std::string getIsotopeCode(void) const  { return isotopeCode_; }
    std::string getMolName(void) const  { return molName_; }
    
    /**
     * Return the code for the molecule type, e.g. _NNO_T
     */
//    molecule_t getMoleculeCode(void) const { return moleculeCode_; }
    
    // TODO: rely on INI / DB interface for as much of this as possible
    /**
     * Return the name of the EV corresponding to the peak
     */
    std::string getEVName(void) const  { return evName_; }
    double getRange(void) const             { return range_; }
    
    double getGroundState(void) const   { return epp_; }
    double getLineStrength(void) const  { return lineStrength_; }
    void setLSFudge(double lsf)         { lsFudge_ = lsf; }
    double getLSFudge(void) const       { return lsFudge_; }
    bool wantConc(void) const           { return wantConc_; }
    
    void setParam(double,int);
    double getParam(int param, bool adjust = false) const;
    
    /**
     * Get cached peak Voigt value from last evaluation of f() or df().
     */
    double getV(void) const             { return V_; } // UNUSED
    /**
     * Get cached peak derivative dV/d(area) from last evaluation of df().
     */
    double getdVda(void) const          { return dVda_; }
    /**
     * Get cached peak derivative dV/d(center) from last evaluation of df().
     */
    double getdVdx(void) const          { return dVdx_; }
    /**
     * Get cached peak derivative dV/d(doppler width) from last evaluation of
     * df().
     */
    double getdVddw(void) const         { return dVddw_; }
    /**
     * Get cached peak derivative dV/d(pressure width) from last evaluation
     * of df().
     */
    double getdVdpw(void) const         { return dVdpw_; }

    void addXSlavePeak(Peak*);
    void addAreaSlavePeak(Peak*);
    void addDWSlavePeak(Peak*);
    void addPWSlavePeak(Peak*);
    void setXSlave(void);
    void setAreaSlave(void);
    void setDWSlave(void);
    void setPWSlave(void);
    
    void setCenterOffset(double offset) { masterSlaveCenterOffset_ = offset; }
    void setAreaRatio(double ar) { masterSlaveAreaRatio_ = ar; }
    void setDWRatio(double dwr) { masterSlaveDWRatio_ = dwr; }
    void setPWRatio(double pwr) { masterSlavePWRatio_ = pwr; }
    double getCenterOffset(void) const { return masterSlaveCenterOffset_; }
    double getAreaRatio(void) const { return masterSlaveAreaRatio_; }
    double getDWRatio(void) const { return masterSlaveDWRatio_; }
    double getPWRatio(void) const { return masterSlavePWRatio_; }
    bool isAreaSlave(void) { return isAreaSlave_; }

    void resetDFFlag(void) { dfComputed_ = false; }

    // Print flag, currently mirrors wantConc. Flags whether or not to print
    // out this peak's fit params to the full text output.  This was created
    // to avoid printing out all the little extraneous peaks that we don't
    // care about.
    //
    bool printFlag(void) { return wantConc_; }
    
    // TODO: INI/DB
    double getMicrons(void) const       { return microns_; }
    
    double getCenter(void) const        { return getParam(CENTER); }
    double getArea(void) const          { return getParam(AREA); }
    void setArea(double area)           { setParam(area,AREA); }
    double getDopplerWidth() const      { return getParam(DW); }
    double getPressureWidth() const     { return getParam(PW); }
    
    //double getGammaAir_(double P_torr, double T_celsius) const; // move
    
    const std::string & id() const { return _peakId_; }
    
    friend std::ostream& operator<<(std::ostream & str, const Peak & rhs);
    
private:
    double microns_; // wavelength from INI file, for easy identification
    double range_;
    
    std::string isotopeCode_; // e.g. H2O18
    std::string molName_;
//    molecule_t moleculeCode_; // e.g. _H2O_T
    std::string evName_; // e.g. "H2O to measure"

    // A label for debugging, used to make it easy to associate a given
    // peak object to a peak in an ini file.
    std::string _peakId_;
    
    lineshape lineshape_;

    // HITRAN molecular constant(s) need for concentration calculations.
    double epp_; // Ground state energy

    // If a gas can't be calibrated with a known reference, we can compute
    // concentration using the lineStrength ('S') from HITRAN.
    // Additionally with have a fudge factor, lsFudge_, that we can use
    // to scale the linestrength calculated concentrations.  This fudge factor
    // is initialized to 1.0 and adjusted when we calibrate with a reference
    // gas.
    //
    double lineStrength_; // CACHED FROM INI, used in gas conc
    double lsFudge_; // CACHED FROM INI, used in gas conc

    // Ian came up with a peak area dependent compensation function that
    // adjusts the fitted peak area based upon the final fit value.  The intent
    // is to account for the non-linear effects of skew and absorption that
    // can't be handled with a single calibration adjustment.  The coefficients
    // of the function are found in calibration_?.ini and are stored in the peak
    // object with the ac_ vector.
    // ac_ : [A]rea [C]oefficients
    //
//    std::vector<double> areaCoefficients_; // UNUSED

    // The flag is true if we want to calc and report the gas concentration
    // using this peak. fittedConc_ is the most measured concentration.  This
    // value is not computed in this class but by external code.
    //
    bool wantConc_; // cached from peak INI

    // Use the HITRAN formula for computing pressure width. g == gamma.
    //
    // g(p,T) = (Tref/T)^n * (g_air*(p_total - p_self) + g_self*p_self)
    //
    // Refer to "The HITRAN Molecular Spectroscopic Database and HAWKS (
    // HITRAN Atmospheric Workstation): 1996 Edition", J. Quant. Spectrosc.
    // Radiat. Transfer Vol. 60, No. 5, pp. 665-710, 1998, equation A12.
    //
    // 10FEB2010
    // Modified the pressure width to include a term for water broadening.
    //
    // g(p,T) = (Tref/T)^n *
    //          (g_air*(p_total - p_self - p_H2O) + g_self*p_self + g_H2O*p_H2O)
    //
    // This used for all molecules except water. Water uses the original above
    // forumla.
    //
    double gammaAir_;    // gamma_air in GHz/Torr
    double gammaSelf_;   // gamma_self in GHz/Torr
    double gammaH2O_;    // gamma_water in GHz/Torr
    double tExp_;    // exponent 'n'
    
    // Cache the Voigt values
    double V_, dVda_, dVdx_, dVddw_, dVdpw_;
//    double x_; // unused
    
    std::vector<double> params_;
    std::vector<bool> fitFlags_;  // fit flags

    std::vector<Peak*> xSlavePeaks_, areaSlavePeaks_, dwSlavePeaks_, pwSlavePeaks_;
    bool isXSlave_, isAreaSlave_, isDWSlave_, isPWSlave_;
    void setSlaveParams_(void);

    // The slave parameters of area, doppler width, and pressure width are
    // expressed as
    //    param_slave = ratio * param_master
    // with
    //    ratio = orig_master_param/orig_slave_param
    //
    // These variables store the ratio for easy recall when setting parameters
    // or scaling slave parameter derivatives.
    //
    double masterSlaveAreaRatio_, masterSlaveDWRatio_, masterSlavePWRatio_;
    double masterSlaveCenterOffset_;
    bool dfComputed_;
    double press_shift_cm1;
};



#endif




