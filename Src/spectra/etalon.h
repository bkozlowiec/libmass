//
//  etalon.hpp
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 2/13/16.
//
//

#ifndef etalon_hpp
#define etalon_hpp

#include <vector>
#include <iostream>

/**
 * Etalon function.
 *
 * The form of the fit function is:
 *
 * xghz = a + b*x + c*x/ln(x) + d*ln(x)/x + e/x
 */
class Etalon
{
public:
    Etalon();
    ~Etalon();
    
    void setCoefficients(double aa, double bb, double cc, double dd, double ee);
    void setCoefficients(const std::vector<double> & coeffs);
    const std::vector<double> & getCoefficients(void) const { return coefficients_; }
    
    void setFitInterval(int seconds) { fitInterval_ = seconds; }
    int getFitInterval() const { return fitInterval_; }
    
    void setFitEnabled(bool enabled) { fitEnabled_ = enabled; }
    bool isFitEnabled() const { return fitEnabled_; }
    
    void setFitFlags(bool aa, bool bb, bool cc, bool dd, bool ee);
    void setFitFlags(const std::vector<bool> & coeffs);
    const std::vector<bool> & getFitFlags() { return fitFlags_; }
    
    // Evaluate the etalon function
    void getFrequencies_GHz(int numPoints, std::vector<double> & outFreqs) const;
    
    int getPoint(double freq_GHz, const std::vector<double> & frequencies) const;
    void getPoints(const std::vector<double> & frequencies, std::vector<int> & outPoints) const;
    
    
    static const int NUM_COEFFICIENTS;
	// Etalon stretching
	void setStretchEnabled(bool enabled) { stretchEnabled_ = enabled; };
	bool getStretchEnabled() const { return stretchEnabled_; };

	void setStretchPeakIndexes(int idx0, int idx1);
	void setStretchPeakIndexes(const std::vector<int> & indexes);
	const std::vector<int> & getStretchPeakIndexes(void) const { return stretchPeakIndexes_; }

	void setStretchFactor(double factor);
	double getStretchFactor() const { return stretchFactor_; };

	void setStretchNu0(double nu0) { stretchNu0_ = nu0; };
	double getStretchNu0() const { return stretchNu0_; };

	static const int NUM_STRETCH_INDEXES;
	static const double MAX_STRETCH_FACTOR;

protected:
    // etalonCoefficients_ : Etalon coefficients
    // etalonFitEnabled_ : Allow etalon fitting to spectra
    // etalonFitInterval_ : Seconds between etalon fits
    // etalonFitFlags_ : Etalon fit flags
    
    std::vector<double> coefficients_;
    // TODO: when can we discard these unused variables? PCH170215
    bool fitEnabled_; // unused (accessor is unused by client code!!)
    int fitInterval_; // unused
    std::vector<bool> fitFlags_; // unused

	// Etalon stretching
	bool stretchEnabled_;
	std::vector<int> stretchPeakIndexes_;
private:
	double stretchFactor_;
	double stretchNu0_;

};

#endif /* etalon_hpp */
