#include <stdexcept>
#include "concentrationcalculator.h"

//ConcentrationCalculator::ConcentrationCalculator()
//{
//}

// Loop through the collection of peaks in a spectra object and calculate
// a gas concentration for each fitted peak.  Peaks that have a NULL in
// a required field are skipped.
//
//std::map<std::string,double> ConcentrationCalculator::gasConcentrations(
//    SpectraPeaksBase &spectraPeaks,
//    double celsius,
//    double pressure,
//    double tau)
//{
//    std::map<std::string,double> concMap;
//    std::vector<Peak*> peaks;
//    spectraPeaks.getAllPeaks(peaks);
//    
//    for (int iPeak = 0; iPeak<peaks.size(); iPeak++)
//    if (peaks.at(iPeak)->wantConc())
//    {
//        // Peak values
//        // The 'true' on area implements Ian's area compensation that helps
//        // CCIA. To turn it off, don't change the code here but rather
//        // change the coefficients in calibration_?.ini accordingly.
//        //
//        
//        double conc = gasConcentration(*peaks[iPeak], celsius, pressure, tau);
//        
//        concMap[peaks[iPeak]->getMoleculeName()] = conc;
//    }
//    return concMap;
//}

// Loop through the collection of peaks in a spectra object and calculate
// a gas concentration for each fitted peak.  Peaks that have a NULL in
// a required field are skipped.
//
std::map<std::string,double> ConcentrationCalculator::speciesConcentrations(
    const SpectraPeaksBase & spectraPeaks,
	const PhysConstsINI & physConstsINI,
    double celsius,
    double pressure,
    double tau)
{
    std::map<std::string,double> concMap;
    std::vector<Peak*> peaks;
    spectraPeaks.getAllPeaks(peaks);
    
    for (std::size_t iPeak = 0; iPeak<peaks.size(); iPeak++)
    if (peaks.at(iPeak)->wantConc())
    {
        // Peak values
        // The 'true' on area implements Ian's area compensation that helps
        // CCIA. To turn it off, don't change the code here but rather
        // change the coefficients in calibration_?.ini accordingly.
		std::vector<double> qCoeffs = physConstsINI.partitionFunctionCoefficents(peaks[iPeak]->getIsotopeCode());
        double conc = gasConcentration(*peaks[iPeak], qCoeffs, celsius, pressure, tau);
        
        concMap[peaks[iPeak]->getEVName()] = conc;
    }
    return concMap;
}

// Loop through the collection of peaks in a spectra object and calculate
// a gas concentration for each fitted peak.  Peaks that have a NULL in
// a required field are skipped.
//
double ConcentrationCalculator::gasConcentration(
    const Peak & peak,
	const std::vector<double> & qCoeffs,
    double celsius,
    double pressure,
    double tau)
{
    double area = peak.getParam(AREA,true);
    double ls = peak.getLineStrength();
    double lsFudge = peak.getLSFudge();
    double epp = peak.getGroundState();
    std::string isotopeCode = peak.getIsotopeCode();

    // Instrument/gas values
    //
    double kelvin = celsius + 273.15;
    
    double conc = gasConcentration_(area, kelvin, qCoeffs, epp, pressure,
        tau, ls, lsFudge);
    
    return conc;
}



/*std::map<std::string,double> ConcentrationCalculator::
gasNumberDensities(SpectraPeaksBase &s, double celsius, double tau)
{

    double numberDensity;
    std::map<std::string,double> numberDensities;
    std::vector<Peak*> peaks;
    s.getAllPeaks(peaks);
    
    for(int i=0; i<peaks.size(); i++) {

        // Peak values
        // The 'true' on area implements Ian's area compensation that helps
        // CCIA. To turn it off, don't change the code here but rather
        // change the coefficients in calibration_?.ini accordingly.
        //
        double area = peaks.at(i)->getParam(AREA,true);
        double ls = peaks.at(i)->getLineStrength();
        double lsFudge = peaks.at(i)->getLSFudge();
        double epp = peaks.at(i)->getGroundState();
        std::string name = peaks.at(i)->getIsotopeCode();

        // Instrument/gas values
        //
        double kelvin = celsius + 273.15;

        if(peaks.at(i)->wantConc()) {
            numberDensity = gasNumberDensity_(area, kelvin, name, epp, tau, ls, lsFudge);
//            peaks.at(i)->setFittedConc(numberDensity); // this does nothing useful PCH160302
            std::string qStr = peaks.at(i)->getEVName();
            numberDensities[qStr] = numberDensity;
        }
    }
    return numberDensities;
}
*/
// Similar to gasConcentration above but this version uses line strength
// constants.  Typically we use this when we can't calibrate a reference
// gas such as in the case of liquid water.
//
// Line Strength is 'S' in Hi-tran, not optical depth.
//
// Inputs:
// Area - unitless(?)
// T - Abs temp in Kelvin
// edp - E" in cm-1
// pressure - Torr
// tau - ringdown in microseconds
// lineStrength - cm-1*mol-1
//
double ConcentrationCalculator::
gasConcentration_ ( const double area,
                    const double temperature,             // Kelvin
					const std::vector<double> & qCoeffs, //partition function Coefficients
                    const double epp,           // E" in cm-1
                    const double pressure,      // Torr
                    const double tau,           // microseconds
                    const double lineStrength,  // HITRAN S
                    const double lsFudge )          // unitless fudge
{
    double c = c::speedOfLight / 1e4; // c in cm/us
    double cmGHz = c::cmGHz;
    double Tref = c::Tref;

    double QT = PhysicalConsts::partitionFunction(qCoeffs,temperature);
    double QTo = PhysicalConsts::partitionFunction(qCoeffs,Tref);
    double ZT = PhysicalConsts::Z_Et(temperature, epp);
    double ZTo = PhysicalConsts::Z_Et(Tref,epp);
    double ls = lineStrength * (QTo/QT) * (ZT/ZTo);

    // number density
    double numDen = (area * cmGHz) / (c * tau * ls * lsFudge);

    double ppm = PhysicalConsts::numDen2ppm(numDen,pressure,temperature);

    // Check to see if ppm concentration is realistic.  If the fit fails, the
    // computed peak area can be garbage and will give us negative or +100%
    // concentrations.  Here we bound the aphysical ppm to 1000001 and -1.
    // These serve as flags that something has failed and also, hopefully,
    // keeps the time chart from blowing up.
    //
    if(ppm > 1e6)
        ppm = 1e6 + 1;
    if(ppm < 0)
        ppm = 0.0;//-0.01;
    return ppm;
}

/*
// Number density in units [N/cm3]
//
double ConcentrationCalculator::gasNumberDensity_ ( const double area,
                            const double temperature,             // Kelvin
                            const std::string name,
                            const double edp,           // E" in cm-1
                            const double tau,           // microseconds
                            const double lineStrength,  // HITRAN S
                            const double lsFudge )          // unitless fudge
{
    double c = c::speedOfLight / 1e4; // c in cm/us
    double cmGHz = c::cmGHz;
    double Tref = c::Tref;

    double QT = PhysicalConsts::partitionFunction(name,temperature);
    double QTo = PhysicalConsts::partitionFunction(name,Tref);
    double ZT = PhysicalConsts::Z_Et(temperature, edp);
    double ZTo = PhysicalConsts::Z_Et(Tref,edp);
    double ls = lineStrength * (QTo/QT) * (ZT/ZTo);

    // number density
    double numDen = (area * cmGHz) / (c * tau * ls * lsFudge);
    return numDen;
}

*/

