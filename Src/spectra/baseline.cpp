//---------------------------------------------------------------------------
#include "baseline.h"

//---------------------------------------------------------------------------

Baseline::Baseline(void) :
	model_(0),
    oa_(4,0),
    a_(4,0),
    ia_(4,true)
{
    ia_[3] = false;
}

Baseline::Baseline(int model, std::vector<double> a, std::vector<bool> ia) :
	model_(model),
    oa_(a),
    a_(a),
    ia_(ia)
{
}

void Baseline::setOrigParams(double a0, double a1, double a2, double a3)
{
    a_[0] = a0;
    a_[1] = a1;
    a_[2] = a2;
    a_[3] = a3;

    oa_[0] = a0;
    oa_[1] = a1;
    oa_[2] = a2;
    oa_[3] = a3;
}

void Baseline::setParams(double a0, double a1, double a2, double a3) {
    a_[0] = a0;
    a_[1] = a1;
    a_[2] = a2;
    a_[3] = a3;
}


void Baseline::setParams(std::vector<double> a) {
    for(std::size_t i=0; i<a.size(); i++)
        a_[i] = a.at(i);
}

void Baseline::setOrigParams(std::vector<double> a) {
    for(std::size_t i=0; i<a.size(); i++)
        a_[i] = oa_[i] = a.at(i);
}
void Baseline::resetParams(void) {
    a_ = oa_;
}


const std::vector<double> & Baseline::getParams(void) const
{
    return a_;
}

std::vector<std::string> Baseline::getParamNames(void) {
    std::vector<std::string> n;
    if(ia_.at(0))
        n.push_back("BL0T");
    else
        n.push_back("BL0T");

    if(ia_.at(1))
        n.push_back("BL1T");
    else
        n.push_back("BL1F");

    if(ia_.at(2))
        n.push_back("BL2T");
    else
        n.push_back("BL2F");

    if(ia_.at(3))
        n.push_back("BL3T");
    else
        n.push_back("BL3F");
    return n;
}

void Baseline::setFitFlags(bool a0f, bool a1f, bool a2f, bool a3f) {
    ia_[0] = a0f;
    ia_[1] = a1f;
    ia_[2] = a2f;
    ia_[3] = a3f;
}

void Baseline::setFitFlags(std::vector<bool> ia) {
    ia_ = ia;
}

std::vector<bool> Baseline::getFitFlags(void) {
    return ia_;
}

double Baseline::f(double x, int xpt) const
{
    double xpt_float = xpt; // forestall overflows
    if(model_ == 0) {
        return a_[0] + a_[1]*x + a_[2]*x*x + a_[3]*x*x*x;
    }else if(model_ == 1) {
        return a_[0] + a_[1]*x + a_[1]*a_[2]*x*x;
    }else if(model_ == 2) {
        return a_[0] + xpt_float*(a_[1] + xpt_float*(a_[2] + xpt_float*a_[3]));
    }else if(model_ == 3) {
        return a_[0] + xpt_float*(a_[1] + xpt_float*(a_[2]*a_[1]));
//        return a_[0] + a_[1]*xpt_float + a_[1]*a_[2]*xpt*xpt;
    }else{
        return 0;
    }
}

std::vector<double> Baseline::dfda(double x, int xpt) const
{
    double xpt_float = xpt; // forestall overflows
    
	std::vector<double> da (4, 1.0);
	if(model_ == 0) {
		da[1] = x;
		da[2] = x*x;
		da[3] = x*x*x;
    }else if(model_ == 1) {
		da[1] = x + a_[2]*x*x;    // dy/da1
		da[2] = a_[1]*x*x;        // dy/da2
		da[3] = 0.0;              // dy/da3
    }else if(model_ == 2) {
		da[1] = xpt_float;              // dy/da1
		da[2] = xpt_float*xpt_float;          // dy/da2
		da[3] = xpt_float*xpt_float*xpt_float;      // dy/da3
    }else if(model_ == 3) {
		da[1] = xpt_float+a_[2]*xpt_float*xpt_float;// dy/da1
		da[2] = a_[1]*xpt_float*xpt_float;    // dy/da2
		da[3] = 0.0;              // dy/da3
	}
    return da;
}

bool Baseline::isBinFit(void) {
    if(model_ == 2 || model_ == 3)
        return true;
    else
        return false;
}
