//
//  peaks_ini.cpp
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 2/13/16.
//
//

#include "peaks_ini.h"

#include <fstream>
#include <sstream>
#include <float.h>
#include <stdexcept>
#include <cstdlib>
#include <cassert>

PeaksINI::PeaksINI()
{
	updatePeaksINI();
}

PeaksINI::PeaksINI(const std::string & fname) :
    fileName_(fname),
    flangeFitPeriod_(0)
{
	updatePeaksINI();
}

PeaksINI::PeaksINI(const StructPeaksIni & strPeaksINI) :
	fileName_(""),
	flangeFitPeriod_(0)
{
	updatePeaksINI(strPeaksINI);
}

PeaksINI::~PeaksINI()
{
}

void PeaksINI::clear()
{
    lowerBounds_.clear();
    upperBounds_.clear();
    baselineParameters_.clear();
    baselineFlags_.clear();
    baselineFeedForwardFlags_.clear();
    peaks_.clear();
    blSample_.clear();
    blLmEst_.clear();
    fitWindow_.clear();
    flangeFitPeriod_ = 0; // this means never fit the flanges.
    
//    initialConcs_.clear();
}

void PeaksINI::updatePeaksINI()
{
    using namespace Constants;
    
    // Eventually just be more careful and don't nuke data structures in
    // advance like this.
    clear();
    
    bool didBeginReadingFlanges = false;
    iFirstBackgroundPeak_ = 0;
    
    int idx, master, slave;
    double microns, ghzShift, ls, lsf, epp, range, gammaAir, gammaSelf, gammaWater,
            gammaAirAdj, gammaSelfAdj, gammaH2OAdj, tExp, gConc, gwConc, press_shift_cm1;
    
    std::string token;
    std::string ab, bb, cb, db, eb;
    int ai, bi, ci, di;
    std::string conc, fitFlags, mode;
    std::string lbStr, ubStr;
    std::vector<double> lowerBounds;
    std::vector<double> upperBounds;
    
    lowerBounds_.clear();
    upperBounds_.clear();
    
    std::ifstream peaksFile;
    peaksFile.open(fileName_.c_str());
    std::string rlstr;
    
    if (false == peaksFile.is_open())
    {
        throw std::runtime_error("Cannot open peaks INI file.");
    }

    while ( getline (peaksFile, rlstr) )
    {
        // Skip blank lines and comments
        if (rlstr.length() < 1 || rlstr.at(0) == '#')
            continue;
    
        std::stringstream ss;
        ss << rlstr;
        std::string key;
        std::string str2;

        // Read a key and convert to uppercase
        ss >> std::uppercase >> key;

		if (key.compare("LINELOCK_INDEXES") == 0)
		{
			int tmp;
			linelockPeakIndexes_.clear();
			linelockPeakIndexes_.reserve(100);
			while (ss >> tmp)
			{
				linelockPeakIndexes_.push_back(tmp);
			}
		}
        if (key.compare("PEAKS_INI_LABEL") == 0)
        {
            ss >> peaksIniLabel_;
        }
        if (key == "CONC")
        {
            throw std::runtime_error("CONC is deprecated");
        }
//            if (key.compare("CONC") == 0)
//            {
//                std::string usePrevious_YesNo;
//                int numConcsToAverage;
//                
//                ss >> isotopeName;
//                ss >> estConc;
//                ss >> usePrevious_YesNo;
//                ss >> numConcsToAverage;
//                
//                bool doUsePreviousConc = false;
//                if (usePrevious_YesNo[0] == 'y' ||
//                    usePrevious_YesNo[0] == 'Y')
//                {
//                    doUsePreviousConc = true;
//                }
//                
//                molecule_t isotopeCode = PhysicalConsts::getIsotopeCode(isotopeName);
//                if (isotopeCode == _null)
//                {
//                    std::cerr << "Unknown isotope.\n";
//                }
//                
//                initialConcs_[isotopeCode] = IsotopeConcentration(isotopeCode,
//                    estConc, doUsePreviousConc, numConcsToAverage);
//            }
        
        // ETALON FUNCTION
        //
        // model coefficients
        // fit enabled flag (UNUSED)
        // fit flags for each coefficient (UNUSED)
        // fit interval in seconds (UNUSED)
        if (key.compare("ETALON_COEFFICIENTS") == 0)
        {
            double aa, bb, cc, dd, ee;
            ss >> aa >> bb >> cc >> dd >> ee;
            etalon_.setCoefficients(aa, bb, cc, dd, ee);
        }
        if (key.compare("ETALON_FIT_ENABLED") == 0)
        {
            ss >> str2;
            if((str2.compare("Y") == 0) || (str2.compare("y") == 0))
                etalon_.setFitEnabled(true);
            else
                etalon_.setFitEnabled(false);
        }
        if (key.compare("ETALON_FIT_INTERVAL") == 0)
        {
            int fitInterval_seconds;
            ss >> fitInterval_seconds;
            etalon_.setFitInterval(fitInterval_seconds);
        }
        if (key.compare("ETALON_FIT_FLAGS") == 0)
        {
            std::vector<bool> fitFlags(5, false);
            ss >> ab >> bb >> cb >> db >> eb;
            if((ab.compare("T") == 0) || (ab.compare("t") == 0))
                fitFlags[0] = true;
            if((bb.compare("T") == 0) || (bb.compare("t") == 0))
                fitFlags[1] = true;
            if((cb.compare("T") == 0) || (cb.compare("t") == 0))
                fitFlags[2] = true;
            if((db.compare("T") == 0) || (db.compare("t") == 0))
                fitFlags[3] = true;
            if((eb.compare("T") == 0) || (eb.compare("t") == 0))
                fitFlags[4] = true;
            
            etalon_.setFitFlags(fitFlags);
        }
		if (key.compare("ETALON_STRETCH_ENABLED") == 0)
		{
			ss >> str2;
			if ((str2.compare("Y") == 0) || (str2.compare("y") == 0))
				etalon_.setStretchEnabled(true);
			else
				etalon_.setStretchEnabled(false);
		}
		if (key.compare("ETALON_STRETCH_PEAK_INDEXES") == 0)
		{
			ss >> ai >> bi;
			etalon_.setStretchPeakIndexes(ai, bi);
		}

        // BASELINE
        
        if (key.compare("BL_SAMPLE") == 0) {
            ss >> ai >> bi >> ci >> di;
            blSample_.push_back(ai);
            blSample_.push_back(bi);
            blSample_.push_back(ci);
            blSample_.push_back(di);
        }
        if (key.compare("BL_LM_EST") == 0)
        {
            ss >> ai >> bi;
            blLmEst_.push_back(ai);
            blLmEst_.push_back(bi);
        }
        if (key.compare("FIT_WINDOW") == 0)
        {
            ss >> ai >> bi;
            fitWindow_.push_back(ai);
            fitWindow_.push_back(bi);
        }
        if (key.compare("ENABLE_FIT") == 0)
        {
            ss >> fitFlags;
            if((fitFlags.compare("Y") == 0) || (fitFlags.compare("y") == 0))
                enableFit_ = true;
            else
                enableFit_ = false;
        }
        if (key.compare("BAD_CHI_SQ") == 0)
        {
            ss >> badChisq_;
        }
        if (key.compare("RESET_EVERY_FIT") == 0)
        {
            ss >> fitFlags;
            if((fitFlags.compare("Y") == 0) || (fitFlags.compare("y") == 0))
                resetEveryFit_ = true;
            else
                resetEveryFit_ = false;
        }
        if (key.compare("BASELINE") == 0)
        {
            baselineFlags_.assign(4, false);
            baselineParameters_.assign(4, 0.0);
            
            // Baseline model, an integer
            ss >> blModel_;
            
            // Baseline parameters, doubles
            for (int ii = 0; ii < 4; ii++)
                ss >> baselineParameters_[ii];
            
            // Baseline fit flags, bools
            std::string flag;
            for (int ii = 0; ii < 4; ii++)
            {
                ss >> flag;
                if (flag.compare("T") == 0 || flag.compare("t") == 0)
                    baselineFlags_[ii] = true;
            }
        }
        if (key.compare("BASELINE_FEED_FORWARD") == 0)
        {
            // These flags are UNUSED
            
            baselineFeedForwardFlags_.assign(4, false);
            
            std::string flag;
            for (int ii = 0; ii < 4; ii++)
            {
                ss >> flag;
                if (flag.compare("T") == 0 || flag.compare("t") == 0)
                    baselineFeedForwardFlags_[ii] = true;
            }
        }
        //if (key.compare("CELSIUS") == 0)
        //    ss >> celsius_;
        //if (key.compare("TORR") == 0)
        //    ss >> torr_;
        if (key.compare("TAU") == 0)
        {
            throw std::runtime_error("TAU has been superceded by CAVITY_LENGTH in spc_config.ini");
            //ss >> tau;
        }
        if (key.compare("ZERO_PEAK") == 0)
            ss >> zeroIdx_;
        if (key.compare("SHIFT") == 0)
            ss >> shift_;
        if (key == "PEAK" || key == "FLANGE")
        {
            std::string isotopeName, molName, evName;
            
            ss >> idx >> isotopeName >> evName >> microns >> ghzShift >> ls >> lsf;
            ss >> gammaAir >> gammaSelf >> gammaWater >> gammaAirAdj >> gammaSelfAdj >> gammaH2OAdj;
            ss >> tExp >> press_shift_cm1 >> gConc >> gwConc >> epp >> range >> conc >> fitFlags;
            // Gammas are in cm/atm and will be converted to GHz/torr below.
            gammaAir *= gammaAirAdj;
            gammaSelf *= gammaSelfAdj;
            gammaWater *= gammaH2OAdj;
            
            molName = evName;
            // If we know the relative abundance of the given isotope,
            // scale its linestrength accordingly.
            // std::string isotopeCode = PhysicalConsts::getIsotopeCode(isotopeName);
			std::string isotopeCode = isotopeName;
            /* We do not handle isotopes
            std::string moleculeCode = PhysicalConsts::getMoleculeType(isotopeName);

            double fraction = PhysicalConsts::getIsotopeAbundance(isotopeCode);
            if (fraction != -1)
                ls /= fraction;
            */
            
            //If the wavelength is longer than 100um, assume that the units
            //wavenumbers. This is a convenience as the HITRAN data is
            //normally published in cm-1.
            //
            if(microns > 100)
            {
                microns = 1.0e4/microns;
            }
            
            if (key == "PEAK")
            {
                if (didBeginReadingFlanges)
                {
                    throw std::runtime_error("All PEAK lines must precede all FLANGE lines");
                }
                
                peaks_.push_back(PeaksINI::PeakConstants(
                    idx,
                    PhysicalConsts::cmPerAtmToGHzPerTorr(gammaAir),
                    PhysicalConsts::cmPerAtmToGHzPerTorr(gammaWater),
                    PhysicalConsts::cmPerAtmToGHzPerTorr(gammaSelf),
                    tExp,
                    gConc,
                    gwConc,
                    ghzShift,
                    ls,
                    lsf,
                    conc=="Y" || conc=="y",
                    epp,
                    range,
                    microns,
                    isotopeCode,
                    molName,
                    evName,
                    fitFlags,
                    press_shift_cm1));
                
                iFirstBackgroundPeak_ = peaks_.size();
            }
            else
            {
                assert(key == "FLANGE");
                if (didBeginReadingFlanges == false)
                {
                    didBeginReadingFlanges = true;
                }
                
                int flangeIndex;
                std::string temperatureString, pressureString;
                double temperature_C, pressure_torr, length_m;
                
                ss >> flangeIndex >> temperatureString >> pressureString >> length_m;
                
                // Just in PeaksINI::PeakConstants, we let T and P == -1 mean
                // that T and P will take process values.
                
                if (temperatureString == "N")
                    temperature_C = -274.0;
                else
                    temperature_C = atof(temperatureString.c_str());
                
                if (pressureString == "N")
                    pressure_torr = -1.0;
                else
                {
                    double pressure_bar = atof(pressureString.c_str());
                    pressure_torr = pressure_bar * 750.062;
                }
                
                peaks_.push_back(PeaksINI::PeakConstants(
                    idx,
                    PhysicalConsts::cmPerAtmToGHzPerTorr(gammaAir),
                    PhysicalConsts::cmPerAtmToGHzPerTorr(gammaWater),
                    PhysicalConsts::cmPerAtmToGHzPerTorr(gammaSelf),
                    tExp,
                    gConc,
                    gwConc,
                    ghzShift,
                    ls,
                    lsf,
                    conc=="Y" || conc=="y",
                    epp,
                    range,
                    microns,
                    isotopeCode,
                    molName,
                    evName,
                    fitFlags,
                    press_shift_cm1,
                    flangeIndex));
            }
        }
        if (key.compare("RAMP_TARGET") == 0)
        {
            std::cerr << "Not using RAMP_TARGET anymore\n";
//                ss >> idx >> rampTarget >> rampTargetRange;
//                rampTargetsm_[idx] = rampTarget;
//                rampTargetRangem_[idx] = rampTargetRange;
        }
        if (key.compare("X_SLAVE") == 0)
        {
            ss >> master;
            ss >> slave;
            peaks_[master].xSlaves().push_back(slave);
//                x_slave_[master].push_back(slave);
        }
        if (key.compare("AREA_SLAVE") == 0)
        {
            ss >> master;
            ss >> slave;
            peaks_[master].areaSlaves().push_back(slave);
//                area_slave_[master].push_back(slave);
        }
        if (key.compare("DW_SLAVE") == 0)
        {
            ss >> master;
            ss >> slave;
            peaks_[master].dopplerWidthSlaves().push_back(slave);
//                dw_slave_[master].push_back(slave);
        }
        if (key.compare("PW_SLAVE") == 0)
        {
            ss >> master;
            ss >> slave;
            peaks_[master].pressureWidthSlaves().push_back(slave);
//                pw_slave_[master].push_back(slave);
        }
        if (key.compare("FIT_ALGORITHM") == 0)
        {
            ss >> fitAlgorithm_;
        }
        if (key.compare("BOUNDS") == 0)
        {
            std::string moleculeName_UNUSED;
            
            ss >> moleculeName_UNUSED >> lbStr >> ubStr;
            double tempD = atof(lbStr.c_str());
            
            if(tempD != 0.0)
                lowerBounds_.push_back(tempD);
            else
                lowerBounds_.push_back(-DBL_MAX);

            tempD = atof(ubStr.c_str());
            if(tempD != 0.0) {
                lowerBounds_.push_back(tempD);
            }else{
                lowerBounds_.push_back(DBL_MAX);
            }
        }
        if (key.compare("PLOT_FIT") == 0)
        {
            ss >> mode;
            if((mode.compare("Y") == 0) || (mode.compare("y") == 0))
                plotEnabled_ = true;
            else
                plotEnabled_ = false;
        }
        if (key == "FLANGE_FIT_PERIOD")
        {
            ss >> flangeFitPeriod_;
            
            if (flangeFitPeriod_ <= 0)
                throw std::runtime_error("Flange fit period must be positive");
        }
        
    }
    peaksFile.close();
}

void PeaksINI::updatePeaksINI(const StructPeaksIni & strPeaksINI)
{
	using namespace Constants;

	// Eventually just be more careful and don't nuke data structures in
	// advance like this.
	clear();

	bool didBeginReadingFlanges = false;
	iFirstBackgroundPeak_ = 0;

	int idx, master, slave;
	double microns, ghzShift, ls, lsf, epp, range, gammaAir, gammaSelf, gammaWater,
		gammaAirAdj, gammaSelfAdj, gammaH2OAdj, tExp, gConc, gwConc, press_shift_cm1;

	//std::string token;
	std::string key;
	std::string fitFlags, mode;
	bool conc;

	// Etalon
	etalon_.setCoefficients(strPeaksINI.etalonCoeffients);
	etalon_.setFitEnabled(false);
	etalon_.setStretchEnabled(strPeaksINI.etalonStretchEnabled);
	etalon_.setStretchPeakIndexes(strPeaksINI.etalonStretchIndexes);
	shift_ = strPeaksINI.shift_GHz;

	zeroIdx_ = strPeaksINI.zeroIdx;

	// BASELINE
	blModel_ = strPeaksINI.blModel_;

	blSample_.clear();
	blSample_.reserve(strPeaksINI.blSample.size());
	blSample_ = strPeaksINI.blSample;

	baselineFlags_.clear();
	baselineFlags_.reserve(strPeaksINI.baselineFlags.size());
	baselineFlags_ = strPeaksINI.baselineFlags;

	baselineParameters_.clear();
	baselineParameters_.reserve(strPeaksINI.baselineParameters.size());
	baselineParameters_ = strPeaksINI.baselineParameters;

	// Fit Window
	fitWindow_.clear();
	fitWindow_.reserve(strPeaksINI.fitWindow.size());
	fitWindow_ = strPeaksINI.fitWindow;

	// Standard Parameters

	enableFit_ = strPeaksINI.enableFit;
	resetEveryFit_ = strPeaksINI.resetEveryFit;
	fitAlgorithm_ = strPeaksINI.fitAlgorithm;
	flangeFitPeriod_ = strPeaksINI.flangeFitPeriod;

    // in config INI now
	//celsius_ = strPeaksINI.celsius;
	//torr_ = strPeaksINI.torr;

	for (std::size_t i = 0; i < strPeaksINI.peaks.size(); i++)
	{
		if (strPeaksINI.peaks[i].key == "PEAK" || strPeaksINI.peaks[i].key == "FLANGE")
		{
			std::string isotopeName, molName, evName;

			idx = strPeaksINI.peaks[i].idx;
			isotopeName = strPeaksINI.peaks[i].isotopeName;
			molName = strPeaksINI.peaks[i].molName;
			evName = strPeaksINI.peaks[i].evName;
			microns = strPeaksINI.peaks[i].microns;
			ghzShift = strPeaksINI.peaks[i].shift_GHz;
			ls = strPeaksINI.peaks[i].lineStrength;
			lsf = strPeaksINI.peaks[i].lineStrengthFudgeFactor;
			gammaAir = strPeaksINI.peaks[i].gammaAir;
			gammaSelf = strPeaksINI.peaks[i].gammaSelf;
			gammaWater = strPeaksINI.peaks[i].gammaWater;
			gammaAirAdj = strPeaksINI.peaks[i].gammaAirAdj;
			gammaSelfAdj = strPeaksINI.peaks[i].gammaSelfAdj;
			gammaH2OAdj = strPeaksINI.peaks[i].gammaH2OAdj;
			tExp = strPeaksINI.peaks[i].tExponent;
			gConc = strPeaksINI.peaks[i].self_ppm;
			gwConc = strPeaksINI.peaks[i].water_ppm;
			epp = strPeaksINI.peaks[i].epp;
			range = strPeaksINI.peaks[i].range_GHz;
			conc = strPeaksINI.peaks[i].wantConc;
			fitFlags = strPeaksINI.peaks[i].fitFlagString;
			press_shift_cm1 = strPeaksINI.peaks[i].press_shift_cm1;

			// Gammas are in cm/atm and will be converted to GHz/torr below.
			gammaAir *= gammaAirAdj;
			gammaSelf *= gammaSelfAdj;
			gammaWater *= gammaH2OAdj;

			// If we know the relative abundance of the given isotope,
			// scale its linestrength accordingly.
			//std::string isotopeCode = PhysicalConsts::getIsotopeCode(isotopeName);
			std::string isotopeCode = isotopeName;

			/* We do not handle Isotopes properly
			std::string moleculeCode = PhysicalConsts::getMoleculeType(isotopeName);

			double fraction = PhysicalConsts::getIsotopeAbundance(isotopeCode);
			if (fraction != -1)
				ls /= fraction;
			*/

			//If the wavelength is longer than 100um, assume that the units
			//wavenumbers. This is a convenience as the HITRAN data is
			//normally published in cm-1.
			//
			if (microns > 100)
			{
				microns = 1.0e4 / microns;
			}

			if (strPeaksINI.peaks[i].key == "PEAK")
			{
				if (didBeginReadingFlanges)
				{
					throw std::runtime_error("All PEAK lines must precede all FLANGE lines");
				}
				peaks_.push_back(PeaksINI::PeakConstants(
					idx,
					PhysicalConsts::cmPerAtmToGHzPerTorr(gammaAir),
					PhysicalConsts::cmPerAtmToGHzPerTorr(gammaWater),
					PhysicalConsts::cmPerAtmToGHzPerTorr(gammaSelf),
					tExp,
					gConc,
					gwConc,
					ghzShift,
					ls,
					lsf,
					conc,
					epp,
					range,
					microns,
					isotopeCode,
					molName,
					evName,
					fitFlags,
					press_shift_cm1));

				iFirstBackgroundPeak_ = peaks_.size();
			}
			else
			{
				assert(strPeaksINI.peaks[i].key == "FLANGE");
				if (didBeginReadingFlanges == false)
				{
					didBeginReadingFlanges = true;
				}

				int flangeIndex;
				std::string temperatureString, pressureString;
				//double temperature_C, pressure_torr, length_m;

				flangeIndex = strPeaksINI.peaks[i].flangeIndex;
//				// Use T < -273.15 to set Tfalnge = Tstack
//				// Use P < 0 to set Pflange = Pstack
//				temperature_C = strPeaksINI.peaks[i].flangeTemperature_C;
//
//				if (strPeaksINI.peaks[i].flangePressure_bar < 0)
//				{
//					pressure_torr = -1.0;
//				}
//				else
//				{
//					pressure_torr = strPeaksINI.peaks[i].flangePressure_bar * 750.062;
//				}
//
//				length_m = strPeaksINI.peaks[i].flangeLength_m;

				peaks_.push_back(PeaksINI::PeakConstants(
					idx,
					PhysicalConsts::cmPerAtmToGHzPerTorr(gammaAir),
					PhysicalConsts::cmPerAtmToGHzPerTorr(gammaWater),
					PhysicalConsts::cmPerAtmToGHzPerTorr(gammaSelf),
					tExp,
					gConc,
					gwConc,
					ghzShift,
					ls,
					lsf,
					conc,
					epp,
					range,
					microns,
					isotopeCode,
					molName,
					evName,
					fitFlags,
					press_shift_cm1,
					flangeIndex)); //,
//					temperature_C,
//					pressure_torr,
//					length_m));
			}
		}
	}
	for (std::size_t i = 0; i < strPeaksINI.slaveMasters.size(); i++)
	{
		master = strPeaksINI.slaveMasters[i].master;
		slave = strPeaksINI.slaveMasters[i].slave;
		key = strPeaksINI.slaveMasters[i].key;

		if (key.compare("X_SLAVE") == 0)
		{
			peaks_[master].xSlaves().push_back(slave);
		}
		if (key.compare("AREA_SLAVE") == 0)
		{
			peaks_[master].areaSlaves().push_back(slave);
		}
		if (key.compare("DW_SLAVE") == 0)
		{
			peaks_[master].dopplerWidthSlaves().push_back(slave);
		}
		if (key.compare("PW_SLAVE") == 0)
		{
			peaks_[master].pressureWidthSlaves().push_back(slave);
		}
	}
}

/*
void PeaksINI::updateFlangeTP(const std::vector<double> & flangeTemperature_C,
    const std::vector<double> & flangePressure_torr)
{
	if (PeaksINI::numBackgroundPeaks() == 0)
    {
        // no flange peaks.
        return;
    }
    
    for (size_t iPeak = 0; iPeak < peaks_.size(); iPeak++)
    {
        if (peaks_[iPeak].isFlange() == true)
        {
            int flangeIndex = peaks_[iPeak].flangeIndex();
            peaks_[iPeak].setFlangeTemperature_C(flangeTemperature_C.at(flangeIndex));
            peaks_[iPeak].setFlangePressure_torr(flangePressure_torr.at(flangeIndex));
        }
    }
}
*/


//PeaksINI::IsotopeConcentration::
//IsotopeConcentration(std::string isotopeCode, double expectedConc_ppm,
//    bool doUsePreviousConcentration, int numValuesToAverage) :
//    isotopeCode_(isotopeCode),
//    isotope_ppm_(expectedConc_ppm),
//    doUsePrev_(doUsePreviousConcentration),
//    numValuesToAvg_(numValuesToAverage)
//{
//    //moleculeCode_ = PhysicalConsts::getMoleculeType(isotopeCode_);
//}


PeaksINI::PeakConstants::PeakConstants(
    int index,
    double gammaAir,
    double gammaWater,
    double gammaSelf,
    double tExponent,
    double self_ppm,
    double water_ppm,
    double shiftGHz,
    double lineStrength,
    double lineStrengthFudgeFactor,
    bool wantConc,
    double epp,
    double rangeGHz,
    double microns,
    const std::string & isotopeCode,
    const std::string & molName,
    const std::string & evName,
    const std::string & fitFlagString,
    double press_shift_cm1) :
    index_(index),
    gammaAir_(gammaAir),
    gammaWater_(gammaWater),
    gammaSelf_(gammaSelf),
    tExp_(tExponent),
    selfConcentration_(self_ppm),
    waterConcentration_(water_ppm),
    ghzShift_(shiftGHz),
    lineStrength_(lineStrength),
    lsf_(lineStrengthFudgeFactor),
    wantConc_(wantConc),
    epp_(epp),
    range_(rangeGHz),
    microns_(microns),
    isotopeCode_(isotopeCode),
    molName_(molName),
    evName_(evName),
    fitFlagString_(fitFlagString),
    press_shift_cm1_(press_shift_cm1),
    flangeIndex_(-1),
    xSlaves_(),
    areaSlaves_(),
    dopplerWidthSlaves_(),
    pressureWidthSlaves_()
{
}

// Flange peak constructor
PeaksINI::PeakConstants::PeakConstants(
    int index,
    double gammaAir,
    double gammaWater,
    double gammaSelf,
    double tExponent,
    double self_ppm,
    double water_ppm,
    double shiftGHz,
    double lineStrength,
    double lineStrengthFudgeFactor,
    bool wantConc,
    double epp,
    double rangeGHz,
    double microns,
    const std::string & isotopeCode,
    const std::string & molName,
    const std::string & evName,
    const std::string & fitFlagString,
    double press_shift_cm1,
    int flangeIndex) :
    index_(index),
    gammaAir_(gammaAir),
    gammaWater_(gammaWater),
    gammaSelf_(gammaSelf),
    tExp_(tExponent),
    selfConcentration_(self_ppm),
    waterConcentration_(water_ppm),
    ghzShift_(shiftGHz),
    lineStrength_(lineStrength),
    lsf_(lineStrengthFudgeFactor),
    wantConc_(wantConc),
    epp_(epp),
    range_(rangeGHz),
    microns_(microns),
    isotopeCode_(isotopeCode),
    molName_(molName),
    evName_(evName),
    fitFlagString_(fitFlagString),
    press_shift_cm1_(press_shift_cm1),
    flangeIndex_(flangeIndex),
    xSlaves_(),
    areaSlaves_(),
    dopplerWidthSlaves_(),
    pressureWidthSlaves_()
{
}

// Index   Parameter
// -----------------
//   0     X (position)
//   1     A (area)
//   2     D (doppler width)
//   3     P (pressure width)
//
// Values
// ------
//   F     Fit this parameter, initial value from first principles
//   .     Don't fit, value computed from first principles
//   f     Fit this paramter, initial from feed forward
//   -     Don't fit, fixed value from feed forward
//

std::vector<bool> PeaksINI::PeakConstants::fitFlags() const
{
    std::vector<bool> fitFlagVec(4);
    
    if (fitFlagString_.size() < 4 || (
                (fitFlagString_.find("X") != std::string::npos) ||
                (fitFlagString_.find("A") != std::string::npos) ||
                (fitFlagString_.find("D") != std::string::npos) ||
                (fitFlagString_.find("P") != std::string::npos) ||
                (fitFlagString_.find("N") != std::string::npos)) )
    {

        if(fitFlagString_.find("X") != std::string::npos)
            fitFlagVec[0] = true;

        if(fitFlagString_.find("A") != std::string::npos)
            fitFlagVec[1] = true;

        if(fitFlagString_.find("D") != std::string::npos)
            fitFlagVec[2] = true;

        if(fitFlagString_.find("P") != std::string::npos)
            fitFlagVec[3] = true;
    }
    else
    {
        if((fitFlagString_[0] == 'F') || (fitFlagString_[0] == 'f'))
            fitFlagVec[0] = true;

        if((fitFlagString_[1] == 'F') || (fitFlagString_[1] == 'f'))
            fitFlagVec[1] = true;

        if((fitFlagString_[2] == 'F') || (fitFlagString_[2] == 'f'))
            fitFlagVec[2] = true;

        if((fitFlagString_[3] == 'F') || (fitFlagString_[3] == 'f'))
            fitFlagVec[3] = true;
    }

    return fitFlagVec;
}



// Flange things
bool PeaksINI::PeakConstants::isFlange() const
{
    return flangeIndex_ != -1;
}

int PeaksINI::PeakConstants::flangeIndex() const
{
    if (!isFlange())
        throw std::logic_error("Requesting flange index from non-flange peak");
    return flangeIndex_;
}

/*
bool PeaksINI::PeakConstants::hasFlangeTemperature() const
{
    if (!isFlange())
        throw std::logic_error("Requesting flange T from non-flange peak");
    return temperature_C_ >= -273.15;
}

double PeaksINI::PeakConstants::flangeTemperature_C() const
{
    if (!hasFlangeTemperature())
        throw std::logic_error("Requesting flange temperature from flange without fixed T");
    return temperature_C_;
}

bool PeaksINI::PeakConstants::hasFlangePressure() const
{
    if (!isFlange())
        throw std::logic_error("Requesting flange P from non-flange peak");
    return pressure_torr_ >= 0.0;
}

double PeaksINI::PeakConstants::flangePressure_torr() const
{
    if (!hasFlangePressure())
        throw std::logic_error("Requesting flange pressure from flange without fixed P");
    return pressure_torr_;
}

double PeaksINI::PeakConstants::flangeLength_m() const
{
    if (!isFlange())
        throw std::logic_error("Requesting flange length from non-flange peak");
    return length_m_;
}
*/



//#
//#    IDX TYPE    NAME  MICRON     GS      LS     LSA     GA      GS      GW GAA   GSA   GWA     N   GCONC   GWCONC    E''       RANGE   C FIT
//#------------------------------------------------------------------------------------------------------
//#PEAK 0   O2   O2   1.26882411   0.0   1.086e-25  1     0.051  0.0500  0.0900  1.0    1.0   1.0    0.71  2.0e5   1e5    81.5805      2.0     Y XAP 
//
//PEAK 0   O2   O2   13140.567357 0.0   7.338E-24  1     .0490  0.048   0.000   1.0    1.0    1.0    0.74  2.1e5   0    81.5805  0     N N

std::ostream & operator<<(std::ostream & str, const PeaksINI::PeakConstants & p)
{
    str << p.isotopeCode() << " "
        << p.evName() << " "
        << p.microns() << " "
        << p.shift_GHz() << " "
        << p.lineStrength() << " "
        << p.lineStrengthFudgeFactor() << " "
        << p.gammaAir() << " "
        << p.gammaSelf() << " "
        << p.gammaWater() << " "
        << "(1.0) (1.0) (1.0) "
        << p.tExponent() << " " // "N"
        << p.concentrationEstimate_ppm() << " "
//        << p.waterConcentrationEstimate_ppm() << " "
        << p.epp() << " "
        << p.range_GHz() << " "
        << (p.doWantConcentration() ? "Y" : "N") << " "
        << p.fitFlagString();
    str << " xSlaves";
    for (std::size_t ii = 0; ii < p.xSlaves().size(); ii++)
        str << " " << ii;
    str << " aSlaves";
    for (std::size_t ii = 0; ii < p.areaSlaves().size(); ii++)
        str << " " << ii;
    str << " dwSlaves";
    for (std::size_t ii = 0; ii < p.dopplerWidthSlaves().size(); ii++)
        str << " " << ii;
    str << " pwSlaves";
    for (std::size_t ii = 0; ii < p.pressureWidthSlaves().size(); ii++)
        str << " " << ii;
    
    return str;
}



