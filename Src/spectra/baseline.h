//---------------------------------------------------------------------------
//
// ICOS Baseline class
//
// This code supports multiple baseline functions. Currently two are supported
//
//      y = a0 + a1*x + a2*x^2 + a3*x^3
//
//      y = a0 + a1*x + a1*a2*x^2           (a3 set to 0)
//
// The type of baseline used is set with the model_ flag. The first above
// (model_ = 0) is the usual polynomial form. The second (model_ = 1) was
// concocted by Feng Dong and/or Manish Gupta to enhance concentration
// linearity measurements over a wide range
//
#ifndef baselineH
#define baselineH

#include <vector>
#include <string>

//---------------------------------------------------------------------------

/**
 * Representation of polynomial baseline signal.
 *
 * This code supports multiple baseline functions. Currently two are supported
 *
 *      y = a0 + a1*x + a2*x^2 + a3*x^3
 *
 *      y = a0 + a1*x + a1*a2*x^2           (a3 set to 0)
 *
 * The type of baseline used is set with the model_ flag. The first above
 * (model_ = 0) is the usual polynomial form. The second (model_ = 1) was
 * concocted by Feng Dong and/or Manish Gupta to enhance concentration
 * linearity measurements over a wide range.
 *
 */
class Baseline {
public:
    Baseline(void);
    Baseline(int model, std::vector<double> a, std::vector<bool> ia);

//In one spectral fit (5 calls to the Levenberg-Marquardt routine) we start
//off with the parameters read from the peaks ini files and are stored in
//params and origParams.  With each estimate and Levenberg-Marquardt fit
//iteration, the parameters in params are changed until the final fit.  Params
//are then used to report the gas concentration.  When a new spectra is
//eported, params are reset with origParams to start the fit again.
    int getNumParams() const { return 4; }
    void setOrigParams(double a0, double a1, double a2, double a3);
    void setParams(double a0, double a1, double a2, double a3);
    void setParams(std::vector<double>);
    void setOrigParams(std::vector<double>);
    void resetParams(void);
    void setFitFlags(bool, bool, bool, bool);
    void setFitFlags(std::vector<bool>);

    const std::vector<double> & getParams(void) const;
    std::vector<std::string> getParamNames(void);
    std::vector<bool> getFitFlags(void);
    bool isBinFit(void);
    
    /**
     * Evaluate the baseline at a given frequency or DAQ point.
     * Either x or xPt will be used, but not both.
     *
     * Behavior depends on model_:
     *  model_ == 0: usual polynomial, evaluated in x
     *  model_ == 1: FD & MG constrained model, evaluated in x
     *  model_ == 2: usual polynomial, evaluated in xPt
     *  model_ == 3: FD & MG constrained model, evaluated in xPt
     *
     * @param x: frequency
     * @param xPt: DAQ point
     */
    double f(double x, int xpt) const;
    std::vector<double> dfda(double x, int xpt) const;

private:
    // Select the function form of the baseline (define f in y=f(x)).  In some
    // models the x is in GHz, in others it's a bin number or daq point index.
    // We've added the bin number representation because Ian believes that
    // the 2um CCIA fit works better if we use evenly spaced points.
    //
    int model_; // TODO enumerate models

    std::vector<double> oa_, a_;
    std::vector<bool> ia_;
};
#endif
