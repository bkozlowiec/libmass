//
//  etalon.cpp
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 2/13/16.
//
//

#include "etalon.h"

#include <cassert>
#include <cmath>

const int Etalon::NUM_COEFFICIENTS = 5;
const int Etalon::NUM_STRETCH_INDEXES = 2;
//Allow etalon stretch only to +/-10%
const double Etalon::MAX_STRETCH_FACTOR = 1.1;

Etalon::Etalon() :
    coefficients_(NUM_COEFFICIENTS, 0.0),
    fitEnabled_(false),
    fitInterval_(1),
    fitFlags_(NUM_COEFFICIENTS, false),
	stretchEnabled_(false),
	stretchPeakIndexes_(NUM_STRETCH_INDEXES, 0),
	stretchFactor_(1.0),
	stretchNu0_(0.0)
{
}

Etalon::~Etalon()
{
    // hi
}

// Etalon stretching
void Etalon::setStretchFactor(double factor)
{
	if (factor > MAX_STRETCH_FACTOR)
	{
		stretchFactor_ = MAX_STRETCH_FACTOR;
	}
	else if (factor < 2 - MAX_STRETCH_FACTOR)
	{
		stretchFactor_ = 2 - MAX_STRETCH_FACTOR;
	}
	else
	{
		stretchFactor_ = factor;
	}
}


void Etalon::setStretchPeakIndexes(const std::vector<int> & indexes)
{
	assert(indexes.size() == 2);
	assert(stretchPeakIndexes_.size() == 2);
	stretchPeakIndexes_ = indexes;
}

void Etalon::setStretchPeakIndexes(int idx0, int idx1)
{
	assert(stretchPeakIndexes_.size() == 2);
	stretchPeakIndexes_[0] = idx0;
	stretchPeakIndexes_[1] = idx1;
}

void Etalon::setCoefficients(const std::vector<double> & coeffs)
{
    assert(coeffs.size() == 5);
    assert(coefficients_.size() == 5);
    coefficients_ = coeffs;
}

void Etalon::setCoefficients(double aa, double bb, double cc, double dd, double ee)
{
    assert(coefficients_.size() == 5);
    
    coefficients_[0] = aa;
    coefficients_[1] = bb;
    coefficients_[2] = cc;
    coefficients_[3] = dd;
    coefficients_[4] = ee;
}



void Etalon::setFitFlags(bool aa, bool bb, bool cc, bool dd, bool ee)
{
    assert(fitFlags_.size() == 5);
    
    fitFlags_[0] = aa;
    fitFlags_[1] = bb;
    fitFlags_[2] = cc;
    fitFlags_[3] = dd;
    fitFlags_[4] = ee;
}

void Etalon::setFitFlags(const std::vector<bool> & flags)
{
    assert(flags.size() == 5);
    assert(fitFlags_.size() == 5);
    fitFlags_ = flags;
}




void Etalon::getFrequencies_GHz(int numPoints, std::vector<double> & outFreqs) const
{
    double xghz;
    outFreqs.assign(numPoints, 0.0);
    
    // Skip the first two points because they cause NaNs
    for(int ii = 2; ii < numPoints; ii++)
    {
		if (stretchEnabled_ == true)
		{
			xghz =  coefficients_[0] * stretchFactor_ + (1 - stretchFactor_) * stretchNu0_ +
					coefficients_[1] * stretchFactor_ * ii +
					coefficients_[2] * stretchFactor_ * ii / log(ii) +
					coefficients_[3] * stretchFactor_ * log(ii) / ii +
					coefficients_[4] * stretchFactor_ / ii;
		}
		else
		{
			xghz =  coefficients_[0] +
					coefficients_[1] * ii +
					coefficients_[2] * ii / log(ii) +
					coefficients_[3] * log(ii) / ii +
					coefficients_[4] / ii;
		}
		outFreqs[ii] = xghz;
    }
}

int Etalon::getPoint(double xghz, const std::vector<double> & frequencies) const
{
    double pt = 0;
    for(std::size_t ii = 50; ii < frequencies.size()-1; ii++)
    {
        double ghz1 = frequencies.at(ii);
        double ghz2 = frequencies.at(ii+1);
        double dghz = fabs(ghz1 - ghz2);
        if(dghz > 1e-10 && fabs(xghz-ghz1) <= dghz && fabs(xghz-ghz2) < dghz)
            pt = ii + (xghz-ghz1)/(ghz2-ghz1);
    }
    return static_cast<int>(pt);
}

void Etalon::getPoints(const std::vector<double> & frequencies, std::vector<int> & outPoints) const
{
    outPoints.assign(frequencies.size(), 0);
    
    for (std::size_t ii = 0; ii < frequencies.size(); ii++)
    {
        outPoints[ii] = getPoint(frequencies[ii], frequencies);
    }
}

