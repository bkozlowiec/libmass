#ifndef LEVMARPEAKFIT_H
#define LEVMARPEAKFIT_H

#include <vector>
#include <map>
#include "../nr/nrutil.h"
#include "../nr/nrtypes.h"
#include "../nr/nrr.h"
#include "spectrapeaks.h"
#include "concentrationcalculator.h"
#include "../utils/tools.h"
#include "peaks_ini.h"

//#include <mutex>

/**
 * 
 */
class LevmarPeakFit
{
public:
    //static std::mutex mut;
    explicit LevmarPeakFit(const PeaksINI & peaksINI);
    
    /**
     * Saves the spectra peaks and sets haveReadICOSStruct = false and
     * shiftFitWindow_ = false
     */
    explicit LevmarPeakFit(SpectraPeaksBase* s);
    
    void setSpectrumFunction(SpectraPeaksBase* spectraFunc) { spectraFunc_ = spectraFunc; }
    SpectraPeaksBase* getSpectra() const { return spectraFunc_; }
    
    // SPCImplementation uses this
    const std::vector<double> & getFrequencies() const { return FitXs_; }
    
    // SPCImplementation
    void initFrequenciesAndRegions(const PeaksINI & peaksINI, int numFrequencies);
    
    // LMPF::fitSpectra
    void nrLoop(int iters, int stride, const std::vector<double> & inVoltage);
    
    // LMPF::fitSpectra
    std::vector<double> nrLoopBl(int iters, int stride, const std::vector<double> & inVoltage);
    
    // SPC::performFit
    void fitSpectra(const PeaksINI & peaksINI,
                    const std::vector<double> & inVoltage,
                    std::vector<double>& outInitYs,
                    std::vector<double>& outEstYs,
                    std::vector<double>& outFitYs,
                    bool doCalculateInitialY,
                    bool doCalculateEstimatedY,
                    bool doCalculateFitY,
                    bool doEstimateBaseline,
                    bool doHandlePeakShifts,
                    bool doEstimatePeakAmplitudes,
                    int numAmplitudeEstimateIterations,
                    int numFitIterations,
                    int stride,
                    double pressure_torr);
    
    // unused (keep it though)
    double getSpectraFitChisq(void) const { return chisq_; }
	// Reduced ch_squared = chi_squared / (# degrees of freedom). #DoF = N_points-N_variables

	double getSpectraFitReducedChisq(void) const { return reduced_chisq_; }
    
    // REVIEW getBaseline... functions look like clutter since there are only
    // ever two baseline regions and they're never meaningfully used as vectors.
    // PCH151024
    // initFrequenciesAnd...
    void setBaselineRegions(int s0, int l0, int s1, int l1);
    // baselineEst_()
    std::vector<int> getBaselineRegionStart(void) const; // always returns length 2 PCH151024
    // baselineEst_()
    std::vector<int> getBaselineRegionLength(void) const; // always returns length 2 PCH151024

    // Define fittable spectra in DAQ points space.
    //
    // initFrequ...
    void setSpectraRegion(int iStart, int numPoints);
    
    // nrLoop, nrLoopBl, tauAbsorptionCorr
    int getSpectraStart(void) const;
    
    // nrLoop, nrLoopBl, tauAbsorptionCorr
    int getSpectraLength(void) const;
    
    /**
     * Tau correction compensates for the cases where there is an absorber at the
     * laser frequency where we are measuring ringdown.  In congested spectra such
     * as water vapor and CH4 isotope, this problem is unavoidable.  The absorber
     * will result in a fitted tau that is too fast, i.e. the mirrors appear more
     * lossy and the derived effective path length is too short.
     */
    double tauAbsorptionCorrection(void) const; // SPC::performFit
    
    // Divide out the baseline signal and remove voltage offset (Ygnd_).
    // The frequency, if needed, is obtained from xpt.
    //
    /**
     * @param inVoltage  Laser ramp signal minus offset voltage
     */
    double debaselinedY(int xpt, double tinyBaseline,
        const std::vector<double> & inVoltage) const; // estPeakPos
    
    // Divide out the baseline signal and remove voltage offset (Ygnd_).
    // xGHz is only used if the baseline evaluates at frequencies and not DAQ
    // indices!  so this function is really deprecated.  PCH151230
    
    /**
     * @param inVoltage  Laser ramp signal minus offset voltage
     */
    double debaselinedY(double xGHz, int xpt, double tinyBaseline,
        const std::vector<double> & inVoltage) const; // estPeakAmpl
    
	/**
	* Return Current Etalon Stretch factor
	*/
	double getEtalonStretch() const { return etalon_.getStretchFactor(); };

private:
    //
    // Object containing the funcs to generate spectra from the parameters
    // and calculated the spectra derivative function.
    //
    SpectraPeaksBase* spectraFunc_;
    
	// Etalon
	// We need to modify thios object if Etalon Stretching is enabled.
	// We will need to recalculate X coordinates from Etalon every time if Etalon Stretch is enabled
	Etalon etalon_;
	double etalonStretchDeltaOriginalGHz_;

    // Baseline estimator vars
    //
    int baselineRegion0Start_;
    int baselineRegion1Start_;
    int baselineRegion0Length_;
    int baselineRegion1Length_;
    
    // Define in DAQ points space where we can fit spectra
    int origSpectraFitStartPt_;
    int spectraFitStartPt_; // in estPeakAmplitudes, setSpectraRegion, getSpectraStart
    int spectraFitLengthPt_; // estPeakAmplitudes, setSpectraRegion, getSpectraLength

    // Allow the fit window to shift as the laser drifts with temperature. This
    // is required for ch4iso.
    //
    bool shiftFitWindow_; // basically unused

    // Basic data
    std::vector<double> FitXs_; // Convert DAQ points into GHz (y = frequency)
    
    std::vector<double> getSnapshotOfFit(void) const;
    
    /**
     * Guess baseline offset and slope, in volts/wavenumber (wavenumber??)
     *
     * @param binFit if true, return slope in volts/bin (i.e. volts/sample)
     * @param inY    laser voltage minus voltage offset
     * @return [intercept, slope]
    */
    std::vector<double> baselineEst_(bool binFit, const std::vector<double> & inY) const;

    // replaced with updatePeakShifts
    
    /**
     * Update the running estimate of how far peaks are shifted from their
     * INI center values.
     */
    void updatePeakShifts(int fitWindowStart, int fitWindowEnd,
        const std::vector<double> & inVoltage);
    
    /**
     * Estimate a peak's amplitude before fitting.  Ignore peaks whose centers
     * are outside the fitting window because the peak center could be in the
     * ringdown section of the laser ramp.
     *
     * Estimation is performed with a fixed number of Levenberg-Marquardt
     * iterations.
     */
    void estPeakAmplitudes_(const Etalon & etalon, int numIterations,
        const std::vector<double> & inVoltage);
    
    Tools::RunningAverage recentSpectralShifts_;
    
    // TODO: need to reset this flag when the etalon function is changed!!!
    bool haveReadICOStruct_;

    // Save X^2 (chisq from Levenberg-Marqardt spectra fit) of most recent fit.
    // Presently we use this to decide if the spectra fit was good enough to use
    // the peak centers for linelock.
    // If chisq_ < badChisq_, the fit is considered "good". badChisq_ is set in
    // the peaks.ini file.
    //
    double chisq_;
    double reduced_chisq_;
    
    //double badChisq_; // Ray says it's not used.  PCH151103
};

#endif // LEVMARPEAKFIT_H
