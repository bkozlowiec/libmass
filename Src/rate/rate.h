#ifndef RATE_H
#define RATE_H

#include <string>

class Rate
{
	public:
        // TODO: does this enum serve any purpose; can the ID be a string? PCH170215
		// Unique rate id to distinguish this rate from other programmed rates.
		// We use a name based upon the sampling rate displayed on the GUI.
		enum RateID
		{
			_1000s,
			_500s,
			_200s,
			_100s,
			_75s,
			_50s,
			_25s,
			_20s,
			_10s,
			_5s,
			_2s,
			_1s,
			_0_5s,  // 2Hz
			_0_2s,  // 5Hz
			_0_1s,  // 10Hz
			_0_05s,  // 20Hz
			UNDEFINED
		};

		Rate ();
		Rate (RateID rate, int sweeps, int fits, int estIters, int iters, int stride, int rdFits);
    
		Rate::RateID getID() const; // PCH170215 never called!!!!
		int getNAvg() const;
		int getNFits() const;
		int getNIters() const;
		int getEstIters() const;
		int getMrqcofStride() const;
		int getNRDFits() const;
//		int getPlotInterval();
//		void setPlotInterval (int);
//		bool getPlotOnDemand();
//		void setPlotOnDemand (bool b);
//		std::string getIcon();

	private:
		RateID myId_;

		// Number of laser scans (raw spectra) to average before a fit.
		int nAvg_;

		// Number of fits to average at this rate before reporting a measurement.
		int nFits_;

		// Number of iterations in estPeakAmplitudes_().
		int estIters_;

		// Number of mrqmin() iterations per fit.
		int nIters_;

		// Stride through the data in mrqcof(). stride=1 means all the elements of
		// x[] are used to compute chisq, stride=2 means every other element is
		// used, stride=3 means every third element is used, etc.  The speed up in
		// the fit is proportional to the stride since this determines how many
		// times the peak functions are called.
		//
		// Normally, we'll keep stride = 1, except at rates greater than 5Hz where
		// a fit with many peaks maybe just too slow.
		//
		int mrqcofStride_;

		// Number of ringdown fits per spectra fit.
		int nRDFits_;

		// Number of fits to perform between plots. Normal set to 1, plot every fit,
		// but in fast mode the user may want to plot less frequently to avoid dropouts.
		// Or the user might want to disable automatic plotting and resort to plotting
		// on demand (user clicks a button to update the plots). The range is 1 - ?
		// This setting is not stored in rate.ini. The default
		// values are set so that plotting is about ~1Hz. The user selected value is not
		// saved.
//		int plotInterval_;
//    void setDefaultPlotInterval_();

		// Flag on demand plotting. Default is false.
		//
//		bool plotOnDemand_;

		// This is the name of the rate as it appears in the GUI and human readable text output.
		//
//		std::string name_;

};
#endif // RATE_H
