#include "rate.h"

Rate::Rate ()
{
}
Rate::Rate (RateID rate, int avg, int fits, int estIters, int iters, int stride, int rdFits) :
    myId_ (rate), nAvg_ (avg), nFits_ (fits), estIters_ (estIters),
    nIters_ (iters), mrqcofStride_ (stride), nRDFits_ (rdFits)

{

}

int Rate::getNAvg() const
{
	return nAvg_;
}

int Rate::getNFits() const
{
	return nFits_;
}

int Rate::getNIters() const
{
	return nIters_;
}

int Rate::getEstIters() const
{
	return estIters_;
}

int Rate::getMrqcofStride() const
{
	return mrqcofStride_;
}

int Rate::getNRDFits() const
{
	return nRDFits_;
}

//int Rate::getPlotInterval()
//{
//	return plotInterval_;
//}
//
//bool Rate::getPlotOnDemand()
//{
//	return plotOnDemand_;
//}
//
//void Rate::setPlotOnDemand (bool b)
//{
//	plotOnDemand_ = b;
//}

Rate::RateID Rate::getID() const
{
	return myId_;
}

//std::string Rate::getIcon()
//{
//	return name_;
//}
//
//void Rate::setPlotInterval (int i)
//{
//	plotInterval_ = i;
//}



