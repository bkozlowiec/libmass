//
//  cavityproperties.h
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 3/21/17.
//
//

#ifndef cavityproperties_h
#define cavityproperties_h


class CavityProperties
{
public:
    CavityProperties()
    {
    }
    
    CavityProperties(double T_C, double P_torr, double tau_us) :
        temperature_C_(T_C),
        pressure_torr_(P_torr),
        tau_us_(tau_us)
    {
    }
    
    // using failsafe T & P
    CavityProperties(double T_C, double P_torr, double tau_us, double safeT_C, double safeP_torr) :
        temperature_C_(chooseTemperature(T_C, safeT_C)),
        pressure_torr_(choosePressure(P_torr, safeP_torr)),
        tau_us_(tau_us)
    {
    }
    
    double temperature_C() const { return temperature_C_; }
    double pressure_torr() const { return pressure_torr_; }
    double tau_us() const { return tau_us_; }

private:

    double choosePressure(double instrument_torr, double default_torr) const
    {
        double P_torr;
        
        // If the instrument is providing a valid pressure, use that, else use
        // the provisional value that was read previously from INIs.
        if (instrument_torr > 0.0)
            P_torr = instrument_torr;
        else
            P_torr = default_torr;
        return P_torr;
    }

    double chooseTemperature(double instrument_C, double default_C) const
    {
        double T_celsius;
        
        // If the instrument is providing a valid temperature, use that, else use
        // the provisional value that was read previously from INIs.
        if (instrument_C > -273.15)
            T_celsius = instrument_C;
        else
            T_celsius = default_C;
        return T_celsius;
    }

    double temperature_C_;
    double pressure_torr_;
    double tau_us_;
};


#endif /* cavityproperties_h */
