/**
 * spc_interface.h
 *
 *  This interface header contains an interface class for the
 *  ATW SPC process to use the functionality of data processing.
 */

#ifndef SPC_INTERFACE_H_
#define SPC_INTERFACE_H_

#include <vector>
#include <map>
#include <string>

#include "ini_structs.h"

class SPCManager;

/**
 * @class SPCInterface
 * @brief Provide interface method to access the data processing
 * functionality of the input spectrum.
 *
 * There is a strict pattern in which the different interfaces of this
 * SPCInterface needs to be called. The exact usage of the different
 * interfaces can be seen in the testProgram provided alongwith library.
 * However, the pattern is:
 *
 * 1. Call SPCFactory::createInterface(configurationDirectory) to create
 * 		a single instance (say objSPCInterface) of this interface with
 * 		a path to the configuration directory of INI files.
 *
 * 2. Call objSPCInterface->initSPC(numOfLasers) to initialize the SPCInterface
 * 		with a number of laser configured in the system. Specify 1 for
 * 		Cross Stack Analyzer as it is a single laser path system.
 *
 * 3. Once the spectrum data is received, call following methods of the
 * 		SPCInterface to get the parameters on how to segment the spectrum
 * 		in laser ramp and ringdown/laser-off arrays.
 * 		get laser ramp start index using objSPCInterface->getLaserRampStartIdx()
 * 		get length of laser ramp using objSPCInterface->getLaserRampLength()
 * 		get laser off start index using objSPCInterface->getLaserOFFStartIdx()
 * 		get length of laser off using objSPCInterface->getLaserOFFLength()
 *
 * 4. Call objSPCInterface->computeSpectrum(.) to process the spectrum data
 *
 * 5. Now to get the processed data call following different interface methods.
 * 	i.   Call objSPCInterface->getRawSignal(laserIdx) to get raw signal for the
 * 			transmitted intensity plot. This actually is a combined vector of
 * 			laser ramp and ringdown array passed to the computeSpectrum.
 *
 * 	ii.  Call objSPCInterface->getBaselineFit(laserIdx) to get the baseline fit
 * 			data points for the transmitted intensity plot.
 *
 * 	iii. Call objSPCInterface->getRingdownFit(laserIdx) to get the Ringdown fit
 * 			data points for the transmitted intensity plot.
 *
 * 	iv.  Call objSPCInterface->getSamplePts(laserIdx) to get the X-axis data
 * 			points for the transmitted intensity plot.
 *
 * 	v.   Call objSPCInterface->getBaselineFitStart(laserIdx) to get the starting
 * 			index of the baseline fit to start plotting on transmitted
 * 			intensity plot. Discard data points earlier than this index of
 * 			vector returned from objSPCInterface->getBaselineFit(laserIdx)
 *
 * 	vi.  Call objSPCInterface->getRingdownFitStart(laserIdx) to get the starting
 * 			index of the ringdown fit to start plotting on transmitted
 * 			intensity plot. Discard data points earlier than this index of
 * 			vector returned from objSPCInterface->getRingdownFit(laserIdx)
 *
 * 	vii. Call objSPCInterface->getMeasuredData(laserIdx) to get the data points
 * 			of the measured data for the Absorption plot.
 *
 * 	viii.Call objSPCInterface->getPeakFit(laserIdx) to get the data points
 * 			of the peak fit data for the Absorption plot.
 *
 * 	ix.  Call objSPCInterface->getGHzPts(laserIdx) to get the X-axis data
 * 			points (frequency) for the absorption plot.
 *
 * 	x.   Call objSPCInterface->getEVs(laserIdx) to get the gas concentration
 * 			values computed for the given spectrum for the time chart.
 *
 * TODO Error codes translation
 *
 */
class SPCInterface
{
	public:
		virtual ~SPCInterface ();

		/**
		 * @brief Initializes the SPCInterface library with number
		 * of lasers in the system.
		 *
		 * @param p_laserCount Number of lasers
		 *
		 * @return 0 if success.
		 */
		virtual int initSPC (const std::string & iniFilePath, unsigned char p_laserCount) = 0;
    
        // Method that does the computing
		/**
		 * This method process the spectrum data and generates the
		 * computed spectrums for the different data plots.
		 *
		 * @param p_temperature Temperature of the laser cavity
		 * @param p_pressure Gas pressure in laser cavity
		 * @param p_lr Laser ramp from the spectrum data
		 * @param p_rdData ringdown from the spectrum data
		 * @param p_rdCnt Number of ringdown in the spectrum.
		 *
		 * @return 0 if success.
		 */
		virtual int computeSpectrum (double p_temperature, double p_pressure,
		                     const std::vector<std::vector<double> > & p_lr,
		                     const std::vector<std::vector<double> > & p_rdData,
		                     const std::vector<int> & p_rdCnt) = 0;
		// Method that does the computing
		/**
		* This method process the spectrum data and generates the
		* computed spectrums for the different data plots.
		*
		* @param p_temperature Temperature of the laser cavity
		* @param p_flangesTP array of Temperatures and Pressures for Flanges
		* @param p_pressure Gas pressure in laser cavity
		* @param p_lr Laser ramp from the spectrum data
		* @param p_rdData ringdown from the spectrum data
		* @param p_rdCnt Number of ringdown in the spectrum.
		*
		* @return 0 if success.
		*/
		virtual int computeSpectrum (double temperature_C,
                            double pressure_torr,
                            const std::vector<double> & flangeTemperature_C,
                            const std::vector<double> & flangePressure_torr,
                            const std::vector<std::vector<double> > & p_lr,
                            const std::vector<std::vector<double> > & p_rdData,
                            const std::vector<int> & p_rdCnt) = 0;

		//Configuration methods to get parameters on how to divide the spectrum data
		/**
		 * This method gets the start Index of Laser ramp in the
		 * Spectrum data.
		 *
		 * @return Start Index of Laser ramp in the Spectrum data
		 */
		virtual unsigned int getLaserRampStartIdx() const = 0;

		/**
		 * This method gets the length of the laser ramp in the
		 * spectrum data.
		 *
		 * @return Length of laser ramp in spectrum
		 */
		virtual unsigned int getLaserRampLength() const = 0;

		/**
		 * This method gets the start Index of Laser ramp in the
		 * Spectrum data.
		 *
		 * @return Start Index of Laser ramp in the Spectrum data
		 */
		virtual unsigned int getLaserOFFStartIdx() const = 0;

		/**
		 * This method gets the length of the laser off/ringdown
		 *  in the spectrum data.
		 *
		 * @return Length of laser off in spectrum
		 */
		virtual unsigned int getLaserOFFLength() const = 0;

		/**
		 * This method gets the number of ringdowns
		 *  in the spectrum data.
		 *
		 * @return Number of ringdowns in the spectrum
		 */
		virtual unsigned int getRDFitCnt() const = 0;

		//computed vectors after the data is processed
		/**
		 * This method gets raw signal for the	transmitted
		 * intensity plot.
		 *
		 * @param laserIdx Laser Index, default 0
		 * @return List of data points for raw signal
		 */
		virtual std::vector<double> getRawSignal(unsigned int laserIdx) const = 0;

		/**
		 * This method gets the baseline fit data points for the
		 * transmitted intensity plot.
		 *
		 * @param laserIdx Laser Index, default 0
		 * @return List of data points for baseline fit plot
		 */
		virtual std::vector<double> getBaselineFit(unsigned int laserIdx) const = 0;

		/**
		 * This method gets the Ringdown fit data points for the
		 * transmitted intensity plot.
		 *
		 * @param laserIdx Laser Index, default 0
		 * @return List of data points for ringdown fit plot
		 */
		virtual std::vector<double> getRingdownFit(unsigned int laserIdx) const = 0;

		/**
		 * This method gets the X-axis data	points for the
		 * transmitted intensity plot.
		 *
		 * @param laserIdx Laser Index, default 0
		 * @return List of x-axis data points
		 */
		virtual std::vector<unsigned int> getSamplePts(unsigned int laserIdx) const = 0;

		/**
		 * This method gets the starting index of the baseline fit
		 * to start plotting on transmitted	intensity plot.
		 *
		 * @param laserIdx Laser Index, default 0
		 * @return Starting index of baseline fit
		 */
		virtual unsigned int getBaselineFitStart(unsigned int laserIdx) const = 0;

		/**
		 * This method gets the starting index of the ringdown fit
		 * to start plotting on transmitted	intensity plot.
		 *
		 * @param laserIdx Laser Index, default 0
		 * @return Starting index of ringdown fit
		 */
		virtual unsigned int getRingdownFitStart(unsigned int laserIdx) const = 0;

		/**
		 * This method gets the data points	of the measured data
		 * for the Absorption plot.
		 *
		 * @param laserIdx Laser Index, default 0
		 * @return List of data points for measured data
		 */
		virtual std::vector<double> getMeasuredData(unsigned int laserIdx) const = 0;

		/**
		 * This method gets the data points	of the peak fit data
		 * for the Absorption plot.
		 *
		 * @param laserIdx Laser Index, default 0
		 * @return List of data points for peak fit
		 */
		virtual std::vector<double> getPeakFit(unsigned int laserIdx) const = 0;

		/**
		* This method gets the data points	of the fit residual data
		* for the Absorption plot.
		*
		* @param laserIdx Laser Index, default 0
		* @return List of data points for peak fit
		*/
		virtual std::vector<double> getFitResidual(unsigned int laserIdx) const = 0;

		/**
		* This method gets the data points	of the initial guess fit data
		* for the Absorption plot.
		*
		* @param laserIdx Laser Index, default 0
		* @return List of data points for peak fit
		*/
		virtual std::vector<double> getInitialGuessFit(unsigned int laserIdx) const = 0;

		/**
		* This method gets the data points	of the estimated fit data
		* for the Absorption plot.
		*
		* @param laserIdx Laser Index, default 0
		* @return List of data points for peak fit
		*/
		virtual std::vector<double> getEstimatedFit(unsigned int laserIdx) const = 0;

		/**
		* This method gets the data points	of the measured data
		* with Baseline.
		*
		* @param laserIdx Laser Index, default 0
		* @return List of data points for measured data
		*/
		virtual std::vector<double> getMeasuredDataWithBL(unsigned int laserIdx) const = 0;

		/**
		* This method gets the data points	of the peak fit data
 		* with Baseline.
		*
		* @param laserIdx Laser Index, default 0
		* @return List of data points for peak fit
		*/
		virtual std::vector<double> getPeakFitWithBL(unsigned int laserIdx) const = 0;

		/**
		* This method gets the data points	of the initial guess fit data
		* with Baseline.
		*
		* @param laserIdx Laser Index, default 0
		* @return List of data points for peak fit
		*/
		virtual std::vector<double> getInitialGuessFitWithBL(unsigned int laserIdx) const = 0;

		/**
		* This method gets the data points	of the estimated fit data
		* with Baseline.
		*
		* @param laserIdx Laser Index, default 0
		* @return List of data points for peak fit
		*/
		virtual std::vector<double> getEstimatedFitWithBL(unsigned int laserIdx) const = 0;

		/**
		 * This method gets the X-axis data	points (frequency) for
		 * the absorption plot.
		 *
		 * @param laserIdx Laser Index, default 0
		 * @return List of frequency points for X axis
		 */
		virtual std::vector<double> getGHzPts(unsigned int laserIdx) const = 0;

		/**
		 * This method gets the gas concentration values computed
		 * for the given spectrum for the time chart.
		 *
		 * @return Key value pair of gas concentrations with key as
		 * 			gas and value as concentrations.
		 */
		virtual std::map<std::string, double> getEVs() const = 0;
    
		/**
		 * Return the baseline parameters used for fitting the ramp data.
         * The meaning of the parameters will vary depending on baseline model.
		 *
		 * @param laserIdx Laser Index, default 0
		 * @return Vector of baseline parameters
		 */
        virtual std::vector<double> getBaselineParameters(unsigned int laserIdx) const = 0;
    
		/**
		 * Return the peak parameters used for fitting the ramp data.
         * The meaning of the parameters will vary depending on baseline model.
		 *
		 * @param laserIdx Laser Index, default 0
		 * @return Vector of baseline parameters
		 */
        virtual std::vector<double> getPeakParameters(unsigned int laserIdx) const = 0;
        /**
 		 * Return the Goodness of fit parameter.
		 *
		 * @param laserIdx Laser Index, default 0
		 * @return Double representing goodness of fit
		 */
		virtual double getGoodnessOfFit(unsigned int laserIdx) const = 0;
	
		/**
		* Return the deltaTEC value for Linelock.
		*
		* @param laserIdx Laser Index, default 0
		* @return Double representing deltaTEC
		*/
		virtual double getLinelockDeltaTEC(unsigned int laserIdx) const = 0;

		/**
		* Enable/Disable Linelock.
		*
		* @param laserIdx Laser Index, default 0
		* @param Enabled is boolean
		*/
		virtual void setEnableLinelock(unsigned int laserIdx, bool Enabled) = 0;

		/**
		* Return the Laser power in Arbitrary units.
		*
		* @param laserIdx Laser Index, default 0
		* @return Double representing laser power in arb units
		* Calculated as Value of the Baseline at last point of the fit window
		*/

		virtual double getLaserPowerArbUnits(unsigned int laserIdx) const = 0;
		/**
		* Return Etalon stretch factor
		*
		* @param laserIdx Laser Index, default 0
		* @return Double representing etalon stretch factor
		*/

		virtual double getEtalonStretchFactor(unsigned int laserIdx) const = 0;

		/**
		* Return Peak Zero Shift in GHz
		*
		* @param laserIdx Laser Index, default 0
		* @return Double Peak 0 shift in GHz
		*/
		virtual double getPeakZeroShiftGHz(unsigned int laserIdx) const = 0;

		/**
		* return Linelock Out-of-Bounds condition.
		*
		* @param laserIdx Laser Index, default 0
		* @param returns Boolean representation of OutOfBounds Event
		*/
		virtual bool getIsLinelockOOB(unsigned int laserIdx) const = 0;

		/**
		* return True is Area is negative condition.
		*
		* @param laserIdx Laser Index, default 0
		* @param returns Boolean representation of Is negative area
		*/
		virtual bool getIsAreaNegative(unsigned int laserIdx) const = 0;

		/**
		* return True is PW is negative condition.
		*
		* @param laserIdx Laser Index, default 0
		* @param returns Boolean representation of Is negative PW
		*/
		virtual bool getIsPWNegative(unsigned int laserIdx) const = 0;

		/**
		* return DetectorNoise.
		*
		* @param laserIdx Laser Index, default 0
		* @param returns Double representing standard deviation for the laser off signal
		*/
		virtual double getDetectorNoise(unsigned int laserIdx) const = 0;

		/**
		* return TRUE if fitter will calculate Initial guess spectrum.
		*
		* @param laserIdx Laser Index, default 0
		*/
		virtual bool getDoCalculateInitialY(unsigned int laserIdx) const = 0;

		/**
		* return TRUE if fitter will calculate Estimated spectrum.
		*
		* @param laserIdx Laser Index, default 0
		*/
		virtual bool getDoCalculateEstimatedY(unsigned int laserIdx) const = 0;

		/**
		* return TRUE if fitter will calculate Fitted spectrum.
		*
		* @param laserIdx Laser Index, default 0
		*/
		virtual bool getDoCalculateFitY(unsigned int laserIdx) const = 0;

		/**
		* enables fitter to calculate Fitted guess spectrum.
		* This calcualtion take some time and set to FALSE by default.
		* Set it to TRUE if using as spectrum simulator
		* The results of this calculation are available in getInitialGuessFit()
		*
		* @param laserIdx Laser Index, default 0
		* @param enable, default false
		*/
		virtual void setDoCalculateInitialY(unsigned int laserIdx, bool enable) = 0;

		/**
		* enables fitter to calculate Estimated spectrum.
		* This calcualtion take some time and set to FALSE by default.
		* Set it to TRUE if using as spectrum simulator
		* The results of this calculation are available in getEstimatedFit()
		*
		* @param laserIdx Laser Index, default 0
		* @param enable, default false
		*/
		virtual void setDoCalculateEstimatedY(unsigned int laserIdx, bool enable) = 0;

		/**
		* enables fitter to calculate Fitted spectrum.
		* This calcualtion take some time and set to TRUE by default.
		* The results of this calculation are available in getPeakFit() and
		* used for plotting
		*
		* @param laserIdx Laser Index, default 0
		* @param enable, default true
		*/
		virtual void setDoCalculateFitY(unsigned int laserIdx, bool enable) = 0;

        enum ConfigModes
        {
            kDefaultMode = 0,
            kPressureWidthMode = 1
        };
};

class SPCManager
{
public:
    virtual ~SPCManager();
    
    // Sets peak self_ppm in strPeaksINI_
    // Sets strConfigINI_.cavityLength_
	virtual void initStack(const StructStackConfig & stack) = 0;
    
    // Appends flange peaks to strPeaksINI_.peaks_; sets slaveMasters_ too
	virtual void initFlanges(const std::vector<StructStackConfig> & flanges) = 0;
    
    // Sets etalonCoefficients_ and shift_ in strPeaksINI_
	virtual void initLaserRampArguments(const StructLaserRampArguments & strEEPROM) = 0;
    
    // Appends a bunch of slaving to strPeaksINI_
	virtual void setMode(int mode) = 0;

	// Returns Error structure
	virtual const std::vector<StructError> & getErrors() = 0;

	// Returns INI version
	virtual const std::string & getVersion() const = 0;

        
    virtual SPCInterface* getInterface() = 0;
};

/**
 * This class provides the instance of the SPCInterface.
 */
/*#ifdef EXPORT
#define DLL __declspec(dllexport)
#else
#define DLL __declspec(dllimport)
#endif*/

class SPCFactory
{
	public:
    
		/**
		 * This method creates a single instance of the SPCInterface.
		 *
		 * @return single instance of the SPCInterface class
		 */
		static SPCInterface* createInterface();
    
		/**
		 * This method creates a single instance of the LibMASS manager.
         *
         * @param iniFilePath   path of the configuration directory containing
         *                      the crossstack.ini file
         * @param analyzerType  string defining analyzer parameters inside .ini
         *                      file (crossstack.ini)
		 *
		 * @return single instance of the SPCManager class
         */
#if defined(_MSC_VER)
        static DLL SPCManager* createManager(const std::string & iniFilePath, const std::string & analyzerType);
#else
        static SPCManager* createManager(const std::string & iniFilePath, const std::string & analyzerType);
#endif

	private:
		/**
		 * Local instance of the SPCInterface.
		 */
		static SPCInterface* instance;
};

#endif /* SPC_INTERFACE_H_ */
