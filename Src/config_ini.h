//
//  config_ini.h
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 2/17/16.
//
//

#ifndef config_ini_h
#define config_ini_h

#include <stdio.h>
#include <string>
#include <vector>

#include "rate/rate.h"
#include "spc_implementation_data.h"


class ConfigINI
{
public:
    ConfigINI();
    ConfigINI(const std::string & filePath);
	ConfigINI(const StructConfigIni & strConfIni);
    ~ConfigINI();
    
	void updateConfigINI(const StructConfigIni & strConfIni);

	void updateConfigINI(); // TODO: UNUSED
	void read();
    
    unsigned int getDacRate() const { return dacRate_; }
    int getSparseFactor() const { return sparseFactor_; }
    unsigned int getLaserRampStart() const { return laserRampStart_; }
    unsigned int getLaserRampLength() const { return laserRampLength_; }
    unsigned int getLaserOffStart() const { return laserOffStart_; }
    unsigned int getLaserOffLength() const { return laserOffLength_; }
    unsigned int getLaserPathAlgo() const { return laserPathAlgo_; }
    unsigned int getNumRingdownsToFit() const { return numRDToFit_; }
    const std::vector<int> & getRingdownClip() const { return rdClip_; }
    const Rate & getRate() const { return configRate_; }
    
    int numFlanges() const { return flangeLength_.size(); }
    
    double getCavityLength() const { return cavityLength_; } // TODO: move out of this class, or else move flange things in.
    const std::vector<double> & getFlangeLength() const { return flangeLength_; }
    double getDefaultTemperature_C() const { return defaultTemperature_C_; }
    double getDefaultPressure_torr() const { return defaultPressure_torr_; }
    
    const StructCavityGasIni & stackGasProperties() const { return stackGasProperties_; }
    const std::vector<StructCavityGasIni> & flangeGasProperties() const { return flangeGasProperties_; }
    const std::map<std::string, bool> & trustFitConcentration() const { return trustFitConcentration_; }
    
    enum
    {
        FIRST_LASER = 0,
        SINGLE_PATH_LASER = 1,
        RINGDOWN_LASER_PATH = 0
    };
    
protected:
    std::string fileName_;
    
    // These go into the RingDown constructor eventually
    unsigned int dacRate_;
    int sparseFactor_;
    std::vector<int> rdClip_;
    // end RingDown arguments
    
    // These are just copy of StructConfigIni
    unsigned int laserRampStart_;
    unsigned int laserRampLength_;
    unsigned int laserOffStart_;
    unsigned int laserOffLength_;
    unsigned int laserPathAlgo_;
    // TODO: only missing thing from StructConfigIni is fitIterations, which goes into configRate_.
    // end StructConfigIni fields
    
    unsigned int numRDToFit_; // TODO: if not in INI FILE, is 1 by default; no other way to set

    Rate configRate_;
    
    double cavityLength_;
    std::vector<double> flangeLength_;
    double defaultTemperature_C_;
    double defaultPressure_torr_;

    StructCavityGasIni stackGasProperties_;
    std::vector<StructCavityGasIni> flangeGasProperties_;
    std::map<std::string, bool> trustFitConcentration_;
};

#endif /* config_ini_h */
