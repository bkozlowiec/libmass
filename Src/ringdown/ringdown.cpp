#include <math.h>
#include <iostream>
#include "ringdown.h"

RingDown::RingDown()
{
    dt_ = 0; // set with setDACRate()
    tau_ = 5; // tau in microseconds
    a0_ = 0.0;   // ringdown baseline offset
    a1_ = 1.0;   // ringdown amplitude
    frontClip_ = 0; // set with setFrontClip()
    firstPoint_ = 0;
    sparseFactor_ = 1;
    fixedTau_ = -1; // < 0 value means do not override tau_.

//    atry_p = new double[3];
//    beta_p = new double[3];
//    da_p = new double[3];
}

RingDown::RingDown(int dacRate, int sparseFactor, int frontClip, double fixedTau)
{
    a0_ = 0.0;   // ringdown baseline offset
    a1_ = 1.0;   // ringdown amplitude
    firstPoint_ = 0;
    
    setDACRate(dacRate);
    setSparse(sparseFactor);
    setFrontClip(frontClip);
    setFixedTau(fixedTau);
}

void RingDown::setFrontClip(int clip) {
    if(clip < 0)
        std::cerr << "Ringdown front clip out of bounds: " << clip;
    frontClip_ = clip;
}

void RingDown::setDACRate(int dacRate)
{
    dacRate_ = dacRate;
    dt_ = 1.0e6/dacRate_;
}

void RingDown::setFixedTau(double ft)
{
    fixedTau_ = ft;
}

void RingDown::setRDTrace(const std::vector<double>& data, int rdCnt)
{
    rdToFitRaw_ = data;
    numRDInRawData_ = rdCnt;
}

void RingDown::setRDFit(double *p, int n)
{
    rdFit_.clear();
    for(int i=0; i<n; ++i){
        rdFit_.push_back(p[0]*exp(-dt_*i*p[1]) + p[2]);
    }
}

std::vector<double> RingDown::getRDFit(void) {
    double avg = 0;
    std::vector<double> sparsedFit;
    for(std::size_t i=0; i<rdFit_.size()-sparseFactor_; i+=sparseFactor_) {
        avg = 0;
        for(int j=0; j<sparseFactor_; j++) {
            avg += rdFit_.at(i+j);
        }
        avg /= sparseFactor_;
        sparsedFit.push_back(avg);
    }
    return sparsedFit;
}
int RingDown::getRDFitFirstPoint(void) {
    return firstPoint_- frontClip_;
}

std::vector<double> RingDown::getRDTrace(void) {
    double avg = 0;
    std::vector<double> sparsedData;
    for(std::size_t i=0; i<rdToFitRaw_.size()-sparseFactor_; i+=sparseFactor_) {
        avg = 0;
        for(int j=0; j<sparseFactor_; j++) {
            avg += rdToFitRaw_.at(i+j);
        }
        avg /= sparseFactor_;
        sparsedData.push_back(avg);
    }
    return sparsedData;
}

std::vector<double> RingDown::getSimRD(double p0, double p1, double p2, int numPts) {
    std::vector<double> x;
    for(int i=0; i<numPts; i++)
        x.push_back(p0*exp(-i*p1) + p2);
    return x;
}

void RingDown::fitExp(double &avgTau, double &avgTauStdDev,
                      double &avgOffset, double &avgOffsetStdDev,
                      int &nTau) {
    int n=rdToFitRaw_.size()/numRDInRawData_;
    const int m=3;
    double p[m];
    int sparse; // REVIEW what is this, looks like flag but isn't flag PCH150929
    std::vector<double> pVec(3);
    std::vector<double> tau;
    std::vector<double> offset;
    std::vector<double> rdToFitRaw;
    
    for(int fitCnt=0; fitCnt<numRDInRawData_; fitCnt++) {
        rdToFitRaw.clear();
        //get the section of rdToFitRaw_ vector
        for(int tIdx = 0; tIdx < n; tIdx++)
        {
            rdToFitRaw.push_back(rdToFitRaw_.at(fitCnt*n + tIdx));
        }
        sparse = ExpPreProcess(rdToFitRaw, rdToFit_, pVec);

        // FIXME: these three lines seemingly do nothing, remove them PCH150929
//        p[0] = pVec[0];
//        p[1] = pVec[1]*2;
//        p[2] = pVec[2];
        
        ExpLMFit(rdToFit_,pVec); // THIS DOES EVERYTHING PCH150929

        p[0] = pVec[0];
        p[1] = 1.0/(pVec[1]*dt_);
        p[2] = pVec[2];

        setRDFit(p,rdToFit_.size());
        if(fixedTau_ > 0.0)
            tau.push_back(fixedTau_);
        else
            tau.push_back(sparse/p[1]);
        offset.push_back(p[2]);
    }

    nTau = tau.size();
    Tools::standardDeviation(tau, avgTau, avgTauStdDev);
    Tools::standardDeviation(offset, avgOffset, avgOffsetStdDev);
}

int RingDown::ExpPreProcess(std::vector<double> rdToFitRaw, std::vector<double>& rdToFit,
                            std::vector<double>& p) {
    int j = 0;
    int sparse = 0;
    int Ns = 0;
    int NrdToFitRaw = rdToFitRaw.size();
    int firstPt = 0;
    firstPoint_ = 0;
    rdToFit.clear();

    // FIXME there's no reason for these numbers to be an array; never indexed
    // sequentially or anything PCH151005
    // clips[0] = N pts leading edge clip (pS->RDFrontEdge)
    // clips[1] = Top edge clip (2000)
    // clips[2] = N smooth for preliminary Tau estimate (3)
    // clips[3] = N Tau for baseline terminus (tail clip) (10)
    // clips[4] = N points for baseline estimator (20)
    int clips[] = {frontClip_,2000,3,15,20};

    // offset    - p[2] or Cs[2] in the old code
    // amplitude - p[0] or Cs[0] in the old code
    // exponent  - p[1] or Cs[1] (tau/(dt*sparse))????
    double offset, amplitude, exponent;
    double s1 = 0, s2 = 0;

    // Avg the last N=clips[4] points to get the baseline.
    //
    int lastRDIdx = NrdToFitRaw-1;
    offset = 0;
    for(j=0; j<clips[4]; j++)
        offset += rdToFitRaw[lastRDIdx-j];
    offset /= clips[4]; // TODO: put all averaging tasks in funcs PCH151005

    // Skip N leading points where n=clips[0]
    // Avg N points where N=clips[2]
    // Skip to the next point if the average exceeds clips[1]
    //
    for(j=clips[0]; j<NrdToFitRaw-clips[2]; j++)
    {
        s1 = 0;  // needed ? not in original code
        for(int k=0; k<clips[2]; k++)
            s1 += rdToFitRaw[j+k];
        s1 = s1/clips[2] + offset;
        if(fabs(s1) <= abs(clips[1]))
            break;
    }

    // Not enough points left after front and top clips
    //
    if(j >= NrdToFitRaw - clips[2] - 10) {
        std::cout << "Not enough points left after front and top clips";
        return -7;
    }

    // Save the first point for fit plotting purposes
    //
    firstPoint_ = firstPt = j;

    // Follow the curve to find the 1/e (0.368) crossing
    //
    for(; j<NrdToFitRaw-clips[2]-10; j++) {
        s2 = 0;
        for(int k=0; k<clips[2]; k++) {
            s2 += rdToFitRaw[j+k];
        }
        s2 = s2/clips[2] - offset;
        if(fabs(s2) < fabs(0.368*s1))
            break;
    }

    // 10 points per tau is ideal
    //
    sparse = (j-firstPt)/10;
    if(sparse < 2)
        sparse = 1;
    Ns = (NrdToFitRaw - firstPt)/sparse;

    if(Ns < 10) {
        std::cout << "Not enough RD points after 1/e cut or sparsing";
        return -1;
    }

    // Boxcar avg N=sparse neighbors
    //
    rdToFit.assign(Ns, 0.0);
    if(sparse == 1) {
        for(std::size_t k=0; k<rdToFit.size(); k++)
            rdToFit[k] = rdToFitRaw[firstPt + k];
    }else{
        for(std::size_t k=0; k<rdToFit.size(); k++) {
            for(int m=0; m<sparse; m++) {
                rdToFit[k] += rdToFitRaw[firstPt + sparse*k + m];
            }
            rdToFit[k] /= sparse;
        }
    }

    // Boxcar integrate first and second tau/2
    //
    int boxLength = 3 + ((j-firstPt)/sparse)/2;
    if(2*boxLength > Ns) {
        boxLength = Ns/2;
    }
    s1 = s2 = 0;
    for(j=0; j<boxLength; j++) {
        s1 += rdToFit[j];
        s2 += rdToFit[j+boxLength];
    }
    s1 -= boxLength * offset;
    s2 -= boxLength * offset;

    // Avoid division by zero
    //
    if(fabs(s2) < 1e-10) {
        std::cout << "s2 too small in RD estimator";
        return -2;
    }

    // If tau < boxLength/7 the boxcar method fails (i.e. tau too small)
    //
    if(fabs(s1/s2) > 1000) {
        std::cout << "s1/s2 too large in RD estimator";
        return -3;
    }

    // If tau > 10*boxLength boxcar method fails (i.e. tau too big)
    //
    if(fabs(s1/s2) < 1.1) {
//        std::cout << "s1/s2 too small in RD estimator";
        std::cout << "s1/s2 too small in RD estimator";
        return -4;
    }

    // REVIEW units of exponent? is sparseFactor_ involved? PCH151005
    exponent = (double)boxLength/log(s1/s2);
    amplitude = s1/( exponent*(1 - exp( -(double)boxLength/exponent)));

    // REVIEW why sparseFactor_ used here? PCH151005
    // Truncate baseline at tau*clips[3]
    // I'm pretty sure that resize() of a QVector should preserve the remaining element values.
    //
    if( rdToFit.size() > exponent*clips[3]*sparseFactor_)
        rdToFit.resize(static_cast<int>(exponent*clips[3]*sparseFactor_));

    // offset    - p[2] or Cs[2] in the old code
    // amplitude - p[0] or Cs[0] in the old code
    // exponent  - p[1] or Cs[1] (tau/(dt*sparse))????
    p[0] = amplitude;
    p[1] = exponent;
    p[2] = offset;

    return sparse; // REVIEW yes but what is it? PCH150929
}

//////// Old fit from kICOS ////////////////////////////////////////////////////////////////////////
double RingDown::ExpLMFit(std::vector<double> Ys, std::vector<double> &p) {
    int nFits = 3;
    double alamda,chisq;//,ochisq;
    static double covar[3][3],alpha[3][3];
    static double Cs[3];
    int N = Ys.size();
    double* y = new double[N];

    for(int i=0; i<3; i++)
        Cs[i] = p[i];
    for(int i=0; i<N; i++)
        y[i] = Ys.at(i);
    alamda = -1;
    for(int n=0; n<nFits; n++)
        mrqmin(y,Cs,covar,alpha,&chisq,&alamda,N);
    alamda=0.0;
    mrqmin(y,Cs,covar,alpha,&chisq,&alamda,N);
    for(int i=0; i<3; i++)
        p[i] = Cs[i];
    delete[] y;
    return chisq;
}

void RingDown::mrqmin(double *y, double *a, double covar[3][3], double alpha[3][3],
    double *chisq, double *alamda, int ndata) {
    /*static*/ double ochisq = 0.0;
    int j,k,l;

    ///*static*/ double *atry_p,*beta_p,*da_p;
    if ((*alamda) < 0.0)
    {
        (*alamda)=0.001;
        mrqcof(y,a,alpha,beta_p,chisq,ndata);
        ochisq=(*chisq);
        for (j=0;j<3;j++) atry_p[j]=a[j];
    }
    double temp[3][3];
    double oneda[3][1];
    for (j=0;j<3;j++)
    {
        for (k=0;k<3;k++) covar[j][k]=alpha[j][k];
        covar[j][j]=alpha[j][j]*(1.0+(*alamda));
        for (k=0;k<3;k++) temp[j][k]=covar[j][k];
        oneda[j][0]=beta_p[j];
    }
    gaussj(temp,oneda);
    for (j=0;j<3;j++)
    {
        for (k=0;k<3;k++) covar[j][k]=temp[j][k];
        da_p[j]=oneda[j][0];
    }

    if (*alamda == 0.0)
    {
        return;
    }
    for (j=0,l=0;l<3;l++)
        atry_p[l]=a[l]+da_p[j++];

    mrqcof(y,atry_p,covar,da_p,chisq,ndata);
    if ((*chisq) < ochisq)
    {
        (*alamda) *= 0.1;
        ochisq=(*chisq);
        for (j=0;j<3;j++)
        {
            for (k=0;k<3;k++) alpha[j][k]=covar[j][k];
                beta_p[j]=da_p[j];
        }
        for (l=0;l<3;l++) a[l]=atry_p[l];
    }
    else
    {
        (*alamda) *= 10.0;
        (*chisq) = ochisq;
    }
}
void RingDown::mrqcof(double *y, double *a, double alpha[3][3], double beta[3],
            double *chisq, int ndata)
{
    int i,j,k,l,m;
    double ymod,wt,dy;


    double dyda[3];

    alpha[0][0]=0.0;
    alpha[1][0]=0.0;
    alpha[1][1]=0.0;
    alpha[2][0]=0.0;
    alpha[2][1]=0.0;
    alpha[2][2]=0.0;
    beta[0]=0.0;
    beta[1]=0.0;
    beta[2]=0.0;

    *chisq=0.0;

    if(a[1] < 0.10) a[1] = 0.10;	//PREVENT DIVIDE BY ZERO
        if(a[1] > 2000.0) a[1] = 2000.0;
        if(a[0] > 100000.0) a[0] = 100000.0;
        if(a[0] < -100000.0) a[0] = -100000.0;
        if(fabs(a[0]) < 0.0001) a[0] = 0.0001*( (a[0]<0) ? -1.0 : 1.0 );
        if(a[2] > 100000.0) a[2] = 100000.0;
        if(a[2] < -100000.0) a[2] = -100000.0;
    double ex=exp(1.0/a[1]);
    double dex=exp(-1.0/a[1]);

    for (i=0;i<ndata;i++)
    {
        ex *= dex;
        ymod = a[0]* ex + a[2];
        dyda[0] = ex;
        dyda[1] = a[0]*i*ex/(a[1]*a[1]);
        dyda[2] = 1.0;

        dy=y[i]-ymod;
        for (j=0,l=0;l<3;l++)
        {
            wt=dyda[l];
            for (k=0,m=0;m<l+1;m++)
                alpha[j][k++] += wt*dyda[m];
            beta[j++] += dy*wt;
        }
        *chisq += dy*dy;
    }
    alpha[0][1]=alpha[1][0];
    alpha[0][2]=alpha[2][0];
    alpha[1][2]=alpha[2][1];
}
void RingDown::gaussj(double a[3][3], double b[3][1])
{
    int i = 0, icol = 0, irow = 0, j = 0, k = 0, l = 0, ll = 0;
    double big = 0, dum = 0, pivinv = 0;
    double tmp;
    const double epsilon = 1.0e-6;

    int indxc[3],indxr[3],ipiv[3];

    ipiv[0]=0;
    ipiv[1]=0;
    ipiv[2]=0;

    for (i=0;i<3;i++)
    {
        big=0.0;
        for (j=0;j<3;j++)
            if (ipiv[j] != 1)
                for (k=0;k<3;k++)
                {
                    if (ipiv[k] == 0)
                    {
                        if (fabs(a[j][k]) >= big)
                        {
                            big=fabs(a[j][k]);
                            irow=j;
                            icol=k;
                        }
                    }
                }
        ++(ipiv[icol]);
        if (irow != icol)
        {
            for (l=0;l<3;l++)
            {
                tmp=a[irow][l];
                a[irow][l]=a[icol][l];
                a[icol][l]=tmp;
            }
            tmp=b[irow][0];
            b[irow][0]=b[icol][0];
            b[icol][0]=tmp;
        }
        indxr[i]=irow;
        indxc[i]=icol;
        if (fabs(a[icol][icol]) < epsilon)
            a[icol][icol] = epsilon*   //PREVENT SINGULAR MATRIX
                                ( (a[icol][icol]<0) ? -1.0 : 1.0 );
        pivinv=1.0/a[icol][icol];
        a[icol][icol]=1.0;
        for (l=0;l<3;l++) a[icol][l] *= pivinv;
        b[icol][0] *= pivinv;
        for (ll=0;ll<3;ll++)
            if (ll != icol)
            {
                dum=a[ll][icol];
                a[ll][icol]=0.0;
                for (l=0;l<3;l++) a[ll][l] -= a[icol][l]*dum;
                b[ll][0] -= b[icol][0]*dum;
            }
    }
    for (l=2;l>=0;l--)
    {
        if (indxr[l] != indxc[l])
            for (k=0;k<3;k++)
            {
                tmp=a[k][indxr[l]];
                a[k][indxr[l]]=a[k][indxc[l]];
                a[k][indxc[l]]=tmp;
            }
    }
}

