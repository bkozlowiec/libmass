#ifndef RINGDOWN_H
#define RINGDOWN_H

//#include <QObject>
//#include <QMetaType>
//#include <QVector>
#include <vector>
//#include <QTimer>
//#include <QDebug>
//#include <QVector>
#include "../utils/tools.h"
//#undef REPEATABLE_RANDOM
//#define DBL_RAND_MAX (double)(RAND_MAX)
//#include <sys/types.h>
//#include <unistd.h>
//#define GETPID  getpid
//#ifdef REPEATABLE_RANDOM
//#define INIT_RANDOM(seed) srandom(seed)
//#else
//#define INIT_RANDOM(seed) srandom((int)GETPID()) // seed unused
//#endif

// TODO const references (multiple) PCH150929
// (both non-const input parameters and non-reference return values)
class RingDown
{
public:
    explicit RingDown();
    explicit RingDown(int dacRate, int sparseFactor, int frontClip, double fixedTau);
    double f(int x); // TODO is this implemented? PCH150929
    std::vector<double> f(void); // TODO is this implemented?
    
    // REVIEW why does the ringdown class ever need "RD" in method names? PCH151005
    // FIXME these are very deceptive: getRDTrace, getRDFit return downsampled
    // boxcar averages (sparseFactor samples), so they don't return the same
    // thing as the setters. PCH151005
    // TODO boxcar averaging should move to Tools, it's copy-pasted in the cpp
    // making two essentially-identical functions PCH151005
    // REVIEW this class seems too aware of the needs of the plotting routines. PCH151005
    void setRDTrace(const std::vector<double>& data, int rdCnt);
    std::vector<double> getRDTrace(void);
    void setRDFit(double *p, int n);
    std::vector<double> getRDFit(void);
    int getRDFitFirstPoint(void);
    
    // TODO: rename by-reference arguments to out* if things go out PCH 150929
    void fitExp(double &avgTau, double &avgTauStdDev, double &avgOffset,
                double &avgOffsetStdDev, int &nTau); // Calls ExpLMFit PCH151005
    static std::vector<double> getSimRD(double p0, double p1, double p2, int numPts);
    void setSparse(int sparse) { sparseFactor_ = sparse; } // TODO setSparseFactor? consider PCH151005
    // TODO sparseFactor is only used when recovering data with getSimRD & getRDTrace; should be an argument there!!!!
    void setDACRate(int dacRate); // it's megasamples/sec; document in method name and/or parameter PCH151005
    void setFrontClip(int clip);
    void setFixedTau(double ft);// { fixedTau_ = ft; }
    
    bool keepRunning; // REVIEW what is this for? not in .cpp PCH150929

    // TODO make all these private? PCH151005
    // -- old fit code --
    // REVIEW is the old fit code the code we use? (YES) PCH150929
    // REVIEW what does each one of these functions do? references? PCH150929
    double ExpLMFit(std::vector<double> Ys, std::vector<double> &p); // could be private PCH151005
    void mrqmin(double *y, double *a, double covar[3][3], double alpha[3][3], // could be private
                double *chisq, double *alamda, int ndata);
    void mrqcof(double *y, double *a, double alpha[3][3], double beta[3], // could be private
                double *chisq, int ndata);
    void gaussj(double a[3][3], double b[3][1]); // could be private

private:
    double dt_;   // time per dac point in microseconds
    double tau_; // tau in microseconds // unused internally PCH151005
    int frontClip_;
    double a0_;   // ringdown baseline offset // always 0.0, unused, inaccessible PCH151005
    double a1_;   // ringdown amplitude // always 1.0, unused, inaccessible PCH151005
    int numRDInRawData_;
    int firstPoint_;
    int dacRate_; // Megasamples/second of the DM7520

    // In some instruments, the ringdown is too fast for a reliable fit. In
    // those cases tau is fixed to some arbritrary value. If fixedTau_ > 0,
    // the reported tau is fixedTau_ regardless of the fit.
    double fixedTau_;

    // TODO these are fixed-size (only ever length 3); do that here? PCH150929
    double atry_p[3];
    double beta_p[3];
    double da_p[3];
    //double *atry_p,*beta_p,*da_p;

    // Sparse factor used in the spectra data. We don't sparse the RD data used to fit
    // but we do sparse the RD data and RD fit that is displayed in the GUI
    //
    int sparseFactor_;

    std::vector<double> rdToFitRaw_;
    std::vector<double> rdToFit_;
    std::vector<double> rdFit_;

    //double gNoise_(double m, double s);

    // Returns rdToFit and parameters [amplitude, exponent, offset].
    // It internally uses sparse_; this is the only method that actually needs it
    // for anything (it's just used for resizing rdToFit, seemingly not even for
    // downsampling or averaging or anything).
    int ExpPreProcess(std::vector<double> rdToFitRaw, std::vector<double>& rdToFit,
                      std::vector<double>& parameters);
};

#endif // RINGDOWN_H
