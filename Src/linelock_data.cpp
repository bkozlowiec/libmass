//
//  linelock_data.h
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Dmitry Skvortsov on 5/18/16.
//
//

#include "linelock_data.h"

LinelockData::LinelockData()
{
	goodnessOfFit_ = 10;
}

LinelockData::~LinelockData()
{
	this->clear();
}

void LinelockData::clear()
{
	currentPeakPositions_.clear();
	currentPeakAreas_.clear();
}

void LinelockData::setCurrentPeakPositions(const std::vector<double> & peakPositions)
{
	currentPeakPositions_.clear();
	currentPeakPositions_.reserve(peakPositions.size());
	currentPeakPositions_ = peakPositions;
}
void LinelockData::setCurrentPeakAreas(const std::vector<double> & peakAreas)
{
	currentPeakAreas_.clear();
	currentPeakAreas_.reserve(peakAreas.size());
	currentPeakAreas_ = peakAreas;
}
void LinelockData::setGoodnessOfFit(const double & goodnessOfFit)
{
	goodnessOfFit_ = goodnessOfFit;
}

