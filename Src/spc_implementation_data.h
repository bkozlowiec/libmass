//
//  spc_implementation_data.h
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 3/16/17.
//
//

#ifndef spc_implementation_data_h
#define spc_implementation_data_h

#include <string>
#include <vector>
#include <iostream>

#include "ini_structs.h"
#include "utils/labeledmatrix.h"
#include "utils/tools.h"

// Helper Structs
struct StructPeakConstants // StructPeaksIni
{
	// key = "PEAK" for peaks
	// key = "FLANGE" for flanges
	std::string key;
	int idx;
	double gammaAir;
	double gammaAirAdj;
	double gammaSelf;
	double gammaSelfAdj;
	double gammaWater;
	double gammaH2OAdj;
	double tExponent;
	double water_ppm;
	double shift_GHz;
	double lineStrength;
	double lineStrengthFudgeFactor;
	bool wantConc;
	double epp;
	double range_GHz;
	double microns;
	std::string isotopeName;
	std::string molName;
	std::string evName;
	std::string fitFlagString;
	double press_shift_cm1;

	double self_ppm; // Concentration
	int flangeIndex;
    // deprecating the hell out of these crazy fields
//	double flangeTemperature_C; // Temperature
//	double flangePressure_bar; // Pressure
//	double flangeLength_m; // Length
};

struct StructSlaveParameters // StructPeaksIni
{
	std::string key;
	int master;
	int slave;
};

struct StructLinelockIni // SPCInterface::initSPC
{
	float safetyGHzThreshold;
	float startGHzThreshold;
	float stopGHzThreshold;
	float kelvinPerGHz;
	int iterations2StopLineLock;
	float updateIntervalSec;
	float scaleAdjustmentFactorTEC;
	float deltaTECmax;
	std::vector<int> linelockPeakIndexes;
	std::vector<double> peakAreaThresholds;
	double goodnessOfFitMax;
};

struct StructCalibrationIni // SPCInterface::initSPC
{
	std::vector<double> lineStrengthFudge;
};


struct StructCavityGasIni
{
    LabeledMatrix broadeningCoefficients;
    std::map<std::string, double> initialGasConcentration_ppm;
};

inline std::ostream & operator<<(std::ostream & str, const StructCavityGasIni & rhs)
{
    str << "[[\n";
    str << rhs.broadeningCoefficients.matrix() << "\n";
    std::map<std::string, double>::const_iterator itr;
    for (itr = rhs.initialGasConcentration_ppm.begin();
        itr != rhs.initialGasConcentration_ppm.end(); itr++)
    {
        str << itr->first << " => " << itr->second << "\n";
    }
    str << "]]\n";
    return str;
}

// If you need ONE FOR ALL LASERS it goes here
struct StructConfigIni // SPCInterface::initSPC
{
	unsigned int laserRampStart;
	unsigned int laserRampLength;
	unsigned int laserOffStart;
	unsigned int laserOffLength;
	double cavityLength_m;
    std::vector<double> flangeLength_m;

    StructCavityGasIni stackGasProperties;
    std::vector<StructCavityGasIni> flangeGasProperties;
//    LabeledMatrix cavityBroadeningCoefficients;
//    std::vector<LabeledMatrix> flangeBroadeningCoefficients;

    std::map<std::string, bool> trustFitConcentration;
    
	// FailSafe Temperature [K] and Pressure [torr]
	// in case T and P coming to fitter code are bad
	// celsius_ = 22;
	// torr_ = 760;
	double celsius;
	double torr;
    
	unsigned int laserPathAlgo;
	unsigned int fitIterations;
};


// If you need ONE PER LASER it goes here
struct StructPeaksIni // SPCInterface::initSPC
{
	// Etalon: Parameters from EEPROM
	std::vector<double> etalonCoeffients;
	double shift_GHz;
	// Etalon: Other settings
	bool etalonStretchEnabled;
	std::vector<int> etalonStretchIndexes;

	// Zero peak index
	int zeroIdx;
    
	// Standard settings
	// enableFit_ = true;
	// fitAlgorithm_ = 0;
	// resetEveryFit_ = true;
	// flangeFitPeriod_ = 1;
	bool enableFit;
	int fitAlgorithm;
	bool resetEveryFit;
	int flangeFitPeriod;

	// Baseline parameters
	int blModel_;
	std::vector<double> baselineParameters;
	std::vector<bool> baselineFlags;
	std::vector<int> blSample;

	// Fit window parameters
	std::vector<int> fitWindow;

	std::vector<StructPeakConstants> peaks;
	std::vector<StructSlaveParameters> slaveMasters;
};


struct StructIsotopeConsts // SPCInterface::initSPC
{
	// Isotope Name
	std::string isotopeName;
	// Molecule Name
	std::string moleculeName;
	// Molar mass [g/mol]
	double molarMass_g;
	// Relative abundance
	double abundance;
	// Five parameters for partition function polynomial
	double partitionConst0;
	double partitionConst1;
	double partitionConst2;
	double partitionConst3;
	double partitionConst4;
	double partitionConst5;
};


LabeledMatrix makeBroadeningMatrix(const std::vector<StructAbsorber> & absorbers);

#endif /* spc_implementation_data_h */
