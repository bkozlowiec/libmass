//
//  manylogs.cpp
//  MultiAbsorptionSpectroscopySystem
//
//  Created by Paul Hansen on 4/26/16.
//
//

#include "manylogs.h"

#include <fstream>
#include <map>

using namespace std;

namespace ManyLogs
{

std::map<std::string, std::ofstream*> streamMap;

std::ostream & log(const std::string & fileName)
{
    map<string, ofstream*>::const_iterator itr = streamMap.find(fileName);
    
    if (itr == streamMap.end())
    {
        ofstream *str = new ofstream(fileName.c_str());
        streamMap[fileName] = str;
        
        return *str;
    }
    else
    {
        return *itr->second;
    }
}

};
